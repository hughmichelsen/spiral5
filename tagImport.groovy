def storyService = ctx.getBean("storyService")

def oldSql = storyService.connectToOriginalDatabase()
def newSql = storyService.connectToNewDatabase()

def currentStories = storyService.fetchCurrentStories(newSql, true)
//def oldStories = storyService.fetchOldStories(oldSql, true)

//def missingStories = storyService.findMissingStories(currentStories, oldStories)
//missingStories.size