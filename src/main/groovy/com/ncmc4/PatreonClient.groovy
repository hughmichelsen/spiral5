package com.ncmc4

import com.github.jasminb.jsonapi.JSONAPIDocument
import com.patreon.PatreonAPI
import com.patreon.resources.Campaign
import com.patreon.resources.Goal
import com.patreon.resources.Pledge
import com.patreon.resources.User
import groovy.transform.Synchronized
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


class PatreonClient {

    int cacheTimeout = 60 * 10
    long cacheTime = 0
    int cachedPercentage = 0
    List<String> cachedpatreonEmailAddresses = []
    Logger log

    PatreonAPI apiClient = null
    Campaign campaign = null

    PatreonClient(accessToken){
        apiClient = createPatreonAPI(accessToken)
        log = LoggerFactory.getLogger(this.getClass().name)
    }

    private int getGoalPercentage() {
        try {
            JSONAPIDocument<List<Campaign>> campaigns = apiClient.fetchCampaigns()

            campaign = campaigns.get().find {it.getCreationName() == "Gay Spiral Stories"}
            if(campaign) {
                List<Goal> goals = campaign.getGoals()
                Goal goal = goals.get(0)

                return Math.round(100.0 * campaign.getPledgeSum() / goal.getAmountCents())
            }
            else
                return 0
        }
        catch(Exception e) {
            log.error "Error fetching Patreon percentage data: ", e
            return -1
        }
    }

    private List<String> getPatreonEmailAddresses() {
        try {
            if(campaign) {
                List<Pledge> pledges = apiClient.fetchAllPledges(campaign.id)

                List<User> patrons = pledges.findAll{ it.declinedSince == null && ! it.paused}*.getPatron()

                return patrons*.email
            }
            else
                return []
        }
        catch(Exception e) {
            log.error "Error fetching Patreon pledges data: ", e
            return null
        }
    }

    @Synchronized
    def checkCache() {
        def curTime = System.currentTimeSeconds()

        if(curTime - cacheTime > cacheTimeout)
        {
            cacheTime = curTime
            cachedPercentage = getGoalPercentage()
            cachedpatreonEmailAddresses = getPatreonEmailAddresses()
        }
    }

    int getCachedGoalPercentage() {
        checkCache()

        return cachedPercentage
    }

    List<String> getCachedPatreonEmailAddresses() {
        checkCache()

        return cachedpatreonEmailAddresses ?: []
    }

    private def createPatreonAPI(accessToken) {
        if (!accessToken?.size())
            return null

        def apiClient = new PatreonAPI(accessToken)
        return apiClient
    }
}
