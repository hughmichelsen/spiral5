package com.ncmc4

import org.springframework.context.ApplicationListener
import org.springframework.security.authentication.event.InteractiveAuthenticationSuccessEvent

class SecurityEventHandler
    implements ApplicationListener<InteractiveAuthenticationSuccessEvent> {

        void onApplicationEvent(InteractiveAuthenticationSuccessEvent event) {
        }
    }
