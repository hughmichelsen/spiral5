package com.ncmc4

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import javax.servlet.http.HttpSession
import javax.servlet.http.HttpSessionEvent
import javax.servlet.http.HttpSessionListener

class HttpSessionServletListener implements HttpSessionListener {

    private static final Logger LOG = LoggerFactory.getLogger(this)

    // called by servlet container upon session creation
    void sessionCreated(HttpSessionEvent event) {

        // make the session never timeout
        event.session.setMaxInactiveInterval(-1)
    }

    // called by servlet container upon session destruction
    void sessionDestroyed(HttpSessionEvent event) {
        //notify("example:httpSessionDestroyed", event.session)
    }
}
