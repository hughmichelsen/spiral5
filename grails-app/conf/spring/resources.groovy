import com.ncmc4.PatreonClient
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean
import com.ncmc4.HttpSessionServletListener
import com.ncmc4.SecurityEventHandler

// Place your Spring DSL code here
beans = {
    httpSessionServletListener(ServletListenerRegistrationBean) {
        listener = bean(HttpSessionServletListener)
    }

    patreonClient(PatreonClient, application.config.patreon.creator.client.accessToken)

    securityEventHandler(SecurityEventHandler)
}
