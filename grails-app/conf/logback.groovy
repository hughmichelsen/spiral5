import grails.util.BuildSettings
import grails.util.Environment


// See http://logback.qos.ch/manual/groovy.html for details on configuration
appender('STDOUT', ConsoleAppender) {
    if(Environment.current == Environment.DEVELOPMENT) {
        encoder(PatternLayoutEncoder) {
            pattern = "%d{yyyy-MM-dd HH:mm:ss} %level %logger - %msg%n"
        }
    }
    else {
        withJansi = true
        encoder(PatternLayoutEncoder) {
            pattern = "%d{yyyy-MM-dd HH:mm:ss} %highlight(%-5level) %cyan(%logger{25}) - %msg%n%ex{3}"
        }
    }
}

root(WARN, ['STDOUT'])
// root(INFO, ['STDOUT'])
// root(DEBUG, ['STDOUT'])

logger("grails.com.ncmc4", INFO)
logger("com.ncmc4", INFO)
logger("ncmc4", INFO)
logger("grails.plugins", INFO)
logger("elasticsearch", INFO)
logger("org.elasticsearch", INFO)
logger("org.hibernate.SQL", INFO)
logger("grails.plugin.springsecurity.web.access.intercept.AnnotationFilterInvocationDefinition", ERROR)

if(Environment.current == Environment.DEVELOPMENT) {
    logger("groovy.sql",DEBUG)
    logger("grails.app", DEBUG)
    logger("com.ncmc4", DEBUG)
    logger("grails.plugins.elasticsearch", INFO)
}
