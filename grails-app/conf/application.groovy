import grails.util.Environment

import java.util.logging.Logger

Logger log = Logger.getLogger("application.groovy")

// Added by the Spring Security Core plugin:
grails.plugin.springsecurity.userLookup.userDomainClassName = 'com.ncmc4.SecUser'
grails.plugin.springsecurity.userLookup.authorityJoinClassName = 'com.ncmc4.SecUserSecRole'
grails.plugin.springsecurity.authority.className = 'com.ncmc4.SecRole'
grails.plugin.springsecurity.requestMap.className = 'com.ncmc4.SecUserSecRole'
grails.plugin.springsecurity.securityConfigType = 'Requestmap'

grails.plugin.springsecurity.controllerAnnotations.staticRules = [
	[pattern: '/',      					access: ['permitAll']],
	[pattern: '/error',         			access: ['permitAll']],
	[pattern: '/index',           			access: ['permitAll']],
	[pattern: '/index.gsp',       			access: ['permitAll']],
	[pattern: '/shutdown',        			access: ['permitAll']],
	[pattern: '/timezone/**',     			access: ['permitAll']],
	[pattern: '/info/**',     				access: ['permitAll']],
	[pattern: '/assets/**',       			access: ['permitAll']],
	[pattern: '/**/js/**',        			access: ['permitAll']],
	[pattern: '/**/css/**',       			access: ['permitAll']],
	[pattern: '/**/images/**',    			access: ['permitAll']],
	[pattern: '/**/images/banner/**',   	access: ['permitAll']],
	[pattern: '/**/images/banner/skin/**',	access: ['permitAll']],
	[pattern: '/**/javascripts/**',     	access: ['permitAll']],
	[pattern: '/**/stylesheets/**',     	access: ['permitAll']],
	[pattern: '/**/fonts/**',     			access: ['permitAll']],
	[pattern: '/favicon/**',  				access: ['permitAll']],
]

grails.plugin.springsecurity.filterChain.chainMap = [
		[pattern: '/assets/**',      filters: 'none'],
		[pattern: '/**/js/**',       filters: 'none'],
		[pattern: '/**/css/**',      filters: 'none'],
		[pattern: '/**/images/**',   filters: 'none'],
		[pattern: '/favicon/**', 	 filters: 'none'],
		[pattern: '/**',             filters: 'JOINED_FILTERS']
]

grails.plugin.springsecurity.securityConfigType = "Annotation"
grails.plugin.springsecurity.rejectIfNoRule = false
grails.plugin.springsecurity.fii.rejectPublicInvocations = false
grails.plugin.springsecurity.logout.postOnly = false
grails.plugin.springsecurity.gsp.layoutAuth = "newMain"

grails.plugin.springsecurity.useSecurityEventListener = true

grails.plugin.springsecurity.rememberMe.persistent = true
grails.plugin.springsecurity.rememberMe.persistentToken.domainClassName = 'com.ncmc4.PersistentLogin'

// set the token validity to 1 year
grails.plugin.springsecurity.rememberMe.tokenValiditySeconds = 31449600
grails.plugin.springsecurity.rememberMe.alwaysRemember = true

// Database migration
grails {
	plugin {
		databasemigration {
			dropOnStart = false
			updateOnStart = true
			updateOnStartFileName = 'changelog.groovy'
			changelogFileName = 'changelog.groovy'
			excludeObjects = 'rating_statistics,tag_count'
		}
	}
}

// Kein Bundling bei Development
// grails.assets.bundle = false
grails.assets.minifyJs = true
grails.assets.minifyCss = true

// Hot reloading
grails.reload.enabled = false

grails.gorm.failOnError = true

elasticSearch {
	client.mode = "local"
	index.store.type = 'niofs'
	migration.strategy = 'delete'
	plugin.mapperAttachment.enabled = false

	datastoreImpl = 'hibernateDatastore'
	disableAutoIndex = true
	if (Environment.current == Environment.PRODUCTION) {
		bulkIndexOnStartup = true
	}
	else {
		bulkIndexOnStartup = false
	}
}

htmlcleaner {
	whitelists = {
		whitelist("spiral5") {
			startwith "relaxed"
			allow "hr"
		}
	}
}
