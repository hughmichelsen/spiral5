package com.ncmc4

import grails.plugin.springsecurity.annotation.Secured

import static org.springframework.http.HttpStatus.*
import grails.gorm.transactions.Transactional
import static grails.plugin.springsecurity.SpringSecurityUtils.*

class NewCommentController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    CommentService commentService
    HeaderService headerService
    def springSecurityService
    def systemAdminService

    @Transactional(readOnly = true)
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        params.sort = "creationDate"
        params.order = "desc"
        params.ownOnly = params.ownOnly == 'true' ? true : false

        SecUser userInstance = springSecurityService.isLoggedIn() ? springSecurityService.loadCurrentUser() : null

        def authorID = -1
        if(params.ownOnly)
        {
            if(userInstance && userInstance.hasRole(SecRole.ROLE_AUTHOR))
            {
                authorID = userInstance.id
            }
        }


        def cacheStamp = commentService.getCacheLastUpdate()
        def isAdmin = ifAnyGranted(SecRole.ROLE_SUPER_ADMIN)

        def curTheme = request.getCookies().find{ it.name=='currentTheme4' }?.value ?: "default"

        headerService.withCacheHeaders(this) {
            etag {
                "${cacheStamp}.$max.${params.offset ?: 0}.${isAdmin ? 1 : 0}.${params.ownOnly ? 1 : 0}.${curTheme} }"
            }

            generate {
                def queryResults = commentService.list(params, authorID)
                def comments = queryResults.list
                def formattedComments = comments?.collect {
                    def isOwn = (it.poster && it.poster == userInstance)
                    commentService.formatComment(it, isAdmin, isOwn)
                }

                render(model:[commentInstanceList:formattedComments, commentInstanceCount: queryResults.count], view: "index")
            }
        }
    }

    @Secured(value=["hasRole('ROLE_SUPER_ADMIN')"])
    @Transactional(readOnly = true)
    def showIps(Integer id, Integer max)
    {
        params.max = Math.min(max ?: 10, 100)
        params.sort = "creationDate"
        params.order = "desc"

        SecUser userInstance = springSecurityService.isLoggedIn() ? springSecurityService.loadCurrentUser() : null

        def comment = Comment.read(id)
        def comments = []
        def totalCount = 0
        if (comment != null)
        {
            // do with a criteria because Postgres has issues with the date (ERROR: column "this_.creation_date" must appear in teh GROUP BY clause...
            def criteria = Comment.createCriteria()
            comments = criteria.list(params) {
                eq("ipAddress", comment.ipAddress)
            }

            totalCount = comments.totalCount
        }

        def formattedComments = comments?.collect {
            def isOwn = (it.poster && it.poster == userInstance)
            commentService.formatComment(it, true, isOwn)
        }

        render(model:[commentInstanceList:formattedComments, commentInstanceCount: totalCount, id: id, ip: comment.ipAddress], view: "ips")
    }

    private def showComments(Integer max, SecUser userInstance, String view) {
        params.max = Math.min(max ?: 10, 100)
        params.sort = "creationDate"
        params.order = "desc"

        // do with a criteria because Postgres has issues with the date (ERROR: column "this_.creation_date" must appear in teh GROUP BY clause...
        def criteria = Comment.createCriteria()
        def comments = criteria.list(params) {
            eq("poster", userInstance)
        }

        def totalCount = comments.totalCount

        boolean isAdmin = ifAnyGranted('ROLE_SUPER_ADMIN,ROLE_ADMIN,ROLE_ULTRA_ADMIN')

        def formattedComments = comments?.collect {
            def isOwn = (it.poster && it.poster == userInstance)
            commentService.formatComment(it, isAdmin, isOwn)
        }

        render(model:[commentInstanceList:formattedComments, commentInstanceCount: totalCount, user: userInstance], view: view)
    }

    @Secured(value=["hasRole('ROLE_BASIC')"])
    def mine(Integer max)
    {
        SecUser userInstance = springSecurityService.isLoggedIn() ? springSecurityService.loadCurrentUser() : null
        showComments(max, userInstance, "mine")
    }

    @Secured(value=["hasRole('ROLE_SUPER_ADMIN')"])
    def showUser(SecUser userInstance, Integer max)
    {
        showComments(max, userInstance, "user")
    }

    @Transactional()
    def deleteComment(Integer id)
    {
        Comment comment = Comment.get(id)

        SecUser userInstance = springSecurityService.isLoggedIn() ? springSecurityService.loadCurrentUser() : null

        if (comment != null)
        {
            if(comment.poster != null) {

                if(comment.poster != userInstance && ! ifAnyGranted('ROLE_SUPER_ADMIN,ROLE_ADMIN,ROLE_ULTRA_ADMIN')) {
                    log.error "${systemAdminService?.logPrefix}Unauthorized attempt to to delete comment by ${comment.poster}"
                    return response.sendError(FORBIDDEN.value())
                }
                comment.poster.commentCount--
                comment.poster.save flush:true, failOnError:true
            }

            // make sure to update the story so it recaches
            comment.story.lastCommentDate = new Date()
            comment.story.commentCount = comment.story.commentCount - 1

            comment.delete(flush: true)
        }

        commentService.updateCache()
        render "Success"
    }
}
