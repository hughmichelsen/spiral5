package com.ncmc4

import grails.plugin.springsecurity.annotation.Secured
import grails.gorm.transactions.Transactional
import static grails.plugin.springsecurity.SpringSecurityUtils.*
import grails.converters.JSON

@Secured(value=["hasRole('ROLE_SUPER_ADMIN')"])
class SecUserController {
    def springSecurityService
    def systemAdminService

    //static scaffold = SecUser

    def index(Integer max) {

        params.max = Math.min(max ?: 30, 100)

        def query = SecUser.where {}
        if(params.filterMailApproved)
            query = query.where { emailApproved == params.filterMailApproved}
        if(params.filterAnon)
            query = query.where { anonForum == params.filterAnon}
        if(params.filterMailComment)
            query = query.where { emailComment == params.filterMailComment}
        if(params.filterMailUnapproved)
            query = query.where { emailUnapproved == params.filterMailUnapproved}
        if(params.filterResetPW == "true")
            query = query.where { resetToken != null && resetRequestedOn != null}
        if(params.filterPatreon == "true")
            query = query.where {
                id in SecUserSecRole.where { secRole.authority == SecRole.ROLE_PATREON }.secUser
            }
        if(params.filterAdmin == "true")
            query = query.where {
                id in SecUserSecRole.where { secRole.authority == SecRole.ROLE_ADMIN }.secUser
            }
        if(params.filterSuperAdmin == "true")
            query = query.where {
                id in SecUserSecRole.where { secRole.authority == SecRole.ROLE_SUPER_ADMIN }.secUser
            }
        if(params.filterUltraAdmin == "true")
            query = query.where {
                id in SecUserSecRole.where { secRole.authority == SecRole.ROLE_ULTRA_ADMIN }.secUser
            }
        if(params.filterAuthor == "true")
            query = query.where {
                id in SecUserSecRole.where { secRole.authority == SecRole.ROLE_AUTHOR }.secUser
            }

        def users = query.list(params)

        render view:"index", model:[isUltraAdmin: ifAllGranted(SecRole.ROLE_ULTRA_ADMIN), userInstanceCount: users.totalCount, userInstanceList: users, title: "User Admin", ids: users*.id]
    }

    def searchUser(Integer max, String searchTerm, String topic)
    {
        if (!searchTerm) {
            redirect action: "index"
            return
        }

        // honey-pot; if topic has something in it, it's spam
        if (topic)
        {
            log.error "${systemAdminService?.logPrefix}Topic not null"
            redirect action: "index"
            return
        }

        max = Math.min(max ?: 30, 100)
        params.max = max
        params.size = max
        params.from = params.offset ?: 0
        params.sort = "emailAddress"

        def query = SecUser.where { emailAddress =~ "%${searchTerm}%" || authorName =~ "%${searchTerm}%" }

        def users = query.list(params)

        render view:"index", model:[isUltraAdmin: ifAllGranted(SecRole.ROLE_ULTRA_ADMIN), userInstanceCount: users.totalCount, userInstanceList: users, title: "User Admin", filterAuthor: false, filterMailApproved: false, filterMailUnapproved: false, filterAdmin: false, filterSuperAdmin: false, filterUltraAdmin: false, filterResetPW: false, ids: users*.id]
    }


    def create() {
        render view:"create", model:[userInstance:null]
    }

    def edit(SecUser secUserInstance) {
        if(secUserInstance.hasRole('ROLE_SUPER_ADMIN') || secUserInstance.hasRole('ROLE_ULTRA_ADMIN')) {
            if(! ifAnyGranted('ROLE_ULTRA_ADMIN')) {
                redirect(action: "index")
                return;
            }
        }

        render view:"edit", model:[userInstance:secUserInstance]
    }

    @Transactional
    def update(SecUser secUserInstance, String newPassword) {
        secUserInstance.emailAddress = secUserInstance.username

        def criteria = SecUser.createCriteria()
        def existingUsers = criteria.list() {
            eq('username', secUserInstance.username, ignoreCase: true)
        }
        if (existingUsers?.size > 0 && existingUsers[0].id != secUserInstance.id) {
            flash.message = "<div class='flashMsg text-danger'><p><strong>That email address is already in use!</strong></p>Please try another, or reset your password if the existing account is yours.</div>"
            edit(secUserInstance)
            return
        }

        if(newPassword != null && newPassword.trim().length() > 0)
        {
            secUserInstance.password = newPassword
        }

        if (!secUserInstance.validate()) {
            flash.message = "<div class='flashMsg text-danger'><strong>There was an error creating the new account.</strong></div>"
            edit(secUserInstance)
            return
        }

        withForm {
            secUserInstance.save flush:true, failOnError:true

            flash.message = "<div class='flashMsg text-danger'><strong>${secUserInstance.username} successfully saved.</strong></div>"

            logActivity(secUserInstance, "updated")

            index()
        }.invalidToken {
            flash.message = "<div class='flashMsg text-danger'><strong>The user was already updated!</strong></div>"

            index()
        }
    }

    @Transactional
    @Secured(value=["hasRole('ROLE_ULTRA_ADMIN')"])
    def deleteAjax(Integer id)
    {
        SecUser secUserInstance = SecUser.get(id)

        try {
            def mappings = SecUserSecRole.findAllBySecUser(secUserInstance)
            mappings.each {
                it.delete flush:true, failOnError:true
            }

            def activities = Activity.findAllByUser(secUserInstance)
            activities.each {
                it.user = null
                it.save flush:true, failOnError:true
            }

            def ratings = Rating.findAllByRatedBy(secUserInstance)
            ratings.each {
                it.ratedBy = null
                it.save flush:true, failOnError:true
            }

            def stats = UserStoryStats.findAllByUser(secUserInstance)
            stats.each {
                it.delete flush:true, failOnError:true
            }

            def stories = Story.withCriteria { owners { eq('id', secUserInstance.id) }}
            stories.each {
                it.removeFromOwners(secUserInstance)
                it.save flush:true, failOnError:true
            }

            secUserInstance.delete flush:true, failOnError:true

            logActivity(secUserInstance, "deleted")

            render "success"
        }
        catch (Exception ex) {
            render "Error deleting user ${secUserInstance.emailAddress}: ${ex.getMessage()}"
        }
    }


    @Transactional
    def save(SecUser secUserInstance) {
        secUserInstance.emailAddress = secUserInstance.username

        // check for unique
        def criteria = SecUser.createCriteria()
        def existingUsers = criteria.list() {
            eq('username', secUserInstance.username, ignoreCase: true)
        }
        if (existingUsers?.size > 0) {
            flash.message = "<div class='flashMsg text-danger'><p><strong>That email address is already in use!</strong></p>Please try another, or reset your password if the existing account is yours.</div>"
            secUserInstance.username = ""
            render view:"create", model:[userInstance:secUserInstance]
            return
        }

        if (!secUserInstance.validate()) {
            flash.message = "<div class='flashMsg text-danger'><strong>There was an error creating the new account.</strong></div>"
            render view:"create", model:[userInstance:secUserInstance]
            return
        }

        withForm {
            secUserInstance.save flush:true, failOnError:true
            SecRole basicRole = SecRole.findByAuthority("ROLE_BASIC")
            SecUserSecRole userRoleMapping = new SecUserSecRole(secUserInstance, basicRole)
            userRoleMapping.save flush:true, failOnError:true

            logActivity(secUserInstance, "created")

            flash.message = "<div class='flashMsg text-danger'><strong>${secUserInstance.username} successfully added.</strong></div>"

            redirect(action: "index")
        }.invalidToken {
            flash.message = "<div class='flashMsg text-danger'><strong>The user was already submitted!</strong></div>"

            redirect(action: "index")
        }
    }

    @Transactional
    def saveAnonForum(Integer id, Boolean value, String role) {
        SecUser user = SecUser.get(id)

        user.anonForum = value
        user.save(flush:true, failOnError:true)

        logActivity(user, "anon forum " + (value ? "on" : "off"))

        def ret = [success:true]
        render ret as JSON
    }

    @Transactional
    def saveEmailComment(Integer id, Boolean value, String role) {
        SecUser user = SecUser.get(id)

        user.emailComment = value
        user.save(flush:true, failOnError:true)

        logActivity(user, "email comment " + (value ? "on" : "off"))

        def ret = [success:true]
        render ret as JSON
    }

    @Transactional
    def saveEmailApproved(Integer id, Boolean value, String role) {
        SecUser user = SecUser.get(id)

        user.emailApproved = value
        user.save(flush:true, failOnError:true)

        logActivity(user, "email approved " + (value ? "on" : "off"))

        def ret = [success:true]
        render ret as JSON
    }

    @Transactional
    def savePasswordReset(Integer id, Boolean value, String role) {
        SecUser user = SecUser.get(id)

        if (!value && ((user.resetToken != null) && (user.resetRequestedOn != null))) {
            user.resetToken = null
            user.resetRequestedOn = null
            user.save(flush: true, failOnError: true)

            logActivity(user, "reset password token removed")
        }

        def ret = [success:true]
        render ret as JSON
    }

    @Transactional
    def saveEmailUnapproved(Integer id, Boolean value, String role) {
        SecUser user = SecUser.get(id)

        user.emailUnapproved = value
        user.save(flush:true, failOnError:true)

        logActivity(user, "email unapproved " + (value ? "on" : "off"))

        def ret = [success:true]
        render ret as JSON
    }

    @Transactional
    def saveRole(Integer id, Boolean value, String role) {
        SecUser user = SecUser.get(id)

        if(role in ['ROLE_SUPER_ADMIN', 'ROLE_ULTRA_ADMIN']) {
            if(! ifAnyGranted('ROLE_ULTRA_ADMIN')) {
                return;     // only ULTRA_ADMIN is allowed to change these!
            }
        }

        if (value && !user.hasRole(role)) {
            SecRole adminRole = SecRole.findByAuthority(role)
            SecUserSecRole userRoleMapping = new SecUserSecRole(user, adminRole)
            userRoleMapping.save flush:true, failOnError:true
        } else if (!value && user.hasRole(role)) {
            def mappings = SecUserSecRole.findAllBySecUser(user)

            def mappingToDelete = mappings.find { it.secRole.authority == role }

            mappingToDelete.delete(flush:true, failOnError:true)
        }

        logActivity(user, "Role: " + role + ": " + (value ? "on" : "off"))

        def ret = [success:true]
        render ret as JSON
    }

    private def logActivity(SecUser userInstance, String action)
    {
        def user = springSecurityService.isLoggedIn() ? springSecurityService.loadCurrentUser() : null

        def activity = new UserActivity(user: user, activityUserId: userInstance.id, activityUsername: userInstance.username, action: action);
        activity.save flush:true, failOnError: true
    }
}
