package com.ncmc4

import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured
import grails.util.Environment
import org.springframework.mail.MailSendException

import java.time.ZonedDateTime
import java.util.concurrent.TimeUnit

import static grails.plugin.springsecurity.SpringSecurityUtils.*
import static org.springframework.http.HttpStatus.*
import grails.gorm.transactions.Transactional


import com.google.common.cache.*

//@Transactional(readOnly = true)
class NewStoryController {

    static allowedMethods = [save: "POST", update: "POST", delete: "DELETE"]

    static scope = "singleton"

    StoryService storyService
    TagService tagService
    def springSecurityService
    def mailService
    def elasticSearchService
    CommentService commentService
    def archiveService
    def systemAdminService

    SeriesService seriesService

    RatingService ratingService

    UserStoryStatService userStoryStatService

    private Cache<String, String> commentCache = CacheBuilder.newBuilder()
            .maximumSize(100)
            .build();

    private Cache<String, String> tagCache = CacheBuilder.newBuilder()
            .maximumSize(100)
            .build();

    private Cache<String, String> authorCache = CacheBuilder.newBuilder()
            .maximumSize(4)
            .expireAfterWrite(1, TimeUnit.DAYS)
            .build();

    private Cache<Integer, String> storyTextCache = CacheBuilder.newBuilder()
            .maximumSize(100)
            .build();

    def headerService
    def markdownParserService

    @Transactional(readOnly = true)
    def index(Integer max) {
        params.max = Math.min(max ?: 30, 100)

        def catFilterList = storyService.getCategoryFilterList()
        
        def query = storyService.list(params)

        render view:"index", model:[storyInstanceCount: query.count, storyInstanceList: query.list, title: "All Stories", categoryFilterList: catFilterList]
    }

    @Transactional(readOnly = true)
    def letter(String id, Integer max)
    {
        params.max = Math.min(max ?: 30, 100)

        def catFilterList = storyService.getCategoryFilterList()

        String letter = id

        if (!letter)
            letter = "a"
        else
            letter = letter.substring(0, 1)

        params.storyLetter = letter;

        def query = storyService.findAllByLetter(letter, params)

        def storyList = query.list
        render view:"letterIndex", model:[storyInstanceCount: query.count, title:"Stories starting with '${letter.toUpperCase()}'", storyInstanceList: storyList, letter: letter, categoryFilterList: catFilterList]
    }

    @Secured(['ROLE_ADMIN','ROLE_SUPER_ADMIN','ROLE_ULTRA_ADMIN'])
    @Transactional(readOnly = true)
    def listUnapproved(Integer max) {
        params.max = Math.min(max ?: 30, 100)

        def query = storyService.findAllUnapproved(params)

        def storyList = query.list

        render view:"indexUnapproved", model:[storyInstanceCount: query.count, storyInstanceList: storyList, title: "Unapproved Stories"]
    }

    @Secured(['ROLE_ADMIN','ROLE_SUPER_ADMIN','ROLE_ULTRA_ADMIN'])
    @Transactional(readOnly = true)
    def listTagged(Integer max, String id, boolean findDeleted, boolean resolveAlias, boolean findUnApproved) {
        params.max = Math.min(max ?: 30, 100)

        def catFilterList = storyService.getCategoryFilterList()

        Tag tag = Tag.get(id)

        def query = storyService.findAllByTags(params,[tag], findDeleted, resolveAlias, findUnApproved)

        def storyList = query.list

        render view:"indexTagged", model:[storyInstanceCount: query.count, storyInstanceList: storyList, title: "Stories tagged with '${tag}'", params: params, tag: tag, categoryFilterList: catFilterList]
    }

    @Secured(value=["hasRole('ROLE_SUPER_ADMIN')"])
    @Transactional(readOnly = true)
    def listDeleted(Integer max) {
        params.max = Math.min(max ?: 30, 100)

        def catFilterList = storyService.getCategoryFilterList()

        def query = storyService.findAllDeleted(params)

        def storyList = query.list

        render view:"indexDeleted", model:[storyInstanceCount: query.count, title:"Deleted Stories", storyInstanceList: storyList, categoryFilterList: catFilterList]
    }

    @Transactional
    def listTopRated() {
        TimeZone timeZone = systemAdminService.getUserTimeZone()

        ZonedDateTime now = ZonedDateTime.now(timeZone.toZoneId())

        // now = ZonedDateTime.of(2019, 1, 05, 00, 00, 01, 01, timeZone.toZoneId())

        // Top Series List
        Map topStoriesDefinition = ratingService.prepareRandomTopStoriesData(now)

        def catFilter = (storyService.getCategoryFilterCookie() ?: "").toString()
        def curTheme = request.getCookies().find{ it.name=='currentTheme4' }?.value ?: "default"
        def isAdmin = ifAnyGranted(SecRole.ROLE_SUPER_ADMIN)
        def format = { input -> input ? 1 : 0 }
        def catFilterList = storyService.getCategoryFilterList()

        headerService.withCacheHeaders(this) {
            etag {
                "${format(isAdmin)}.${topStoriesDefinition?.topStoriesSegmentId}.${catFilter}.${curTheme}"
            }

            generate {
                ratingService.fetchTopSeriesMap(topStoriesDefinition,true)

                render  view:"topRated",
                        model:[ categoryFilterList: catFilterList] + (topStoriesDefinition ?: [:])
            }
        }
    }

    @Transactional(readOnly = true)
    def defaultedStories(Integer max) {
        params.max = Math.min(max ?: 30, 100)
        params.sort = params.sort ?: 'nameSlug'
        params.order = params.order ?: 'asc'

        def catFilterList = storyService.getCategoryFilterList()

        def query = storyService.listAllDefaulted(params)

        render view:"defaultedStories", model:[storyInstanceCount: query.count, storyInstanceList: query.list, title: "Defaulted Stories", categoryFilterList: catFilterList]
    }

    @Transactional
    private Boolean saveApprovedInternal(Integer id, Boolean approved, Boolean sendEmail, String comment) {
        Story storyInstance = Story.get(id)
        if (storyInstance == null) {
            return false
        }

        approved = approved ?: false

        // check if we need to do anything at all
        if (approved && storyInstance.approved && ! comment)
            return true

        boolean logApproved = false
        boolean logRejected = false

        boolean wasEdit = storyInstance.previouslyApproved

        Story.withSession { dbSession ->
            if (approved && ! storyInstance.previouslyApproved) {
                storyInstance.publishDate = new Date()
            }

            // If manually disapproved (not by edit), reset previouslyApproved as well
            storyInstance.previouslyApproved = approved

            storyInstance.lastReviewDate = new Date()

            logApproved = approved && (storyInstance.approved == false)
            logRejected = !approved && (storyInstance.approved == true)

            storyInstance.approved = approved

            if(comment)
                storyInstance.adminComment = comment

            storyInstance.lastModifiedDate = new Date()

            storyInstance.save failOnError: true, flush: true

            if (approved) {
                StoryTextArchive archive = StoryTextArchive.findByStory(storyInstance)
                if (archive) {
                    archive.delete failOnError: true, flush: true
                }
            }

            dbSession.flush()
            dbSession.clear()
        }

        storyInstance.refresh()

        storyService.updateStoryCache()

        if (storyInstance.series != null)
            seriesService.resetSeriesLinks(storyInstance.series, storyInstance.category)

        elasticSearchService.index(storyInstance)

        if (logApproved)
            logActivity(storyInstance, "approved");
        if (logRejected)
            logActivity(storyInstance, "rejected");

        if (logApproved)
        {
            try {
                if(sendEmail) {
                    try {

                        def users = SecUser.findAllByEmailApproved(true)

                        users = users.findAll {
                            it.hasAnyRole([SecRole.ROLE_ADMIN, SecRole.ROLE_SUPER_ADMIN, SecRole.ROLE_ULTRA_ADMIN, SecRole.ROLE_PATREON])
                        }

                        def showEmailUsers = []
                        def hideEmailUsers = []
                        def adminEmailUsers = []

                        users.each { SecUser user ->
                            if (user.hasRole("ROLE_ADMIN"))
                                adminEmailUsers.add(user.emailAddress)
                            else if (storyInstance.showEmail)
                                showEmailUsers.add(user.emailAddress)
                            else
                                hideEmailUsers.add(user.emailAddress)
                        }

                        if (showEmailUsers.size() > 0) {
                            mailService.sendMail {
                                async true
                                bcc showEmailUsers.toArray()
                                subject "New story '${storyInstance.name}' by '${storyInstance.author}' has been posted on GSS!"
                                html g.render(template: "/email/approved", model: [storyInstance: storyInstance, showEmail: true])
                            }
                        }
                        if (hideEmailUsers.size() > 0) {
                            mailService.sendMail {
                                async true
                                bcc hideEmailUsers.toArray()
                                subject "New story '${storyInstance.name}' by '${storyInstance.author}' has been posted on GSS!"
                                html g.render(template: "/email/approved", model: [storyInstance: storyInstance, showEmail: false])
                            }
                        }
                        if (adminEmailUsers.size() > 0) {
                            def adminUser = springSecurityService.isLoggedIn() ? springSecurityService.loadCurrentUser() : null
                            mailService.sendMail {
                                async true
                                to adminEmailUsers.toArray()
                                subject "New story '${storyInstance.name}' by '${storyInstance.author}' has been posted on GSS!"
                                html g.render(template: "/email/approved", model: [storyInstance: storyInstance, showEmail: true, adminUser: adminUser])
                            }
                        }
                    }
                    catch (MailSendException ex) {
                        log.error "${systemAdminService?.logPrefix}Error sending EMail.", ex
                    }
                }

                if ((storyInstance.emailAddress != null)) {
                    try {
                        def showLink = g.createLink(action:"show", controller:"NewStory", id:storyInstance.id, absolute:true)
                        def editLink = g.createLink(action:"edit", controller:"NewStory", id:storyInstance.id, absolute:true)

                        def ownerEmailAddresses = storyInstance.owners.collect { user ->
                            user.emailAddress.toLowerCase()
                        }
                        def createdWithUserAccount = ownerEmailAddresses.contains(storyInstance.emailAddress.toLowerCase())

                        mailService.sendMail {
                            async true
                            to storyInstance.emailAddress
                            subject wasEdit ? "The edit of your story '${storyInstance.name}' has been approved!" : "Your story '${storyInstance.name}' has been approved!"
                            html g.render(template: "/email/authorApproved", model: [storyInstance: storyInstance, showLink: showLink, createdWithUserAccount: createdWithUserAccount, editLink: editLink])
                        }
                    }
                    catch (MailSendException ex) {
                        log.error "${systemAdminService?.logPrefix}Error sending EMail.", ex
                    }
                }
            }
            catch (ex) {
                log.error "${systemAdminService?.logPrefix}Error sending mail: ", ex
            }
        }

        return true
    }

    @Transactional
    private Boolean saveDisableCommentsInternal(Integer id, Boolean disableComments) {
        Story storyInstance = Story.get(id)
        if (storyInstance == null) {
            log.info "Story not found! $id"
            return false
        }

        disableComments = disableComments ?: false

        // early out if we don't need to do anything
        if (disableComments == storyInstance.disableNewComments)
            return true

        storyInstance.disableNewComments = disableComments
        storyInstance.lastModifiedDate = new Date()

        storyInstance.save flush:true, failOnError: true

        storyService.updateStoryCache()

        elasticSearchService.index(storyInstance)

        logActivity(storyInstance, disableComments ? "lockComments" : "unlockComments")

        return true
    }

    @Transactional
    def saveAdminCommentInternal(Integer id, String adminComment) {
        Story storyInstance = Story.get(id)
        if (! storyInstance) {
            return false
        }

        storyInstance.adminComment = adminComment
        storyInstance.lastModifiedDate = new Date()
        storyInstance.save(flush: true)

        storyService.updateStoryCache()

        elasticSearchService.index(storyInstance)

        logActivity(storyInstance, "changeAdminComment")

        return true
    }

    @Secured(value=["hasAnyRole('ROLE_ADMIN')"])
    def saveAdminComment(String adminComment, Integer id) {
        try {

            if (!saveAdminCommentInternal(id, adminComment)) {
                def ret = [errorMessage: "Invalid story ID!"]
                render ret as JSON
            } else {
                def ret = [message: "Comment saved!", adminComment: adminComment]
                render ret as JSON
            }
        }
        catch(Exception ex) {
            def ret = [errorMessage: "Exception: ${ex.toString()}"]
            render ret as JSON
        }
    }

    @Secured(value=["hasRole('ROLE_SUPER_ADMIN')"])
    def saveDisableComments(Integer id, Boolean disableComments) {
        if (saveDisableCommentsInternal(id, disableComments)) {
            def data = [success: true, message: "success"]
            render data as JSON
        } else {
            log.info "Story not found! $id"
            def data = [success: false, message: "story not found"]
            render data as JSON
        }
    }

    @Secured(['ROLE_ADMIN','ROLE_SUPER_ADMIN'])
    def saveApproved(Integer id, Boolean approved, Boolean sendEmail, String comment) {
        if (saveApprovedInternal(id, approved, sendEmail, comment)) {
            def data = [success: true, message: "success", adminComment: comment]
            render data as JSON
        } else {
            log.info "Story not found! $id"
            def data = [success: false, message: "story not found", adminComment: comment]
            render data as JSON
        }
    }

    @Transactional
    private Boolean saveDeletedInternal(Integer id, Boolean deleted, String comment) {
        Story.withNewSession { session ->
            Story storyInstance = Story.get(id)
            if (storyInstance == null) {
                log.info "Story not found! $id"
                return false
            }

            deleted = deleted ?: false

            // early out if we don't need to do anything
            if (deleted == storyInstance.deleted && ! comment)
                return true

            storyInstance.deleted = deleted
            if(comment)
                storyInstance.adminComment = comment
            storyInstance.lastModifiedDate = new Date()

            storyInstance.save flush:true, failOnError: true

            session.flush()
        }

        Story storyInstance = Story.get(id)

        storyService.updateStoryCache()

        if (storyInstance.series != null)
            seriesService.resetSeriesLinks(storyInstance.series, storyInstance.category)

        elasticSearchService.index(storyInstance)

        logActivity(storyInstance, deleted ? "deleted" : "undeleted")

        return true
    }

    @Transactional
    @Secured(value=["hasRole('ROLE_SUPER_ADMIN')"])
    def saveDeleted(Integer id, Boolean deleted, String comment) {
        if (saveDeletedInternal(id, deleted, comment)) {
            def data = [success: true, message: "success", adminComment: comment]
            render data as JSON
        } else {
            log.info "Story not found! $id"
            def data = [success: false, message: "story not found", adminComment: comment]
            render data as JSON
        }
    }

    def create() {
        def story = new Story(params)

        // default to showing email
        story.showEmail = true

        SecUser user = springSecurityService.isLoggedIn() ? springSecurityService.loadCurrentUser() : null
        if (user != null) {
            def authorName = user.authorName ?: user.username
            def authorSlug = storyService.makeAuthorSlug(authorName)

            Author author = new Author(name: authorName)
            story.author = author
            story.authorSlug = authorSlug
            author.discard()

            story.emailAddress = user.emailAddress
            story.showEmail = user.showEmail
        }

        Boolean ageGate = !springSecurityService.isLoggedIn()
        renderCreateView(story, ageGate)
    }

    @Secured(value=["hasAnyRole('ROLE_AUTHOR,ROLE_SUPER_ADMIN,ROLE_ADMIN,ROLE_ULTRA_ADMIN')"])
    @Transactional(readOnly = true)
    def edit(Story storyInstance) {

        boolean accessDenied = false

        if (!checkAdminPrivileges() && checkAuthorPrivileges()) {
            def user = springSecurityService.loadCurrentUser()
            if (!storyInstance.owners.contains(user)) {
                accessDenied = true
            }
        }

        def tagList = storyInstance.tags?.join(",")

        storyInstance?.discard()

        if (accessDenied) {
            log.error "${systemAdminService?.logPrefix}Unauhtorized attempt to edit story ${storyInstance}"
            return response.sendError(FORBIDDEN.value())
        }
        else
            render view: "edit", model: [storyInstance: storyInstance, includeId: true, tagList: tagList, categories: tagService.getCategorySelection()]
    }

    def editSecret(String secretWord) {
        if (secretWord == null) {
            log.error "${systemAdminService?.logPrefix}Empty SecretWord on attempt to editSecret story."
            return response.sendError(BAD_REQUEST.value())
        }

        Story storyInstance = Story.findByUuid(secretWord)
        if (storyInstance == null) {
            log.error "${systemAdminService?.logPrefix}Access to edit story with invalid secret Word: ${secretWord}."
            return response.sendError(BAD_REQUEST.value())
        }

        if (storyInstance.deleted) {
            log.error "${systemAdminService?.logPrefix}Access to edit deleted story by secret Word: ${secretWord}."
            return response.sendError(FORBIDDEN.value())
        }

        def tagList = storyInstance.tags?.join(",")

        storyInstance?.discard()

        render view: "editSecret", model: [storyInstance: storyInstance, secret: true, tagList: tagList, categories: tagService.getCategorySelection()]
    }

    @Transactional
    private def setSeries(Story storyInstance, String seriesName) {
        try {
            storyInstance = Story.get(storyInstance.id)

            ArrayList<Series> seriesToRebuild = []

            seriesName = seriesName ?: ""

            seriesName = seriesName.trim()

            // default the name of the series to the same as the story
            if (seriesName.startsWith('"') && seriesName.endsWith('"')) {
                seriesName = seriesName.substring(1, seriesName.size() - 1);
                seriesName = seriesName.trim()
            }

            if (!seriesName || seriesName.length() == 0)
                seriesName = storyInstance.name
            else {

            }

            if (storyInstance.series != null)
                seriesToRebuild.add(storyInstance.series)

            Series newSeries = Series.findByNameSlug(storyService.makeStoryTitleSlug(seriesName))
            if (newSeries == null) {
                newSeries = seriesService.CreateSeries(storyInstance, seriesName)
            } else if (newSeries == storyInstance.series)
                return

            seriesToRebuild.add(newSeries)

            // save the story and flush
            storyInstance.series = newSeries
            storyInstance.save flush: true, failOnError: true

            seriesToRebuild.each { Series series ->
                seriesService.resetSeriesLinks(series, storyInstance.category)
            }
        } catch (e) {
            log.error "${systemAdminService?.logPrefix}Error setting series $seriesName on story ${storyInstance} (id: ${storyInstance.id}: ", e
            throw e
        }
    }

    public enum SaveStatus {
        Done,
        NotFound,
        Spam,
        Errors,
        Duplicate,
        AccessDenied
    }

    @Transactional
    private SaveStatus saveStory(Story storyInstance, String authorName, String tagList, String password, String secretWord, checkForDuplicates, Boolean forceErrors = false)
    {
        if (storyInstance == null) {
            return SaveStatus.NotFound
        }

        if (password || secretWord)
        {
            // the password field is a honeypot; this is spam!!!
            log.error "${systemAdminService?.logPrefix}Spam!!! (password or secretWord set)"

            // don't save this!!!!
            storyInstance?.discard()

            return SaveStatus.Spam
        }

        storyInstance.validate()

        if (authorName.startsWith('"') && authorName.endsWith('"'))
            authorName = authorName.substring(1, authorName.size() - 1);

        String text = storyInstance.text

        if (!checkAdminPrivileges()) {
            String trimmedText = text.trim()
            def minLength = Environment.current == Environment.DEVELOPMENT ? 1 : 500
            if (trimmedText.length() < minLength) {
                storyInstance.errors.reject("text.invalid.min.size.message", ['The text', minLength] as Object[], "{0} is less than the minimum length of {1} characters")
            }

            def maxLength = 200000
            if (text.length() > maxLength) {
                storyInstance.errors.reject("text.invalid.max.size.message", ['The text', maxLength] as Object[], "{0} is greater than the maximum length of {1} characters")
            }

            def minWordCount = Environment.current == Environment.DEVELOPMENT ? 1 : 800
            def wordCount = trimmedText.split("\\W+").length;
            if (wordCount < minWordCount) {
                storyInstance.errors.reject("text.invalid.min.wordcount.message", ['The text', minWordCount] as Object[], "{0} must have at least {1} words")
            }
        }

        def submittedTags = tagService.parseTags(tagList)

        if(submittedTags?.size() < 3)
        {
            storyInstance.errors.rejectValue('tags', 'story.tags.minsize.notmet',null,'At least three tags need to be specified!')
        }

        authorName = authorName ?: "Anonymous"

        def minAuthorLength = 3
        if (authorName.length() < minAuthorLength)
        {
            storyInstance.errors.reject("text.invalid.min.size.message", ['The author name', minAuthorLength] as Object[], "{0} is less than the minimum length of {1} characters")
        }

        def maxAuthorLength = 30
        if (authorName.length() > maxAuthorLength)
        {
            storyInstance.errors.reject("text.invalid.max.size.message", ['The author name', maxAuthorLength] as Object[], "{0} is greater than the maximum length of {1} characters")
        }

        if (storyInstance.hasErrors() || forceErrors) {
            def tempAuthor = new Author(name: authorName)
            tempAuthor.discard()

            storyInstance.author = tempAuthor

            storyInstance?.discard()

            return SaveStatus.Errors
        }

        storyInstance.nameSlug = storyService.makeStoryTitleSlug(storyInstance.name)
        storyInstance.authorSlug = storyService.makeAuthorSlug(authorName)

        Author author = Author.findByNameIlike("$authorName")
        if (author == null)
        {
            author = new Author(name: authorName)
            author.save flush:true, failOnError:true
        }

        storyInstance.author = author

        storyInstance.unformatted = true

        // check if the user's already added this story
        if (checkForDuplicates && ifNotGranted(SecRole.ROLE_SUPER_ADMIN)) {
            def existingStory = Story.findByNameSlugAndAuthorSlugAndDeleted(storyInstance.nameSlug, storyInstance.authorSlug, false)
            if ((existingStory?.text == storyInstance.text) && (existingStory?.prevInSeries == storyInstance.prevInSeries)) {
                // make sure we don't save it
                storyInstance?.discard()

                return SaveStatus.Duplicate
            }
        }

        tagService.editTags(submittedTags, storyInstance)

        storyInstance.lastModifiedDate = new Date()
        storyInstance.save flush:true, failOnError: true

        elasticSearchService.index(storyInstance)

        return SaveStatus.Done
    }

    def updateSecret(Story storyInstance, String secretCode, String authorName, String tagList, String password, String secretWord, String seriesName) {

        if (! storyInstance || storyInstance?.uuid != secretCode || secretCode?.length() == 0) {
            String ip = systemAdminService.getIp()
            log.error "${systemAdminService?.logPrefix}Invalid or empty SecretWord on attempt to updateSecret story: ${storyInstance} - ${secretCode}"
            return response.sendError(FORBIDDEN.value())
        }

        SaveStatus status

        Boolean wasApproved = storyInstance?.approved

        Story.withTransaction {
            // create the archive first, before any database operations are triggered
            // only create one if the story wasn't approved already
            StoryTextArchive archive = archiveService.createRevision(storyInstance, authorName, seriesName)

            def checkForDuplicates = false

            // make sure it's not approved this time, no matter what
            if ((storyInstance != null) && !checkAdminPrivileges())
                storyInstance.approved = false

            status = saveStory(storyInstance, authorName, tagList, password, secretWord, checkForDuplicates)

            if (status == SaveStatus.Done) {
                archive?.save(flush: true, failOnError: true)
            } else {
                archive?.discard()
            }
        }

        switch (status)
        {
            case SaveStatus.NotFound:
                notFound()
                break

            case SaveStatus.Spam:
                flash.message = '<div class="flashMsg text-success>Updated ${storyInstance}</div>'
                redirect action: "showSecret", params: [secretWord: storyInstance.uuid]
                break

            case SaveStatus.Errors:
                render view:'edit', model: [storyInstance: storyInstance, secret: true, categories: tagService.getCategorySelection()]
                break

            case SaveStatus.Duplicate:
                flash.message = "<div class='flashMsg text-danger'><strong>Already submitted!</strong> ${storyInstance} is awaiting approval. Please wait a little longer, or email the site admin.</div>"
                redirect action: "showSecret", params: [secretWord: storyInstance.uuid]
                break

            case SaveStatus.Done:
                logActivity(storyInstance, "updatedSecret")

                storyTextCache.invalidate(storyInstance.id.toInteger())

                storyService.updateStoryCache()

                if (wasApproved)
                    sendUnapprovedMail(storyInstance, "EDITED: '${storyInstance?.name}' by '${storyInstance?.author?.displayName}'!")

                storyInstance.refresh()
                setSeries(storyInstance, seriesName)
                storyInstance.refresh()

                elasticSearchService.index(storyInstance)

                String link = g.createLink(action: "editSecret", controller: "NewStory", params: [secretWord: storyInstance.uuid], absolute: true)

                flash.message = """
                    <div class='flashMsg text-danger'>
                        <p><strong>Success!</strong> '${storyInstance}' was updated successfully.<br/>
                        Your changes wont be visible publicly until they have been approved.</p>
                        If you need to edit your story, you can do so here: <a href='${link}'>$link</a>. AND KEEP THIS LINK SECRET!
                    </div>
                """
                redirect action: "showSecret", params: [secretWord: storyInstance.uuid]
                break
        }
    }

    @Secured(value=["hasAnyRole('ROLE_AUTHOR,ROLE_SUPER_ADMIN,ROLE_ADMIN,ROLE_ULTRA_ADMIN')"])
    @Transactional
    def update(Story storyInstance, String authorName, String tagList, String password, String secretWord, String seriesName) {

        SaveStatus status = SaveStatus.Done

        Boolean wasApproved = storyInstance?.approved

        if (!checkAdminPrivileges() && checkAuthorPrivileges()) {
            def user = springSecurityService.loadCurrentUser()
            if (!storyInstance.owners.contains(user)) {
                status = SaveStatus.AccessDenied
            }
        }

        if (status != SaveStatus.AccessDenied) {
            // create the archive first, before any database operations are triggered
            // only create one if the story wasn't approved already
            StoryTextArchive archive = archiveService.createRevision(storyInstance, authorName, seriesName)

            // make sure it's not approved this time, no matter what
            if ((storyInstance != null) && !checkAdminPrivileges())
                storyInstance.approved = false

            def checkForDuplicates = false
            status = saveStory(storyInstance, authorName, tagList, password, secretWord, checkForDuplicates)

            if (status == SaveStatus.Done) {
                archive?.save(flush: true, failOnError: true)

                if(storyInstance.approved && storyInstance.series != null)
                    seriesService.resetSeriesLinks(storyInstance.series, storyInstance.category)
            }
        }

        switch (status)
        {
            case SaveStatus.NotFound:
                notFound()
                break

            case SaveStatus.Spam:
                flash.message = "<div class='flashMsg text-success'>Updated ${storyInstance}</div>"
                redirect action: "show", id: storyInstance.id
                break

            case SaveStatus.Errors:
                render view:'edit', model: [storyInstance: storyInstance, tagList: tagList, includeId: true, categories: tagService.getCategorySelection()]
                break

            case SaveStatus.Duplicate:
                flash.message = "<div class='flashMsg text-danger'><strong>Already submitted!</strong> ${storyInstance} is awaiting approval. Please wait a little longer, or email the site admin.</div>"
                redirect action: "show", id: storyInstance.id
                break

            case SaveStatus.Done:
                logActivity(storyInstance, "updated")

                storyTextCache.invalidate(storyInstance.id.toInteger())

                storyService.updateStoryCache()

                if (wasApproved)
                    sendUnapprovedMail(storyInstance, "EDITED: '${storyInstance?.name}' by '${storyInstance?.author?.displayName}'!")

                storyInstance.refresh()
                setSeries(storyInstance, seriesName)
                storyInstance.refresh()

                flash.message = """
                    <div class='flashMsg text-danger'>
                        <strong>Success!</strong> '${storyInstance}' was updated successfully.<br/>
                        Your changes wont be visible publicly until they have been approved.
                    </div>
                """

                redirect action: "show", id: storyInstance.id
                break

            case SaveStatus.AccessDenied:
                render view:"/accessDenied"
                break
        }
    }

    private def sendUnapprovedMail(Story storyInstance, String emailTitle)
    {
        if (!storyInstance.sponsored) {
            try {
                Collection<SecUser> users = SecUser.findAllByEmailUnapproved(true)
                def emails = users.findAll({ it.enabled }).collect({ it.emailAddress })

                mailService.sendMail {
                    async true
                    from "GSS Administration <admin@gayspiralstories.com>"
                    to emails.toArray()
                    subject emailTitle
                    html g.render(template: "/email/unapproved", model: [storyInstance: storyInstance])
                }
            }
            catch (e) {
                log.error "${systemAdminService?.logPrefix}Error sending mail: ", e
            }
        }
    }

    @Secured(value=["hasRole('ROLE_SUPER_ADMIN')"])
    def addStoriesToSeries(String seriesName) {
        try {
            def listIds = params.list("ids[]")

            def stories = Story.findAllByIdInList(listIds)

            stories.each { Story story ->
                setSeries(story, seriesName)
            }

            def ret = [success:true, message:"${stories.size()} Stories successfully added to series ${seriesName}!"]
            render ret as JSON
        }
        catch(Exception ex) {
            def ret = [success:false, errorMessage:"Exception: ${ex.toString()}!"]
            render ret as JSON
        }
    }

    @Secured(value=["hasRole('ROLE_SUPER_ADMIN')"])
    def assignAuthorToStories(String authorEmailAddress) {
        def ret = [success:false]

        def listIds = params.list("ids[]")

        def owner = SecUser.findByEmailAddressIlike(authorEmailAddress)
        if (owner != null) {
            try {
                def stories = Story.findAllByIdInList(listIds)

                stories.each { Story story ->
                    story.addToOwners(owner)

                    story.save failOnError:true, flush:true
                }

                def authorRole = SecRole.findByAuthority(SecRole.ROLE_AUTHOR)
                if (!SecUserSecRole.exists(owner.id, authorRole.id)) {
                    SecUserSecRole.create(owner, authorRole, true)
                }

                // recount this author's stories
                def criteria = Story.createCriteria()
                owner.storyCount = criteria.count {
                    owners {
                        eq("id", owner.id)
                    }
                }
                owner.save flush:true, failOnError:true

                ret.message = "${stories.size()} Stories assigned successfully to ${owner.username}!"
                ret.success = true

            } catch (ex) {
                log.error "${systemAdminService?.logPrefix}Exception assigning author ${authorEmailAddress} to ${listIds}", ex
                ret.errorMessage = "Exception: ${ex.toString()}!"
            }
        }
        else
            ret.ErrorMessage = "Author ${authorEmailAddress} not found!"

        render ret as JSON
    }

    @Secured(value=["hasRole('ROLE_SUPER_ADMIN')"])
    def removeStoriesFromSeries()
    {
        try {
            def listIds = params.list("ids[]")

            def stories = Story.findAllByIdInList(listIds)

            stories.each { Story story ->
                setSeries(story, "")
            }

            def ret = [success:true, message:"${stories.size()} Stories successfully unassigned from any series!"]
            render ret as JSON
        }
        catch(Exception ex) {
            def ret = [success:false, errorMessage:"Exception: ${ex.toString()}!"]
            render ret as JSON
        }
    }

    @Secured(['ROLE_SUPER_ADMIN'])
    def saveStoriesApproved(Boolean approved, Boolean sendEmail, String comment, Boolean reload) {
        def listIds = params.list("ids[]")
        def data = [success: true]

        try {
            List failedIDs

            listIds.each { String id ->
                if (!saveApprovedInternal(id.toInteger(), approved, sendEmail, comment)) {
                    data.success = false
                    failedIDs.add(id)
                }
            }

            if(data.success) {
                data.message = "${listIds.size()} Stories successfully ${approved ? 'approved' : 'unapproved'}!"
                if(reload)
                    flash.message = "<div class='flashMsg text-success'>${data.message}</div>"
            }
            else {
                data.errorMessage = "Error changing stories: ${failedIDs}"
                if(reload)
                    flash.message = "<div class='flashMsg text-danger'>${data.errorMessage}</div>"
            }

            render data as JSON
        }
        catch(Exception ex) {
            def ret = [success:false, errorMessage:"Exception: ${ex.toString()}!"]
            render ret as JSON
        }
    }

    @Transactional
    @Secured(value=["hasRole('ROLE_SUPER_ADMIN')"])
    def saveStoriesDeleted(Boolean deleted, String comment, Boolean reload) {
        def listIds = params.list("ids[]")

        def data = [success: true]
        try {
            List failedIDs

            listIds.each { String id ->
                if (!saveDeletedInternal(id.toInteger(), deleted, comment)) {
                    data.success = false
                    failedIDs.add(id)
                }
            }

            if (!data.success) {
                data.errorMessage = "Error deleting stories: ${failedIDs}"
                if(reload)
                    flash.message = "<div class='flashMsg text-success'>${data.message}</div>"
            }
            else {
                data.message ="${listIds.size()} Stories successfully ${deleted ? 'deleted' : 'undeleted'}!"
                if(reload)
                    flash.message = "<div class='flashMsg text-danger'>${data.errorMessage}</div>"
            }

            render data as JSON
        }
        catch(Exception ex) {
            def ret = [success:false, errorMessage:"Exception: ${ex.toString()}!"]
            render ret as JSON
        }
    }

    @Transactional
    @Secured(value=["hasRole('ROLE_ADMIN')"])
    def saveStoriesAdminComment(String comment, Boolean reload) {
        def listIds = params.list("ids[]")

        def data = [success: true]
        try {
            List failedIDs

            listIds.each { String id ->
                if (!saveAdminCommentInternal(id.toInteger(), comment)) {
                    data.success = false
                    failedIDs.add(id)
                }
            }

            if(data.success) {
                data.message ="${listIds.size()} Stories successfully set to a new admin comment!"
                if(reload)
                    flash.message = "<div class='flashMsg text-success'>${data.message}</div>"
            }
            else {
                data.errorMessage = "Error changing stories: ${failedIDs}"
                if(reload)
                    flash.message = "<div class='flashMsg text-danger'>${data.errorMessage}</div>"
            }

            render data as JSON
        }
        catch(Exception ex) {
            def ret = [success:false, errorMessage:"Exception: ${ex.toString()}!"]
            render ret as JSON
        }
    }

    @Transactional
    def removeFromSeries(Story storyInstance)
    {
        if (storyInstance.series != null) {
            def next = storyInstance.nextInSeries
            def prev = storyInstance.prevInSeries

            def series = storyInstance.series

            // fix this instance up
            storyInstance.nextInSeries = null
            storyInstance.prevInSeries = null
            storyInstance.series = null
            storyInstance.save flush:true, failOnError:true

            if (series != null)
                seriesService.resetSeriesLinks(series)
        }

        def ret = [success:true]
        render ret as JSON
    }

    private def addStoryToUser(def user, Story storyInstance)
    {
        // give them the sec role for the main page
        def authorRole = SecRole.findByAuthority(SecRole.ROLE_AUTHOR)
        if (!SecUserSecRole.exists(user.id, authorRole.id)) {
            SecUserSecRole.create(user, authorRole, true)
        }

        // flag that this story is this author's
        storyInstance.addToOwners(user)
        storyInstance.save flush: true, failOnError: true

        // add another story to their count
        user.storyCount++
        user.save flush: true, failOnError: true
    }

    private def renderCreateView(Story storyInstance, Boolean ageGate, String tagList = "") {
        render view: "create", model: [storyInstance: storyInstance, title: "Submit New Story", mode: "create", ageGate: ageGate, categories: tagService.getCategorySelection(), tagList:tagList]
    }

    def save(Story storyInstance, String authorName, String tagList, String password, String secretWord, Boolean over18, String seriesName) {

        withForm {
            try {
                def user = springSecurityService.isLoggedIn() ? springSecurityService.loadCurrentUser() : null

                def forceErrors = false

                if (!over18 && !user) {
                    flash.message = """
<div class="flashMsg text-danger">
    <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
    <span class="sr-only">Error:</span>
    You must be over 18 to post a story to this site!
</div>"""
                    forceErrors = true
                }

                SaveStatus status = SaveStatus.Errors

                Story.withTransaction {
                    def checkForDuplicates = true
                    status = saveStory(storyInstance, authorName, tagList, password, secretWord, checkForDuplicates, forceErrors)
                }

                switch (status) {
                    case SaveStatus.NotFound:
                        notFound()
                        break

                    case SaveStatus.Spam:
                        flash.message = "<div class='flashMsg text-success'>Created ${storyInstance.name}!</div>"
                        render view: "show", model: [storyInstance: storyInstance]
                        break

                    case SaveStatus.Errors:
                        Boolean ageGate = !springSecurityService.isLoggedIn()
                        renderCreateView(storyInstance, ageGate, tagList)
                        break

                    case SaveStatus.Duplicate:
                        flash.message = "<div class='flashMsg text-danger'><strong>Already submitted!</strong> ${storyInstance} is awaiting approval. Please wait a little longer, or email the site admin.</div>"
                        render view: "show", model: [storyInstance: storyInstance]
                        break

                    case SaveStatus.Done:
                        logActivity(storyInstance, "created")

                        sendUnapprovedMail(storyInstance, "SUBMITTED: '${storyInstance?.name}' by '${storyInstance?.author?.displayName}'!")

                        try {
                            if (storyInstance.emailAddress) {
                                mailService.sendMail {
                                    async true
                                    to storyInstance.emailAddress
                                    subject "Your story has been submitted!"
                                    html g.render(template: "/email/submitted", model: [storyInstance: storyInstance, showLink: g.createLink(action: "showSecret", controller: "NewStory", params: [secretWord: storyInstance.uuid], absolute: true), editLink: g.createLink(action: "editSecret", controller: "NewStory", params: [secretWord: storyInstance.uuid], absolute: true)])
                                }
                            }
                        }
                        catch (MailSendException ex) {
                            log.error "${systemAdminService?.logPrefix}Error sending EMail.", ex
                        }
                        
                        log.info "Saved new story: ${storyInstance.id}"

                        // make sure we have a fresh copy
                        storyInstance.refresh()
                        setSeries(storyInstance, seriesName)

                        // log that this user is now an author
                        if (user) {
                            addStoryToUser(user, storyInstance)
                        } else if (storyInstance.emailAddress) {
                            user = SecUser.findByUsername(storyInstance.emailAddress)
                            if (user) {
                                addStoryToUser(user, storyInstance)
                            }
                        }

                        elasticSearchService.index(storyInstance)

                        // have to create a story stats object for this new story
                        StoryStats storyStats = new StoryStats(story: storyInstance)
                        storyStats.save flush: true, failOnError: true

                        String link = g.createLink(action: "editSecret", controller: "NewStory", params: [secretWord: storyInstance.uuid], absolute: true)
                        flash.message = """
                            <div class='flashMsg text-success'>
                                <p><strong>Success!</strong> '${storyInstance}' is awaiting approval, which should be within the next 12 hours.</p>
                                If you need to edit your story, you can do so here: <a href='${link}'>$link</a>. AND KEEP THIS LINK SECRET!
                            </div>
                        """
                        if (springSecurityService.isLoggedIn()) {
                            redirect action: "show", id: storyInstance.id
                        } else {
                            redirect action: "showSecret", params: [secretWord: storyInstance.uuid]
                        }
                        break
                }
            } catch (e) {
                log.error "${systemAdminService?.logPrefix}Error while saving ${storyInstance}. Exception: ", e

                Boolean ageGate = !springSecurityService.isLoggedIn()
                flash.message = "<div class='flashMsg text-danger'>Error submitting story. Please email the site admin.</div>"
                renderCreateView(storyInstance, ageGate, tagList)
            }
        }.invalidToken {
            flash.message = "<div class='flashMsg text-danger'><strong>Already submitted!</strong> ${storyInstance} is awaiting approval. Please wait a little longer, or email the site admin.</div>"
            render view: "show", model: [storyInstance: storyInstance]

            storyInstance?.discard()
        }

        // null
    }

    @Transactional
    def editTags(String newTags, Integer id, String tagAuthor) {

        try {
            Story storyInstance = Story.get(id);
            if (!storyInstance)
            {
                render (template: "tags", model: [storyInstance: storyInstance, errorText: "An error occurred. Please try adding tags later!"])
                return
            }

            // check for spam
            if (tagAuthor)
            {
                def submittedTags = tagService.parseTags(newTags)

                def fakeTags = []
                submittedTags.each {
                    fakeTags.add([tag: it])
                }
                storyInstance.tags.each {
                    fakeTags.add([tag: it.tag])
                }

                render(template: "tags", model: [storyInstance: storyInstance])
                return
            }

            def submittedTags = tagService.parseTags(newTags)
            if (!submittedTags)
            {
                render(template: "tags", model: [storyInstance: storyInstance])
                return
            }

            if(submittedTags.size() < 3) {
                render (template: "tags", model: [storyInstance: storyInstance, errorText: "Please specify at least three tags!"])
                return
            }

            TagService.EditTagStatus tagSubmission = tagService.editTags(submittedTags, storyInstance)
            if (tagSubmission.tagsAdded.size() > 0 || tagSubmission.tagsDeleted.size() > 0)
            {
                storyInstance.lastModifiedDate = new Date()
                storyInstance.save flush: true, failOnError: true
                storyService.updateStoryCache()
                seriesService.refreshTags(storyInstance.series, storyInstance.category)

                elasticSearchService.index(storyInstance)

                // Delete unused tags
                tagSubmission.tagsDeleted.each { delTag ->
                    def stories = Story.withCriteria {
                        tags {
                            eq('tag',delTag.tag)
                        }
                    }

                    if(stories.size == 0){
                        delTag.delete()
                    }
                }
            }
            // return the template with all of the tags for the story now
            render(template: "tags", model: [storyInstance: storyInstance])
        }
        catch(Exception ex) {
            Story storyInstance = Story.get(id)
            render (template: "tags", model: [storyInstance: storyInstance, errorText: ex.getMessage()])
        }
    }

    @Transactional
    def editCategory(Integer category, Integer id) {

        try {
            Story storyInstance = Story.get(id)
            Tag cat = Tag.get(category)
            if (!storyInstance || ! cat)
            {
                render(template: "tags", model: [storyInstance: storyInstance, errorText: "An error occurred. Please try changing the category later!"])
                return
            }

            storyInstance.category = cat;
            storyInstance.save(flush:true)
            seriesService.refreshTags(storyInstance.series, storyInstance.category)

            elasticSearchService.index(storyInstance)

            // return the template with all of the tags for the story now
            render(template: "tags", model: [storyInstance: storyInstance])
        }
        catch(Exception ex) {
            Story storyInstance = Story.get(id)
            render(template: "tags", model: [storyInstance: storyInstance, errorText: ex.getMessage()])
        }
    }

    @Transactional
    def addNewComment(String commentAuthor, String password, String commentText, Long storyId, String family) {
        boolean isAdmin = checkAdminPrivileges()
        def ret = [admin: isAdmin, commentHtml: ""]
        if (!commentText?.size()) {
            // return the response and add it to the page
            render ret as JSON
            return
        }

        SecUser user = springSecurityService.isLoggedIn() ? springSecurityService.loadCurrentUser() : null
        Comment comment = new Comment(name: commentAuthor, comment: commentText, ipAddress: systemAdminService.getIp(), poster: user)

        def formattedComment = commentService.formatComment(comment, isAdmin, true)

        if (password || family) {
            // the password field is a honeypot; this is spam!!!
            log.error "${systemAdminService?.logPrefix}Spam!!! (password or family set)"

            // return the response and add it to the page
            comment?.discard()
            ret.commentHtml = g.render(template: "comment", model: [comment: formattedComment])
            render ret as JSON
            return
        }

        Story story = Story.get(storyId)
        if (!story) {
            // error
            comment?.discard()
            ret.commentHtml = g.render(template: "comment", model: [comment: formattedComment])
            render ret as JSON
            return
        }

        try {
            if (!story.disableNewComments || isAdmin) {
                story.addToComments(comment)
                story.commentCount = story.commentCount + 1
                story.lastCommentDate = new Date()
                story.save flush: true, failOnError: true

                commentService.updateCache()

                if (user != null) {
                    user.commentCount++
                    user.save flush: true, failOnError: true
                }

                formattedComment.id = comment.id

                List<String> ownerEmails = story.owners.collect { it -> it.emailComment ? it.emailAddress : null }.findAll {
                    it != null
                }
                if (ownerEmails.size() > 0) {
                    def convertedComment = markdownParserService.parseAndSanitizeText(commentText, true, false)
                    mailService.sendMail {
                        async true
                        to ownerEmails.toArray()
                        subject "New comment for your story '${story.name}' has been written by '${commentAuthor ?: 'Anonymous'}' has been posted on GSS!"
                        html g.render(template: "/email/newComment", model: [storyInstance: story, commentText: convertedComment, commentAuthor: commentAuthor])
                    }
                }

                ret.commentHtml = g.render(template: "comment", model: [comment: formattedComment])
            } else {
                formattedComment.text = "Comments on this story have been disabled."
                ret.commentHtml = g.render(template: "comment", model: [comment: formattedComment])
            }

            ret.commentCount = story.commentCount
        }
        catch(Exception ex) {
            log.error("Exception adding Comment to story ${storyId}: ${ex.toString()}",ex)
            formattedComment.text = "**Error adding the comment. Sorry for the inconvenience. Please try again and contact admin@gayspiralstories.com if this error persists!**<p>" + formattedComment.text
            ret.commentHtml = g.render(template: "comment", model: [comment: formattedComment])
        }

        render ret as JSON
    }

    @Transactional
    def updateComment(Comment commentId, String commentAuthor, String password, String commentText, String family)
    {
        boolean isAdmin = checkAdminPrivileges()
        def ret = [admin:isAdmin, commentHtml: ""]
        if (!commentText?.size())
        {
            // return the response and add it to the page
            render ret as JSON
            return
        }

        SecUser user = springSecurityService.isLoggedIn() ? springSecurityService.loadCurrentUser() : null
        Comment comment = Comment.get(commentId)

        comment.comment = commentText
        comment.name = commentAuthor

        def formattedComment = commentService.formatComment(comment, isAdmin, true)

        if (password || family)
        {
            // the password field is a honeypot; this is spam!!!
            log.error "${systemAdminService?.logPrefix}Spam!!!"

            // return the response and add it to the page
            ret.commentHtml = g.render(template: "comment", model: [comment: formattedComment])
            render ret as JSON
            return
        }

        comment.save flush:true, failOnError:true

        commentService.updateCache()

        ret.commentHtml = g.render(template: "comment", model: [comment: formattedComment])

        render ret as JSON
    }

    def showSecret(String secretWord) {

        headerService.cache(response, validFor: 0)

        String ip = systemAdminService.getIp()

        if (secretWord == null) {
            log.error "${systemAdminService?.logPrefix}Empty SecretWord on attempt to showSecret story."
            return response.sendError(BAD_REQUEST.value())
        }

        Story storyInstance = Story.findByUuid(secretWord)
        if (storyInstance == null) {
            log.error "${systemAdminService?.logPrefix}Access to show story by invalid secret Word: ${secretWord}."
            return response.sendError(BAD_REQUEST.value())
        }

        if (storyInstance.deleted) {
            log.error "${systemAdminService?.logPrefix}Access to show deleted story by secret Word: ${secretWord}."
            return response.sendError(FORBIDDEN.value())
        }

        showStoryRender(storyInstance, true, true)
    }

    private def showStoryRender(Story storyInstance, boolean showLatest, boolean showDiff)
    {
        def storyText = null
        def summaryText = storyInstance.summary
        def isShowingOlderVersion = false
        Boolean isReview = false

        StoryTextArchive archive = null
        if (!storyInstance.approved) {
            // read in the archive story
            archive = StoryTextArchive.findByStory(storyInstance)
            if (archive) {
                // send the archive to the differencer
                if(showDiff) {
                    storyText = archiveService.generateHtmlDiff(
                            storyInstance.parseAndSanitizeText(archive.oldText),
                            storyInstance.parseAndSanitizeText(storyInstance.text))

                    if (summaryText != archive.oldSummary && archive.oldSummary != null) {
                        summaryText = archiveService.generateHtmlDiff(archive.oldSummary, storyInstance.summary)
                    }
                    isReview = true
                }
                else if(! showLatest)
                {
                    storyText = storyInstance.parseAndSanitizeText(archive.oldText)
                    isShowingOlderVersion = true
                }
                else
                    storyText = storyInstance.textFormatted
            }
        }


        if (storyText == null) {
            storyText = storyInstance.textFormatted
        }

        SecUser user = springSecurityService.isLoggedIn() ? springSecurityService.loadCurrentUser() : null
        boolean isAuthor = user && storyInstance.owners.contains(user)
        boolean ratingsVisible = user && (user.hasRole("ROLE_ADMIN") || isAuthor)
        // def twoWeeksAgo = LocalDateTime.now().minusDays(2).toInstant(ZoneOffset.UTC)
        // if(Date.from(twoWeeksAgo).after(storyInstance.publishDate)) {
            // ratingHint = (ratings < 60) ? 2 : (ratings < 100) ? 1 : 0
        //}

        Map model = [storyInstance: storyInstance, storyText: storyText, isShowingOlderVersion: isShowingOlderVersion, isReview: isReview, archive: archive, summary: summaryText, disableNewComments: storyInstance.disableNewComments, categories: tagService.getCategorySelection(), ratingsVisible: ratingsVisible, isAuthor: isAuthor, ratingHint: storyInstance.getRatingCountHint()]

        render view: "show", model: model
    }

    def show(String id) {
        if (!id?.isInteger())
        {
            String ip = systemAdminService.getIp()
            log.error "${systemAdminService?.logPrefix}Tried to look up a story which an invalid id (${params.id}). Looks like bots; throttling."

            sleep(4000)
            return response.sendError(BAD_REQUEST.value())
        }

        Long storyId = id.toLong()

        Story storyInstance = Story.read(storyId)

        headerService.cache(response, validFor: 0)

        String ip = systemAdminService.getIp()

        headerService.withCacheHeaders(this) {
            etag {
                if (storyInstance == null)
                    return "x"

                boolean isLoggedIn = springSecurityService.isLoggedIn()
                boolean authorPrivileges = checkAuthorPrivileges()
                boolean adminPrivileges = checkAdminPrivileges()

                def seriesId = storyInstance.series?.id ?: 0
                def seriesVersion = storyInstance.series?.version ?: 0
                def commentsDisabled = storyInstance.disableNewComments ? 1 : 0

                def curTheme = request.getCookies().find{ it.name=='currentTheme4' }?.value ?: "default"

                return "${seriesId}.${seriesVersion}.${storyInstance?.lastModifiedDate?.time}.${storyInstance.version}.${storyInstance?.approved ? 1 : 0}.${storyInstance?.previouslyApproved ? 1 : 0}.${storyInstance?.deleted ? 1 : 0}.${isLoggedIn ? 1 : 0}.${authorPrivileges ? 1 : 0}.${adminPrivileges ? 1 : 0}.${commentsDisabled}.${curTheme}"
            }

            lastModified {
                storyInstance?.lastModifiedDate ?: 0
            }

            generate {
                if (storyInstance == null) {
                    log.error "${systemAdminService?.logPrefix}Tried to look up a story that doesn't exist (for id: ${id})"
                    return response.sendError(NOT_FOUND.value())
                }

                boolean adminPrivileges = checkAdminPrivileges()
                boolean showLatest = adminPrivileges
                boolean showDiff = adminPrivileges

                if (!adminPrivileges && (!storyInstance.approved || storyInstance.deleted)) {
                    if (storyInstance?.deleted) {
                        log.error "${systemAdminService?.logPrefix}Invalid access to deleted story ${storyInstance.id}"
                        return response.sendError(FORBIDDEN.value())
                    }

                    def user = springSecurityService.loadCurrentUser()
                    if (storyInstance.owners.contains(user)) {
                        showLatest = true
                        showDiff = true
                    }
                    else
                    {
                        if(! storyInstance.previouslyApproved){
                            log.error "${systemAdminService?.logPrefix}Invalid access to deleted story ${storyInstance.id}"
                            return response.sendError(FORBIDDEN.value())
                        }
                        else{
                            showLatest = false
                            showDiff = false
                        }

                    }
                }

                showStoryRender(storyInstance, showLatest, showDiff)
            }
        }
    }

    private def createCommentETag(Story storyInstance) {
        if (storyInstance == null)
            return "x"

        return "${storyInstance?.lastTagDate?.time}.${storyInstance?.lastCommentDate?.time}"
    }

    private def renderAjaxCommentData(String comments, boolean valid, boolean admin, String tags, boolean myStories, boolean isLoggedIn, boolean liked, String commentAuthor, boolean deleted, boolean approved, Boolean disableComments) {
        def ret = [
            comments        : comments,
            valid           : valid,
            admin           : admin,
            tags            : tags,
            myStories       : myStories,
            isLoggedIn      : isLoggedIn,
            liked           : liked,
            commentAuthor   : commentAuthor,
            deleted         : admin ? deleted : false,
            approved        : admin ? approved : false,
            disableComments : disableComments
        ]

        render ret as JSON
    }

    def fetchComments(Integer id) {
        Story.withSession { dbSession ->
            dbSession.defaultReadOnly = true

            Story storyInstance = Story.read(id)

            if(! storyInstance) {
                String ip = systemAdminService.getIp()
                log.error "${systemAdminService?.logPrefix}Attempt to fetchComments with invalid id: {id}"
                return response.sendError(BAD_REQUEST.value())
            }

            boolean adminPrivileges = checkAdminPrivileges()

            boolean isMyStories = false

            boolean isLoggedIn = springSecurityService.isLoggedIn()

            SecUser user = isLoggedIn ? springSecurityService.loadCurrentUser() : null

            if (checkAuthorPrivileges()) {
                isMyStories = storyInstance.owners.contains(user)
            }

            def isLiked = false
            if (isLoggedIn) {
                isLiked = userStoryStatService.isLiked(user, storyInstance)
            }

            userStoryStatService.incrementUserStoryViewCounts(user, storyInstance)

            def curTheme = request.getCookies().find{ it.name=='currentTheme4' }?.value ?: "default"

            headerService.withCacheHeaders(this) {
                etag {
                    def userETag = "${adminPrivileges ? 1 : 0}.${isMyStories ? 1 : 0}.${isLoggedIn ? 1 : 0}.${isLiked ? 1 : 0}.${user ? user.version : 0}"
                    def storyETag = "${storyInstance.approved ? 1 : 0}.${storyInstance.deleted ? 1 : 0}"
                    return "${createCommentETag(storyInstance)}.${userETag}.${storyETag}.${curTheme}"
                }

                generate {
                    if (storyInstance == null) {
                        renderAjaxCommentData("", false, adminPrivileges, "", false, isLoggedIn, false, "", false, false, false)

                        return
                    }

                    if ((!storyInstance?.approved || storyInstance?.deleted) && !springSecurityService.isLoggedIn()) {
                        renderAjaxCommentData("", false, adminPrivileges, "", false, isLoggedIn, false, "", storyInstance.deleted, storyInstance.approved, storyInstance.disableNewComments)

                        return
                    }

                    String commentCacheKey = "${storyInstance?.lastCommentDate?.time}.${adminPrivileges}.2".toString()
                    String commentHtml = commentCache.get(commentCacheKey, {
                        def formattedComments = ""

                        Comment.withNewSession {
                            def newStoryInstance = Story.read(id)
                            def sortedComments = newStoryInstance?.comments?.sort { Comment a, Comment b ->
                                a.creationDate.time <=> b.creationDate.time
                            }

                            def comments = sortedComments?.collect {
                                def isOwn = (it.poster && it.poster == user)
                                commentService.formatComment(it, adminPrivileges, isOwn)
                            }

                            formattedComments = g.render(template: "comment", collection: comments, var: "comment")
                        }

                        return formattedComments
                    })

                    String tagCacheKey = "${storyInstance.id}.${storyInstance?.lastTagDate?.time ?: 0}.7"
                    String tagsHtml = tagCache.get(tagCacheKey) {
                        def formattedTags = ""
                        Tag.withNewSession {
                            def newStoryInstance = Story.read(id)
                        }

                        return formattedTags
                    }

                    // send email address back
                    String emailAddress = ""

                    boolean valid = checkStoryApprovedToShow(storyInstance)

                    renderAjaxCommentData(commentHtml, valid, adminPrivileges, tagsHtml, isMyStories, isLoggedIn, isLiked, user ? (user.authorName ?: user.emailAddress) : "", storyInstance.deleted, storyInstance.approved, storyInstance.disableNewComments)

                    // make sure we never save anything, and never get a stale object exception
                    dbSession.clear()
                }
            }
        }
    }

    @Secured(value=["hasAnyRole('ROLE_AUTHOR,ROLE_SUPER_ADMIN,ROLE_ADMIN,ROLE_ULTRA_ADMIN')"])
    public def fetchStoryAuthorData(Story storyInstance) {
        boolean isLoggedIn = springSecurityService.isLoggedIn()

        SecUser user = isLoggedIn ? springSecurityService.loadCurrentUser() : null

        boolean canView = checkAdminPrivileges()
        if (!canView) {
            canView = storyInstance.owners.contains(user)
        }

        def ret = [storyLikes: 0, totalViewCount: 0, uniqueViewCount: 0]

        if (canView) {
            ret.storyLikes = userStoryStatService.countStoryLikes(storyInstance)

            StoryStats storyStats = StoryStats.findByStory(storyInstance, [readOnly: true])
            ret.totalViewCount = storyStats ? storyStats.totalViewCount : 0
            ret.uniqueViewCount = storyStats ? storyStats.loggedInUserViewCount : 0
        }

        render ret as JSON
    }

    @Secured(value=["hasRole('ROLE_BASIC')"])
    public def toggleLike(Story storyInstance, Boolean liked) {
        def user = springSecurityService.loadCurrentUser()
        userStoryStatService.toggleLike(user, storyInstance, liked)

        user.likeCount += (liked ? 1 : -1)
        user.save flush:true, failOnError:true

        def ret = [success:true]
        render ret as JSON
    }

    @Secured(value=["hasRole('ROLE_BASIC')"])
    public def listLiked(Integer max) {
        def user = springSecurityService.loadCurrentUser()

        params.max = Math.min(max ?: 10, 100)

        def query = userStoryStatService.findLikedStories(params, user)

        render view:"likedIndex", model:[storyInstanceCount: query.count, storyInstanceList: query.list, title: "Your Liked Stories"]
    }

    @Secured(value=["hasRole('ROLE_ADMIN')"])
    public def listUserLiked(SecUser secUserInstance, Integer max) {
        params.max = Math.min(max ?: 30, 100)

        def query = userStoryStatService.findLikedStories(params, secUserInstance)

        render view:"adminLikedIndex", model:[storyInstanceCount: query.count, storyInstanceList: query.list, title: "${secUserInstance.username}'s Liked Stories", user: secUserInstance]
    }

    private boolean checkStoryApprovedToShow(Story storyInstance) {
        return ifAnyGranted(SecRole.ROLE_SUPER_ADMIN) || ((storyInstance?.approved) && (!storyInstance?.deleted))
    }

    private boolean checkAdminPrivileges() {
        return ifAnyGranted('ROLE_SUPER_ADMIN,ROLE_ADMIN,ROLE_ULTRA_ADMIN')
    }

    private boolean checkAuthorPrivileges() {
        return ifAnyGranted(SecRole.ROLE_AUTHOR)
    }

    /*
    def tags() {
        def tag = Tag.last(sort: "id", order: "desc")

        headerService.withCacheHeaders(this) {
            etag {
                return "${tag.id}"
            }

            generate {
                def tagList = tagService.getTagListCached(tag?.id?.toString())
                render(tagList as JSON)
            }
        }
    }
    */

    def authors() {
        def author = Author.last(sort: "id", order: "desc")
        def cacheKey = "${author.id}".toString()

        def curTheme = request.getCookies().find{ it.name=='currentTheme4' }?.value ?: "default"

        headerService.withCacheHeaders(this) {
            etag {
                return cacheKey + ".${curTheme}"
            }

            generate {
                def authorList = authorCache.get(cacheKey, {
                    Collection<String> ret = null

                    Author.withNewSession {
                        def authors = Author.list(readOnly: true)
                        ret = (authors*.name)
                    }

                    return (String[]) ret.toArray()
                })

                render(authorList as JSON)
            }
        }
    }

    @Transactional(readOnly = true)
    def search(Integer max, String searchTerm, String tagList, Integer srchCategory, String tagSearchMode, String useAlias, String topic)
    {
        def catList = tagService.getCategorySelection("All categories")

        def model = [storyInstanceList: [], storyInstanceCount: 0, title: "Search", searchTerm: searchTerm, tagList: tagList, srchCategory: srchCategory, tagSearchMode: tagSearchMode, useAlias: useAlias, categories: catList]

        try {

            max = Math.min(max ?: 30, 100)
            params.max = max
            params.size = max
            params.offset = params.offset ?: 0
            params.sort = params.sort ?: "publishDate"
            params.order = params.order ?: "desc"

            if(! searchTerm && ! tagList && ! srchCategory)
            {
                // Default: UseAlias true
                if(useAlias == null)
                    useAlias = "true"
            }
            else{
                if(useAlias == null)
                    useAlias = "false"
            }

            if(searchTerm == null)
                searchTerm = ""

            if(tagList == null)
                tagList = ""

            if(tagSearchMode == null)
                tagSearchMode = "and"

            model = [storyInstanceList: [], storyInstanceCount: 0, title: "Search", searchTerm: searchTerm, tagList: tagList, srchCategory: srchCategory, tagSearchMode: tagSearchMode, useAlias: useAlias, categories: catList]

            // honey-pot; if topic has something in it, it's spam
            if (topic)
            {
                log.error "${systemAdminService?.logPrefix}Topic not null"
            }
            else {
                if(tagList.size() != 0 || searchTerm.length() != 0 || srchCategory > 0) {
                    Set<Tag> tags = null
                    if(tagList) {
                        def tagStrList = tagService.parseTags(tagList)

                        tags = Tag.findAllByTagInList(tagStrList)
                    }

                    def cat = null
                    if(srchCategory)
                        cat = Tag.get(srchCategory)

                    def results = storyService.advancedStorySearch(tags, searchTerm, cat, (tagSearchMode == 'and'), useAlias == "true", params)

                    if (results) {
                        model.storyInstanceCount = results.totalCount
                        model.storyInstanceList = results.list
                    }
                }
            }
        }
        catch(Exception ex) {
            def logParamStr = "Search Term: <${searchTerm}>, Category: <${srchCategory}>, Tags: <${tagList}>, Category: <${srchCategory}>, useAlias: ${useAlias}, searchMode: ${tagSearchMode}."
            log.error"${systemAdminService?.logPrefix}Exception when searching! ${logParamStr}", ex
            systemAdminService.sendAdminMail("Fishy search", g.render(template: "/email/fishySearch", model: [searchParams: logParamStr, exception: ex, source: systemAdminService.getLogPrefix()]).toString())
        }

        model.categoryFilterList = storyService.getCategoryFilterList()
        render view:"search", model:model
    }

    def changeRating(Integer id, String ratingId, String rating){
        try {
            log.debug("Entered changeResults!")
            Story story = Story.get(id)
            if(! story)
            {
                log.error "${systemAdminService?.logPrefix}ChangeRating: tried to look up a story which doesn't exist (for id: ${params.id}). Maybe a bot?"
                return [:] as JSON
            }
            SecUser user = springSecurityService.isLoggedIn() ? springSecurityService.loadCurrentUser() : null
            String ip = systemAdminService.getIp()

            ratingService.updateRating(story,ip,user,ratingId,rating)

            log.debug("Exited changeResults!")
            return [:] as JSON
        }
        catch(Exception e) {
            log.error "${systemAdminService?.logPrefix}Exception when changing rating!",e
            throw e
        }
    }

    def ratings(Integer id)
    {
        try {
            Story story = Story.get(id)
            if(! story)
            {
                log.error "${systemAdminService?.logPrefix}Ratings: tried to look up a story which doesn't exist (for id: ${params.id}). Maybe a bot?"

                // sleep(4000)
                return [:] as JSON;
            }

            SecUser user = springSecurityService.isLoggedIn() ? springSecurityService.loadCurrentUser() : null
            Map ratingMap = ratingService.getRatingMap(story, systemAdminService.getIp(), user)

            render ratingMap as JSON
        }
        catch(Exception e) {
            log.error"${systemAdminService?.logPrefix}Exception when getting ratings",e
            throw e
        }
    }

    @Secured(value=["hasAnyRole('ROLE_AUTHOR', 'ROLE_ADMIN')"])
    def ratingResults(Integer id)
    {
        Map ratingMap = [:]
        try {
            Story story = Story.get(id)
            if(! story)
            {
                String ip = systemAdminService.getIp()
                log.error "${systemAdminService?.logPrefix}RatingsResults: tried to look up a story which doesn't exist (for id: ${params.id}). Maybe a bot?"
            }
            else
            {
                SecUser user = springSecurityService.isLoggedIn() ? springSecurityService.loadCurrentUser() : null
                if(user && (story.owners.contains(user) || user.hasRole('ROLE_ADMIN')))
                {
                    ratingMap = ratingService.getRatingResults(story)
                }
                else
                {
                    log.error("${systemAdminService?.logPrefix}Unauthorized fetching of rating results! User: ${user}")
                }
            }
        }
        catch(Exception e) {
            log.error"${systemAdminService?.logPrefix}Exception when getting ratings",e
        }

        render ratingMap as JSON
    }

    @Secured(value=["hasAnyRole('ROLE_AUTHOR', 'ROLE_ADMIN')"])
    def ratingStats(Integer id)
    {
        Map ratingStats = [:]
        try {
            Story story = Story.get(id)
            if(! story)
            {
                log.error "${systemAdminService?.logPrefix}RatingsStats: tried to look up a story which doesn't exist (for id: ${params.id}). Maybe a bot?"
            }
            else
            {
                SecUser user = springSecurityService.isLoggedIn() ? springSecurityService.loadCurrentUser() : null
                if(user && (story.owners.contains(user) || user.hasRole('ROLE_ADMIN')))
                {
                    ratingStats = ratingService.getStatisticsForStoryMap(story)
                }
                else
                {
                    log.error "${systemAdminService?.logPrefix}Unauthorized fetching of rating statistics! User: ${user}"
                }
            }
        }
        catch(Exception e) {
            log.error"${systemAdminService?.logPrefix}Exception when getting rating statistics", e
        }

        render ratingStats as JSON
    }


    def storyNames()
    {
        def namedParams = [:]
        def storyNames = Story.executeQuery("Select name from Story where deleted = false", namedParams, [readOnly: true])

        render(storyNames as JSON);
    }

    def seriesNames()
    {
        def namedParams = []
        def seriesNames = Series.executeQuery("Select name from Series", namedParams, [readOnly: true])

        render(seriesNames as JSON);
    }

    private def logActivity(Story story, String action)
    {
        def user = springSecurityService.isLoggedIn() ? springSecurityService.loadCurrentUser() : null

        def activity = new StoryActivity(user: user, story: story, action: action);
        activity.save flush:true, failOnError: true
    }
}
