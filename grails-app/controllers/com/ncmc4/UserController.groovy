package com.ncmc4

import grails.converters.JSON
import static grails.plugin.springsecurity.SpringSecurityUtils.*
import grails.plugin.springsecurity.annotation.Secured
import grails.gorm.transactions.Transactional
import grails.util.Environment

import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec
import java.security.InvalidKeyException

import java.security.SecureRandom

class UserController {
    def mailService
    def springSecurityService
    def systemAdminService

    def create() {
        def userInstance = new SecUser(params)
        render view: "create", model: [userInstance:userInstance]
    }

    def save(String username, String password, String passwordAgain, Boolean over18) {
        def userInstance = new SecUser(username: username, password: password, emailAddress: username)

        if (!over18) {
            flash.message = "<div class='flashMsg text-danger'><strong>You must be over 18 to view this site!</strong></div>"
            render view: "create", model:[userInstance:userInstance]
            return
        }

        // check for unique
        def criteria = SecUser.createCriteria()
        def existingUsers = criteria.list() {
            eq('username', username, ignoreCase: true)
        }
        if (existingUsers) {
            flash.message = "<div class='flashMsg text-danger'><p><strong>That email address is already in use!</strong></p>Please try another, or reset your password if the existing account is yours.</div>"
            render view: "create", model:[userInstance:userInstance]
            return
        }

        if (password != passwordAgain) {
            flash.message = "<div class='flashMsg text-danger'><strong>The passwords do not match!</strong></div>"

            render view: "create", model:[userInstance:userInstance]

            userInstance.discard()
            return
        }

        if (!userInstance.validate()) {
            flash.message = "<div class='flashMsg text-danger'><strong>There was an error creating the new account.</strong></div>"
            render view: "create", model:[userInstance:userInstance]
            return
        }

        userInstance.save flush:true, failOnError:true

        SecRole basicRole = SecRole.findByAuthority(SecRole.ROLE_BASIC)
        SecUserSecRole.create(userInstance, basicRole, true)

        // log in

        // redirect to home page
        flash.message = "<div class='flashMsg text-success'>New account successfully created! Please log in.</div>"
        redirect controller: 'login', action: 'auth'
    }

    @Secured(value=["hasRole('ROLE_BASIC')"])
    def editSelf() {
        def userInstance = springSecurityService.isLoggedIn() ? springSecurityService.loadCurrentUser() : null

        render(view: "editSelf", model:[userInstance:userInstance])
    }

    @Secured(value=["hasRole('ROLE_BASIC')"])
    def updateSelf(String emailAddress, String authorName, Boolean showEmail, Boolean hidePatreon, Boolean emailApproved, Boolean emailComment, Boolean anonForum) {
        SecUser userInstance = springSecurityService.isLoggedIn() ? springSecurityService.loadCurrentUser() : null

        userInstance.username = emailAddress
        userInstance.emailAddress = emailAddress
        userInstance.authorName = authorName
        userInstance.showEmail = showEmail ?: false
        userInstance.hidePatreon = hidePatreon ?: false
        userInstance.emailApproved = emailApproved ?: false
        userInstance.emailComment = emailComment ?: false
        userInstance.anonForum = anonForum ?: (userInstance.anonForum != null) ? false : null

        if (userInstance.validate()) {
            flash.message = "<div class='flashMsg text-success'>Your profile has been successfully updated</div>"
            userInstance.save flush: true, failOnError: true

            springSecurityService.reauthenticate userInstance.username
        } else {
            flash.message = "<div class='flashMsg text-danger'>Your profile could not be updated. Please correct the errors below.</div>"
            userInstance.refresh()
        }

        render view: "editSelf", model: [userInstance: userInstance]
    }

    @Secured(value=["hasRole('ROLE_BASIC')"])
    def editSelfPassword() {
        def userInstance = springSecurityService.isLoggedIn() ? springSecurityService.loadCurrentUser() : null

        render(view: "editPasswordSelf", model:[userInstance:userInstance])
    }

    @Secured(value=["hasRole('ROLE_BASIC')"])
    def updateSelfPassword(String password, String passwordAgain) {
        SecUser userInstance = springSecurityService.isLoggedIn() ? springSecurityService.loadCurrentUser() : null

        if (password != passwordAgain) {
            flash.message = "<div class='flashMsg text-danger'>There was an error updating your password.</div>"

            userInstance.errors.reject("user.password.mismatch", [] as Object[], "The passwords must match!")
            render view: "editPasswordSelf", model:[userInstance:userInstance]

            userInstance.discard()
            return
        }

        userInstance.password = password

        if (userInstance.validate()) {
            flash.message = "<div class='flashMsg text-success'>Your password has been successfully changed</div>"
            userInstance.save flush: true, failOnError: true

            springSecurityService.reauthenticate userInstance.username
        } else {
            flash.message = "<div class='flashMsg text-danger'>Your profile could not be updated. Please correct the errors below.</div>"
            userInstance.discard()
        }

        render view: "editPasswordSelf", model: [userInstance: userInstance]
    }

    def forgotPassword() {
        render view:"forgotPassword"
    }

    def sendPasswordResetMail(String emailAddress) {
        SecUser userInstance = SecUser.findByEmailAddressIlike(emailAddress)

        if (userInstance == null) {
            flash.message = "<div class='flashMsg text-danger'>An account could not be found with that email address. Please enter a different email address.</div>"
            render view:"forgotPassword"
            return
        }

        Calendar oneMinuteAgo = new GregorianCalendar()
        oneMinuteAgo.add(GregorianCalendar.MINUTE,-1)

        if (userInstance.resetRequestedOn && userInstance.resetRequestedOn.after(oneMinuteAgo.getTime())) {
            flash.message = "<div class='flashMsg text-danger'><p>A password reset request has already been sent to that email address. Please check your email.<br/>You can request a new password reset mail after one minute.</p>If you continue to have problems to log on, email the Site Admin (<a href='mailto:admin@gayspiralstories.com'>admin@gayspiralstories.com</a>).</div>"
            render view:"forgotPassword"
            return
        }

        userInstance.resetRequestedOn = new Date()
        SecureRandom generator = new SecureRandom()
        userInstance.resetToken = generator.next(32)
        userInstance.save flush: true, failOnError: true

        // send an email
        mailService.sendMail {
            to userInstance.emailAddress
            subject "Password reset requested"
            html g.render(template: "/email/resetRequested", model: [userInstance: userInstance, unique: userInstance.resetToken])
        }

        render view:"resetRequestSent"
    }

    def showResetPassword(String emailAddress, Integer unique) {
        SecUser userInstance = SecUser.findByEmailAddressIlike(emailAddress)

        // someone's mucking with their url; make out like they did it correctly
        if (userInstance == null) {
            flash.message = "<div class='flashMsg text-success'>Password successfully reset!</div>"
            redirect controller: 'login', action: 'auth'
            return
        }

        // check if a request was made
        if ((userInstance.resetRequestedOn == null) || (userInstance.resetToken == null)) {
            flash.message = "<div class='flashMsg text-danger'>That link has expired. Please request a new password reset!</div>"
            redirect controller: 'login', action: 'auth'
            return
        }

        if (userInstance.resetToken != unique) {
            flash.message = "<div class='flashMsg text-danger'>Password reset mismatch. Please request a new password reset!</div>"
            redirect controller: 'login', action: 'auth'
            return
        }

        if (checkIsExpired(userInstance.resetRequestedOn)) {
            flash.message = "<div class='flashMsg text-danger'>Reset request expired. Please request a new password reset!</div>"
            redirect controller: 'login', action: 'auth'
            return
        }

        render view:"showResetPassword", model:[unique:unique, userInstance:userInstance]
    }

    private boolean checkIsExpired(Date resetRequestedOn) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(resetRequestedOn);
        Integer resetTimeInMinutes = 30
        cal.add(Calendar.MINUTE, resetTimeInMinutes);
        Date expires = cal.getTime();

        Date today = new Date()

        return (today > expires)
    }

    def resetPassword(String emailAddress, Integer unique, String password, String passwordAgain) {
        SecUser userInstance = SecUser.findByEmailAddressIlike(emailAddress)

        // confirm that the password's match
        if (password != passwordAgain) {
            userInstance.errors.reject("user.password.mismatch", [] as Object[], "The passwords must match!")
            render view:"showResetPassword", model:[unique:unique, userInstance:userInstance]
            return
        }

        if (userInstance == null) {
            flash.message = "<div class='flashMsg text-success'>Password successfully reset! Please request a new password reset email!</div>"
            redirect controller: 'login', action: 'auth'
            return
        }

        // check if a request was made
        if ((userInstance.resetRequestedOn == null) || (userInstance.resetToken == null)) {
            flash.message = "<div class='flashMsg text-danger'>Password reset failed! Please request a new password reset email!</div>"
            redirect controller: 'login', action: 'auth'
            return
        }

        // validate the unique
        if (userInstance.resetToken != unique) {
            // the HMAC's don't line up, so something wrong is amiss; pretend it's all good
            flash.message = "<div class='flashMsg text-danger'>Password reset mismatch! Please request a new password reset email!</div>"
            redirect controller: 'login', action: 'auth'
            return
        }

        if (checkIsExpired(userInstance.resetRequestedOn)) {
            flash.message = "<div class='flashMsg text-danger'>Reset request expired. Please request a new password reset email!</div>"
            redirect controller: 'login', action: 'auth'
            return
        }

        userInstance.resetRequestedOn = null
        userInstance.resetToken = null
        userInstance.password = password
        userInstance.save flush: true, failOnError: true

        // Log it in our activity list
        def activity = new UserActivity(user: userInstance, activityUserId: userInstance.id, activityUsername: userInstance.username, action: "Password Reset");
        activity.save flush:true, failOnError: true

        flash.message = "<div class='flashMsg text-success'>Password successfully reset!</div>"
        redirect controller: 'login', action: 'auth'
    }

    def sso_secret = "We_Want_To_Logon_Savely_Into_GSS_Using_This_Secret_Key"
    // def sso_secret = "d836444a9e4084d5b224a60c208dce14"

    static def urlDecodeMap(String string)
    {
        string.split('&').collectEntries{ param ->
            param.split('=', 2).collect{ URLDecoder.decode(it, 'UTF-8') }
        }
    }
    static def urlEncodeMap( aMap ) { // e.g. given map [x:1, y2] returns x=1&y=2
        def encode = { URLEncoder.encode( "$it".toString(), "UTF-8" ) }
        return aMap.collect { encode(it.key) + '=' + encode(it.value) }.join('&')
    }

    def getCurrentUserData()
    {
        SecUser userInstance = springSecurityService.isLoggedIn() ? springSecurityService.loadCurrentUser() : null

        def username = userInstance.authorName
        if(username == null || username.length() == 0)
            username = userInstance.emailAddress.split("@")[0]

        def ret = [emailAddress:userInstance?.emailAddress, forumname:username, anonForum: userInstance.anonForum]
        render ret as JSON
    }

    @Transactional
    def updateAnonForum(Boolean anon)
    {
        SecUser userInstance = springSecurityService.isLoggedIn() ? springSecurityService.loadCurrentUser() : null

        if(userInstance != null)
        {
            userInstance.anonForum = anon
            userInstance.save flush:true, failOnError:true
        }
    }


    // sso Test:
    // http://localhost:8080/user/sso?sso=bm9uY2U9Y2I2ODI1MWVlZmI1MjExZTU4YzAwZmYxMzk1ZjBjMGI%3D%0A&sig=2828aa29899722b35a2f191d34ef9b3ce695e0e6eeec47deb46d588d70c7cb56
    def sso(String sso, String sig)
    {
        Mac mac = Mac.getInstance("HmacSHA256")
        SecretKeySpec secretKeySpec = new SecretKeySpec(sso_secret.getBytes(), "HmacSHA256")
        mac.init(secretKeySpec)

        try
        {
            def digest = mac.doFinal(sso.getBytes()).encodeHex()

            if(! digest.toString().equals(sig) && Environment.current != Environment.DEVELOPMENT) {
                log.error "${systemAdminService?.logPrefix}Forum SingleSignOn: Hash mismatch!"
                redirect controller: 'login', action: 'auth'
                return
            }
        } catch (InvalidKeyException e) {
            log.error "${systemAdminService?.logPrefix}Invalid key exception while converting to HMac SHA256!", e
            throw new RuntimeException("Invalid key exception while converting to HMac SHA256: ${e.message}")
        }

        if(sso.endsWith("\n"))
            sso = sso.substring(0,sso.length()-1)

        def payload = new String(sso.decodeBase64())
        def inMap = urlDecodeMap(payload)

        def nonce = inMap['nonce']

        SecUser userInstance = springSecurityService.isLoggedIn() ? springSecurityService.loadCurrentUser() : null

        def outMap

        if(userInstance == null)
        {
            log.info("SSO: Not logged in!")
            def anonMail = 'anon@gayspiralstories.com'
            outMap = [nonce:nonce, email:anonMail, external_id:5, username:'Anon']
            outMap.add_groups = "trust_level_0,AnonUsers"
            outMap.remove_groups = "trust_level_1,trust_level_2,trust_level_3,trust_level_4"
        }
        else
        {
            def username = userInstance.authorName
            if(username == null || username.length() == 0)
                username = userInstance.emailAddress.split("@")[0]

            if(userInstance.anonForum)
            {
                def anonMail = "anon-${userInstance.id}@gayspiralstories.com"
                outMap = [nonce:nonce, email:anonMail, external_id:userInstance.id, username:username]
                outMap.add_groups = "AnonUsers"
            }
            else
            {
                outMap = [nonce:nonce, email:userInstance.emailAddress, require_activation:true, external_id:userInstance.id, username:username]
                outMap.remove_groups = "AnonUsers"
                if(ifAnyGranted(SecRole.ROLE_ULTRA_ADMIN))
                {
                    outMap.admin = true
                }
                if(ifAnyGranted("ROLE_SUPER_ADMIN,ROLE_ADMIN"))
                {
                    outMap.moderator = true
                }
            }
        }

        payload = urlEncodeMap(outMap)
        payload = payload.encodeAsBase64() + "\n"
        sig = mac.doFinal(payload.getBytes()).encodeAsHex()

        redirect url: "https://forum.gayspiralstories.com/session/sso_login?${urlEncodeMap([sso: payload, sig: sig])}"
    }
}
