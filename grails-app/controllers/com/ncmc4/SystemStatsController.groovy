package com.ncmc4

import grails.plugin.springsecurity.annotation.Secured

@Secured(value=["hasRole('ROLE_SUPER_ADMIN')"])
class SystemStatsController {

    SystemAdminService systemAdminService
    StoryService storyService
    CommentService commentService
    TagService tagService
    HeaderService headerService
    def elasticSearchService


    def index() {

        //render view:"index"
        def serverInfo = session.servletContext.getServerInfo()
        
        Runtime runtime = Runtime.getRuntime();

        long maxMemory = runtime.maxMemory();
        long allocatedMemory = runtime.totalMemory();
        long freeMemory = runtime.freeMemory();

        render view:"index", model:[freeMemory: freeMemory, allocatedMemory: allocatedMemory, maxMemory: maxMemory, totalFreeMemory: freeMemory + (maxMemory - allocatedMemory), serverInfo: serverInfo]
    }

    def resetEtags() {
        headerService.resetEtags()

        flash.message = "Etags Reset"

        index()
    }

    def resetCommentCache() {
        commentService.resetCache()

        flash.message = "Updated comment cache"

        index()
    }

    def resetPatreon() {
        systemAdminService.resetPatreon()

        flash.message = "Patreon role reset"

        index()
    }

    def fixUserCounts() {
        systemAdminService.fixUserCounts()

        flash.message = "Queued fix of user counts"

        index()
    }

    def resetSeriesLinks() {
        systemAdminService.resetSeriesLinks()

        flash.message = "Queued fix series"

        index()
    }

    def fixStoryCommentCounts() {
        systemAdminService.fixCommentCounts(true)

        flash.message = "Queued fix of story comment counts"

        index()
    }

    def resetTagCache() {
        tagService.resetCache()

        flash.message = "Reset tag cache"

        index()
    }

    def reindexSearch() {
        elasticSearchService.index()

        flash.message = "Search index recreated"

        index()
    }

    def garbageCollect() {
        System.gc()

        flash.message = "Garbage Collection Performed"

        index()
    }
}