package com.ncmc4

import grails.plugin.springsecurity.annotation.Secured

import static org.springframework.http.HttpStatus.*
import grails.gorm.transactions.Transactional

@Transactional(readOnly = true)
class NewAuthorController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def springSecurityService
    def storyService

    def systemAdminService

    def index(Integer max) {
        params.max = Math.min(max ?: 30, 100)

        def authorList = Author.list(params)

        render view: "index", model: [authorInstanceCount: Author.count(), authorInstanceList: authorList, title: "Authors"]
    }

    def all() {
        def authorList = Author.list()

        render model: [authorInstanceCount: 0, authorInstanceList: authorList], view: "index"
    }

    def letter(String id, Integer max) {
        params.max = Math.min(max ?: 30, 100)

        String letter = id

        if (!letter)
            letter = "a"
        else
            letter = letter.substring(0, 1)

        params.authorLetter = letter;

        //def authorList = Author.findAllByNameIlike("$letter%", params)


        def criteria = Author.createCriteria()
        def authors = criteria.list(params) {
            ilike("name", "${letter}%")
        }

        render view: "letterIndex", model: [authorInstanceCount: authors.totalCount, authorInstanceList: authors, title: "Authors starting with '${letter.toUpperCase()}'", letter: letter]
    }

    def show(Author authorInstance, Integer max) {
        if (authorInstance?.name == null)
        {
            String id = params.id
            if (!id?.isInteger())
            {
                log.error "${systemAdminService?.logPrefix}Tried to look up an author with an invalid id (${params.id}). Looks like bots; throttling."

                sleep(4000)
                return response.sendError(BAD_REQUEST.value())
            }

            log.error "${systemAdminService?.logPrefix}Tried to look up an author that doesn't exist (for id: ${params.id})"
            return response.sendError(NOT_FOUND.value())
        }

        params.max = Math.min(max ?: 10, 100)
        def authorSlug = storyService.makeAuthorSlug(authorInstance.name)

        def catFilterList = storyService.getCategoryFilterList()

        def query = storyService.findAllByAuthorSlug(authorSlug, params)
        def storyList = query.list
        def count = query.count

        render view: "show", model: [storyInstanceCount: count, authorInstance: authorInstance, storyInstanceList: storyList, categoryFilterList: catFilterList, title: "Stories by ${authorInstance.displayName}"]
    }

    @Secured(value=["hasAnyRole('ROLE_AUTHOR,ROLE_SUPER_ADMIN,ROLE_ADMIN,ROLE_ULTRA_ADMIN')"])
    def listAuthorStories(Integer max)
    {
        def user = springSecurityService.loadCurrentUser()

        params.max = Math.min(max ?: 30, 100)

        def query = storyService.findAllByOwner(user, params)

        def storyList = query.list
        render view:"showMe", model:[storyInstanceCount: query.count, storyInstanceList: storyList, user: user]
    }

    @Secured(value=["hasRole('ROLE_ADMIN')"])
    def listAdminAuthorStories(SecUser secUserInstance, Integer max)
    {
        params.max = Math.min(max ?: 30, 100)

        def query = storyService.findAllByOwner(secUserInstance, params)

        def storyList = query.list
        render view:"adminAuthorStories", model:[storyInstanceCount: query.count, storyInstanceList: storyList, user: secUserInstance, title: "${secUserInstance}'s Stories"]
    }
}