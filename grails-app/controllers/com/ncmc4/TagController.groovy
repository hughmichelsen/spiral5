package com.ncmc4

import grails.converters.JSON
import grails.gorm.transactions.Transactional
import grails.plugin.springsecurity.annotation.Secured

@Transactional()
class TagController {

    def storyService
    def tagService
    def systemAdminService

    @Secured(value=["hasRole('ROLE_ADMIN')"])
    def index(Integer max)
    {
        def filterCategory = params.filterCategory == "true"
        def filterPromoted = params.filterPromoted == "true"

        def query = Tag.where {}
        if(filterPromoted)
            query = query.where { promoted == 'true'}
        if(filterCategory)
            query = query.where { isCategory == 'true'}

        params.max = Math.min(max ?: 25, 200)
        if (!params.sort)
            params.sort = "tag"
        if (!params.order)
            params.order = "asc"

        def tags = query.list(params)

        render view:"index", model:[params: params, tagInstanceCount: tags.totalCount, tagInstanceList: tags, filterCategory: filterCategory, filterPromoted: filterPromoted]
    }

    @Secured(value=["hasRole('ROLE_ADMIN')"])
    def searchTag(Integer max, String searchTerm, String topic)
    {
        if (!searchTerm) {
            redirect action: "index"
            return
        }

        // honey-pot; if topic has something in it, it's spam
        if (topic)
        {
            log.error "${systemAdminService?.logPrefix}Topic not null"
            redirect action: "index"
            return
        }

        params.max = Math.min(max ?: 50, 200)
        if (!params.sort)
            params.sort = "tag"
        if (!params.order)
            params.order = "asc"

        def query = Tag.where { tag =~ "%${searchTerm}%" }

        def tags = query.list(params)

        render view:"index", model:[tagInstanceCount: tags.totalCount, tagInstanceList: tags, filterCategory: false, filterPromoted: false]
    }

    @Secured(value=["hasRole('ROLE_ADMIN')"])
    def mergeTags(Integer mergeTagId, String mergeTagList)
    {
        Tag tag = Tag.get(mergeTagId);
        if(! tag) {
            log.error "${systemAdminService?.logPrefix}mergeAjax: Main Tag with id ${mergeTagId} not found!"
            return "Tag not found!";
        }

        def result = tagService.doMergeTags(tag, mergeTagList)
        if(result != "")
            flash.message = "<div class='flashMsg text-danger'>Error merging tag '${tag.tag}' with '${mergeTagList}': ${result}</div>"
        else
            flash.message = "<div class='flashMsg text-success'>Merged tag '${tag.tag}' with '${mergeTagList}'!</div>"

        def filterCategory = params.filterCategory == "true"
        def filterPromoted = params.filterPromoted == "true"

        def query = Tag.where {}

        if(filterPromoted)
            query = query.where { promoted == 'true'}
        if(filterCategory)
            query = query.where { isCategory == 'true'}
        if(params.searchTerm)
            query = query.where { tag =~ "%${params.searchTerm}%" }

        if (!params.max)
            params.max = 50
        if (!params.sort)
            params.sort = "tag"
        if (!params.order)
            params.order = "asc"

        def tags = query.list(params)

        render view:"index", model:[params: params, tagInstanceCount: tags.totalCount, tagInstanceList: tags, filterCategory: filterCategory, filterPromoted: filterPromoted]
    }

    @Secured(value=["hasRole('ROLE_ADMIN')"])
    def mergeTagsAjax(String id, String mergeTags)
    {
        Tag tag = Tag.get(id);
        if(! tag) {
            log.error "${systemAdminService?.logPrefix}mergeAjax: Main Tag with id ${id} not found!"
            return "Tag not found!";
        }

        def result = tagService.doMergeTags(tag, mergeTags)
        if(result != "")
            flash.message = "<div class='flashMsg text-danger'>Error merging tag ${tag.tag} with ${mergeTags}: ${result}</div>"
        else
            flash.message = "<div class='flashMsg text-success'>Merged tag ${tag.tag} with ${mergeTags}!</div>"

        render "success"
    }


    @Secured(value=["hasRole('ROLE_ADMIN')"])
    def addAliasTagsAjax(String id, String aliasTags)
    {
        Tag tag = Tag.get(id);
        if(! tag) {
            log.error "${systemAdminService?.logPrefix}mergeAjax: Main Tag with id ${id} not found!"
            return "Tag not found!"
        }

        def result = tagService.doAddAliases(tag, aliasTags)
        if(result != "")
            flash.message = "<div class='flashMsg text-danger'>Error adding aliases ${aliasTags} to tag ${tag.tag}: ${result}</div>"
        else
            flash.message = "<div class='flashMsg text-success'>Added aliases ${aliasTags} to tag ${tag.tag}!</div>"

        render "success"
    }


    @Transactional
    @Secured(value=["hasRole('ROLE_ADMIN')"])
    def changePromote(Integer id, Boolean promoted)
    {
        try {
            def tag = Tag.get(id)
            tag.promoted = promoted
            tag.save flush:true, failOnError: true

            render "success"
        }
        catch(Exception ex){
            render ex.message
        }
    }

    @Transactional
    @Secured(value=["hasRole('ROLE_ADMIN')"])
    def changeIsCategory(Integer id, Boolean isCategory)
    {
        try {
            def tag = Tag.get(id)
            tag.isCategory = isCategory
            tag.save flush:true, failOnError: true

            render "success"
        }
        catch(Exception ex){
            render ex.message
        }
    }

    @Secured(value=["hasRole('ROLE_ADMIN')"])
    def deleteAjax(Integer id)
    {
        Tag tag = Tag.get(id)

        def result = tagService.doDeleteTag(tag)

        if(result != "")
        {
            flash.message = "<div class='flashMsg text-danger'>Error deleting tag ${tag.tag}: ${result}</div>"
            render result
        }

        render "success"
    }

    @Transactional
    @Secured(value=["hasRole('ROLE_ADMIN')"])
    def updateNameAjax(Integer id, String newName)
    {
        try {
            if(newName.trim().length() > 0)
            {
                Tag tag = Tag.get(id)

                def oldName = tag.tag

                if(oldName != newName)
                {
                    def result = tagService.doUpdateName(tag, newName)

                    if(result == ""){
                        tagService.logActivity(tag, "renamed from ${oldName}")
                        return "success"
                    }
                    else{
                        flash.message = "<div class='flashMsg text-danger'>Error renaming tag ${tag.tag} to ${newName}: ${result}</div>"
                        return result
                    }
                }
                render "success"
            }
            else
                render 'Text must not be empty!'
        }
        catch(Exception ex){
            flash.message = "<div class='flashMsg text-danger'>Error creating duplicate: ${ex.message}</div>"
            render  ex.message
        }
    }

    @Transactional
    @Secured(value=["hasRole('ROLE_ADMIN')"])
    def createAliasAjax(Integer id)
    {

        try {
            Tag tag = Tag.get(id)

            def oldName = tag.tag
            def aliasName = oldName + "-Alias"

            if(oldName != aliasName)
            {
                def result = tagService.doCreateAlias(tag, aliasName)

                if(result == ""){
                    tagService.logActivity(tag, "created alias ${aliasName}")
                    return "success"
                }
                else{
                    flash.message = "<div class='flashMsg text-danger'>Error creating alias ${aliasName} for tag ${tag.tag}: ${result}</div>"
                    return result
                }
            }
            render "success"
        }
        catch(Exception ex){
            flash.message = "<div class='flashMsg text-danger'>Error creating duplicate: ${ex.message}</div>"
            render  ex.message
        }
    }

    @Transactional
    @Secured(value=["hasRole('ROLE_ADMIN')"])
    def createDuplicateAjax(Integer id)
    {
        try {
            Tag tag = Tag.get(id)

            def oldName = tag.tag
            def duplicateName = oldName + "-Duplicate"

            if(oldName != duplicateName)
            {
                def result = tagService.doCreateDuplicate(tag, duplicateName)

                if(result == ""){
                    tagService.logActivity(tag, "created duplicate ${duplicateName}")
                    return "success"
                }
                else{
                    flash.message = "<div class='flashMsg text-danger'>Error creating duplicate ${duplicateName} for tag ${tag.tag}: ${result}</div>"
                    return result
                }
            }
            render "success"
        }
        catch(Exception ex){
            flash.message = "<div class='flashMsg text-danger'>Error creating duplicate: ${ex.message}</div>"
            render  ex.message
        }
    }

    def allTagsJSon(Integer excludeStory){
        def result = tagService.getAllTags(excludeStory)

        render result as JSON
    }

    def fullSpecTagsJSon(Boolean promoted, String start, String end){
        def result = tagService.getSpecTagsJSon(promoted, start, end)

        render result as JSON
    }

}
