package com.ncmc4

import grails.core.GrailsApplication

class ErrorController {

    def systemAdminService

    GrailsApplication grailsApplication

    def internalError = {
        // admin email is specified in the Config.groovy file
        // An email will be sent to the admin person whenever an internalError occurred.

        def ex = request.exception

        systemAdminService.sendAdminMail("Exception!", g.render(template: "/email/exception", model: [request: request, exception: ex, source: systemAdminService.getLogPrefix()]).toString())

        render view:'/error'
    }

    def index() { }
}
