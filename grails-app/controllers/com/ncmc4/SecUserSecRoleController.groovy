package com.ncmc4

import grails.plugin.springsecurity.annotation.Secured

@Secured(value=["hasRole('ROLE_ULTRA_ADMIN')"])
class SecUserSecRoleController {

    static scaffold = SecUserSecRole

}
