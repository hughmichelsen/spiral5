package com.ncmc4

class SitemapController {

    def storyService
    def seriesService

    def sitemap() {

        def stories = storyService.list([offs: 0, max: 100000, sort: "id", order: "desc"]).list
        def series = seriesService.findAllApproved([offs: 0, max: 100000, sort: "id", order: "desc"]).list
        def authors = Author.list()
        def categories = Tag.findAllByIsCategory(true)
        def tags = Tag.list()

        render view: "/sitemap", model: [stories: stories, series: series, authors: authors, categories: categories, tags: tags]
    }
}
