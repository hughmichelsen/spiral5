package com.ncmc4

class UrlMappings {

    static excludes = [
            '/favicon/*',
            '/favicon/**'
    ]

    static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }
        "/robots.txt" (view: "/robots")
        "/sitemap.xml" (controller: "sitemap", view: "sitemap")


        "/" {
            controller = "newHome"
        }

        "500"(controller: "error", action: "internalError")
        "400"(view:'/badrequest')
        "403"(view:'/accessDenied')
        "404"(view:'/notFound')
    }
}
