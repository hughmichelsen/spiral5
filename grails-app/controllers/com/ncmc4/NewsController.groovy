package com.ncmc4

import grails.plugin.springsecurity.annotation.Secured
import static grails.plugin.springsecurity.SpringSecurityUtils.*

class NewsController {

    def springSecurityService
    def systemAdminService

    def index(Integer max) {
        params.max = Math.min(max ?: 30, 100)
        params.sort = "creationDate"
        params.order = "desc"

        def newsList = News.list(params)
        def newsCount = News.count()

        def isAdmin = ifAnyGranted(SecRole.ROLE_SUPER_ADMIN)

        def showPagination = (newsCount > params.max)

        render (view:"index", model:[newsList:newsList, newsInstanceCount:newsCount, isAdmin: isAdmin, showPagination: showPagination])
    }

    def show(Integer id) {
        News newsInstance = News.read(id)

        render (view:"show", model:[newsInstance:newsInstance])
    }

    @Secured(value=["hasRole('ROLE_SUPER_ADMIN')"])
    def create() {
        def news = new News(params)

        render (view:"create", model: [newsInstance: news])
    }

    @Secured(value=["hasAnyRole('ROLE_SUPER_ADMIN')"])
    def edit(News newsInstance) {
        render view: "edit", model: [newsInstance: newsInstance, includeId: true]
    }

    @Secured(value=["hasRole('ROLE_SUPER_ADMIN')"])
    def save(News newsInstance) {
        withForm {
            try {
                SecUser user = springSecurityService.isLoggedIn() ? springSecurityService.loadCurrentUser() : null
                newsInstance.user = user

                newsInstance.save(flush:true, failOnError:true)

                flash.message = "<div class='flashMsg text-success'>News item successfully created.</div>"

                redirect(action:"index")
            } catch (e) {
                log.error "${systemAdminService?.logPrefix}Error while saving ${newsInstance}. Exception: ", e

                flash.message = "<div class='flashMsg text-danger'>Error submitting news. Please try again.</div>"
                render view: 'create', model: [newsInstance: newsInstance]
            }
        }.invalidToken {
            flash.message = "<div class='flashMsg text-danger'><strong>Already submitted!</strong></div>"
            render view: 'create', model: [newsInstance: newsInstance]

            newsInstance?.discard()
        }
    }

    @Secured(value=["hasRole('ROLE_SUPER_ADMIN')"])
    def update(News newsInstance) {
        withForm {
            try {
                SecUser user = springSecurityService.isLoggedIn() ? springSecurityService.loadCurrentUser() : null
                newsInstance.user = user

                newsInstance.save(flush:true, failOnError:true)

                flash.message = "<div class='flashMsg text-success'>News item successfully updated.</div>"

                redirect(action:"index")
            } catch (e) {
                log.error "${systemAdminService?.logPrefix}Error while saving ${newsInstance}. Exception: ", e

                flash.message = "<div class='flashMsg text-danger'>Error submitting news. Please try again.</div>"
                render view: "edit", model: [newsInstance: newsInstance, includeId: true]
            }
        }.invalidToken {
            flash.message = "<div class='flashMsg text-danger'><strong>Already submitted!</strong></div>"
            render view: "edit", model: [newsInstance: newsInstance, includeId: true]
        }
    }

    @Secured(value=["hasRole('ROLE_SUPER_ADMIN')"])
    def deleteAjax(Integer id)
    {
        News news = News.get(id)

        if (news != null)
        {
            news.delete(flush: true)
        }

        render "Success"
    }
}
