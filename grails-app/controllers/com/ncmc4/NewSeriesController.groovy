package com.ncmc4


import grails.gorm.transactions.Transactional
import grails.plugin.springsecurity.annotation.Secured
import grails.converters.JSON

import static org.springframework.http.HttpStatus.BAD_REQUEST
import static org.springframework.http.HttpStatus.NOT_FOUND


class NewSeriesController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    StoryService storyService
    SeriesService seriesService

    def systemAdminService

    def show(Series seriesInstance, Integer max, Boolean autoRedirect) {
        if (seriesInstance?.name == null)
        {
            String id = params.id
            if (!id?.isInteger())
            {
                log.error "${systemAdminService?.logPrefix}Tried to look up a series with an invalid id (${params.id}). Looks like bots; throttling."

                sleep(4000)
                return response.sendError(BAD_REQUEST.value())
            }

            log.error "${systemAdminService?.logPrefix}Tried to look up a series that doesn't exist (for id: ${params.id})"
            return response.sendError(NOT_FOUND.value())
        }

        params.max = Math.min(max ?: 10, 100)

        def query = storyService.findAllBySeries(seriesInstance, params)

        def storyList = query.list
        def count = query.count

        if ((count == 1) && (autoRedirect))
            redirect(controller:"NewStory", action:"show", id:storyList[0].id)
        else
            render view: "show", model: [seriesInstance: seriesInstance, storyInstanceCount: count, storyInstanceList: storyList, title: "${seriesInstance.name}"]
    }

    @Secured(value=["hasRole('ROLE_SUPER_ADMIN')"])
    @Transactional
    def updateSeriesName(Series seriesInstance, String name) {
        seriesInstance.nameSlug = storyService.makeStoryTitleSlug(name)
        seriesInstance.name = name

        seriesInstance.save flush:true, failOnError:true
    }

    @Secured(value=["hasRole('ROLE_SUPER_ADMIN')"])
    @Transactional
    def forceUpdate(Series seriesInstance) {
        seriesService.resetSeriesLinks(seriesInstance)

        def ret = [success: true]
        render ret as JSON
    }
}