package com.ncmc4

class InfoController {
    def privacy() {
        render view:"privacy"
    }

    def tos() {
        render view:"termsOfService"
    }

    def health() {
        render view:"termsOfService"
    }
}