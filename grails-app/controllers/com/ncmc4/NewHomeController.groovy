package com.ncmc4

import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured
import static grails.plugin.springsecurity.SpringSecurityUtils.*
import org.apache.commons.lang.builder.HashCodeBuilder
import org.hibernate.FlushMode

import com.google.common.cache.*

import java.time.ZoneId
import java.time.ZonedDateTime

class NewHomeController {

    StoryService storyService
    HeaderService headerService
    SeriesService seriesService
    CommentService commentService
    TagService tagService
    def springSecurityService
    def ratingService
    def systemAdminService

    private Cache<Integer, ArrayList<Map<String, String>>> commentCountCache = CacheBuilder.newBuilder()
            .maximumSize(100)
            .build();

    def index(Integer max) {
        max = max ?: 10
        params.max = max

        def bannerNews = News.findAllByShowBanner("true", [sort: "creationDate", order: "desc"])

        def cacheStamp = storyService.getStoryCacheLastUpdate()
        def isAdmin = ifAnyGranted(SecRole.ROLE_SUPER_ADMIN)

        def currentUser = springSecurityService.isLoggedIn() ? springSecurityService.getCurrentUser() : null

        def catFilterList = storyService.getCategoryFilterList()

        // Update Patreon Status
        if (currentUser)
            currentUser.getIsPatreon()

        def format = { input -> input ? 1 : 0 }

        // Get timezone from cookie if present
        TimeZone timeZone = systemAdminService.getUserTimeZone()

        ZonedDateTime now = ZonedDateTime.now(timeZone.toZoneId())

        // now = ZonedDateTime.of(2019, 6, 30, 00, 00, 00, 01, ZoneId.systemDefault())

        // Random Series

        def randomSeriesDefinition
        if (now.isAfter(ZonedDateTime.of(2018, 9, 19, 0, 0, 0, 0, ZoneId.systemDefault()))) {
            randomSeriesDefinition = seriesService.calcRandomPeriodSegmentData(now, 10)
        }

        // Top Series List
        Map topStoriesDefinition = ratingService.prepareRandomTopStoriesData(now)

        def catFilter = (storyService.getCategoryFilterCookie() ?: "")
        def curTheme = request.getCookies().find{ it.name=='currentTheme4' }?.value ?: "default"

        headerService.withCacheHeaders(this) {
            etag {
                "${cacheStamp}.$max.${params.offset ?: 0}.${format(isAdmin)}.${topStoriesDefinition?.topStoriesSegmentId}.${randomSeriesDefinition?.randomSeriesSegmentId}.${catFilter}.${curTheme}"
            }

            generate {
                Series.withSession { dbSession ->
                    def options = [max:max, offset:params.offset, sort:"lastChapterUpdated", order:"desc"]

                    def query = seriesService.findAllApproved(options)

                    query.list.each { Series series ->
                        if ((series.mostRecentStory == null))
                            seriesService.loadMostRecentStory(series)
                    }

                    def seriesToQueryForCommentCounts = query.list.findAll { Series series -> (series.wikiStory == null) }
                    def ids = seriesToQueryForCommentCounts*.id

                    if(randomSeriesDefinition) {
                        seriesService.fetchRandomSeriesList(randomSeriesDefinition)
                    }

                    ratingService.fetchTopSeriesMap(topStoriesDefinition,false)

                    render  view:"index",
                            model:[seriesInstanceCount : query.count, seriesInstanceList: query.list, updateHash: cacheStamp, ids:ids, isAdmin:isAdmin, bannerNews : bannerNews,
                                   topListHasAllRatings: topListHasAllRatings, categoryFilterList: catFilterList] + (topStoriesDefinition ?: [:]) + (randomSeriesDefinition ?: [])

                    dbSession.flushMode = FlushMode.MANUAL
                }
            }
        }
    }

    def fetchCommentCounts(String updateHash) {
        def ids = params.list("ids[]")

        def hashCodeBuilder = new HashCodeBuilder()
        ids.each { hashCodeBuilder.append(it) }

        // make sure to include the comment cache update value, to ensure that
        // when a comment is added or deleted from the db, the home page comment counts refresh
        hashCodeBuilder.append(commentService.getCacheLastUpdate())

        // include the input hash, so that we don't have to look it up again
        // since we looked it up in the index
        hashCodeBuilder.append(updateHash)

        Integer hashCode = hashCodeBuilder.hashCode().toInteger()

        headerService.withCacheHeaders(this) {

            etag {
                hashCode
            }

            generate {
                try {
                    ArrayList<Map<String, String>> countsMap = [:]

                    if (ids) {
                        countsMap = commentCountCache.get(hashCode) {
                            Series.withNewSession {
                                def seriesList = Series.findAllByIdInList(ids)

                                def ret = []

                                seriesList.each { Series series ->
                                    def mostRecentStory = seriesService.loadMostRecentStory(series)
                                    if (mostRecentStory != null) {
                                        def item = [:]
                                        item["commentCount"] = mostRecentStory?.commentCount
                                        item["seriesId"] = series.id
                                        item["commentLink"] = g.createLink(action: "show", controller: "NewStory", id: mostRecentStory?.id, absolute: true)

                                        ret.add(item)
                                    } else {
                                        log.error "${systemAdminService?.logPrefix}Couldn't load most recent story for '${series.name}', id: ${series.id}"
                                    }
                                }

                                return ret
                            }
                        }
                    }

                    def ret = [comments: countsMap]
                    render ret as JSON
                } catch (Exception e) {
                    log.error "${systemAdminService?.logPrefix}Error fetching comment counts for: [$ids]. (max: ${params.max}, offset: ${params.offset}) Exception: ", e
                }
            }
        }
    }

    def affiliates() {
        render view:"affiliates"
    }

    def about() {
        render view:"about"
    }

    def news() {
        render view:"news"
    }

    @Secured(value=["hasRole('ROLE_ADMIN')"])
    def activity(Integer max) {
        max = max ?: 50
        params.max = max
        if(!params.sort)
            params.sort = "created"
        if(!params.order)
            params.order = "desc"

        def activityList = Activity.list(params)

        render view:"activity", model:[activityList: activityList, count: activityList.totalCount]
    }
}
