package ncmc4

import org.slf4j.Logger
import org.slf4j.LoggerFactory

class BootStrap {
    private static final Logger LOG = LoggerFactory.getLogger(this)

    def storyService
    def commentService
    def mailService
    def systemAdminService

    def init = { servletContext ->
        System.setProperty("user.timezone", "GMT");
        TimeZone.setDefault(TimeZone.getTimeZone("GMT"))

        LOG.info "Starting Spiral..."

        // limit email sending to one thread
        mailService.poolSize = 1

        // set up the caches
        commentService.updateCache()
        storyService.updateStoryCache()

        LOG.info "Spiral now running"
    }

    def destroy = {
    }
}
