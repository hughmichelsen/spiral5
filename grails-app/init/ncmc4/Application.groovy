package ncmc4

import com.captivatelabs.grails.timezone.detection.TimeZoneAwareDateEditor

import com.ncmc4.UserDetailsService
import grails.boot.GrailsApp
import grails.boot.config.GrailsAutoConfiguration
import org.springframework.context.EnvironmentAware
import org.springframework.core.env.Environment
import org.springframework.core.env.MapPropertySource

class Application extends GrailsAutoConfiguration implements EnvironmentAware {
    static void main(String[] args) {
        GrailsApp.run(Application, args)
    }

    private ConfigObject homeConfig = new ConfigObject()

    @Override
    void setEnvironment(Environment environment) {
        //Set up Configuration directory
        def home = System.getProperty("user.home")

        println ""
        println "Loading configuration from ${home}"
        def spiralConfig = 'GrailsConfig/SpiralConfig.groovy'
        def file = new File(home, spiralConfig)
        def appConfigured = file.exists()
        println "Loading configuration file ${file}"
        println "Config file found : " + appConfigured

        if (appConfigured) {
            ConfigObject config = new ConfigSlurper().parse(file.toURL())
            environment.propertySources.addFirst(new MapPropertySource("HomeConfig", config))

            homeConfig = config
        }
        else
            throw new Exception("Configuration missing!")
    }

    Closure doWithSpring() {
        def beans = {
            timeZoneAwareDateEditor(TimeZoneAwareDateEditor)

            userDetailsService(UserDetailsService) {
                grailsApplication = ref('grailsApplication')
            }
        }

        return beans
    }
}