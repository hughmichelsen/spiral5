// FileName: categorySelector.js

(function ($, window, document, undefined)
{
    'use strict';

    function filtercat_updateAll() {
        let allSet = true;
        let noneSet = true;
        $('.filterCatCheckbox').each(function () {
            if(! $(this).is(":checked"))
                allSet = false;
            else
                noneSet = false;
        });

        $('.cat_All').prop("checked", allSet);
        $('.applyFilterCatBtn').prop("disabled", noneSet);
    }

    filtercat_updateAll();

    $('.filterCatCheckbox').change(function(e){
        filtercat_updateAll();
    });

    $('.cat_All').change(function(e){
        let checked = $(this).is(":checked");
        $('.filterCatCheckbox').each(function () {
            $(this).prop("checked", checked);
        });
        $('.applyFilterCatBtn').prop("disabled", ! checked);
    });

    $('.applyFilterCat').submit(function(e){
        e.preventDefault();
        let allSet = true;
        let tags = [];
        $(this).find('.filterCatCheckbox').each(function () {
            if(! $(this).is(":checked"))
                allSet = false;
            else
                tags.push($(this).attr("name"))
        });
        let catFilter = allSet ? "" : tags.join(",");
        location.replace("?catFilter=" + catFilter)
    });

})(jQuery, window, document);
