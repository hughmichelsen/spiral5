package com.ncmc4

import groovy.transform.EqualsAndHashCode

@EqualsAndHashCode(includes=["uuid","name","creationDate"])
class WikiStory {
    String fileId
    String header

    String summary
    String emailAddress

    Series series

    Date creationDate = new Date()
    Collection tags

    Boolean approved = false
    Boolean deleted = false

    String editLink

    Story submittedStory

    static constraints = {
        fileId unique:true, nullable:false
        header nullable:true
        submittedStory nullable:true
        series nullable:true

        approved nullable:true
        deleted nullable:true

        editLink nullable:true
    }

    static hasMany = [
        tags: Tag
    ]

    static transients = ['fullData']

    static mapping = {
        header type: "text"
        summary type: "text"
        fileId type: "text"
        editLink type: "text"
    }

    Boolean getApproved()
    {
        def ret = this.@approved

        return ret ?: false
    }

    Boolean getDeleted()
    {
        def ret = this.@deleted

        return ret ?: false
    }
}