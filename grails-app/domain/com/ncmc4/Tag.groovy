package com.ncmc4

import groovy.transform.EqualsAndHashCode

@EqualsAndHashCode(includes="tag")
class Tag {
    static searchable = {
        only = 'tag'

        root false
    }

    String tag
    Boolean promoted
    Boolean isCategory
    Date created = new Date()

    TagAliasGroup aliasGroup

    static belongsTo = [aliasGroup: TagAliasGroup]

    static mapping =
    {
        table "grails_tag"
    }

    static constraints = {
        tag nullable: false, blank: false, unique: true
        promoted nullable: true
        isCategory nullable: true
        created nullable: true
        aliasGroup nullable: true
    }

    def getAliasStoryCount() {
        return aliasGroup?.getStoryCount()
    }

    def getAliasDeletedStoryCount() {
        return aliasGroup?.getDeletedStoryCount()
    }

    def getStoryCount() {
        return getStories().size
    }

    def getDeletedStoryCount() {
        return getDeletedStories().size
    }

    def getStories() {
        return Story.withCriteria {
            tags {
                eq('id', id)
            }
            eq('deleted', false)
        }
    }

    def getDeletedStories() {
        return Story.withCriteria {
            tags {
                eq('id', id)
            }
            eq('deleted', true)
        }
    }

    def getAliasList() {
        try
        {
            if(aliasGroup)
            {
                Set al = aliasGroup.aliases
                al.remove(this)

                return al.join(", ")
            }
            else return ""
        }
        catch(Exception e){
            return e.getMessage()
        }
    }

    static transients = ['storyCount', 'aliasList']

    String toString()
    {
        return tag
    }

    Date getCreated() {
        def ret = this.@created

        return ret ?: new Date()
    }
}
