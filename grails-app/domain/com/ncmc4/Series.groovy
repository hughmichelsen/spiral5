package com.ncmc4

import groovy.transform.EqualsAndHashCode
import groovy.transform.Synchronized

@EqualsAndHashCode
class Series {
    String name
    String nameSlug
    Author author
    String authorSlug
    Date lastChapterUpdated
    Boolean sponsored
    String summary
    Collection tags
    String tagCache

    Date featuredDate = null
    Tag category

    WikiStory wikiStory

    Integer storyCount = 0

    Story mostRecentStory = null

    static constraints = {
        summary size:(5..400)
        name size:(2..200)
        author nullable: true
        authorSlug nullable: true
        tagCache nullable: true
        mostRecentStory nullable: true
        wikiStory nullable: true
        category nullable: true
        featuredDate nullable: true
    }

    static mapping = {
        sort lastChapterUpdated: "desc"
        tagCache type: "text"
        tags cascade: "save-update"

        category index: 'series_category_idx'
        lastChapterUpdated index: 'series_lastUpdated_idx'

        cache false

        autowire true
    }

    static hasMany = [
            tags: Tag
    ]


    static fetchMode = [
            author: "eager",
            tags: "eager"
    ]

    def ratingService
    def storyService
    def ratingMarker

    @Synchronized
    def getRatingMarker() {
        if(ratingMarker)
            return ratingMarker

        def res = []

        if (! isAttached())
            return res

        def cal = Calendar.getInstance()
        cal.add(Calendar.DATE, -RatingService.ratingCalcDelay)

        def insufficientRatings = true

        List<Story> stories = (List<Story>)(storyService.findAllBySeries(this).list)
        List ratingsList = []
        stories.each { story ->
            def storyRatings = story.getRatingMarker()

            // Wenn genau ein ratingMarker da ist mit effect == "", dann hat die Story zu wenige valide Kategorien
            if (storyRatings?.size() != 1 || storyRatings[0].effect != "") {
                insufficientRatings = false
                ratingsList += storyRatings
            }
        }

        ratingsList = ratingsList.toSorted { a, b -> b.avgAllVal <=> a.avgAllVal }

        for(def i=0; i < ratingsList.size(); i++) {
            if(res.size() < RatingService.maxRatingIconsShown) {
                if(ratingsList[i].effect != "" && res.find { it.ratName == ratingsList[i].ratName } == null) {
                    res.add(ratingsList[i])
                }
            }
            else
                break
        }

        if (insufficientRatings)
            res.add(ratingService.getInsufficientRating())

        ratingMarker = res
        return ratingMarker
    }
}

