package com.ncmc4

class Activity {
    Date created = new Date()
    String action

    static belongsTo = [
        user: SecUser
    ]

    static constraints = {
        user nullable: true
        action nullable: false
    }

    static mapping = {
        table "grails_activity"
    }
}
