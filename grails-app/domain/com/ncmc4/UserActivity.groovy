package com.ncmc4

class UserActivity extends Activity {
    // for user activity, because I set this up wrong initially and don't care now

    Integer activityUserId
    String activityUsername

    static constraints = {
        activityUserId nullable: false
        activityUsername nullable: true
    }
}

