package com.ncmc4


import groovy.transform.EqualsAndHashCode
import groovy.transform.Synchronized

@EqualsAndHashCode(includes=["uuid","name","creationDate"])
class Story {

    static transients = ['ratingMarker']

    static searchable = {
        only = ['author', 'authorSlug', 'creationDate', 'publishDate', 'owners', 'name', 'nameSlug', 'summary', 'text', 'category', 'approved', 'previouslyApproved', 'deleted', 'tags']

        author component:true
        owners component:true
        category component:true
        tags component:true
        name alias:'title'
        authorSlug fielddata:true
        nameSlug fielddata:true

        root true
    }

    String name
    String nameSlug
    Date creationDate = new Date()
    Date publishDate = new Date()
    Date lastModifiedDate = new Date()

    Integer copyrightYear = null

    Collection comments
    Set tags
    Set owners

    Boolean approved = false
    Boolean anonymous = false
    Boolean deleted = false
    Boolean hideCopyright = false

    Boolean showEmail = false
    Boolean unformatted = false

    Boolean sponsored = false;
    String coverImageLink = null
    String endTeaser = null
    String amazonBookId = null

    String text

    Story nextInSeries
    Story prevInSeries

    Author author
    String authorSlug
    String emailAddress

    String summary

    String adminComment

    Boolean previouslyApproved = false
    Date lastReviewDate = null

    Date lastCommentDate = null
    Date lastTagDate = null

    Boolean autoExpandLinksInText = false

    Boolean disableNewComments = false

    Series series

    Tag category
    Integer commentCount = 0
    Integer tagCount

    Collection ratings

    String uuid = UUID.randomUUID().toString()

    def markdownParserService

    static mapping = {
        table "grails_story"
        sort publishDate: "desc"
        text type: "text"
        coverImageLink type: "text"
        endTeaser type: "text"

        creationDate index: 'story_creationDate_idx'
        publishDate index: 'story_publishDate_idx'
        approved index: 'story_approval_idx'
        previouslyApproved index: 'story_approval_idx'
        category index: 'story_category_idx'

        tags cascade: "save-update"

        // Make tagCount readOnly, it's calculated on the DB with a triggered update
        tagCount nullable: false, formula: 'tag_count'

        cache false

        autowire true
    }

    static hasMany = [
        comments: Comment,
        tags: Tag,
        owners: SecUser,
        ratings: Rating
    ]

    static fetchMode = [author: 'lazy', series: 'lazy', tags: 'lazy', owner: 'lazy']

    static constraints = {
        name size:(2..200)
        nameSlug nullable:true

        creationDate nullable:false
        publishDate nullable:true
        lastModifiedDate nullable:true

        copyrightYear nullable:true

        approved nullable:true
        anonymous nullable:true
        deleted nullable:true
        hideCopyright nullable:true

        showEmail nullable:true
        unformatted nullable:true

        sponsored nullable:true
        coverImageLink nullable:true
        endTeaser nullable:true
        amazonBookId nullable:true

        text nullable:false

        nextInSeries nullable:true
        prevInSeries nullable:true

        author nullable: true
        authorSlug nullable:true
        emailAddress email: true, nullable: false, size:(5..500)

        summary size:(5..400)

        adminComment nullable: true, size: (0..2000)

        previouslyApproved nullable:true
        lastReviewDate nullable:true

        lastCommentDate nullable:true
        lastTagDate nullable:true

        autoExpandLinksInText nullable:true
        disableNewComments nullable:true
        series nullable:true

        category nullable: true
        commentCount nullable:true

        ratings nullable:true

        uuid nullable:true, unique:true
    }

    String getTextFormatted()
    {
        return markdownParserService.parseAndSanitizeText(text,unformatted,autoExpandLinksInText)
    }

    String parseAndSanitizeText(String input)
    {
        return markdownParserService.parseAndSanitizeText(input,unformatted,autoExpandLinksInText)
    }

    String getTextUnformatted()
    {
        return text
    }

    String getEndTeaserFormatted()
    {
        return markdownParserService.parseAndSanitizeText(endTeaser,true,false)
    }

    String toString()
    {
        return name
    }

    Date getSavePublishDate()
    {
        def ret = this.@publishDate

        return ret ?: (creationDate)
    }

    Integer getCopyrightYear()
    {
        def ret = this.@copyrightYear

        return ret ?: (publishDate?.year + 1900)
    }

    Date getLastModifiedDate()
    {
        def ret = this.@lastModifiedDate

        return ret ?: (creationDate)
    }

    Date getLastTagDate()
    {
        def ret = this.@lastTagDate

        return ret ?: (lastModifiedDate)
    }

    Date getLastCommentDate()
    {
        def ret = this.@lastCommentDate

        return ret ?: (lastModifiedDate)
    }

    Boolean getHideCopyright()
    {
        def ret = this.@hideCopyright

        return ret ?: false
    }

    Integer getCommentCount()
    {
        // if a story's got null for this field, it means it's been previously approved
        def ret = this.@commentCount

        return (ret != null) ? ret : 0
    }

    def ratingService
    def ratingMarker

    @Synchronized
    def getRatingMarker()
    {
        if(ratingMarker)
            return ratingMarker

        def res = []

        def cal = Calendar.getInstance()
        cal.add(Calendar.DATE, -RatingService.ratingCalcDelay)

        def validRatingCatsLeft = RatingService.minValidRatingCategories

        if(cal.time.after(publishDate)) {
            List ratingsList = ratingService.getStatisticsForStoryMap(this).collect { entry -> entry.value }

            ratingsList = ratingsList.toSorted { a, b -> b.avgAllVal <=> a.avgAllVal }

            for(def i=0; i < ratingsList.size(); i++) {
                if(res.size() < RatingService.maxRatingIconsShown) {
                    if (ratingsList[i].numAll >= RatingService.minNumRatings) {
                        validRatingCatsLeft -= 1

                        if(ratingsList[i].effect != "") {
                            res.add(ratingsList[i])
                        }
                    }
                }
                else
                    break
            }
        }

        if (validRatingCatsLeft > 0)
            ratingMarker = [ratingService.getInsufficientRating()]
        else
            ratingMarker = res

        return ratingMarker
    }

     def getRatingCountHint() {
         def stats = ratingService.getStatisticsForStoryMap(this)
         if(stats.size() == 0)
             return 2

         int minRatings = 65000
         int maxRatings = 0
         stats.each { entry ->
             if(entry.value.numAll < minRatings)
                 minRatings = entry.value.numAll
             if(entry.value.numAll > maxRatings)
                 maxRatings = entry.value.numAll
         }

         // Sum of all ratings over all categories define rating hint
        return (minRatings < 10) ? 2 : (minRatings < 20) ? 1 : 0
    }

}
