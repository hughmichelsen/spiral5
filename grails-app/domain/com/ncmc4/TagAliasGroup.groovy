package com.ncmc4

import groovy.transform.EqualsAndHashCode

@EqualsAndHashCode
class TagAliasGroup {
    Date created = new Date()

    // Set aliases
    // static hasMany = [aliases: Tag]

    def getAliases() {
        return Tag.findAllByAliasGroup(this)
    }

    def getStoryCount(){
        return getStories(false).size()
    }

    def getDeletedStoryCount(){
        return getStories(true).size()
    }

    def getStories(boolean findDeleted)
    {
        Set tagIDSet = []
        aliases.each { alias ->
            tagIDSet << alias.id
        }

        def criteria = Story.createCriteria()
        def stories = criteria.list() {
            tags {
                'in'("id", tagIDSet)
            }
            eq('deleted', findDeleted)
        }

        return stories
    }

}
