package com.ncmc4

class TagActivity extends Activity {
    Integer activityTagId
    String activityTagName

    static constraints = {
        activityTagId nullable: false
        activityTagName nullable: false
    }
}

