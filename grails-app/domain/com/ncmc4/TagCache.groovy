package com.ncmc4

class TagCache
{
    Integer lastTagId
    String tags

    static constraints = {
    }

    static mapping = {
        tags type: "text"
    }
}