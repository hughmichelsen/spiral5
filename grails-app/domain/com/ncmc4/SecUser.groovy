package com.ncmc4

class SecUser implements Serializable {

	static searchable = {
		only = "username"

		root false
	}

	private static final long serialVersionUID = 1

	transient springSecurityService

	String username
	String password
	boolean enabled = true
	boolean accountExpired
	boolean accountLocked
	boolean passwordExpired

	Date resetRequestedOn
	Integer resetToken = null

	Date createdOn = new Date()
	Date lastSignedIn = new Date()

    Boolean emailApproved = true
	Boolean emailUnapproved
	Boolean emailComment = true
	String emailAddress

	Boolean anonForum

	// Patreon Features
	Boolean hidePatreon = false

	// fields for authors
	String authorName
	Boolean showEmail

	Integer commentCount = 0
	Integer likeCount = 0
	Integer storyCount = 0

	transient Boolean isPatreonCache = null

	SecUser(String username, String password) {
		this()
		this.username = username
		this.password = password
	}

	@Override
	int hashCode() {
		username?.hashCode() ?: 0
	}

	@Override
	boolean equals(other) {
		is(other) || (other instanceof SecUser && other.username == username)
	}

	@Override
	String toString() {
		username
	}

	Set<SecRole> getAuthorities() {
		SecUserSecRole.findAllBySecUser(this)*.secRole
	}

	Boolean hasRole(String role) {
		SecRole adminRole = getAuthorities()?.find { it.authority == role }

		return (adminRole != null)
	}

    Boolean hasAnyRole(List roles) {
        SecRole adminRole = getAuthorities()?.find { it.authority in roles }

        return (adminRole != null)
    }

    Integer getStoryCount()
	{
		def ret = this.@storyCount

		return ret ?: 0
	}

	Integer getLikeCount()
	{
		def ret = this.@likeCount

		return ret ?: 0
	}

	Integer getCommentCount()
	{
		def ret = this.@commentCount

		return ret ?: 0
	}

	def beforeInsert() {
		encodePassword()
	}

	def beforeUpdate() {
		if (isDirty('password')) {
			encodePassword()
		}
	}

	boolean getIsPatreon() {
		if(! isPatreonCache) {
			checkPatreonStatus()
		}

		return isPatreonCache.asBoolean()
	}

	protected void encodePassword() {
		password = springSecurityService?.passwordEncoder ? springSecurityService.encodePassword(password) : password
	}

	static transients = ['springSecurityService', 'isAdmin']

	static constraints = {
		username blank: false, unique: true
		password blank: false
		emailApproved nullable: true
		emailUnapproved nullable: true
		emailComment nullable: true
		hidePatreon nullable: true
		emailAddress nullable: true
		createdOn nullable: true
		lastSignedIn nullable: true

		resetRequestedOn nullable: true
		resetToken nullable: true

		anonForum nullable: true

		authorName nullable: true
		showEmail nullable: true

		storyCount nullable: true
		commentCount nullable: true
		likeCount nullable: true
	}

	static mapping = {
		password column: '`password`'
		sort id: "desc"

		autowire true
	}

	def patreonClient

	def checkPatreonStatus() {
		SecRole patreonRole = SecRole.createIfNeeded(SecRole.ROLE_PATREON)
		Set<SecRole> roles = getAuthorities()

		List<String> patreonMailAddresses = patreonClient.getCachedPatreonEmailAddresses()
		if(patreonMailAddresses.find{ it.toUpperCase() == emailAddress?.toUpperCase() }) {
			isPatreonCache = true
			if(! roles.contains(patreonRole)) {
				SecUserSecRole.create(this,patreonRole, true)
			}
		}
		else {
			isPatreonCache = false
			if(roles.contains(patreonRole)) {
				SecUserSecRole.remove(this,patreonRole,true)
			}
		}
	}

}
