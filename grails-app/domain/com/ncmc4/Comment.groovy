package com.ncmc4

import java.beans.Transient
import groovy.transform.EqualsAndHashCode

@EqualsAndHashCode
class Comment {

//    static searchable = {
//        root true
//    }

    String name
    Date creationDate = new Date()
    String comment
    String ipAddress

    SecUser poster

    static belongsTo = [story: Story]

    static constraints = {
        name nullable: true
        ipAddress nullable: true
        poster nullable: true
    }

    static transients = ['title']

    static mapping = {
        table "grails_comment"
        comment type: 'text'
        sort creationDate: "desc"
    }
}
