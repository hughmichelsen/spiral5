package com.ncmc4

import groovy.transform.EqualsAndHashCode

@EqualsAndHashCode
class SystemSettings {

    String name
    String value

}
