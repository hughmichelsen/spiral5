package com.ncmc4

class UserStoryStats {
    SecUser user
    Story story
    Integer viewCount = 0;
    Boolean liked = false;
    Date lastView = new Date()
    Date likedOn = new Date()

    static constraints = {
        user nullable: false
        story nullable: false
    }

    static mapping = {
    }
}
