package com.ncmc4

import groovy.transform.EqualsAndHashCode

@EqualsAndHashCode(includes="name")
class Author {

    static searchable = {
        only = "name"

        root false
    }

    String name

    static mapping = {
        table "grails_author"
        sort "name"
    }

    static constraints = {
        name nullable: false, blank: false
    }

    String getDisplayName()
    {
        return name
    }

    String toString()
    {
        return name
    }

}
