package com.ncmc4

import android.util.Patterns
import groovy.transform.EqualsAndHashCode

@EqualsAndHashCode
class News {

    String text
    Date creationDate = new Date()
    SecUser user
    Boolean autoFormat = true
    Boolean showBanner = false

    def markdownParserService

    static constraints = {
        text nullable: true
        user nullable: true
        autoFormat nullable: true
        showBanner nullable: true
    }

    static mapping = {
        table "grails_news"
        text type: 'text'
        sort id:"desc"

        autowire true
    }

    String getTextFormatted()
    {
        return markdownParserService.parseAndSanitizeText(text,true,false)
    }
}
