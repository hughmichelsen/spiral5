package com.ncmc4

import groovy.transform.ToString

@ToString
class Rating {
    static belongsTo = [
            story: Story,
            ratedBy: SecUser
    ]

    Long ipUpper
    Long ipLower

    Date creationDate = new Date()

    int ratingWriting = -1
    int ratingIdea = -1
    int ratingHot = -1
    int ratingMC = -1
    int ratingCum = -1

    Map<String, Integer> getRatingValueMap(){
        return [ratingWriting: ratingWriting,
                ratingIdea: ratingIdea,
                ratingHot: ratingHot,
                ratingMC: ratingMC,
                ratingCum: ratingCum]
    }

    static List<String> getRatingList(){
        return ['ratingHot', 'ratingMC', 'ratingCum', 'ratingWriting', 'ratingIdea']
    }

    static Map<String, String> getRatingLabelMap(){
        return [ratingWriting: "Writing",
                ratingIdea: "Idea",
                ratingHot: "Hot",
                ratingMC: "Mind control",
                ratingCum: "Wanking material",
                ratingInsufficient: "Needs more readers' ratings!"]
    }

    static Map<String, String> getRatingAdjectiveMap(){
        return [ratingWriting: "best written",
                ratingIdea: "most inventive",
                ratingHot: "hottest",
                ratingMC: "most hypnotic",
                ratingCum: "best wanking"]
    }

    static Map<String, String> getRatingTooltipMap(){
        return [ratingWriting: "well written",
                ratingIdea: "inventive",
                ratingHot: "hot",
                ratingMC: "hypnotic",
                ratingCum: "likely to make you cum"]
    }

    static Map<String, String> getRatingIconMap(){
        return [ratingWriting: "ratings/WritingColor.svg",
                ratingIdea: "ratings/IdeaColor.svg",
                ratingHot: "ratings/HotColor.svg",
                ratingMC: "ratings/BrainColor.svg",
                ratingCum: "ratings/CumColor.svg",
                ratingInsufficient: "ratings/insufficientRatings.svg"]
    }

    static Map<String, String> getRatingActiveIconMap(){
        return [ratingWriting: "ratings/WritingColor.svg",
                ratingIdea: "ratings/IdeaColor.svg",
                ratingHot: "ratings/HotColor.svg",
                ratingMC: "ratings/BrainColor.svg",
                ratingCum: "ratings/CumColor.svg"]
    }

    static Map<String, String> getRatingPassiveIconMap(){
        return [ratingWriting: "ratings/WritingBW.svg",
                ratingIdea: "ratings/IdeaBW.svg",
                ratingHot: "ratings/HotBW.svg",
                ratingMC: "ratings/BrainBW.svg",
                ratingCum: "ratings/CumBW.svg"]
    }

    static Integer scaleRating(String value)
    {
        if(value) {
            int result = (int) Math.round(Float.parseFloat(value) * 20)
            return (result < 0) ? -1 : result
        }
        else
            return null
    }

    static String unscaleRating(def value)
    {
        if(value >= 0)
            return String.format("%.2f", value / 20.0)
        else
            return null
    }

    static String unscaleRating(Integer value)
    {
        if(value)
            return String.format("%.2f", value / 20.0)
        else
            return null
    }

    static constraints = {
        ratedBy nullable:true
        story nullable:false
        creationDate nullable: false

        ipUpper nullable: false
        ipLower nullable: false

        ratingWriting range: -1..100
        ratingIdea range: -1..100
        ratingHot range: -1..100
        ratingMC range: -1..100
        ratingCum range: -1..100
    }
}
