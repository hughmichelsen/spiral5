package com.ncmc4

class ItemList {

    String identifier
    Date contextDate = new Date()
    List<Long> itemList

    static constraints = {
        identifier nullable: false, blank: false, unique: true
        contextDate nullable: false
    }
}
