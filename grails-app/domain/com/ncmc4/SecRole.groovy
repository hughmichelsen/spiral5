package com.ncmc4

class SecRole implements Serializable {

	private static final long serialVersionUID = 1

	static String ROLE_SUPER_ADMIN = "ROLE_SUPER_ADMIN"
	static String ROLE_ADMIN = "ROLE_ADMIN"
	static String ROLE_ULTRA_ADMIN = "ROLE_ULTRA_ADMIN"
	static String ROLE_AUTHOR = "ROLE_AUTHOR"
	static String ROLE_PATREON = "ROLE_PATREON"
	static String ROLE_BASIC = "ROLE_BASIC"

	String authority

	SecRole(String authority) {
		this()
		this.authority = authority
	}

	@Override
	int hashCode() {
		authority?.hashCode() ?: 0
	}

	@Override
	boolean equals(other) {
		is(other) || (other instanceof SecRole && other.authority == authority)
	}

	@Override
	String toString() {
		authority
	}

	static constraints = {
		authority blank: false, unique: true
	}

	static mapping = {
		cache true
	}

	static SecRole createIfNeeded(String roleName)
	{
		SecRole role = SecRole.findByAuthority(roleName)
		if (!role) {
			role = new SecRole(roleName)
			role.save flush: true, failOnError: true
		}

		return role
	}

}
