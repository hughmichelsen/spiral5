package com.ncmc4

class StoryActivity extends Activity {
    Story story

    static constraints = {
        story nullable: true
    }
}
