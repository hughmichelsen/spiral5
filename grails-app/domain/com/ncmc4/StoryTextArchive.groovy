package com.ncmc4

class StoryTextArchive {
    Date creationDate = new Date()
    Story story

    String oldText = null
    String oldAuthorName = null
    String oldName = null
    String oldSummary = null
    String oldSeriesName = null

    static mapping = {
        sort creationDate: "desc"

        oldText type: "text"

        cache false
    }

    static constraints = {
        story nullable: false
        creationDate nullable: false

        oldText nullable: true
        oldAuthorName nullable: true
        oldName nullable: true, size:(2..200)
        oldSummary nullable: true, size:(5..400)
        oldSeriesName nullable: true
    }
}
