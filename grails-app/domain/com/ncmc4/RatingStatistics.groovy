package com.ncmc4

import groovy.sql.Sql

class RatingStatistics {

    static belongsTo = [story: Story]

    String id
    String ratingType
    int publishMonth
    int publishYear
    int numRatings
    float average
    int rankOverall
    int rankMonth
    int rankYear

    static mapping = {
        table "rating_statistics"
        version false

        id generator: "uuid", column: 'id', sqlType: "varchar(255)"

        story column: "story_id"

        story insertable: false
        story updateable: false
    }

    static constraints = {
    }

}
