<g:applyLayout name="storyList" model="[storyInstanceList: storyInstanceList, storyInstanceCount: storyInstanceCount, title: title, showLetterLinks: false]">

    <content tag="paginate">
        <g:paginate class="justify-content-center d-flex flex-wrap" controller="newAuthor" action="show" id="${authorInstance.id}" total="${storyInstanceCount ?: 0}" />
    </content>

</g:applyLayout>