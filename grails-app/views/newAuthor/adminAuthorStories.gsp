<g:applyLayout name="storyList" model="[storyInstanceList: storyInstanceList, storyInstanceCount: storyInstanceCount, title: title, showLetterLinks: false]">

    <content tag="title">
        <h1 class="center-block text-center">Stories written by<br/>${user}</h1>
        <h1 class="text-center"><g:userButtons user="${user}"/></h1>
    </content>

    <content tag="paginate">
        <g:paginate class="justify-content-center d-flex flex-wrap" controller="newAuthor" action="listAdminAuthorStories" id="${user.id}" total="${storyInstanceCount ?: 0}" />
    </content>

</g:applyLayout>