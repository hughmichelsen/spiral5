<g:applyLayout name="storyList" model="[storyInstanceList: storyInstanceList, storyInstanceCount: storyInstanceCount, title: "Your Stories", showLetterLinks: false]">

    <content tag="paginate">
        <g:paginate class="justify-content-center d-flex flex-wrap" controller="newAuthor" action="listAuthorStories" total="${storyInstanceCount ?: 0}" />
    </content>

</g:applyLayout>