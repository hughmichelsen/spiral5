<g:applyLayout name="authorList" model="[authorInstanceList: authorInstanceList, authorInstanceCount: authorInstanceCount, title: title]">

    <content tag="paginate">

        <g:if test="${authorInstanceCount > params.max}">
            <g:paginate class="justify-content-center d-flex flex-wrap" action="letter" id="${letter}" total="${authorInstanceCount ?: 0}" />
        </g:if>
    </content>

</g:applyLayout>