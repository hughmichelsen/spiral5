<g:applyLayout name="newMain" model="[active:'']">
	<html>
<%
    if (!params.sort)
		params.sort = "tag"
	if (!params.order)
		params.order = "asc"
	if (!params.max)
		params.max = "50"
%>
	<head>
		<title><g:pageTitle title='Tags'/></title>

		<asset:stylesheet href="tag.css"/>

	</head>
	<body>


	<div class="row">
		<div class="col">
			<g:form url="[action:'searchTag']" method="GET">
				<div class="input-group mb-3">
					<input type="text" class="form-control topic" placeholder="Topic..." name="topic">
					<input type="text" class="form-control" placeholder="Search for..." name="searchTerm" value="${params?.searchTerm}"/>
					<span class="input-group-append">
						<button type="submit" class="btn btn-default" type="button"><i class="fa fa-search" aria-hidden="true"></i></button>
					</span>
				</div>
			</g:form>
		</div>
	</div>

	<div class="row">
		<div class="col">
			<g:paginate class="justify-content-center d-flex flex-wrap" action="index" total="${tagInstanceCount ?: 0}" params="[filterPromoted: filterPromoted, filterCategory: filterCategory]"/>
		</div>
	</div>
	<div class="table-responsive-lg">
		<table id="tagTable" class="table table-bordered table-striped">
			<thead class="thead-light">
				<tr class="row">
					<g:sortableColumn class="col-1" scope="col" property="id" title="ID" params="[filterPromoted: filterPromoted, filterCategory: filterCategory]"/>
					<g:sortableColumn class="col-5" scope="col" property="tag" title="Tag" params="[filterPromoted: filterPromoted, filterCategory: filterCategory]" style="border-right: 0px;"/>
					<%-- <g:sortableColumn property="storyCount" title="Stories" class="text-center col-sm-1" params="[filterPromoted: filterPromoted, filterCategory: filterCategory]"/> --%>
					<th class="text-center col-2" scope="col" title="Stories" class="text-center"><span class="text-primary">Stories</span></th>
					<th class="text-center col-1" scope="col" title="Category" data-title="Category" class="text-center" style="${filterCategory ? 'background-color: lightblue !important;' : ''}">
						<g:link action="index" params="[filterPromoted: filterPromoted, filterCategory: ! filterCategory]">
							<span class="text-primary">
								<i class="fa fa-list-alt fa-2x"></i>
							</span>
						</g:link>
					</th>
					<th class="text-center col-1" scope="col" title="Promoted" data-title="Promoted" class="text-center" style="${filterPromoted ? 'background-color: lightblue !important;' : ''}">
						<g:link action="index" params="[filterPromoted: ! filterPromoted, filterCategory: filterCategory]">
							<span class="text-primary">
								<i class="fa fa-search fa-2x"></i>
							</span>
						</g:link>
					</th>
					<th class="text-center col-2" scope="col" ><span class="text-primary">Action</span></th>
				</tr>
			</thead>
			<tbody>
				<g:each in="${tagInstanceList}" var="tagInstance">
					<tr class="row" id="row-${tagInstance.id}">
						<th scope="row" class="col-1">${tagInstance.id}</th>
						<td class="col-5"><a href="#" class="tagNameInput" data-type="text" data-id="${tagInstance.id}">${tagInstance.tag}</a>
						<g:if test="${tagInstance?.aliasList}">
								<i style="font-size: 80%">(${tagInstance?.aliasList})</i>
						</g:if></td>
						<td class="text-center col-2"><g:link controller="newStory" action="listTagged" params="[resolveAlias:false, findDeleted:false, findUnApproved: true]" id="${tagInstance.id}">${tagInstance.storyCount}</g:link> / <g:link controller="newStory" action="listTagged" params="[resolveAlias:false, findDeleted:true, findUnApproved: true]" id="${tagInstance.id}">${tagInstance.deletedStoryCount}</g:link>
							<g:if test="${tagInstance?.aliasList?.length() > 0}">
								<i style="font-size: 80%">(<g:link controller="newStory" action="listTagged" params="[resolveAlias:true, findDeleted:false, findUnApproved: true]" id="${tagInstance.id}">${tagInstance?.aliasStoryCount}</g:link> / <g:link class="text-danger" controller="newStory" action="listTagged" params="[findDeleted:true, resolveAlias:true, findUnApproved: true]" id="${tagInstance.id}">${tagInstance?.aliasDeletedStoryCount}</g:link>)</i>
							</g:if>
						</td>
						<td class="text-center col-1"><g:checkBox name="huh" class="categoryCheckbox" value="${tagInstance.isCategory}" data-id="${tagInstance.id}" /></td>
						<td class="text-center col-1"><g:checkBox name="huh" class="promotedCheckbox" value="${tagInstance.promoted}" data-id="${tagInstance.id}" /></td>
						<td class="col-2">
							<div class="d-flex justify-content-between align-items-center">
								<a href="#" class="btn mr-1 btn-sm btn-primary duplicateButton" data-id="${tagInstance.id}" data-tagname="${tagInstance.tag}"><span class="fa fa-share-alt" aria-label="Duplicate"></span></a>
								<a href="#" class="btn mr-1 btn-sm btn-primary aliasButton" data-id="${tagInstance.id}" data-tagname="${tagInstance.tag}"><span class="fa fa-tags" aria-label="Duplicate"></span></a>
								<a href="#" class="btn mr-1 btn-sm btn-warning mergeButton" data-id="${tagInstance.id}" data-tagname="${tagInstance.tag}"><span class="fa fa-code-fork" aria-label="Merge"></span></a>
								<a href="#" class="btn btn-sm btn-danger deleteButton" data-id="${tagInstance.id}" data-tagname="${tagInstance.tag}"><span class="fa fa-trash" aria-label="Delete"></span></a>
							</div>
						</td>
					</tr>
				</g:each>
			</tbody>
		</table>
	</div>

	<div class="row">
		<div class="col">
			<g:paginate class="justify-content-center d-flex flex-wrap" action="index" total="${tagInstanceCount ?: 0}" params="[filterPromoted: filterPromoted, filterCategory: filterCategory]"/>
		</div>
	</div>

		<div id="checkDeleteTag" tabindex="-1" class="modal fade" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-sm" role="document">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<h3 class="modal-title">Delete tag <strong id="deleteTagname"></strong>?</h3>
					</div>
					<div class="modal-body" align="">
						Do you really want to delete this tag?<br/>
						It will be removed from all stories!
					</div>
					<div class="modal-footer">
						<button id="deleteYes" type="button" class="btn btn-warning" data-dismiss="modal">Delete</button>
						<button id="deleteNo" type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
					</div>
				</div>
			</div>
		</div>

	<div id="checkDuplicateTag" tabindex="-1" class="modal fade" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-sm" role="document">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title">Duplicate tag <strong id="duplicateTagname"></strong>?</h3>
				</div>
				<div class="modal-body" align="">
					Do you really want to create a duplicate for this tag?<br/>
					All stories currently tagged will be tagged for the new tag as well. You can rename the duplicate after it has been created.
				</div>
				<div class="modal-footer">
					<button id="duplicateYes" type="button" class="btn btn-warning" data-dismiss="modal">Duplicate</button>
					<button id="duplicateNo" type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</div>
	</div>

	<div id="checkAliasTag" tabindex="-1" class="modal fade" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-sm" role="document">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title">Create alias for tag <strong id="aliasTagname"></strong>?</h3>
				</div>
				<div class="modal-body" align="">
					Do you really want to create an alias for this tag?<br/>
					All stories will be found by either the original or the alias tag.
				</div>
				<div class="modal-footer">
					<button id="aliasYes" type="button" class="btn btn-warning" data-dismiss="modal">Create Alias</button>
					<button id="aliasNo" type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</div>
	</div>

	<div id="mergeDialog" tabindex="-1" class="modal fade" role="dialog" aria-hidden="true">

			<div class="modal-dialog modal-lg" role="document">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<h3 class="modal-title">Merge tags or add aliases to <strong id="mergeTagName"></strong></h3>
					</div>
					<div class="modal-body" align="">
						<div class="form-group fieldcontain">
							<label for="mergeTagList">Tags to merge: </label>
							<input type="text" id="mergeTagList" name="mergeTagList" class="form-control" onkeypress="return event.keyCode !== 13;"/>
						</div>

						<div class="form-check">
							<label class="form-check-label">
								<input type="radio" class="form-check-input" name="MergeOrAlias" id="cbMerge" value="checked"/>
								Merge tags
							</label>
						</div>
						<div class="form-check">
							<label class="form-check-label">
								<input type="radio" class="form-check-input" name="MergeOrAlias" id="cbCreateAlias" value="checked"/>
								Create aliases
							</label>
						</div>
					</div>
					<div class="modal-footer">
						<button id="mergeDialogMerge" type="button" class="btn btn-warning" data-dismiss="modal">Process Tags</button>
						<!-- <button type="submit" id="mergeDialogMerge" class="btn btn-primary" type="button">Merge</button> -->
						<button id="mergeDialogCancel" type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
					</div>
				</div>
			</div>
		</div>

	</body>

	<script>

		<asset:script>
			(function ($, window, document, undefined)
			{
                'use strict';

                $(function ()
				{
                    $.fn.editable.defaults.mode = 'popup';

                    $(".tagNameInput").editable({
						mode: 'inline',
						url: function(params) {
						    var d = new $.Deferred();
                            var id = $(this).data('id');
                            var data = {'id': id, 'newName': params.value};
                            if(params.value.trim() === '')
                                return d.reject('Tag name must not be empty!');
							else
								$.ajax({
									url: "${createLink(action: 'updateNameAjax', controller: 'tag')}",
									type: 'GET',
									data: data,
									success: function(resp) {
										d.resolve();
									},
									error: function(resp) {
										d.reject("Error while saving!");
									},
									complete: function() {
									}
                            });

                            return d.promise();
                        }

					});

                    $(".promotedCheckbox").click(
						function() {

							var promoted = $(this).is(":checked");
							var id = $(this).data('id');
							var data = {'id': id, 'promoted': promoted};

							var checkbox = $(this);

							checkbox.attr("disabled", 1);

							$.ajax({
								url: "${createLink(action: 'changePromote', controller: 'tag')}",
								type: 'GET',
								data: data,
								success: function(response) {
									checkbox.removeAttr("disabled");
								},
								error: function() {
									// on error, restore to the old setting
									checkbox.prop('checked', !promoted);
								},
								complete: function() {
									checkbox.removeAttr("disabled");
								}
							});
						}
					);

                    $(".categoryCheckbox").click(
                        function() {

                            var isCategory = $(this).is(":checked");
                            var id = $(this).data('id');
                            var data = {'id': id, 'isCategory': isCategory};

                            var checkbox = $(this);

                            checkbox.attr("disabled", 1);

                            $.ajax({
                                url: "${createLink(action: 'changeIsCategory', controller: 'tag')}",
                                type: 'GET',
                                data: data,
                                success: function(response) {
                                    checkbox.removeAttr("disabled");
                                },
                                error: function() {
                                    // on error, restore to the old setting
                                    checkbox.prop('checked', !isCategory);
                                },
                                complete: function() {
                                    checkbox.removeAttr("disabled");
                                }
                            });
                        }
                    );

                    $(".deleteButton").click(
                        function() {

                            var id = $(this).data('id');
                            var tagname = $(this).data('tagname');
                            $('#deleteTagname').text(tagname);
                            $('#deleteYes').data("id",id);
                            $('#checkDeleteTag').modal({});
                        }
                    );

                    $('#deleteYes').on("click", function()
					{
                        var id = $(this).data('id');
                        var data = {'id': id};
                        $("#divLoading").addClass('show');

                        $.ajax({
                            url: "${createLink(action: 'deleteAjax', controller: 'tag')}",
                            type: 'GET',
                            data: data,
                            success: function(response) {
                                if (response === 'success') {
                                    $("#divLoading").removeClass('show');

                                    var rowId = "#row-" + id;
$(rowId).hide('slow', function(){ $(rowId).remove(); });
                                }
                            },
                            failure: function(response) {
                                location.reload(true);
                            }
                        });
					});

                    $(".duplicateButton").click(
                        function() {

                            var id = $(this).data('id');
                            var tagname = $(this).data('tagname');
                            $('#duplicateTagname').text(tagname);
                            $('#duplicateYes').data("id",id);
                            $('#checkDuplicateTag').modal({});
                        }
                    );

                    $('#duplicateYes').on("click", function()
                    {
                        $('#checkDuplicateTag').hide({});
                        $("#divLoading").addClass('show');

                        var id = $(this).data('id');
                        var data = {'id': id};

                        $.ajax({
                            url: "${createLink(action: 'createDuplicateAjax', controller: 'tag')}",
                            type: 'GET',
                            data: data,
                            complete: function(response) {
                                location.reload(true);
                            }
                        });
                    });

                    $(".aliasButton").click(
                        function() {

                            var id = $(this).data('id');
                            var tagname = $(this).data('tagname');
                            $('#aliasTagname').text(tagname);
                            $('#aliasYes').data("id",id);
                            $('#checkAliasTag').modal({});
                        }
                    );

                    $('#aliasYes').on("click", function()
                    {
                        $('#checkAliasTag').hide({});
                        $("#divLoading").addClass('show');

                        var id = $(this).data('id');
                        var data = {'id': id};

                        $.ajax({
                            url: "${createLink(action: 'createAliasAjax', controller: 'tag')}",
                            type: 'GET',
                            data: data,
                            complete: function(response) {
								location.reload(true);
                            }
                        });
                    });

                    <g:render template="/templates/tagScripts" model="['allowNew': false, 'selector': '#mergeTagList']"/>

                    $(".mergeButton").click(
                        function() {
                            var id = $(this).data('id');
                            var tagname = $(this).data('tagname');
                            $('#mergeTagName').text(tagname);
                            $('#mergeDialogMerge').data('id',id);
                            $('#cbMerge').checked = false;
                            $('#cbCreateAlias').checked = false;
                            $('#mergeDialog').modal({});
                        }
                    );

                    $('#mergeDialogMerge').on("click", function()
                    {
                        $('#mergeDialog').hide({});
                        $("#divLoading").addClass('show');

                        var id = $(this).data('id');
                        var elems = $('#mergeTagList').val();
                        var merge = $('#cbMerge:checked').val() === "checked";
                        var alias = $('#cbCreateAlias:checked').val() === 'checked';

                        if(! alias && ! merge) {
                            $("#divLoading").removeClass('show');
                            return;
                        }

                        var data;
                        var url;
                        if(alias) {
                            url = "${createLink(action: 'addAliasTagsAjax', controller: 'tag')}";
                            data = {'id': id, 'aliasTags': elems};
                        }
                        else {
                            url = "${createLink(action: 'mergeTagsAjax', controller: 'tag')}";
							data = {'id': id, 'mergeTags': elems};
                        }

                        $.ajax({
                            url: url,
                            type: 'GET',
                            data: data,
                            complete: function(response) {
                                location.reload(true);
                            }
                        });
                    });
                });
			})(jQuery, window, document);

		</asset:script>
	</script>
</html>
</g:applyLayout>