<g:applyLayout name="newMain" model="[active:'stories']" >
    <html>
    <head>
        <title><g:pageTitle title='Login'/></title>

        <asset:stylesheet href="auth.css"/>
    </head>
    <body>

    <div class="container">

        <h2 class="form-signin-heading text-center">Sign in</h2>

        <form action="${postUrl ?: '/login/authenticate'}" method="POST" id="loginForm" class="form-signin cssform">
            <label for="username" class="sr-only">Email</label>
            <input type="text" class="text_ form-control" data-toggle="tooltip" title="The email address you used to create your account!" placeholder="you@you.com" name="username" id="username" required autofocus/>

            <label for="password" class="sr-only">Password</label>
            <input type="password" class="text_ form-control" name="password" id="password" placeholder="Password" required data-toggle="tooltip" title="Your password" data-placement="bottom"/>
            <div class="checkbox custom-control custom-checkbox">
                <g:checkBox class="chk custom-control-input" name="remember-me" id="remember-me" value="true"/>
                <label for="remember-me" class="custom-control-label">
                    Remember me
                </label>
            </div>

            <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>

            <div class="center-block text-center mt-3">
                <g:link rel="noFollow" controller="User" action="forgotPassword" class="">I forgot my password</g:link>
            </div>
        </form>

        <form class="form-signin cssform mt-3">
            <g:link rel="noFollow" controller="User" action="create" class="btn btn-lg btn-primary btn-block">Create new account</g:link>
        </form>

    </div> <!-- /container -->
    </body>
</g:applyLayout>