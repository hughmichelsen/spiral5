<g:if test="${categoryFilterList}">
    <div class="dropdown">
        <button id="catSelDropdown" class="btn ${categoryFilterList?.highlight ? 'btn-warning' : 'btn-primary' } btn-sm dropdown-toggle" data-toggle="dropdown" type="button" aria-haspopup="true" aria-expanded="false">
            ${raw(categoryFilterList?.label)}
        </button>
        <div class="dropdown-menu" aria-labelledby="catSelDropdown">
            <h2 class="dropdown-header">Select Categories</h2>
            <form if="applyFilterCat_${instance}" class="applyFilterCat px-1 py-1">
                <div class="form-check">
                    <div class="custom-control custom-checkbox">
                        <g:checkBox class="custom-control-input cat_All" id="cat_All_${instance}" name="cat_All"/>
                        <label for="cat_All_${instance}" class="custom-control-label" >Show All</label>
                    </div>
                </div>
                <div class="dropdown-divider"> </div>
                <g:each var="filterCat" in="${categoryFilterList?.list}">
                    <div class="form-check">
                        <div class="custom-control custom-checkbox pr-3">
                            <nobr>
                                <g:checkBox class="custom-control-input filterCatCheckbox" id="cat_${filterCat.name}_${instance}" name="${filterCat.name}" checked="${filterCat.selected}"/>
                                <label for="cat_${filterCat.name}_${instance}" class="custom-control-label" >${filterCat.name}</label>
                                <g:if test="${filterCat.aliases}">
                                    <i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="${filterCat.aliases}"></i>
                                </g:if>
                            </nobr>
                        </div>
                    </div>
                </g:each>

                <div class="dropdown-divider"> </div>
                <div class="d-flex justify-content-center">
                    <button type="submit" id="applyFilterCatBtn_${instance}" class="applyFilterCatBtn btn btn-primary">Apply</button>
                </div>
            </form>
        </div>
    </div>

    <asset:javascript src="categorySelector.js"/>
</g:if>