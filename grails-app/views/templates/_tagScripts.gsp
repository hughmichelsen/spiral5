    // Tags Tokenfield
    var tags;

    function findTag(tagName)
    {
        for(var i = 0; i < tags.length; i++)
        {
            if(tags[i].tag === tagName)
            {
                return tags[i];
            }
        }
        return null;   // Not found: New tags not allowed!
    }

    function setTagClass(e)
    {
        var tag = findTag(e.attrs.value);
        if(tag == null)
            $(e.relatedTarget).addClass('tag-created');
        else if(tag.isCategory)
            $(e.relatedTarget).addClass('tag-category');
        else if(tag.promoted)
            $(e.relatedTarget).addClass('tag-promoted');
    }

    function setTagCheckbox(e, check)
    {
        var tagName = e.attrs.value;

        $('#cb_' + tagName).prop("checked", check);
    }

    <g:if test="${newTagSelector}">
        $('${newTagSelector}').hide(0);

        function testTagWarning(){
            var tokens = $('${selector}').tokenfield('getTokens')

            for(var i = 0; i < tokens.length; i++)
            {
                if(findTag(tokens[i].value) == null)
                {
                    $('${newTagSelector}').fadeIn(800);
                    return;
                }
            }

            $('${newTagSelector}').fadeOut(800);
        }
    </g:if>

    $.ajax({
        url: "<g:createLink controller='tag' action='allTagsJSon' params="[excludeStory:thisStory]"/>",
        type: 'GET',
        async: true,
        success: function (resp) {
            tags = resp;
            for(var i=0; i < tags.length; i++)
            {
                tags[i].value=tags[i].tag;
                tags[i].label=tags[i].tag;
            }
            var engine;
            engine = new Bloodhound({
                local: tags,
                identify: function(obj) { return obj.tag; },
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                datumTokenizer: Bloodhound.tokenizers.obj.whitespace('tag'),
            });
            var tagWidget = $('${selector}');
            var tagVal = tagWidget.val();
            tagWidget.val('');
            $('${selector}').tokenfield({
                minLength: 3,
                createTokensOnBlur: true,
                typeahead: [{minLength:2, highlight:true}, { source: engine.ttAdapter(), displayKey:"tag"}]
            })
            .on('tokenfield:createtoken', function (e) {
                if(e.attrs.value)
                {
                    e.attrs.value = e.attrs.value.toLowerCase();
                    e.attrs.label = e.attrs.value.toLowerCase();
                }
                <g:if test="${allowNew}">
                    return true;
                </g:if>
                <g:else>
                    return findTag(e.attrs.value) != null; // Accept existing tags only
                </g:else>
            })
            .on('tokenfield:createdtoken', function (e) {
                setTagClass(e);
                <g:if test="${newTagSelector}">
                    testTagWarning();
                </g:if>
                setTagCheckbox(e,true);
            })
            .on('tokenfield:edittoken', function (e) {
                if(e.attrs.value)
                {
                    e.attrs.value = e.attrs.value.toLowerCase();
                    e.attrs.label = e.attrs.value.toLowerCase();
                }
                <g:if test="${allowNew}">
                    return true;
                </g:if>
                <g:else>
                    return findTag(e.attrs.value) != null; // Accept existing tags only
                </g:else>
            })
            .on('tokenfield:editedtoken', function (e) {
                setTagCheckbox(e,false);
            })
            .on('tokenfield:removedtoken', function (e) {
                <g:if test="${newTagSelector}">
                    testTagWarning();
                </g:if>
                setTagCheckbox(e,false);
            });
            tagWidget.tokenfield('setTokens', tagVal); // Add tags from scratch, so they are channeled through eventhandlers and colored correctly
        },
        error: function (resp) {
        },
        complete: function () {
        }
    });

    <g:if test="${dlgTagListName}">
        var existingTags;
        var loadedTagTabs = [];

        $('#tagTab a').click(function (e) {
            e.preventDefault();
            $(this).tab('show');
            fillTagTab($(this));
        });

        function doFillTagTab(tags, select, columns) {
            var cont = select;
            var elem = ""
            for(var i = 0; i < tags.length; i++)
            {
                var tag = tags[i];
                if(i % columns === 0)
                    elem += "<tr>";

                var cls = "";
                if(tag.isCategory)
                    cls="tag-category-tab";
                else if(tag.promoted)
                    cls="tag-promoted-tab";

                elem += "<td><input type='checkbox' class='tag_checkbox' data-name='" + tag.tag + "' id='cb_" + tag.tag + "'";

                var exists = false;
                for(var j = 0; j < existingTags.length; j++)
                {
                    if(existingTags[j].value === tag.tag)
                    {
                        exists = true;
                        break;
                    }
                }

                if(exists)
                    elem += "checked> ";
                else
                    elem += "> ";

                if(tag.aliases.length > 0)
                    elem += "<span class='" + cls + "' data-toggle='tooltip' title='Aliases: " + tag.aliases + "'>" + tag.tag + " <i class='fa fa-tags'> </i></span>";
                else
                    elem += "<span class='" + cls + "'>" + tag.tag + "</span>";

                elem += "</td>";

                if(i % 2 === (columns-1) || i === tags.length)
                    elem += "</tr>";

            }
            cont.append(elem);

            cont.find('[data-toggle="tooltip"]').tooltip();

            cont.find('.tag_checkbox').change(function(e) {
                var tagName = $(e.target).data("name");
                var checked = $(e.target).prop("checked");

                var tagWidget = $('${selector}');
                var oldTags = tagWidget.val().split(",");
                var newTags = [];
                var exists = false;
                for(var i = 0; i < oldTags.length; i++)
                {
                    if(oldTags[i].trim() !== tagName)
                        newTags.push(oldTags[i]);
                }

                if(checked)
                    newTags.push(tagName);

                tagWidget.tokenfield('setTokens', newTags);
            });

        }

        function fillTagTab(tab) {
            var href = tab.attr('href');

            existingTags = $('${selector}').tokenfield('getTokens');

            if(loadedTagTabs.includes(href))
                return;

            var select = $(href).find(".tagTab");

            var result;

            var data;
            if(href==="#main")
                data = {
                    promoted: true,
                    start: '',
                    end: ''
                };
            else
                data = {
                    promoted: false,
                    start: tab.data("start"),
                    end: tab.data("end")
                };

            var dlgWidth = $("${dlgTagListName}").width();
            var columns;
            if(dlgWidth > 850)
                columns = 5
            else
                columns = Math.trunc((dlgWidth-30) / 200)
            if(columns < 2) columns = 2;

            $("#divLoading").addClass('show');

            $.ajax({
                url: "<g:createLink controller='tag' action='fullSpecTagsJSon'/>",
                type: 'GET',
                data: data,
                async: true,
                success: function (resp) {
                    result = resp;
                    $("#divLoading").removeClass('show');
                    doFillTagTab(resp, select, columns);
                    loadedTagTabs.push(href);
                },
                error: function (resp) {
                },
                complete: function () {
                }
            });
        };

        $("${btnShowTagList}").click(function(e) {
           $("${dlgTagListName}").modal({});
           fillTagTab($('#startTagTabLink'));
        });
    </g:if>

