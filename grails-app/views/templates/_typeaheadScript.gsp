    $.ajax({
        url: "${sourceURL}",
        type: 'GET',
        async: true,
        success: function (resp) {
            var engine = new Bloodhound({
                local: resp,
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                datumTokenizer: Bloodhound.tokenizers.whitespace
            });
            $('${selector}').typeahead({
                minLength:3,
                highlight:true},
            {
                source: engine.ttAdapter()
            })
        },
        error: function (resp) {
        },
        complete: function () {
        }
    });
