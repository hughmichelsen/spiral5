<div id="${dlgTagListName}" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Select Tags</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <ul class="nav nav-pills" role="tablist" id="tagTab">
                    <li role="presentation" class="nav-item"><a class="nav-link active" href="#main" id="startTagTabLink" aria-controls="main" role="tab">Main Tags</a></li>
                    <li role="presentation" class="nav-item"><a class="nav-link" href="#a-c" aria-controls="a-c" role="tab" data-start="a" data-end="d">A - C</a></li>
                    <li role="presentation" class="nav-item"><a class="nav-link" href="#d-f" aria-controls="d-f" role="tab" data-start="d" data-end="g">D - F</a></li>
                    <li role="presentation" class="nav-item"><a class="nav-link" href="#g-i" aria-controls="g-i" role="tab" data-start="g" data-end="j">G - I</a></li>
                    <li role="presentation" class="nav-item"><a class="nav-link" href="#j-l" aria-controls="j-l" role="tab" data-start="j" data-end="m">J - L</a></li>
                    <li role="presentation" class="nav-item"><a class="nav-link" href="#m-o" aria-controls="m-o" role="tab" data-start="m" data-end="p">M - O</a></li>
                    <li role="presentation" class="nav-item"><a class="nav-link" href="#p-r" aria-controls="p-r" role="tab" data-start="p" data-end="s">P - R</a></li>
                    <li role="presentation" class="nav-item"><a class="nav-link" href="#s-u" aria-controls="s-u" role="tab" data-start="s" data-end="v">S - U</a></li>
                    <li role="presentation" class="nav-item"><a class="nav-link" href="#v-z" aria-controls="v-z" role="tab" data-start="v" data-end="">V - Z</a></li>
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade show active" id="main">
                        <table class="table small table-striped tagTab">
                        </table>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="a-c">
                        <table class="table small table-striped tagTab">
                        </table>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="d-f">
                        <table class="table small table-striped tagTab">
                        </table>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="g-i">
                        <table class="table small table-striped tagTab">
                        </table>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="j-l">
                        <table class="table small table-striped tagTab">
                        </table>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="m-o">
                        <table class="table small table-striped tagTab">
                        </table>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="p-r">
                        <table class="table small table-striped tagTab">
                        </table>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="s-u">
                        <table class="table small table-striped tagTab">
                        </table>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="v-z">
                        <table class="table small table-striped tagTab">
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
