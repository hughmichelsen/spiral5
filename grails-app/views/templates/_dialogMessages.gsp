<div id="errorMessage" tabindex="-1" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h3 class="modal-title text-light">Error</h3>
            </div>
            <div class="modal-body" align="">
                <span id="errorMessageText"></span>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
            </div>
        </div>
    </div>
</div>

<div id="infoMessage" tabindex="-1" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm bg-success" role="document">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header bg-success">
                <h3 class="modal-title text-light">Info</h3>
            </div>
            <div class="modal-body" align="">
                <span id="infoMessageText"></span>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
            </div>
        </div>
    </div>
</div>

<div id="adminMessageDlg" tabindex="-1" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lm" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h3 id="adminMessageHeadline" class="modal-title text-light"></h3>
            </div>
            <div class="modal-body" align="">
                <span id="adminMessageText"></span>
                <div class="form-group">
                    <textarea rows="2" class="form-control adminComment" id="adminMessageComment" name="adminMessageComment"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button id="adminMessageOk" type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
                <button id="adminMessageCancel" type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>


<script>
    <asset:script>
        function showInfoMessage(message) {
            $('#infoMessageText').html(message);
            $('#infoMessage').modal({});
        }

        function showErrorMessage(message) {
            $('#errorMessageText').html(message);
            $('#errorMessage').modal({});
        }
    </asset:script>
</script>

