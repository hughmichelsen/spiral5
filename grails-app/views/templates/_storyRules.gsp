<div id="rules" class="card content border-danger" role="main">
    <div class="card-header bg-danger text-dark">The Rules</div>
    <div class="card-body">
        <ul>
            <li>The story <em>should</em> include some kind of mind control of men.</li>
            <li>No sex or any kind of manipulation with children or young teenagers. Any teenager must at least be in the <a href="http://www.healthofchildren.com/P/Puberty.html" target="_blank">late stages of puberty</a>!</li>
            <li>No real-life persons, neither living or deceased!</li>
            <li>No hard violence, blood and gore! No mutilations, no dismemberment!</li>
            <li>The story must be yours! Do not repost someone else's story here. Email the original author and ask them to post it instead.</li>
            <li>The story must be at least 1000 words long.</li>
            <li>You must be over 18 to post a story.</li>
        </ul>
        <p class="content specialwarning">If you're uncertain if your story meets the above criteria, email us first: <a href="mailto:admin@gayspiralstories.com">admin@gayspiralstories.com</a>.</p>
    </div>
</div>