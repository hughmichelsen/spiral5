<%@ page import="grails.plugin.springsecurity.SpringSecurityUtils; com.ncmc4.Rating" %>
<g:each var="rating" in="${Rating.ratingList}">
    <g:if test="${topSeriesMap[rating]?.size() > 0}">
        <div class="row">
            <div class="col alert alert-primary d-flex justify-content-between align-items-center">
                <img class="topRatingIcon" src="${assetPath(src: Rating.getRatingIconMap()[rating])}"/>
                <div class="mx-auto">
                    <h4 class="text-center">${Rating.getRatingLabelMap()[rating]}</h4>
                    <div class="text-center">${topSeriesMap[rating]?.size() > 1 ? "Some" : "One"} of the ${Rating.getRatingAdjectiveMap()[rating]} stories in the ${raw(topPeriodName)}</div>
                </div>
            </div>
        </div>
        <div class="row mb-3">
            <table class="table table-bordered table-striped topStoryList my-responsive-table" data-rtContainerBreakPoint="539">
                <thead class="thead-dark">
                <tr>
                    <th>Author</th>
                    <th>Name</th>
                    <th>Published</th>
                </tr>
                </thead>
                <tbody>
                <g:each in="${topSeriesMap[rating]}" status="i" var="seriesInstance">
                    <g:render template="/newSeries/seriesTableEntry" model="[seriesInstance: seriesInstance]"/>
                </g:each>
                </tbody>
            </table>
        </div>
    </g:if>
</g:each>
