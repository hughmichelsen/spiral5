<%@ page import="com.ncmc4.Rating" %>
<tr class="${seriesInstance?.sponsored ? 'sponsored' : ''}">

    <td>
        <g:link action="show" id="${seriesInstance?.author?.id}" controller="newAuthor">${fieldValue(bean: seriesInstance, field: "author")}</g:link>
        <g:userIcon userid="${seriesInstance?.mostRecentStory?.owners[0]?.id}"/>
    </td>

    <td>
        <g:if test="${seriesInstance?.sponsored}">
            <small class="sponsored">Warning: The following is an incomplete, sponsored teaser / advertisement of a story, not the whole thing.</small><br/>
        </g:if>

        <div class="d-flex justify-content-between align-items-center">
            <div class="">
                <i class="fa fa-star text-success hidden newStoryStar" data-id="${seriesInstance?.mostRecentStoryId}" data-toggle="tooltip" title="New since your last visit!" aria-hidden="true"></i>
                <g:if test="${seriesInstance?.storyCount > 1}">
                    <g:link action="show" id="${seriesInstance?.id}" controller="newSeries" class="storyLink">${seriesInstance?.name}</g:link>
                    <g:link class="badge badge-primary" action="show" id="${seriesInstance?.id}" controller="newSeries" data-toggle="tooltip" title="Multipart series">${seriesInstance?.storyCount} <i class="fa fa-book"></i></g:link>
                    <g:link class="badge badge-primary" action="show" id="${seriesInstance?.mostRecentStoryId}" controller="newStory" data-toggle="tooltip" title="Open latest part"> <i class="fa fa-fast-forward" aria-hidden="true"></i> </g:link>
                </g:if>
                <g:else>
                    <g:link action="show" id="${seriesInstance?.mostRecentStoryId}" controller="newStory" class="storyLink">${seriesInstance.name}</g:link>
                </g:else>
            </div>
            <div id="comments-${seriesInstance?.id}" class="pull-right" data-toggle="tooltip" title="Comments"> </div>
        </div>
        <div class="d-flex justify-content-between align-items-center">
            <div class="">
                <g:if test="${seriesInstance?.summary}">
                    ${seriesInstance?.summary}
                </g:if>

                <div>
                    <g:if test="${seriesInstance.category == null}">
                        <span class="badge badge-secondary">Category missing</span>
                    </g:if>
                    <g:if test="${seriesInstance.tags.size() < 3}">
                        <span class="badge badge-secondary">Insufficient Tags</span>
                    </g:if>
                </div>

                <div class="div-tag-category-small">
                    <g:if test="${seriesInstance?.category}">
                        <g:link action="search" class="div-small-category" controller="newStory" params="[srchCategory: seriesInstance?.category?.id, tagList: [], tagSearchMode: 'or', useAlias: 'true', searchTerm: '']">${seriesInstance?.category?.toString().capitalize()}</g:link> -
                    </g:if>

                    <g:if test="${seriesInstance?.tags}">
                        <span class="div-small-tags">
                            <g:each in="${seriesInstance?.tags}" var="tag">
                                <g:link action="search" controller="newStory" params="[srchCategory: null, tagList: tag.tag, tagSearchMode: 'or', useAlias: 'true', searchTerm: '']">#${tag.tag}</g:link>
                            </g:each>
                        </span>
                    </g:if>
                </div>
            </div>
            <div class="d-flex flex-wrap flex-lg-nowrap align-items-center justify-content-center">
                <g:each in="${seriesInstance?.getRatingMarker()}" status="r" var="rating">
                    <img class="storyListRatingIcon-${r} storyListRating${rating.effect}-${r}" src="${assetPath(src: Rating.getRatingIconMap()[rating?.ratName])}" data-toggle="tooltip" title="${rating.toolTip}"/>
                </g:each>
            </div>
        </div>
    </td>

    <td><g:formatDate date="${seriesInstance?.lastChapterUpdated}" /></td>

</tr>
