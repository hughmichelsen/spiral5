<g:applyLayout name="storyList" model="[storyInstanceList: storyInstanceList, storyInstanceCount: storyInstanceCount, title: title, showLetterLinks: false]">

        <content tag="title">
            <h1 class="center-block text-center" style="color:#337ab7">
                <span id="title-span">
                    %{--<i class="fa fa-book"></i>--}%
                    ${title}
                </span>
                <sec:ifAllGranted roles="ROLE_SUPER_ADMIN">
                    <div class="dropdown btn-sm ml-auto">
                        <button type="button" class="btn btn-sm btn-warning dropdown-toggle" data-toggle="dropdown"><i class="fa fa-wrench" aria-hidden="true"></i></button>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a href="#" class="dropdown-item updateTitleButton">Rename Series</a>
                            <a href="#" class="dropdown-item resetSeriesLinks">Reset Next & Prev Links</a>
                        </div>
                    </div>
                </sec:ifAllGranted>
            </h1>
        </content>

    <content tag="paginate">
        <g:if test="${storyInstanceCount > params.max}">
            <g:paginate class="justify-content-center d-flex flex-wrap" controller="newSeries" action="show" id="${seriesInstance.id}" total="${storyInstanceCount ?: 0}" />
        </g:if>
        <g:else>
            <div class="spacer20"></div>
        </g:else>
    </content>

    <content tag="dialogs">
        <div id="renameSeriesModal" class="modal fade" role="dialog">
            <div class="modal-dialog modal-sm">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header bg-primary">
                        <h3 class="modal-title text-light">Rename Series</h3>
                    </div>
                    <div class="modal-body">
                        <h6>What would you like to rename the series to?</h6>
                        <div class="form-group">
                            <input type="text" id="nameSeriesText" class="form-control" value="${seriesInstance.name}" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button id="renameSeries" type="button" class="btn btn-default" data-dismiss="modal">Change Name</button>
                        <button id="renameSeriesCancel" type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </content>

    <script>
        <asset:script>
            (function ($, window, document, undefined)
            {
                'use strict';

                $(function ()
                {
                    $(".updateTitleButton").click(
                        function() {
                            $('#renameSeriesModal').modal({});

                            $('#renameSeriesCancel').on("click", function() {
                                $("#nameSeriesText").val("");
                            });

                            $('#renameSeries').on("click", function(){
                                var id = ${seriesInstance.id};

                                var name = $("#nameSeriesText").val();
                                var data = {'id': id, name: name};

                                $(".updateTitleButton").attr("disabled", 1);

                                $.ajax({
                                    url: "${createLink(action: 'updateSeriesName', controller: 'newSeries')}",
                                    type: 'GET',
                                    data: data,
                                    success: function(response) {
                                        $("#title-span").text(name);
                                    },
                                    error: function(response) {
                                    },
                                    complete: function() {
                                        $(".updateTitleButton").removeAttr("disabled");
                                    }
                                });
                            });
                        }
                    );

                    $(".resetSeriesLinks").click(
                        function() {

                            var data = {'id': ${seriesInstance.id}};

                            var button = $(this);

                            button.attr("disabled", 1);

                            $.ajax({
                                url: "${createLink(action: 'forceUpdate', controller: 'newSeries')}",
                                type: 'GET',
                                data: data,
                                success: function(response) {
                                },
                                error: function(response) {
                                    button.val("Update failed!");
                                },
                                complete: function() {
                                    button.removeAttr("disabled");
                                }
                            });
                        }
                    );
                });
            })(jQuery, window, document);
        </asset:script>
    </script>
</g:applyLayout>