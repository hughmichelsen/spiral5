<fieldset class="form">
    <div id="username-div" class="form-group fieldcontain ${hasErrors(bean: userInstance, field: 'username', 'has-error')} required">
        <label for="username">
            EMail address
            <span class="required-indicator">*</span>
        </label>
        <g:textField class="form-control" name="username" required="" value="${userInstance?.username}"/>
    </div>

    <g:if test='${userInstance != null}'>
        <div id="password-div" class="form-group fieldcontain ${hasErrors(bean: userInstance, field: 'newPassword', 'has-error')}">
            <label for="newPassword">
                Password
            </label>
            <g:textField class="form-control" name="newPassword" value="" />
        </div>
    </g:if>
    <g:else>
        <div id="password-div" class="form-group fieldcontain ${hasErrors(bean: userInstance, field: 'password', 'has-error')} required">
            <label for="password">
                Password
                <span class="required-indicator">*</span>
            </label>
            <g:textField class="form-control" name="password" required="" value="" />
        </div>
    </g:else>
<%--
    <div id="email-div" class="form-group fieldcontain ${hasErrors(bean: userInstance, field: 'emailAddress', 'has-error')} required">
        <label for="emailAddress">
            Email Address
            <span class="required-indicator">*</span>
        </label>
        <g:textField class="form-control" name="emailAddress" required="" value="${userInstance?.emailAddress}" />
    </div> --%>

    <div id="authorname-div" class="form-group fieldcontain ${hasErrors(bean: userInstance, field: 'authorName', 'has-error')}">
        <label for="authorName">
            Author Name
        </label>
        <g:textField class="form-control" name="authorName" value="${userInstance?.authorName}"/>
    </div>

    <div id="emailApproved-div" class="custom-control custom-checkbox fieldcontain ${hasErrors(bean: userInstance, field: 'emailApproved', 'has-error')}">
        <g:checkBox class="custom-control-input" id="emailApproved" name="emailApproved" value="${userInstance?.emailApproved}"/>
        <label class="custom-control-label" for="emailApproved">
            Email Approved Stories
        </label>
    </div>

    <div id="emailUnapproved-div" class="custom-control custom-checkbox fieldcontain ${hasErrors(bean: userInstance, field: 'emailUnapproved', 'has-error')}">
        <g:checkBox class="custom-control-input" id="emailUnapproved" name="emailUnapproved" value="${userInstance?.emailUnapproved}"/>
        <label class="custom-control-label" for="emailUnapproved">
            Email Unapproved Stories
        </label>
    </div>

    <div id="emailComment-div" class="custom-control custom-checkbox fieldcontain ${hasErrors(bean: userInstance, field: 'emailComment', 'has-error')}">
        <g:checkBox class="custom-control-input" id="emailComment" name="emailComment" value="${userInstance?.emailComment}"/>
        <label class="custom-control-label" for="emailComment">
            Email on new Comments
        </label>
    </div>

    <div id="showEmail-div" class="custom-control custom-checkbox fieldcontain ${hasErrors(bean: userInstance, field: 'showEmail', 'has-error')}">
        <g:checkBox class="custom-control-input" id="showEmail" name="showEmail" value="${userInstance?.showEmail}"/>
        <label class="custom-control-label" for="showEmail">
            Show EMail
        </label>
    </div>

    <div id="anonForum-div" class="custom-control custom-checkbox fieldcontain ${hasErrors(bean: userInstance, field: 'anonForum', 'has-error')}">
        <g:checkBox class="custom-control-input" id="anonForum" name="anonForum" value="${userInstance?.anonForum}"/>
        <label class="custom-control-label" for="anonForum">
            Use forum anonymously
        </label>
    </div>

    <div id="hidePatreon-div" class="custom-control custom-checkbox fieldcontain ${hasErrors(bean: userInstance, field: 'hidePatreon', 'has-error')}">
        <g:checkBox class="custom-control-input" id="hidePatreon" name="hidePatreon" value="${userInstance?.hidePatreon}"/>
        <label class="custom-control-label" for="hidePatreon">
            Hide Patreon
        </label>
    </div>

    <g:if test='${userInstance != null}'>
        <div id="createdOn-div" class="form-group fieldcontain ${hasErrors(bean: userInstance, field: 'createdOn', 'has-error')}">
            <label for="createdOn">
                Created On:
            </label>
            <g:formatDate id="createdOn" name="createdOn" style="LONG" date="${userInstance?.createdOn}"/>
        </div>

        <div id="lastSignedIn-div" class="form-group fieldcontain ${hasErrors(bean: userInstance, field: 'lastSignedIn', 'has-error')}">
            <label for="lastSignedIn">
                Last Sign In:
            </label>
            <g:formatDate id="lastSignedIn" name="lastSignedIn" style="LONG" date="${userInstance?.lastSignedIn}"/>
        </div>
    </g:if>
</fieldset>
<fieldset class="buttons">
    <g:submitButton name="create" class="save btn btn-lg btn-primary" value="${submitButtonText}" />
</fieldset>