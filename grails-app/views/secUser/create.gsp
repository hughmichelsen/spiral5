<g:applyLayout name="newMain" model="[active:'user']">
    <html>
    <head>
        <title><g:pageTitle title='Create User'/></title>

        <asset:stylesheet href="secuser.css"/>
        <meta name="robots" content="noindex">
    </head>
    <body>

    <div class="content container" role="main">
        <h1>Create New User</h1>

        <g:render template="showErrors" model="[userInstance: userInstance]" />

        <g:form url="[action:'save', controller:'secUser']" id="create-form" name="create-form" role="form" useToken="true" >
            <g:render template="createSecUserFields" model="[userInstance: userInstance, submitButtonText: 'Create']" />
        </g:form>
    </div>

    </body>
    </html>

    %{--<asset:script type="text/javascript">--}%
        %{--(function ($, window, document, undefined)--}%
        %{--{--}%
            %{--'use strict';--}%

            %{--$(function ()--}%
            %{--{--}%
            %{--});--}%
        %{--})(jQuery, window, document);--}%
    %{--</asset:script>--}%
</g:applyLayout>