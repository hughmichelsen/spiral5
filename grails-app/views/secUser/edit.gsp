<g:applyLayout name="newMain" model="[active:'user']">
    <html>
    <head>
        <title><g:pageTitle title='${userInstance?.username}'/></title>

        <asset:stylesheet href="secuser.css"/>
        <meta name="robots" content="noindex">
    </head>
    <body>

    <div class="content container" role="main">
        <h1>Edit ${userInstance.username}</h1>

        <g:render template="showErrors" model="[userInstance: userInstance]" />

        <g:form url="[action:'update', controller:'secUser']" id="create-form" name="create-form" role="form" useToken="true" >
            <g:render template="createSecUserFields" model="[userInstance: userInstance, submitButtonText: 'Update']" />
            <input type="hidden" name="id" value="${userInstance?.id}" />
        </g:form>
    </div>

    </body>
    </html>
</g:applyLayout>