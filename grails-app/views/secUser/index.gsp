<g:applyLayout name="newMain" model="[active:'admin']">
    <html>
    <head>
        <title><g:pageTitle title='User Admin'/></title>

        <asset:stylesheet href="secuser.css"/>
        <meta name="robots" content="noindex">
    </head>
    <body>

    <%
        //sort=name&max=30&order=asc
        if (!params.sort)
            params.sort = "id"
        if (!params.order)
            params.order = "desc"
    %>

    <div class="row">
        <div class="col mb-3">
            <g:form url="[action:'searchUser']" method="GET">
                <div class="input-group">
                    <input type="text" class="form-control topic" placeholder="Topic..." name="topic">
                    <input type="text" class="form-control" placeholder="Search for..." name="searchTerm"/>
                    <span class="input-group-append">
                        <button type="submit" class="btn btn-default" type="button"><i class="fa fa-search" aria-hidden="true"></i></button>
                        <g:link class="btn btn-primary" action="create" ><i class="fa fa-plus" aria-hidden="true"></i> Add New</g:link>
                    </span>
                </div>
            </g:form>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <g:paginate class="justify-content-center d-flex flex-wrap" total="${userInstanceCount ?: 0}" params="${params}"/>
        </div>
    </div>

    <div class="table-responsive-lg">
        <table id="userList" class="table table-sm table-bordered table-striped storyList">
            <thead class="thead-light">
                <tr>
                    <g:sortableColumn property="id" title="ID"/>
                    <g:sortableColumn property="createdOn" title="Created On" />
                    <g:sortableColumn property="lastSignedIn" title="Last Login"/>
                    <g:sortableColumn property="username" title="User name"/>
                    <g:sortableColumn property="authorName" title="Author name"/>

                    <th data-title="Stats"><span class="text-primary">Stats</span></th>

                    <th title="Author" data-title="Author" class="text-center ${params.filterAuthor == 'true' ? 'bg-info' : ''}">
                        <g:link action="index" params="${params + [offset: 0, filterAuthor: params.filterAuthor != 'true']}">
                            <span class="text-primary">
                                <i class="fa fa-pencil fa-2x"></i>
                            </span>
                        </g:link>
                    </th>

                    <th title="Use Forum anonymously" data-title="Anonymous Forum" class="text-center ${params.filterAnon == 'true' ? 'bg-success' : params.filterAnon == 'false' ? 'bg-danger' : ''}">
                        <g:link action="index" params="${params + [offset: 0, filterAnon: (params.filterAnon == "true" ? "false" : params.filterAnon == "false" ? null : "true")]}">
                            <span class="text-primary">
                                <i class="fa fa-user-secret fa-2x"></i>
                            </span>
                        </g:link>
                    </th>

                    <th title="Notify on new comments" data-title="Email Comments" class="text-center ${params.filterMailComment == 'true' ? 'bg-success' : params.filterMailComment == 'false' ? 'bg-danger' : ''}">
                        <g:link action="index" params="${params + [offset: 0, filterMailComment: (params.filterMailComment == "true" ? "false" : params.filterMailComment == "false" ? null : "true")]}">
                            <span class="text-primary">
                                <i class="fa fa-comments-o fa-2x"></i>
                            </span>
                        </g:link>
                    </th>

                    <th title="Notify on approved stories" data-title="Email Approved" class="text-center ${params.filterMailApproved == 'true' ? 'bg-success' : params.filterMailApproved == 'false' ? 'bg-danger' : ''}">
                        <g:link action="index" params="${params + [offset: 0, filterMailApproved: (params.filterMailApproved == "true" ? "false" : params.filterMailApproved == "false" ? null : "true")]}">
                            <span class="text-primary">
                                <i class="fa fa-exclamation-circle fa-2x"></i>
                            </span>
                        </g:link>
                    </th>

                    <th title="Notify on unapproved stories" data-title="Email Unapproved" class="text-center ${params.filterMailUnapproved == 'true' ? 'bg-success' : params.filterMailUnapproved == 'false' ? 'bg-danger' : ''}">
                        <g:link action="index" params="${params + [offset: 0, filterMailUnapproved: (params.filterMailUnapproved == "true" ? "false" : params.filterMailUnapproved == "false" ? null : "true")]}">
                            <span class="text-primary">
                                <i class="fa fa-question-circle fa-2x"></i>
                            </span>
                        </g:link>
                    </th>

                    <th title="Patreons" data-title="Patreon" class="text-center ${params.filterPatreon == 'true' ? 'bg-info' : ''}">
                        <g:link action="index" params="${params + [offset: 0, filterPatreon: params.filterPatreon != 'true']}">
                            <span class="text-primary">
                                <i class="fa fa-heart fa-2x"></i>
                            </span>
                        </g:link>
                    </th>

                    <th title="Approver" data-title="Approver" class="text-center ${params.filterAdmin == 'true' ? 'bg-info' : ''}">
                        <g:link action="index" params="${params + [offset: 0, filterAdmin: params.filterAdmin != 'true']}">
                            <span class="text-primary">
                                <i class="fa fa-gavel fa-2x"></i>
                            </span>
                        </g:link>
                    </th>

                    <th title="Admin" data-title="Admin" class="text-center ${params.filterSuperAdmin == 'true' ? 'bg-info' : ''}">
                        <g:link action="index" params="${params + [offset: 0, filterSuperAdmin: params.filterSuperAdmin != 'true']}">
                            <span class="text-primary">
                                <i class="fa fa-star fa-2x"></i>
                            </span>
                        </g:link>
                    </th>

                    <th title="Owner" data-title="Owner" class="text-center ${params.filterUltraAdmin == 'true' ? 'bg-info' : ''}">
                        <g:link action="index" params="${params + [offset: 0, filterUltraAdmin: params.filterUltraAdmin != 'true']}">
                            <span class="text-primary">
                                <i class="fa fa-certificate fa-2x"></i>
                            </span>
                        </g:link>
                    </th>

                    <th title="Password Reset Requested" data-title="Password Reset" class="text-center ${params.filterResetPW == 'true' ? 'bg-info' : ''}">
                        <g:link action="index" params="${params + [offset: 0, filterResetPW: params.filterResetPW != 'true']}">
                            <span class="text-primary">
                                <i class="fa fa-key fa-2x"></i>
                            </span>
                        </g:link>
                    </th>

                    <th class="text-center" style="min-width:90px;"><span class="text-primary">Action</span></th>
                </tr>
            </thead>
            <tbody>
            <g:each in="${userInstanceList}" status="i" var="instance">
                <tr id="row-${instance.id}">
                    <td style="font-size: 8px;">${instance.id}</td>
                    <td style="font-size: 10px;"><g:formatDate date="${instance.createdOn}" type="datetime" dateStyle="SHORT" timeStyle="SHORT"/></td>
                    <td style="font-size: 10px;"><g:formatDate date="${instance.lastSignedIn}" type="datetime" dateStyle="SHORT" timeStyle="SHORT"/></td>
                    <td>${instance.username}</td>
                    <td>${instance.authorName}</td>
                    <td>
                        <span class="pull-left">
                            <!-- Leave a space in here so that even if there are no buttons, this takes up room -->
                            &nbsp;
                            <g:userButtons simple="true" user="${instance}"/>
                        </span>
                    </td>
                    <td class="text-center"><g:checkBox name="huh" disabled="true" class="AuthorCheckbox" value="${instance.hasRole("ROLE_AUTHOR")}" data-id="${instance.id}" /></td>
                    <td class="text-center"><g:checkBox name="huh" class="anonForumCheckbox" value="${instance.anonForum}" data-id="${instance.id}" /></td>
                    <td class="text-center"><g:checkBox name="huh" class="emailCommentCheckbox" value="${instance.emailComment}" data-id="${instance.id}" /></td>
                    <td class="text-center"><g:checkBox name="huh" class="emailApprovedCheckbox" value="${instance.emailApproved}" data-id="${instance.id}" /></td>
                    <td class="text-center"><g:checkBox name="huh" class="emailUnapprovedCheckbox" value="${instance.emailUnapproved}" data-id="${instance.id}" /></td>
                    <td class="text-center"><g:checkBox name="huh" disabled="true" class="patreonCheckbox" value="${instance.hasRole("ROLE_PATREON")}" data-id="${instance.id}" /></td>
                    <td class="text-center"><g:checkBox name="huh" class="adminCheckbox" value="${instance.hasRole("ROLE_ADMIN")}" data-id="${instance.id}" /></td>
                    <td class="text-center"><g:checkBox name="huh" disabled="${! isUltraAdmin}" class="superAdminCheckbox" value="${instance.hasRole("ROLE_SUPER_ADMIN")}" data-id="${instance.id}" /></td>
                    <td class="text-center"><g:checkBox name="huh" disabled="${! isUltraAdmin}" class="ultraAdminCheckbox" value="${instance.hasRole("ROLE_ULTRA_ADMIN")}" data-id="${instance.id}" /></td>

                    <td class="text-center">
                        <g:if test="${(instance.resetToken != null) || (instance.resetRequestedOn != null)}">
                            <g:checkBox name="huh" class="passwordResetRequestedCheckbox" value="${true}" data-id="${instance.id}" />
                        </g:if>
                    </td>

                    <td class="text-center">
                        <g:if test="${(! instance.hasRole('ROLE_SUPER_ADMIN') && ! instance.hasRole('ROLE_ULTRA_ADMIN')) || grails.plugin.springsecurity.SpringSecurityUtils.ifAllGranted('ROLE_ULTRA_ADMIN')}">
                            <g:link class="btn btn-sm btn-primary" action="edit" id="${instance.id}" ><i class="fa fa-pencil" aria-hidden="true"></i></g:link>
                            <a href="#" class="btn btn-sm btn-danger deleteButton" data-id="${instance.id}" data-username="${instance.emailAddress}"><span class="fa fa-trash" aria-label="Delete"></span></a>
                        </g:if>
                    </td>
                </tr>
            </g:each>
            </tbody>
        </table>
    </div>

    <div class="row">
        <div class="col">
            <g:if test="${userInstanceCount > params.max}">
                <g:paginate class="justify-content-center d-flex flex-wrap" total="${userInstanceCount ?: 0}" params="${params}"/>
            </g:if>
        </div>
    </div>

    <div id="checkDeleteUser" tabindex="-1" class="modal fade" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title">Delete user?</h3>
                </div>
                <div class="modal-body" align="">
                    Do you really want to delete the user <strong id="deleteUsername"></strong>?<br/>
                    It will be removed from the database and all his stories will become unassigned!
                </div>
                <div class="modal-footer">
                    <button id="deleteYes" type="button" class="btn btn-warning" data-dismiss="modal">Delete</button>
                    <button id="deleteNo" type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>

    <g:render template="/templates/dialogMessages"/>

    </body>
    </html>

    <script>
        <asset:script>
            (function ($, window, document, undefined)
            {
                'use strict';

                $(function ()
                {
                    function saveCheckbox(checkbox, id, value, role, link) {
                        var data = {
                            'id': id,
                            'value': value,
                            'role': role
                        };

                        checkbox.attr("disabled", 1);

                        $.ajax({
                            url: link,
                            type: 'POST',
                            data: data,
                            dataType: 'json',
                            success: function(response) {
                            },
                            error: function(response) {
                                checkbox.prop('checked', !value);
                            },
                            complete: function() {
                                checkbox.removeAttr("disabled");
                            }
                        });
                    }

                    $(".anonForumCheckbox").click(
                        function() {
                            var checkbox = $(this);
                            var id = $(this).data('id');
                            var value = $(this).is(":checked");

                            saveCheckbox(checkbox, id, value, "", "${createLink(action: 'saveAnonForum')}");
                        }
                    );

                    $(".emailCommentCheckbox").click(
                        function() {
                            var checkbox = $(this);
                            var id = $(this).data('id');
                            var value = $(this).is(":checked");

                            saveCheckbox(checkbox, id, value, "", "${createLink(action: 'saveEmailComment')}");
                        }
                    );

                    $(".emailApprovedCheckbox").click(
                        function() {
                            var checkbox = $(this);
                            var id = $(this).data('id');
                            var value = $(this).is(":checked");

                            saveCheckbox(checkbox, id, value, "", "${createLink(action: 'saveEmailApproved')}");
                        }
                    );

                    $(".emailUnapprovedCheckbox").click(
                        function() {
                            var checkbox = $(this);
                            var id = $(this).data('id');
                            var value = $(this).is(":checked");

                            saveCheckbox(checkbox, id, value, "", "${createLink(action: 'saveEmailUnapproved')}");
                        }
                    );

                    $(".patreonCheckbox").click(
                        function() {
                            var checkbox = $(this);
                            var id = $(this).data('id');
                            var value = $(this).is(":checked");

                            saveCheckbox(checkbox, id, value, "ROLE_PATREON", "${createLink(action: 'saveRole')}");
                        }
                    );

                    $(".adminCheckbox").click(
                        function() {
                            var checkbox = $(this);
                            var id = $(this).data('id');
                            var value = $(this).is(":checked");

                            saveCheckbox(checkbox, id, value, "ROLE_ADMIN", "${createLink(action: 'saveRole')}");
                        }
                    );

                    $(".superAdminCheckbox").click(
                        function() {
                            var checkbox = $(this);
                            var id = $(this).data('id');
                            var value = $(this).is(":checked");

                            saveCheckbox(checkbox, id, value, "ROLE_SUPER_ADMIN", "${createLink(action: 'saveRole')}");
                        }
                    );

                    $(".ultraAdminCheckbox").click(
                        function() {
                            var checkbox = $(this);
                            var id = $(this).data('id');
                            var value = $(this).is(":checked");

                            saveCheckbox(checkbox, id, value, "ROLE_ULTRA_ADMIN", "${createLink(action: 'saveRole')}");
                        }
                    );

                    $(".passwordResetRequestedCheckbox").click(
                        function() {
                            var checkbox = $(this);
                            var id = $(this).data('id');
                            var value = $(this).is(":checked");

                            saveCheckbox(checkbox, id, value, "", "${createLink(action: 'savePasswordReset')}");
                        }
                    );

                    $(".deleteButton").click(
                        function() {

                            var id = $(this).data('id');
                            var username = $(this).data('username');
                            $('#deleteUsername').text(username);
                            $('#deleteYes').data("id",id);
                            $('#checkDeleteUser').modal({});
                        }
                    );

                    $('#deleteYes').on("click", function()
                    {
                        var id = $(this).data('id');
                        var data = {'id': id};
                        $("#divLoading").addClass('show');

                        $.ajax({
                            url: "${createLink(action: 'deleteAjax', controller: 'secUser')}",
                            type: 'GET',
                            data: data,
                            success: function(response) {
                                if (response === 'success') {
                                    $("#divLoading").removeClass('show');

                                    var rowId = "#row-" + id;
                                    $(rowId).hide('slow', function(){ $(rowId).remove(); });
                                }
                                else
                                {
                                    $("#divLoading").removeClass('show');

                                    $('#errorMessageText').html("<p>Error deleting user!</p><p>" + response + "</p>");
                                    $('#errorMessage').modal({});
                                }
                            },
                            failure: function(response) {
                                location.reload(true);
                            }
                        });
                    });
                });
            })(jQuery, window, document);
        </asset:script>
    </script>
</g:applyLayout>