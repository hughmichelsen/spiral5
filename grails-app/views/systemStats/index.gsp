<g:applyLayout name="newMain" model="[active:'admin']">
    <html>
    <head>
        <meta name="robots" content="noindex">

        <title><g:pageTitle title='System Stats'/></title>

        %{--<asset:stylesheet href="secuser.css"/>--}%
    </head>
    <body>

    <div class="row">
        <div class="spacer20"></div>
    </div>

    <div class="row">
        <div class="col">
            <p>Free Memory: ${(int) (freeMemory / (1024 * 1024))} mbs<br/>
            Allocated Memory: ${(int) (allocatedMemory / (1024 * 1024))} mbs<br/>
            Total Free Memory: ${(int) (totalFreeMemory / (1024 * 1024))} mbs<br/>
            Server Info: ${serverInfo}</p>
        </div>
    </div>
    <div class="row">
        <div class="spacer20"></div>
    </div>
    <div class="row">
        <div class="col-6">
            <div class="d-flex flex-wrap justify-content-around">
                <g:link class="btn btn-info" controller="systemStats" action="resetEtags">Reset Etags</g:link>
                <g:link class="btn btn-info" controller="systemStats" action="resetPatreon">Reset Patreon Role</g:link>
                <g:link class="btn btn-info" controller="systemStats" action="resetCommentCache">Reset Comment Cache</g:link>
                <g:link class="btn btn-info" controller="systemStats" action="fixUserCounts">Fix User Counts</g:link>
                <g:link class="btn btn-info" controller="systemStats" action="resetSeriesLinks">Reset Series Links & Categories</g:link>
                <g:link class="btn btn-info" controller="systemStats" action="fixStoryCommentCounts">Fix Story Comment Counts</g:link>
                <g:link class="btn btn-info" controller="systemStats" action="resetTagCache">Reset Tag Cache</g:link>
                <g:link class="btn btn-info" controller="systemStats" action="reindexSearch">Reindex Search</g:link>
                <g:link class="btn btn-info" controller="systemStats" action="garbageCollect">Garbage Collect</g:link>
            </div>
    </div>

    <div class="row">
        <div class="spacer20"></div>
    </div>

    </body>
    </html>
</g:applyLayout>