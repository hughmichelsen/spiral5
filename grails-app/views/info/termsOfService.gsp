<g:applyLayout name="newMain" model="[active:'home']">
    <html>
    <head>
        <title><g:pageTitle title='Terms of Service'/></title>

        <asset:stylesheet href="newhome.css"/>
    </head>
    <body>

    <div class="row">
        <div class="col-xs-12 col-sm-offset-1 col-sm-10 col-md-offset-2 col-md-8">
            <h1><g:fullSiteTitle/> - Terms of Service</h1>
            </br>
            <h4>1. Terms</h4>

            By accessing the website at https://www.gayspiralstories.com, you are agreeing to be bound by these terms of service, all applicable laws and regulations, and agree that you are responsible for compliance with any applicable local laws. If you do not agree with any of these terms, you are prohibited from using or accessing this site. The materials contained in this website are protected by applicable copyright and trademark law.

            <h4>2. Use License</h4>

            Permission is granted to temporarily download one copy of the materials (information or software) on Gay Spiral Stories's website for personal, non-commercial transitory viewing only. This is the grant of a license, not a transfer of title, and under this license you may not:
            modify or copy the materials;
            use the materials for any commercial purpose, or for any public display (commercial or non-commercial);
            attempt to decompile or reverse engineer any software contained on Gay Spiral Stories's website;
            remove any copyright or other proprietary notations from the materials; or
            transfer the materials to another person or "mirror" the materials on any other server.
            This license shall automatically terminate if you violate any of these restrictions and may be terminated by Gay Spiral Stories at any time. Upon terminating your viewing of these materials or upon the termination of this license, you must destroy any downloaded materials in your possession whether in electronic or printed format.

            <h4>3. Disclaimer</h4>

            The materials on Gay Spiral Stories's website are provided on an 'as is' basis. Gay Spiral Stories makes no warranties, expressed or implied, and hereby disclaims and negates all other warranties including, without limitation, implied warranties or conditions of merchantability, fitness for a particular purpose, or non-infringement of intellectual property or other violation of rights.
            Further, Gay Spiral Stories does not warrant or make any representations concerning the accuracy, likely results, or reliability of the use of the materials on its website or otherwise relating to such materials or on any sites linked to this site.

            <h4>4. Limitations</h4>

            In no event shall Gay Spiral Stories or its suppliers be liable for any damages (including, without limitation, damages for loss of data or profit, or due to business interruption) arising out of the use or inability to use the materials on Gay Spiral Stories's website, even if Gay Spiral Stories or a Gay Spiral Stories authorized representative has been notified orally or in writing of the possibility of such damage. Because some jurisdictions do not allow limitations on implied warranties, or limitations of liability for consequential or incidental damages, these limitations may not apply to you.

            <h4>5. Accuracy of materials</h4>

            The materials appearing on Gay Spiral Stories's website could include technical, typographical, or photographic errors. Gay Spiral Stories does not warrant that any of the materials on its website are accurate, complete or current. Gay Spiral Stories may make changes to the materials contained on its website at any time without notice. However Gay Spiral Stories does not make any commitment to update the materials.

            <h4>6. Links</h4>

            Gay Spiral Stories has not reviewed all of the sites linked to its website and is not responsible for the contents of any such linked site. The inclusion of any link does not imply endorsement by Gay Spiral Stories of the site. Use of any such linked website is at the user's own risk.

            <h4>7. Modifications</h4>

            Gay Spiral Stories may revise these terms of service for its website at any time without notice. By using this website you are agreeing to be bound by the then current version of these terms of service.

            <h4>8. Governing Law</h4>

            These terms and conditions are governed by and construed in accordance with the laws of California and you irrevocably submit to the exclusive jurisdiction of the courts in that State or location.
        </div>
    </div>

    %{--<hr>--}%

    <footer>
        %{--<p>&copy; Company 2014</p>--}%
    </footer>

    </body>
    </html>
</g:applyLayout>
