<g:applyLayout name="newMain" model="[active:'home']">
    <html>
    <head>
        <title><g:pageTitle title='Privacy Policy'/></title>

        <asset:stylesheet href="newhome.css"/>
    </head>
    <body>

    <div class="row">
        <div class="col-xs-12 col-sm-offset-1 col-sm-10 col-md-offset-2 col-md-8">
            <h1>Privacy Policy</h1>
            </br>
            <p>Your privacy is important to us.</p>
            </br>
            <p>It is the <g:fullSiteTitle/> site's policy to respect your privacy regarding any information we may collect while operating our website. Accordingly, we have developed this privacy policy in order for you to understand how we collect, use, communicate, disclose and otherwise make use of personal information. We have outlined our privacy policy below.</p>
            <p>We will collect personal information by lawful and fair means and, where appropriate, with the knowledge or consent of the individual concerned.</p>
            <p>Before or at the time of collecting personal information, we will identify the purposes for which information is being collected.</p>
            <p>We will collect and use personal information solely for fulfilling those purposes specified by us and for other ancillary purposes, unless we obtain the consent of the individual concerned or as required by law.</p>
            <p>Personal data should be relevant to the purposes for which it is to be used, and, to the extent necessary for those purposes, should be accurate, complete, and up-to-date.</p>
            <p>We will protect personal information by using reasonable security safeguards against loss or theft, as well as unauthorized access, disclosure, copying, use or modification.</p>
            <p>We will make readily available to customers information about our policies and practices relating to the management of personal information.</p>
            <p>We will only retain personal information for as long as necessary for the fulfilment of those purposes.</p>
            <p>We are committed to conducting our business in accordance with these principles in order to ensure that the confidentiality of personal information is protected and maintained. Gay Spiral Stories may change this privacy policy from time to time at Gay Spiral Stories's sole discretion.</p>
        </div>
    </div>

    %{--<hr>--}%

    <footer>
        %{--<p>&copy; Company 2014</p>--}%
    </footer>

    </body>
    </html>
</g:applyLayout>