<g:applyLayout name="newMain" model="[active:'search']">
    <html>
    <head>
        <title><g:pageTitle title='Search Stories'/></title>

        <asset:stylesheet href="tagsWidget.css"/>
        <asset:stylesheet href="newstories.css"/>
    </head>
    <body>

    <div class="row">
        <div class="col">
            <div class="card mb-3">
                <h5 class="card-header">Search stories</h5>
                <div class="card-body">
                    <g:form url="[action:'search', controller:'newStory']" method="GET" >
                        <div id="searchtext-div" class="form-group">
                            <label for="searchTerm" class="d-flex justify-content-between">
                                <div>
                                    Search Text
                                </div>
                                <div>
                                    <a href="javascript:" rel="nofollow" tabindex="0" role="button" data-toggle="popover" data-html="true" data-trigger="focus"
                                       data-content="
                                        <p>Every word you search for will be found in the stories' title, summary, text and author name.</p>
                                        <p>To search for a multi-word term, wrap it in quotes (&quot;).</p>
                                        <p>Prefix with - to exclude a word.<br/>Prefix with + to enforce a word.</p>
                                        <em>Example:</em><div class='font-weight-bold ml-3'>+musk +cum -stink</div>finds all stories which contain both the words musk and cum but not the word stink.<p/>
                                        <a href='https://forum.gayspiralstories.com/t/help-tips-tricks-for-using-gss/247/6' target='_new'>More hints and examples</a>"
                                       data-title="Hints for Searching">Hints for Searching</a>
                                </div>
                            </label>
                            <div class="input-group">
                                <input type="text" class="form-control topic" placeholder="Topic..." name="topic">
                                <input type="text" class="form-control" placeholder="Search for..." id="searchTerm" name="searchTerm" value="${searchTerm}">
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-primary" type="button"><i class="fa fa-search" aria-hidden="true"></i></button>
                                </div>
                            </div><!-- /input-group -->
                        </div>
                        <div id="searchcategory-div" class="form-group">
                            <label for="srchCategory">
                                Category
                            </label>
                            <div class="input-group">
                                <g:select class="form-control custom-select" id="srchCategory" name="srchCategory" from="${categories}" optionValue="text" optionKey="tagId" value="${srchCategory}"/>
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-primary" type="button"><i class="fa fa-search" aria-hidden="true"></i></button>
                                </div>
                            </div>
                        </div>
                        <div id="searchtags-div" class="form-group">
                            <label for="taglist">
                                Search Tags
                            </label>
                            <div class="input-group">
                                <span class="input-group-prepend">
                                    <button class="btn btn-primary" id="btnShowTags" type="button"><i class="fa fa-list" title="Select tags from list"></i></button>
                                </span>
                                <input type="text" class="form-control" id="taglist" name="tagList" aria-describedby="tagHelp" placeholder="Press comma for each new tag, click x to delete a tag." value="${tagList}"/>
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-primary" type="button"><i class="fa fa-search" aria-hidden="true"></i></button>
                                </div>
                            </div>

                            <input type="text" hidden="hidden" name="tagSearchMode" id="tagSearchMode" value="${tagSearchMode}"/>
                            <input type="text" hidden="hidden" name="useAlias" id="useAlias" value="${useAlias}"/>
                            <div class="btn-group d-flex flex-wrap justify-content-end">
                                <div class="btn-group" role="group">
                                    <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="tagAliasModeBtn">${(useAlias == 'true') ? 'Find Similar Tags' : 'Only Listed Tags'} <span class="caret"></span></button>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" href="#" id="selectSimilarTags">Find Similar Tags</a>
                                        <a class="dropdown-item" href="#" id="selectExactTags">Only Listed Tags</a>
                                    </div>
                                </div>
                                <div class="btn-group" role="group">
                                    <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="tagSearchModeBtn">${(tagSearchMode == 'or') ? 'Match Any Tag' : 'Match All Tags'} <span class="caret"></span></button>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" href="#" id="selectMatchAnyTag">Match Any Tag</a>
                                        <a class="dropdown-item" href="#" id="selectMatchAllTags">Match All Tags</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </g:form>
                </div>
            </div>
        </div>

    </div>

    <div class="spacer40"></div>

    <g:if test="${storyInstanceCount > 0}">
        <div class="row mb-2">
            <div class="col">
                <h2 class="text-center">Search Results</h2>
            </div>
        </div>

        <div class="d-flex flex-column flex-sm-row justify-content-center justify-content-sm-between">
            <div>
                <!-- The category selection is disabled for the search -->
            </div>
            <div class="mb-2">
                <g:paginate class="justify-content-center d-flex flex-wrap" action="search" params="[srchCategory: srchCategory, searchTerm: searchTerm, tagList:tagList, useAlias: useAlias, tagSearchMode: tagSearchMode]" total="${storyInstanceCount ?: 0}" />
            </div>
            <sec:ifAllGranted roles="ROLE_SUPER_ADMIN">
                <div class="mb-2 d-flex justify-content-center">
                    <g:render template="/newStory/storyAdminControls" />
                </div>
            </sec:ifAllGranted>
        </div>

        <div class="row">
            <g:render template="/newStory/storyTable" model="[storyInstanceList: storyInstanceList, columnParams: [srchCategory: srchCategory, searchTerm: searchTerm, tagList:tagList, useAlias: useAlias, tagSearchMode: 'or']]" />
        </div>

        <div class="row">
            <div class="col">
                <g:paginate class="justify-content-center d-flex flex-wrap" action="search" params="[srchCategory: srchCategory, searchTerm: searchTerm, tagList:tagList, useAlias: useAlias, tagSearchMode: tagSearchMode]" total="${storyInstanceCount ?: 0}" />
            </div>
        </div>

    </g:if>
    <g:else>
        <g:if test="${searchTerm || tagList || srchCategory}">
            <div class="row">
                <div class="col">
                    <div class="text-center alert alert-warning">
                        <strong>No matching stories found!</strong>
                    </div>
                </div>
            </div>
        </g:if>
    </g:else>


    <g:render template="/templates/tagSelectDialog" model="['dlgTagListName':'dlgTagList']"/>


    <script>

        <asset:script>

            (function ($, window, document, undefined)
            {
                'use strict';

                $(function() {
                    $('.popover-dismiss').popover({
                        trigger: 'focus'
                    })
                });

                $(function()
                {
                    <g:render template="/templates/tagScripts" model="['allowNew': false, 'selector': '#taglist', 'dlgTagListName': '#dlgTagList', 'btnShowTagList': '#btnShowTags']"/>

                    $('#selectMatchAnyTag').click(function(e) {
                        $('#tagSearchModeBtn').html('Match Any Tag <span class="caret"></span>');
                        $('#tagSearchMode').val('or')
                    });
                    $('#selectMatchAllTags').click(function(e) {
                        $('#tagSearchModeBtn').html('Match All Tags <span class="caret"></span>');
                        $('#tagSearchMode').val('and')
                    });
                    $('#selectSimilarTags').click(function(e) {
                        $('#tagAliasModeBtn').html('Find Similar Tags <span class="caret"></span>');
                        $('#useAlias').val('true')
                    });
                    $('#selectExactTags').click(function(e) {
                        $('#tagAliasModeBtn').html('Only Listed Tags <span class="caret"></span>');
                        $('#useAlias').val('false')
                    });
                });
            })(jQuery, window, document);

        </asset:script>
    </script>

    </body>
    </html>
</g:applyLayout>