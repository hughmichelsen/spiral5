<%@ page import="com.ncmc4.Rating" %>

<div class="w-100">
    <table id="storyListMobile" class="storyList showOnMobile">
        <thead class="w-100">
        <tr class="d-flex justify-content-around">
            <g:sortableColumn property="authorSlug" action="${action}" title="Author" />
            <g:sortableColumn property="nameSlug" action="${action}" title="Name" />
            <g:sortableColumn property="publishDate" action="${action}" title="Published" />
        </tr>
        </thead>
    </table>
</div>

<table id="storyList" class="table table-bordered table-striped storyList my-responsive-table" data-rtContainerBreakPoint="539">
    <thead class="thead-dark">
    <tr>
        <g:sortableColumn property="authorSlug" action="${action}" title="Author" params="${columnParams}"/>

        <g:sortableColumn property="nameSlug" action="${action}" title="Name" params="${columnParams}" />

        <g:sortableColumn property="publishDate" action="${action}" title="Published" params="${columnParams}" />

        <sec:ifAllGranted roles="ROLE_SUPER_ADMIN">
            <th>
                <g:checkBox name="anything" class="masterSelectCheckbox" />
            </th>
        </sec:ifAllGranted>

    </tr>
    </thead>
    <tbody>
    <g:each in="${storyInstanceList}" status="i" var="storyInstance">
        <tr class="${storyInstance.sponsored ? 'sponsored' : ''}" data-id="${storyInstance.id}">

            <td><g:link action="show" id="${storyInstance.author.id}" controller="newAuthor">${fieldValue(bean: storyInstance, field: "author")}</g:link> <g:userIcon userid="${storyInstance.owners[0]?.id}"/></td>

            <td>
                <g:if test="${storyInstance?.sponsored}">
                    <small class="sponsored">Warning: The following is an incomplete, sponsored teaser / advertisement of a story, not the whole thing.</small><br/>
                </g:if>
                <div class="d-flex justify-content-between align-items-center">
                    <g:link action="show" id="${storyInstance.id}" controller="newStory" class="storyLink">${fieldValue(bean: storyInstance, field: "name")}</g:link>
                    <g:if test="${storyInstance.commentCount > 0}"><span class="badge badge-primary pl-1 ml-auto" data-toggle="tooltip" title="Comments">${storyInstance?.commentCount} <i class="fa fa-comments"></i></span></g:if>
                </div>
                <div class="d-flex justify-content-between align-items-center">
                    <div class="">
                        <g:if test="${storyInstance.summary}">
                            <div>
                                ${storyInstance.summary}
                            </div>
                        </g:if>
                        <g:if test="${!storyInstance.approved || storyInstance.deleted}">
                            <div>
                                <g:if test="${!storyInstance.approved}">
                                    <g:if test="${!storyInstance.previouslyApproved}">
                                        <span class="badge badge-warning">Unapproved</span>
                                    </g:if>
                                    <g:else>
                                        <span class="badge badge-info">Edited</span>
                                    </g:else>
                                </g:if>
                                <g:if test="${storyInstance.deleted}">
                                    <span class="badge badge-danger">Deleted</span>
                                </g:if>
                            </div>
                        </g:if>
                        <div>
                            <g:if test="${storyInstance.category == null}">
                                <span class="badge badge-secondary">Category missing</span>
                            </g:if>
                            <g:if test="${storyInstance.tags?.size() < 3}">
                                <span class="badge badge-secondary">Insufficient Tags</span>
                            </g:if>
                        </div>
                        <div class="div-tag-category-small">
                            <g:if test="${storyInstance.category}">
                                <g:link action="search" class="div-small-category" controller="newStory" params="[srchCategory: storyInstance?.category?.id, tagList: [], tagSearchMode: 'or', useAlias: 'true', searchTerm: '']">${storyInstance.category?.toString().capitalize()}</g:link> -
                            </g:if>

                            <g:if test="${storyInstance.tags}">
                                <small class="div-small-tags">
                                    <g:each in="${storyInstance.tags}" var="tag">
                                        <g:link action="search" controller="newStory" params="[srchCategory: null, tagList: tag.tag, tagSearchMode: 'or', useAlias: 'true', searchTerm: '']">#${tag.tag}</g:link>
                                    </g:each>
                                </small>
                            </g:if>
                        </div>
                    </div>
                    <div class="d-flex flex-wrap flex-lg-nowrap align-items-center justify-content-center">
                        <g:each in="${storyInstance?.getRatingMarker()}" status="r" var="rating">
                            <img class="storyListRatingIcon-${r} storyListRating${rating.effect}-${r}" src="${assetPath(src: Rating.getRatingIconMap()[rating?.ratName])}" data-toggle="tooltip" title="${rating.toolTip}"/>
                        </g:each>
                    </div>
                </div>
            </td>

            <td><g:formatDate date="${storyInstance.savePublishDate}" /></td>

            <sec:ifAllGranted roles="ROLE_SUPER_ADMIN">
                <td>
                    <g:checkBox name="anything" class="selectCheckbox" data-id="${storyInstance.id}" data-admincomment="${storyInstance.adminComment}" />
                </td>
            </sec:ifAllGranted>

        </tr>
    </g:each>
    </tbody>
</table>

<div id="addToSeriesModal" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h3 class="modal-title text-light">Add Stories to Series</h3>
            </div>
            <div class="modal-body">
                <h6>Which series would you like to add the stories to?</h6>
                <div class="form-group">
                    <textarea id="nameNewSeriesText" class="form-control" placeholder="Series name..." type="text" rows="1"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button id="changeSeries" type="button" class="btn btn-default" data-dismiss="modal">Add to Series</button>
                <button id="changeSeriesCancel" type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            </div>
        </div>

    </div>
</div>

<div id="assignAuthorModal" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h3 class="modal-title text-light">Assign Author</h3>
            </div>
            <div class="modal-body">
                <h6>Which author would you like to assign the stories to?</h6>
                <div class="form-group">
                    <textarea id="newAuthorEmailAddress" class="form-control" placeholder="Author's email address..." type="text" rows="1"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button id="assignAuthorGo" type="button" class="btn btn-default" data-dismiss="modal">Assign to Author</button>
                <button id="assignAuthorCancel" type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            </div>
        </div>

    </div>
</div>

