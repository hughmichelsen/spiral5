<div class="card card-default mt-3">
    <div class="card-header">
        Story Description
    </div>
    <div class="card-body">
        <div id="name-div" class="form-group fieldcontain ${hasErrors(bean: storyInstance, field: 'name', 'has-error')} required">
            <label for="name">
                Story Title
            </label>
            <g:textField class="form-control" name="name" required="" value="${storyInstance?.name}"/>
            <div class="text-danger"><g:fieldError bean="${storyInstance}" field="name"/></div>
        </div>

        <div id="series-div" class="form-group fieldcontain ${hasErrors(bean: storyInstance, field: 'series', 'has-error')}">
            <label for="seriesName" data-toggle="tooltip" data-placement="right" title="To create a multi-part series of stories, enter the same series name for each of those stories!">
                Series Name <i class="fa fa-info-circle" aria-hidden="true"></i>
            </label>
            <input id="seriesName" name="seriesName" type="text" class="form-control" value="${storyInstance?.series?.name}" aria-describedby="seriesHelp" autocomplete="Series Name"/>
            <div class="text-danger"><g:fieldError bean="${storyInstance}" field="series"/></div>
        </div>

        <input type="hidden" name="approved" value="${storyInstance?.approved}" />
        <input type="hidden" name="deleted" value="${storyInstance?.deleted}" />

        <g:if test="${includeId}">
            <input type="hidden" name="id" value="${storyInstance?.id}" />
        </g:if>
        <g:if test="${includeUuid}">
            <input type="hidden" name="secretCode" value="${storyInstance?.uuid}" />
        </g:if>

        <div id="author-div" class="form-group fieldcontain ${hasErrors(bean: storyInstance, field: 'author', 'has-error')}">
            <label for="authorName" data-toggle="tooltip" data-placement="right" title="The publicly visible author name. You can det your default author name in your account settings!">
                Author Name <i class="fa fa-info-circle" aria-hidden="true"></i>
            </label>
            <input id="authorName" name="authorName" type="text" class="form-control" value="${storyInstance?.author?.displayName}" autocomplete="Author Name" title="Do not use an email as the author name!" pattern="[^@]+">
            <div class="text-danger"><g:fieldError bean="${storyInstance}" field="author"/></div>
        </div>

        <div id="email-div" class="form-group fieldcontain ${hasErrors(bean: storyInstance, field: 'emailAddress', 'has-error')} required">
            <label for="emailAddress" data-toggle="tooltip" data-placement="right" data-html="true" title="<p><b>Use a valid email address!</b></p><p>We might have to contact you!</p>If you want to keep your address private, untick the checkbutton below!">
                Email Address <i class="fa fa-info-circle" aria-hidden="true"></i>
            </label>
            <input class="form-control" id="emailAddress" name="emailAddress" required="" value="${storyInstance?.emailAddress}" type="email"/>
            <div class="text-danger"><g:fieldError bean="${storyInstance}" field="emailAddress"/></div>
        </div>

        <div id="showEmail-div" class="custom-control custom-checkbox mb-3 fieldcontain ${hasErrors(bean: storyInstance, field: 'showEmail', 'has-error')}">
            <g:checkBox class="custom-control-input" id="showEmail" name="showEmail" value="${storyInstance?.showEmail}"/>
            <label class="custom-control-label" for="showEmail">
                Email visible publicly
            </label>
            <i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="Untick this, if you want to keep your email address private."></i>
        </div>

        <div id="summary-div" class="form-group fieldcontain ${hasErrors(bean: storyInstance, field: 'summary', 'has-error')} required">
            <label for="summary" data-toggle="tooltip" data-placement="right" title="A short but descriptive summary of what this story is about">
                Summary <i class="fa fa-info-circle" aria-hidden="true"></i>
            </label>
            <input class="form-control" type="text" id="summary" name="summary" required="" value="${storyInstance?.summary}"/>
            <div class="text-danger"><g:fieldError bean="${storyInstance}" field="summary"/></div>
        </div>

        <div id="category-div" class="form-group fieldcontain ${hasErrors(bean: storyInstance, field: 'category', 'has-error')} required">
            <label for="category" data-toggle="tooltip" data-placement="right" title="Select the category which fits best for your story.">
                Category <i class="fa fa-info-circle" aria-hidden="true"></i>
            </label>
            <g:select class="form-control custom-select" id="category" name="category" required="" from="${categories}" optionValue="text" optionKey="tagId" value="${storyInstance?.category?.id}"/>
            <div class="text-danger"><g:fieldError bean="${storyInstance}" field="category"/></div>
        </div>

        <div id="storytags-div" class="form-group fieldcontain ${hasErrors(bean: storyInstance, field: 'tags', 'has-error')}">
            <label for="taglist" data-toggle="tooltip" data-placement="right" data-html="true" title="<p>3 tags required!</p>Use the button on the left to get a list of all known tags.">
                Tags <i class="fa fa-info-circle" aria-hidden="true"></i>
            </label>
            <div class="input-group">
                <span class="input-group-prepend">
                    <button class="btn btn-primary" id="btnShowTags" type="button"><i class="fa fa-list" title="Select tags from list"></i></button>
                </span>
                <input type="text" class="form-control" id="taglist" name="tagList" rows="1" aria-describedby="tagHelp" placeholder="Press comma for each new tag, click x to delete a tag." value="${tagList}"/>
            </div>
            <div class="text-danger"><g:fieldError bean="${storyInstance}" field="tags"/></div>
            <span id="tagAddWarning" class="tagAddWarning">You've created tags exclusively for this story! Please avoid exclusive tags!</span>
        </div>

        <div id="copyrightYear-div" class="form-group fieldcontain ${hasErrors(bean: storyInstance, field: 'copyrightYear', 'has-error')}">
            <label for="copyrightYear" data-toggle="tooltip" data-placement="right" title="Enter a year here if you are posting an older story previously posted elsewhere.">
                Copyright Year <i class="fa fa-info-circle" aria-hidden="true"></i>
            </label>
            <input class="form-control" type="text" id="copyrightYear" name="copyrightYear" value="${storyInstance?.copyrightYear}"/>
            <div class="text-danger"><g:fieldError bean="${storyInstance}" field="copyrightYear"/></div>
        </div>

        <sec:ifAllGranted roles="ROLE_SUPER_ADMIN">
            <div id="publishDate-div" class="form-group fieldcontain ${hasErrors(bean: storyInstance, field: 'publishDate', 'has-error')}">
                <label for="publishDate">
                    Publish Date
                </label>
                <g:datePicker class="form-control" type="datetime-local" id="publishDate" name="publishDate" precision="minute" value="${storyInstance?.publishDate}"/>
                <div class="text-danger"><g:fieldError bean="${storyInstance}" field="publishDate"/></div>
            </div>
        </sec:ifAllGranted>

        <div id="password-div" class="form-group fieldcontain required">
            <label for="password">
                Password
            </label>
            <g:textField class="form-control" name="password" value=""/>
        </div>

        <div id="showAutoExpandLinks-div" class="custom-control custom-checkbox fieldcontain ${hasErrors(bean: storyInstance, field: 'autoExpandLinksInText', 'has-error')}">
            <g:checkBox class="custom-control-input" id="autoExpandLinksInText" name="autoExpandLinksInText" value="${storyInstance ? storyInstance.autoExpandLinksInText : false}"/>
            <label class="custom-control-label" for="autoExpandLinksInText">
                Automatically generate clickable links
            </label>
            <i class="fa fa-info-circle" aria-hidden="true"  data-toggle="tooltip" data-placement="right" data-html='true' title="<p>Automatically convert web- and email addresses into clickable links.</p>Not compatible with Markdown!"></i>
        </div>

        <div id="hideCopyright-div" class="custom-control custom-checkbox fieldcontain ${hasErrors(bean: storyInstance, field: 'hideCopyright', 'has-error')}">
            <g:checkBox class="custom-control-input" id="hideCopyright" name="hideCopyright" value="${storyInstance?.hideCopyright}"/>
            <label class="custom-control-label" for="hideCopyright">
                Hide Copyright Notice
            </label>
        </div>
    </div>
</div>
<div class="card card-default mt-3">
    <div class="card-header">
        Story Text
    </div>
    <div class="card-body">
        <div class="alert alert-success" role="alert">
            <strong>Tips & Tricks for authors:</strong>
            <a href="javascript:" rel="nofollow" tabindex="0" role="button" class="alert-link" data-toggle="popover" title="<strong>Formatting the story</strong>"
               data-content="
               <p>The text is formatted in plain text, using empty lines to separate paragraphs.</p>
               <p>Using paragraphs makes it much easier to read a text!</p>
               <p>Use Markdown to decorate your text. Examples:
               *<i>italic</i>* **<b>bold</b>**</p>
               <a href='https://forum.gayspiralstories.com/t/help-tips-tricks-for-using-gss/247/9' target='_new'>More about formatting</a>"
                data-html="true" data-trigger="focus" data-placement="bottom">How to format a story (using plain text or Markdown)</a><br>
            <!-- <a href="#" class="alert-link">Generic tips how to write and format a story properly</a> -->
        </div>
        <div id="text-div" class="form-group fieldcontain ${hasErrors(bean: storyInstance, field: 'text', 'has-error')} required">
            <div class="d-flex justify-content-between">
                <label for="commentText">
                    &nbsp;
                </label>
                <div class="custom-checkbox custom-control fieldcontain">
                    <g:checkBox class="custom-control-input" id="extendedEditor" name="extendedEditor"/>
                    <label for="extendedEditor" class="custom-control-label" >Extended editor</label>
                </div>
            </div>
            <g:textArea id="storyText" class="form-control" name="text" value="${storyInstance?.textUnformatted}" rows="40"/>
            <div class="text-danger"><g:fieldError bean="${storyInstance}" field="text"/></div>
        </div>
    </div>
</div>

<sec:ifAllGranted roles="ROLE_SUPER_ADMIN">
    <div class="card card-default mt-3">
        <div class="card-header" id="sponsored-heading"><g:checkBox name="sponsored" id="sponsored" value="${storyInstance?.sponsored ?: false}"/> Sponsored</div>
        <div class="card-body collapse" id="sponsored-accordion">
            <div id="cover-div" class="form-group fieldcontain ${hasErrors(bean: storyInstance, field: 'coverImageLink', 'has-error')}">
                <label for="coverImageLink">
                    Cover Image Link
                </label>
                <g:textField class="form-control sponsoredField" id="coverImageLink" name="coverImageLink" value="${storyInstance?.coverImageLink}"/>
                <div class="text-danger"><g:fieldError bean="${storyInstance}" field="coverImageLink"/></div>
            </div>

            <div id="teaser-div" class="form-group fieldcontain ${hasErrors(bean: storyInstance, field: 'teaser', 'has-error')}">
                <label for="endTeaser">
                    End Teaser
                </label>
                <textarea id="endTeaser" name="endTeaser" type="text" class="form-control sponsoredField" rows="6" placeholder="Bolded teaser at the bottom of the story">${storyInstance?.endTeaser}</textarea>
                <div class="text-danger"><g:fieldError bean="${storyInstance}" field="teaser"/></div>
            </div>

            <div id="amazonBookId-div" class="form-group fieldcontain ${hasErrors(bean: storyInstance, field: 'amazonBookId', 'has-error')}">
                <label for="amazonBookId">
                    Amazon Book Id
                </label>
                <g:textField class="form-control sponsoredField" id="amazonBookId" name="amazonBookId" value="${storyInstance?.amazonBookId}"/>
                <div class="text-danger"><g:fieldError bean="${storyInstance}" field="amazonBookId"/></div>
            </div>
        </div>
    </div>
</sec:ifAllGranted>
<g:if test="${ageGate == true}">
    <div id="over18-div" class="custom-control custom-checkbox fieldcontain required has-error mt-3">
        <g:checkBox class="custom-control-input" id="over18" name="over18" value="${false}"/>
        <label class="custom-control-label" for="over18">
            <span class="ageWarning">I am over 18 years of age</span>
        </label>
    </div>
</g:if>

<fieldset class="buttons mt-3 mb-2">
    <g:submitButton name="create" class="save btn btn-lg btn-primary" value="${submitButtonText}" />
</fieldset>
