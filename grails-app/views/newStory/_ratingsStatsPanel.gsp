    <div class="row hidden statistics mb-3">
        <div class="col">
            <div class="card card-default">
                <h6 class="card-header">Rating Statistics</h6>
                <div class="card-body">
                    <table  id="" class="statisticTable table-striped">
                        <tr>
                            <th></th>
                            <th data-toggle="tooltip" title="Average rating (0-5)">Rating</th>
                            <th colspan="3">Rank</th>
                        </tr>
                        <tr>
                            <th></th>
                            <th></th>
                            <th data-toggle="tooltip" title="Rank among all stories released in the same month">Month</th>
                            <th data-toggle="tooltip" title="Rank among all stories released in the same year">Year</th>
                            <th data-toggle="tooltip" title="Overall rank over all stories">Overall</th>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
