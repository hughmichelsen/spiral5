<g:hasErrors bean="${storyInstance}">
    <g:eachError bean="${storyInstance}" var="error">
        <div class="alert alert-danger mb-3">
            <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
            <span class="sr-only">Error:</span>
            <g:message error="${error}"/>
        </div>
    </g:eachError>
</g:hasErrors>