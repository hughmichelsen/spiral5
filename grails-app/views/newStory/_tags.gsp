<g:if test="${errorText}">
    <p class="text-danger font-weight-bold">${errorText}</p>
</g:if>
<div class="div-category">
    Category:
    <g:if test="${storyInstance?.category}">
        <g:link class="font-weight-bold font-italic" action="search" controller="newStory" params="[srchCategory: storyInstance?.category?.id, tagList: [], tagSearchMode: 'or', useAlias: 'true', searchTerm: '']">${storyInstance?.category?.tag}</g:link>
    </g:if>
    <g:else>
        <span class="font-weight-bold"> - </span>
    </g:else>

    &nbsp;&nbsp;Tags:
    <g:if test="${storyInstance?.tags}">
        <g:each in="${storyInstance?.tags}" var="tag"><g:link class="font-weight-bold" action="search" controller="newStory" params="[srchCategory: null, tagList: tag.tag, tagSearchMode: 'or', useAlias: 'true', searchTerm: '']">#${tag.tag}</g:link> </g:each>
    </g:if>
    <g:else>
        <span class="font-weight-bold"> - </span>
    </g:else>
</div>
