<%@ page import="grails.plugin.springsecurity.SpringSecurityUtils" %>
<g:if test="${storyInstance.series && (storyInstance.series.storyCount > 1 || (! storyInstance.previouslyApproved && grails.plugin.springsecurity.SpringSecurityUtils.ifAnyGranted('ROLE_ADMIN')))}">
    <div class="alert alert-primary card-default">
        <div class="">
            <h6 class="text-center">Series:
                <g:if test="${isReview && (storyInstance?.series?.name != archive?.oldSeriesName)}">
                    <span class='revision revision-removed'>${archive?.oldSeriesName}</span>
                    <g:link class="alert-link" action="show" controller="newSeries" id="${storyInstance?.series?.id}">
                        ${storyInstance?.series?.name}
                    </g:link>
                </g:if>
                <g:elseif test="${isShowingOlderVersion && (storyInstance?.series?.name != archive?.oldSeriesName)}">
                    ${archive?.oldSeriesName}
                </g:elseif>
                <g:else>
                    <g:link class="alert-link" action="show" controller="newSeries" id="${storyInstance?.series?.id}">
                        ${storyInstance?.series?.name}
                    </g:link>
                </g:else>
            </h6>
            <g:if test="${storyInstance?.prevInSeries || storyInstance?.nextInSeries}">
                <div class="d-flex justify-content-center">
                    <g:if test="${storyInstance?.prevInSeries}">
                        <g:link class="alert-link small mr-auto" action="show" id="${storyInstance?.prevInSeries?.id}">« Prev Chapter</g:link>
                    </g:if>
                    <g:if test="${storyInstance?.nextInSeries}">
                        <g:link class="alert-link small ml-auto" action="show" id="${storyInstance?.nextInSeries?.id}">Next Chapter »</g:link>
                    </g:if>
                </div>
            </g:if>
        </div>
    </div>
</g:if>

