<div class="card mb-3" id="comment-${comment.id}">
    <h5 class="card-header commentHead d-flex justify-content-between" data-id="${comment.id}">
        <div>
            <i class="fa fa-star text-success d-none newCommentStar" data-id="${comment.id}" data-toggle="tooltip" title="New since your last visit!" aria-hidden="true"></i>
            ${comment.title}
            <g:if test="${comment.poster}">
                <g:userIcon userid="${comment.poster.id}"/>
            </g:if>
            <sec:ifAllGranted roles="ROLE_SUPER_ADMIN">
                <g:if test="${comment.isAdmin}">
                    <g:if test="${comment.poster}">
                        <g:link class="posterLink" controller="newComment" action="showUser" id="${comment.poster.id}"><small>${comment.poster}</small></g:link>
                    </g:if>
                    <small class="font-italic"><g:link controller="newComment" action="showIps" id="${comment.id}">${comment.ipAddress}</g:link></small>
                </g:if>
            </sec:ifAllGranted>
        </div>
        <div class="text-right">
            <g:formatDate type="datetime" date="${comment.creationDate}" />
        </div>
    </h5>
    <div class="card-body">
        <g:if test="${includeStoryTitle}">
            <div class="card-title larger-text"><g:link controller="newStory" action="show" id="${comment.story?.id}">${comment.story?.name}</g:link> <span class="small font-italic">by </span>${comment.story.author} <span class="small font-italic"> published </span><g:formatDate type="date" style="MEDIUM" date="${comment.story?.savePublishDate}" /></div>
        </g:if>
        <div class="larger-text">${raw(comment.text)}</div>
        <g:if test="${comment.isOwnComment || comment.isAdmin}">
            <button class="deleteCommentButton btn btn-primary btn-sm" type="button" data-id="${comment.id}">Delete</button>
        </g:if>
    </div>
</div>