<g:applyLayout name="storyList" model="[storyInstanceList: storyInstanceList, storyInstanceCount: storyInstanceCount, title: title, showLetterLinks: true, active:'stories']">

    <content tag="paginate">
        <g:paginate class="justify-content-center d-flex flex-wrap" action="index" total="${storyInstanceCount ?: 0}" />
    </content>

</g:applyLayout>