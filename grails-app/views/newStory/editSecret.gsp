<g:applyLayout name="newMain" model="[active:'new story']" >
    <html>
    <head>
        <meta name="robots" content="noindex">

        <title><g:pageTitle title='Edit ${storyInstance.name}'/></title>
        <asset:stylesheet href="create.css"/>
        <asset:stylesheet href="tagsWidget.css"/>
        <asset:stylesheet href="inscrybmde.css"/>
    </head>
    <body>

    <div id="edit-story" class="content container" role="main">
        <h1>Edit <small>${storyInstance.name}</small></h1>

        <g:render template="createShowErrors" model="[storyInstance: storyInstance]" />

        <g:form url="[action:'updateSecret', controller:'newStory']" id="create-form" name="create-form" role="form">
            <g:render template="createFormFields" model="[storyInstance: storyInstance, submitButtonText: 'Update', ageGate: false, includeId:true, includeUuid: true]" />
        </g:form>
    </div>

    <g:render template="/templates/tagSelectDialog" model="['dlgTagListName':'dlgTagList']"/>

    <content tag="scripts">
        <asset:javascript src="inscrybmde.js"/>
        <script>

            <asset:script>

                (function ($, window, document, undefined)
                {
                    'use strict';

                    $(function()
                    {
                        <g:render template="/templates/typeaheadScript" model="['sourceURL': createLink(action: 'seriesNames'), 'selector': '#seriesName']"/>
                        <g:render template="/templates/typeaheadScript" model="['sourceURL': createLink(action: 'authors'), 'selector': '#authorName']"/>
                        <g:render template="/templates/tagScripts" model="['allowNew': true, 'selector': '#taglist', 'newTagSelector': '#tagAddWarning', 'dlgTagListName': '#dlgTagList', 'btnShowTagList': '#btnShowTags']"/>
                        <g:render template="sponsorScripts"/>

                        var showExtendedEditor = localStorage.getItem("showExtendedEditor");
                        if(showExtendedEditor === null)
                            showExtendedEditor = "1";

                        $('#extendedEditor').prop("checked", showExtendedEditor === "1");

                        var inscrybmde = new InscrybMDE({
                            element: $("#storyText")[0],
                            minHeight: "1000px",
                            showIcons: ["bold", "italic", "strikethrough", "code", "heading", "|", "quote", "horizontal-rule", "clean-block", "|", "link", "image", "|", "preview"],
                            hideIcons: ["unordered-list", "ordered-list", "table", "side-by-side", "fullscreen", "guide"],
                            autosave: {
                                enabled: false,
                            },
                            renderingConfig: {singleLineBreaks: false},
                            promptURLs: true,
                            shortcuts: {
                                "toggleOrderedList": null,
                                "toggleUnorderedList": null,
                                "drawTable": null,
                                "toggleSideBySide": null,
                                "toggleFullScreen": null
                            },
                            inputStyle: 'contenteditable',
                            autoRender: showExtendedEditor === "1"
                        });

                        $('#extendedEditor').change(function(e){
                            var sel = $(e.target).is(":checked");
                            if(sel) {
                                inscrybmde.toEditor();
                                showExtendedEditor = "1";
                            }
                            else {
                                inscrybmde.toTextArea();
                                showExtendedEditor = "0";
                            }
                            localStorage.setItem("showExtendedEditor",showExtendedEditor);
                        });

                        function checkCategory() {
                            var elem = $('#category');
                            if (elem.val() === '0') {
                                elem[0].setCustomValidity("Please provide a category!");
                            }
                            else
                                elem[0].setCustomValidity("")
                        }

                        $('#category').change(function(e){
                            checkCategory();
                        })

                        checkCategory();
                    });
                })(jQuery, window, document);
            </asset:script>

        </script>
    </content>

    </body>
    </html>
</g:applyLayout>

