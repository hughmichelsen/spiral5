<g:applyLayout name="storyList" model="[storyInstanceList: storyInstanceList, storyInstanceCount: storyInstanceCount, title: title, showLetterLinks: true]">

    <content tag="paginate">
        <g:paginate class="justify-content-center d-flex flex-wrap" action="letter" id="${letter}" total="${storyInstanceCount ?: 0}" />
    </content>

</g:applyLayout>