$(document).on('click', '#sponsored-heading', function(e){
    var $this = $(this);
    var accordion = $('#sponsored-accordion');
    if(!accordion.hasClass('collapse')) {
        accordion.addClass('collapse');
        $('#sponsored').prop('checked', false);
    } else {
        accordion.removeClass('collapse');
        $('#sponsored').prop('checked', true);
    }
});

function toggleSponsoredControls(sponsored)
{
    if (sponsored)
        $(".sponsoredField").removeAttr("disabled");
    else
        $(".sponsoredField").attr("disabled", 1);
}

$('#sponsored').change(function() {
    var sponsored = $(this).is(":checked");

    toggleSponsoredControls(sponsored);
});

// make sure the controls are setup properly to begin with
toggleSponsoredControls($("#sponsored").is(":checked"));

