<g:applyLayout name="storyList" model="[storyInstanceList: storyInstanceList, storyInstanceCount: storyInstanceCount, title: title, active:'stories']">

    <content tag="headline">
        <div class="alert alert-info mx-auto">
            <h4 class="text-center">Stories missing a category or not tagged properly</h4>
            <div class="text-center">Please help us by adding a category and/or tags to these stories. That will make it much easier for other readers to find them.</div>
            <div class="text-center font-weight-bold">Everyone can add a missing category and add or modify tags!</div>
        </div>
    </content>

    <content tag="paginate">
        <g:paginate class="justify-content-center d-flex flex-wrap" action="defaultedStories" total="${storyInstanceCount ?: 0}" />
    </content>

</g:applyLayout>