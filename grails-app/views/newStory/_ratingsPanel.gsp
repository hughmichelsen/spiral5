<%@ page import="grails.plugin.springsecurity.SpringSecurityUtils; com.ncmc4.Rating" %>
<div class="row" id="ratings">
        <div class="mx-auto mt-3 mb-1 card ${ratingHint >= 2 ? 'text-warning border-warning' : ratingHint == 1 ? 'text-info border-info' : ''}">
            <div class="card-header text-center">
                <g:if test="${! isAuthor && ratingHint > 0}">
                    <g:if test="${ratingHint >= 2}">
                        <h4 class="pulsate text-warning">This story hasn't received enough ratings yet!</h4>
                    </g:if>
                    <g:else>
                        <h5 class="pulsate text-info">This story could use more ratings!</h5>
                    </g:else>
                </g:if>
                <g:if test="${isAuthor}">
                    <div class="larger-text">Current ratings average for your story</div>
                </g:if>
                <g:else>
                    <div class="larger-text">Please use the controls below to rate this story</div>
                </g:else>
            </div>
            <div class="card-body">
                <div class="row ratingtable">
                    <g:each var="rating" in="${Rating.getRatingList()}">
                        <div class="ratingline">
                            <div class="ratinglabel">${Rating.getRatingLabelMap()[rating]}</div>
                            <input id="${rating}" type="number" class="story_rating" data-empty="${assetPath(src: Rating.getRatingPassiveIconMap()[rating])}" data-full="${assetPath(src: Rating.getRatingActiveIconMap()[rating])}"/>
                        </div>
                    </g:each>
                </div>
            </div>
        </div>
</div>
