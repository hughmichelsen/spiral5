<g:applyLayout name="newMain" model="[active:'new story']" >
    <html>
    <head>
        <meta name="robots" content="noindex">

        <title><g:pageTitle title='Submit New Story'/></title>

        <asset:stylesheet href="create.css"/>
        <asset:stylesheet href="tagsWidget.css"/>
        <asset:stylesheet href="inscrybmde.css"/>
    </head>
    <body>

    <div id="create-story" class="content container" role="main">

        <div class="row">
            <h2 class="text-center">Submit New Story</h2>
            <div class="spacer15"></div>
        </div>
        <sec:ifNotLoggedIn>
            <div class="row">
                <div class="col">
                    <div class="alert alert-danger" role="alert">
                        <p><strong>You are not logged on!</strong></p>
                        <p>We allow stories to be submitted without using an account, but this has many disadvantages!</p>
                        <a href="javascript:" rel="nofollow" tabindex="0" role="button" class="alert-link" data-toggle="popover" title="<strong>Using an account</strong>"
                           data-content="
               <p>You can submit stories and even edit them later even without creating an account.</p>
               <p>However, using an account has several advantages:
               <ul>
                <li>You can see statistics and the ratings for your stories</li>
                <li>Editing your stories is much easier</li>
                <li>You can get a list of all comments to your stories</li>
               </ul>
               <strong>An account is still absolutely anonymous!</strong>
                " data-html="true" data-trigger="focus" data-placement="right">
                            Why should I use an account?
                        </a>
                    </div>
                </div>
            </div>
        </sec:ifNotLoggedIn>
        <div class="row">
            <div class="col">
                <g:render template="createShowErrors" model="[storyInstance: storyInstance]" />
            </div>
        </div>
        <div class="row">
            <div class="col">
                <g:render template="/templates/storyRules" />
            </div>
        </div>
        <div class="row">
            <div class="col">
                <g:form url="[action:'save', controller:'newStory']" id="create-form" name="create-form" role="form" useToken="true">
                    <g:render template="createFormFields" model="[storyInstance: storyInstance, submitButtonText: 'Submit your story for approval', ageGate: ageGate]" />
                </g:form>
            </div>
        </div>
    </div>

    <div id="rulesModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h3 class="modal-title text-light">The Rules</h3>
                </div>
                <div class="modal-body">
                    <h6>Do you want your story to be approved?</h6>
                    <p><b>Then make sure it has:</b>
                    <ul>
                        <li>Some kind of mind-control or mind altering effect</li>
                        <li>No sex with children (pre- or early puberty)!</li>
                        <li>No real life persons!</li>
                        <li>No hard violence, blood and gore! No mutilations!</li>
                    </ul>
                    </p>
                    <h6>Does your story meet these rules?</h6>
                </div>
                <div class="modal-footer">
                    <button id="rulesYes" type="button" class="btn btn-default" data-dismiss="modal">Yes</button>
                    <button id="rulesNo" type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>

    <g:render template="/templates/tagSelectDialog" model="['dlgTagListName':'dlgTagList']"/>


    <content tag="scripts">
        <asset:javascript src="inscrybmde.js"/>

        <script>
            <asset:script>

                (function ($, window, document, undefined)
                {
                    'use strict';

                    $(function()
                    {
                        <g:render template="/templates/typeaheadScript" model="['sourceURL': createLink(action: 'seriesNames'), 'selector': '#seriesName']"/>
                        <g:render template="/templates/typeaheadScript" model="['sourceURL': createLink(action: 'authors'), 'selector': '#authorName']"/>
                        <g:render template="/templates/tagScripts" model="['allowNew': true, 'selector': '#taglist', 'newTagSelector': '#tagAddWarning', 'dlgTagListName': '#dlgTagList', 'btnShowTagList': '#btnShowTags']"/>
                        <g:render template="sponsorScripts"/>

                        var showExtendedEditor = localStorage.getItem("showExtendedEditor");
                        if(showExtendedEditor === null)
                            showExtendedEditor = "1";

                        $('#extendedEditor').prop("checked", showExtendedEditor === "1");

                        var inscrybmde = new InscrybMDE({
                            element: $("#storyText")[0],
                            minHeight: "1000px",
                            showIcons: ["bold", "italic", "strikethrough", "code", "heading", "|", "quote", "horizontal-rule", "clean-block", "|", "link", "image", "|", "preview"],
                            hideIcons: ["unordered-list", "ordered-list", "table", "side-by-side", "fullscreen", "guide"],
                            autosave: {
                                enabled: true,
                                uniqueId: "gayspiralstories_newstory"
                                },
                            renderingConfig: {singleLineBreaks: false},
                            promptURLs: true,
                            shortcuts: {
                                    "toggleOrderedList": null,
                                    "toggleUnorderedList": null,
                                    "drawTable": null,
                                    "toggleSideBySide": null,
                                    "toggleFullScreen": null
                                },
                            inputStyle: 'contenteditable',
                            autoRender: showExtendedEditor === "1"
                        });

                        $('#extendedEditor').change(function(e){
                            var sel = $(e.target).is(":checked");
                            if(sel) {
                                inscrybmde.toEditor();
                                showExtendedEditor = "1";
                            }
                            else {
                                inscrybmde.toTextArea();
                                showExtendedEditor = "0";
                            }
                            localStorage.setItem("showExtendedEditor",showExtendedEditor);
                        });

                        $("#create-form").submit(
                            function (e) {
                                if ($('#rulesModal').length > 0) {
                                    e.preventDefault();
                                    e.stopPropagation();

                                    var currentForm = this;

                                    $('#rulesYes').on("click", function () {
                                        currentForm.submit();
                                    });

                                    $('#rulesModal').modal({});
                                }
                            });

                        function checkCategory() {
                            var elem = $('#category');
                            if (elem.val() === '0') {
                                elem[0].setCustomValidity("Please provide a category!");
                            }
                            else
                                elem[0].setCustomValidity("")
                        }

                        $('#category').change(function(e){
                            checkCategory();
                        })

                        checkCategory();
                    });
                })(jQuery, window, document);

            </asset:script>
        </script>
    </content>

    </body>
    </html>
</g:applyLayout>

