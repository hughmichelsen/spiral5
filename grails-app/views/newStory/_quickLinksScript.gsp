<script type="text/javascript">
    (function ($, window, document, undefined)
    {
        'use strict';

        $(function ()
        {
            $('.auto_link_list').on('change', function () {
                var url = $(this).val(); // get selected value
                if (url && (url != "null")) { // require a URL
                    window.location = url; // redirect
                }

                return false;
            });
        });
    })(jQuery, window, document);
</script>

