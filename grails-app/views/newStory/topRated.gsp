<%@ page import="grails.plugin.springsecurity.SpringSecurityUtils; com.ncmc4.Rating" %>
<g:applyLayout name="newMain" model="[active:'stories']">
    <html>
    <head>
        <title><g:pageTitle title="Top Rated Stories"/></title>
    </head>
    <body>
    <g:if test="${topSeriesMap == null}">
        <div class="row mb-1">
            <div class="alert alert-warning mx-auto">
                <h4 class="text-center">Starting on Saturday, January 5th, you'll find a growing list of the top rated stories here!</h4>
                <div class="text-center">Every other week we'll add a new selection of the highest rated stories or series for each rating category.</div>
                <div class="text-center">The latest added selection of the top rated stories will be listed <g:link action="index" controller="newHome">on the home page</g:link></div>
            </div>
        </div>
    </g:if>
    <g:else>
        <div class="row mb-1">
            <div class="alert alert-info mx-auto">
                <h4 class="text-center">Top rated stories - ${raw(topPeriodName)}</h4>
                <h5 class="text-center">This is the list of all the highest rated stories or series we've presented so far.</h5>
                <div class="text-center">On every other Saturday we present a new selection <g:link action="index" controller="newHome">on the home page</g:link></div>
            </div>
        </div>
        <g:render template="/newSeries/topRatedSeriesList" model="[topSeriesMap: topSeriesMap]"/>
    </g:else>

    </body>
    </html>
</g:applyLayout>