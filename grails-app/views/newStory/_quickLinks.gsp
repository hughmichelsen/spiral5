<div class="alphabet-list">
    Quick Titles:
    <ul class="pagination">
        <g:each in="${"abcdefghijklmnopqrstuvwxyz".toCharArray()}">
            <span><g:link controller="newStory" action="letter" id="${it}" class="${it == activeLetter ? 'active' : ''}">${it.toUpperCase()}</g:link></span>
        </g:each>
    </ul>
</div>
<div class="alphabet-list">
    Quick Authors:
    <g:each in="${"abcdefghijklmnopqrstuvwxyz".toCharArray()}">
        <span><g:link controller="newAuthor" action="letter" id="${it}" class="${it == activeAuthorLetter ? 'active' : ''}">${it.toUpperCase()}</g:link></span>
    </g:each>
</div>