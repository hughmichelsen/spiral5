<div class="card hidden mystories mb-3">
    <h6 class="card-header authorPanelHeading">Author Admin</h6>
    <div class="card-body">
        <div class="d-flex justify-content-between flex-wrap mb-3">

            <div class="d-flex mr-1">
                <button type="button" class="btn btn-sm btn-info like-count mr-1" data-toggle="popover" data-placement="bottom" title="Story Like Count" data-content="This is the number of likes this story has received from users logged into the site. You, the author, and the site admins, are the only ones that can see this number."></button>

                <button type="button" class="btn btn-sm btn-info comment-count mr-1" data-toggle="popover" data-placement="bottom" title="Comment Count" data-content="This is the number of comments this story has received.">
                    <g:if test="${storyInstance?.commentCount > 0}">
                        <i class="fa fa-comments"></i>
                    </g:if>
                    <g:else>
                        <i class="fa fa-comments-o"></i>
                    </g:else>
                    ${storyInstance?.commentCount}
                </button>

                <button type="button" class="btn btn-sm btn-info total-view-count mr-1" data-toggle="popover" data-placement="bottom" title="Total View Count" data-content="This is the total number of views of this story. You, the author, and the site admins, are the only ones that can see this number."></button>
                <button type="button" class="btn btn-sm btn-info unique-view-count mr-1" data-toggle="popover" data-placement="bottom" title="Unique, Logged In View Count" data-content="This is the number of views by unique, logged in users. You, the author, and the site admins, are the only ones that can see this number."></button>

                <g:link class="" action="edit" controller="newStory" id="${storyInstance.id}"><button type="button" class="btn btn-sm btn-primary" data-toggle="tooltip" title="Edit this story"><i class="fa fa-edit"></i></button></g:link>
            </div>

            <sec:ifAllGranted roles="ROLE_ADMIN">
                <span class="d-flex ml-auto mb-1 mystories hidden align-items-baseline">
                    <g:link action="showSecret" class="admin hidden mr-1" controller="newStory" params="['secretWord': storyInstance.uuid]"><i class="fa fa-eye"></i></g:link>
                    <g:link action="editSecret" class="admin hidden mr-1" controller="newStory" params="['secretWord': storyInstance.uuid]"><i class="fa fa-pencil"></i></g:link>
                &nbsp;
                    <button type="button" class="admin btn btn-sm btn-warning hidden approvedButton mr-1" style="width:2rem;"><i class="fa fa-lock"></i></button>
                    <sec:ifAllGranted roles="ROLE_SUPER_ADMIN">
                        <button type="button" class="admin btn btn-sm btn-warning hidden deleteButton mr-1" style="width:2rem;"><i class="fa fa-trash"></i></button>
                        <button type="button" class="admin btn btn-sm btn-warning hidden disableCommentsButton" style="width:2rem;" data-toggle="tooltip" title="Toggle whether or not comments are enabled on this story"><i class="fa fa-volume-up"></i></button>
                    </sec:ifAllGranted>
                </span>
            </sec:ifAllGranted>
        </div>
        <sec:ifAllGranted roles="ROLE_ADMIN">
            <div class="form-group ${hasErrors(bean: storyInstance, field: 'adminComment', 'has-error')}">
                <label for="adminComment">
                    Admin Comment
                </label>
                <div class="input-group">
                    <textarea rows="2" class="form-control adminComment" id="adminComment" name="adminComment">${storyInstance.adminComment}</textarea>
                    <span class="input-group-append">
                        <button type="submit" class="btn btn-primary adminComment_save" title="Save admin comment">Save <i class="fa fa-save"></i></button>
                    </span>
                </div>
            </div>
        </sec:ifAllGranted>
    </div>
</div>