
<sec:ifAllGranted roles="ROLE_SUPER_ADMIN">
    <div class="dropdown">
        <button type="button" class="btn btn-sm btn-warning dropdown-toggle" data-toggle="dropdown"><i class="fa fa-wrench" aria-hidden="true"></i></button>
        <div class="dropdown-menu dropdown-menu-right">
            <a href="#" class="dropdown-item storyGearTool approveLink">Approve</a>
            <a href="#" class="dropdown-item storyGearTool unapproveLink">Unapprove</a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item storyGearTool deleteLink">Delete</a>
            <a href="#" class="dropdown-item storyGearTool undeleteLink">Undelete</a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item storyGearTool removeFromSeriesLink">Remove from Series</a>
            <a href="#" class="dropdown-item storyGearTool addToSeriesLink">Add to Series</a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item storyGearTool assignToAuthor">Assign to Author</a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item storyGearTool setAdminComment">Change Admin Comment</a>
        </div>
    </div>
</sec:ifAllGranted>


<g:render template="/templates/dialogMessages"/>

<script>
    <asset:script>
    (function ($, window, document, undefined)
    {
        'use strict';

        <sec:ifAllGranted roles="ROLE_SUPER_ADMIN">

        $(function ()
        {
            $.fn.editable.defaults.mode = 'popup';

            // Merge Tag Tokenfield
            var tags;
            $.ajax({
                url: "${createLink(action: 'seriesNames', controller: 'newStory')}",
                type: 'GET',
                async: true,
                success: function (resp) {
                    tags = resp;
                    var engine = new Bloodhound({
                        local: tags,
                        queryTokenizer: Bloodhound.tokenizers.whitespace,
                        datumTokenizer: Bloodhound.tokenizers.whitespace
                    });
                    $('#nameNewSeriesText').typeahead({
                            minLength:2,
                            highlight:true},
                        {
                            source: engine.ttAdapter()
                        }
                    )
                },
                error: function (resp) {
                },
                complete: function () {
                }
            });

            $(".storyGearTool").addClass("disabled");

            function countSelectedCheckboxes() {
                var count = 0;
                $('.selectCheckbox').each(function() {
                    var checked = $(this).is(":checked");
                    if (checked)
                        count++;
                });

                return count;
            }

            function toggleGearTools(checkboxSetting) {
                if (checkboxSetting)
                    $(".storyGearTool").removeClass("disabled");
                else
                    $(".storyGearTool").addClass("disabled");
            }

            $(".masterSelectCheckbox").on("change", function() {
                var count = countSelectedCheckboxes();

                var checkboxSetting = (count == 0);

                $('.selectCheckbox').each(function() {
                    $(this).prop('checked', checkboxSetting);
                });

                toggleGearTools(checkboxSetting);
            });

            $(".selectCheckbox").on("change", function() {
                // figure out how many are currently checked
                var count = countSelectedCheckboxes();

                toggleGearTools(count > 0);
            });

            function getSelectedStoryIds() {
                var ret = [];

                $('.selectCheckbox').each(function() {
                    var checked = $(this).is(":checked");
                    if (checked)
                        ret.push($(this).data('id'));
                });

                return ret;
            }

            function getFirstSelectedAdminComment() {
                let ret = "";

                $('.selectCheckbox').each(function() {
                    if(ret === "") {
                        let checked = $(this).is(":checked");
                        if (checked) {
                            let adminComment = $(this).data('admincomment');
                            if(adminComment != undefined)
                                ret = adminComment;
                        }
                    }
                });

                return ret;
            }

            function getSelectedCheckboxes() {
                var ret = [];

                $('.selectCheckbox').each(function() {
                    var checked = $(this).is(":checked");
                    if (checked)
                        ret.push($(this));
                });

                return ret;
            }

            function handleCheckboxes(link, success, error, data, reload) {
                let ids = getSelectedStoryIds();

                if (ids.length == 0)
                    return;

                let checkboxes = getSelectedCheckboxes();

                data.ids = ids;
                data.reload = reload;

                let i = 0;
                for (i = 0; i < checkboxes.length; i++) {
                    checkboxes[i].attr("disabled", 1);
                }

                $("#divLoading").addClass('show');

                $.ajax({
                    url: link,
                    type: 'GET',
                    data: data,
                    dataType: "json",
                    success: success,
                    error: error,
                    complete: function(result) {
                        let json = result.responseJSON;
                        $("#divLoading").removeClass('show');
                        if(reload)
                            location.reload(true);
                        else {
                            if(json.errorMessage) {
                                showErrorMessage(json.errorMessage);
                            }
                            else if(json.message) {
                                showInfoMessage(json.message);
                            }
                            for (i = 0; i < checkboxes.length; i++) {
                                checkboxes[i].removeAttr("disabled");
                                if(json.success) checkboxes[i].prop('checked', false);
                            }
                        }
                    }
                });
            }

            $(".addToSeriesLink").click(
                function() {
                    var ids = getSelectedStoryIds();
                    if (ids.length == 0)
                        return;

                    $('#changeSeriesCancel').on("click", function() {
                        $("#nameNewSeriesText").val("");
                    });

                    $('#changeSeries').on("click", function(){
                        var link = "${createLink(action: 'addStoriesToSeries', controller: 'newStory')}";

                        var success = function(response){
                        };
                        var error = function(response){
                        };

                        var seriesName = $("#nameNewSeriesText").val();
                        handleCheckboxes(link, success, error, {seriesName: seriesName}, false);
                    });

                    $('#addToSeriesModal').modal({});
                }
            );

            $(".removeFromSeriesLink").click(
                function() {
                    var link = "${createLink(action: 'removeStoriesFromSeries', controller: 'newStory')}";

                    var success = function(response){
                    };
                    var error = function(response){
                    };

                    handleCheckboxes(link, success, error, {}, false);
                }
            );

            $(".assignToAuthor").click(
                function() {
                    var ids = getSelectedStoryIds();
                    if (ids.length == 0)
                        return;

                    $('#assignAuthorCancel').on("click", function() {
                        $("#newAuthorEmailAddress").val("");
                    });

                    $('#assignAuthorGo').on("click", function(){
                        var link = "${createLink(action: 'assignAuthorToStories', controller: 'newStory')}";

                        var success = function(response){
                        };
                        var error = function(response){
                        };

                        var newAuthorEmailAddress = $("#newAuthorEmailAddress").val();
                        handleCheckboxes(link, success, error, {authorEmailAddress: newAuthorEmailAddress}, false);
                    });

                    $('#assignAuthorModal').modal({});
                }
            );

            var adminLink;
            var adminData;
            var doReload;

            function showAdminMessageDlg(Link, Data, headline, message, adminComment, reload) {
                $('#adminMessageHeadline').html(headline);
                $('#adminMessageText').html(message);
                $('#adminMessageComment').val(adminComment);
                adminLink = Link;
                adminData = Data;
                doReload = reload;
                $('#adminMessageDlg').modal({});
            }

            $('#adminMessageOk').click(function(e) {
                var success = function(response){
                };
                var error = function(response){
                };

                var comment = $('#adminMessageComment').val();
                var data = adminData;
                data.comment = comment;

                handleCheckboxes(adminLink, success, error, data, doReload);
            });

            $('#adminMessageCancel').click(function(e) {
            });

            $(".approveLink").click(function(){
                var ids = getSelectedStoryIds();
                var adminComment = getFirstSelectedAdminComment();
                var link = "${createLink(action: 'saveStoriesApproved', controller: 'newStory')}";
                showAdminMessageDlg(link, {approved: true},"Approve Stories","<h5>Do you really want to approve the " + ids.length + " checked stories?</h5>You can give the reason for approving the stories:",adminComment,true);
            });

            $(".unapproveLink").click(function() {
                var ids = getSelectedStoryIds();
                var adminComment = getFirstSelectedAdminComment();
                var link = "${createLink(action: 'saveStoriesApproved', controller: 'newStory')}";
                showAdminMessageDlg(link, {approved: false},"Un-Approve Stories","<h5>Do you really want to un-approve the " + ids.length + " checked stories?</h5>Please give the reason for un-approving the stories:",adminComment,true);
            });

            $(".deleteLink").click(function() {
                var ids = getSelectedStoryIds();
                var adminComment = getFirstSelectedAdminComment();
                var link = "${createLink(action: 'saveStoriesDeleted', controller: 'newStory')}";
                showAdminMessageDlg(link, {deleted: true},"Delete Stories","<h5>Do you really want to delete the " + ids.length + " checked stories?</h5>Please give the reason for deleting the stories:",adminComment,true);
            });

            $(".undeleteLink").click(function() {
                var ids = getSelectedStoryIds();
                var adminComment = getFirstSelectedAdminComment();
                var link = "${createLink(action: 'saveStoriesDeleted', controller: 'newStory')}";
                showAdminMessageDlg(link, {deleted: false},"Un-Delete Stories","<h5>Do you really want to un-delete the " + ids.length + " checked stories?</h5>Please give the reason for un-deleting the stories:",adminComment,true);
            });

            $(".setAdminComment").click(function() {
                var ids = getSelectedStoryIds();
                var adminComment = getFirstSelectedAdminComment();
                var link = "${createLink(action: 'saveStoriesAdminComment', controller: 'newStory')}";
                showAdminMessageDlg(link, {},"Change Admin Comment","<h5>Do you want to change the admin comment on the " + ids.length + " checked stories?</h5>New comment:",adminComment,false);
            });
        });

        </sec:ifAllGranted>

    })(jQuery, window, document);
    </asset:script>
</script>
