<%@ page import="grails.plugin.springsecurity.SpringSecurityUtils; com.ncmc4.Rating" %>
<g:applyLayout name="newMain" model="[active:'stories']" >
    <html>
    <head>
        <title><g:pageTitle title='${storyInstance?.name}'/></title>

        <asset:stylesheet href="tagsWidget.css"/>
        <asset:stylesheet href="story.css"/>
        <asset:stylesheet href="star-rating.css"/>
        <asset:stylesheet href="inscrybmde.css"/>
    </head>
    <body>
    <div id="topOfPage"></div>

    <g:if test="${isReview}">
        <div class="reviewer-arrows">
            <div class="text-center center-block">
                Changes<br/>
                <a id="review-prev" class="btn btn-primary-outline"><i class="fa fa-caret-up" aria-hidden="true"></i></a>
                <a id="review-next" class="btn btn-primary-outline"><i class="fa fa-caret-down" aria-hidden="true"></i></a>
            </div>
        </div>
    </g:if>

    <div class="row no-gutters">
        <div class="col">
            <section id="container">
                <h2 class="hidden">${storyInstance?.name}</h2>

                <div class='alert approvalSections ${isShowingOlderVersion ? "alert-warning" : "alert-danger"} ${storyInstance.approved ? "hidden" : ""}'>
                    <g:if test="${isReview}">
                        <g:if test="${storyInstance.previouslyApproved}">
                            <strong>This story has been edited and requires administrator approval again.</strong>
                            <br/>Until then, the last approved version of the story will still be publicly visible.
                        </g:if>
                        <g:else>
                            <strong>This story is pending approval and is not live, it also has been edited since it was submitted.</strong>
                        </g:else>
                        <br/>Click the arrows along the bottom of the screen to navigate between the changes under review.
                    </g:if>
                    <g:elseif test="${isShowingOlderVersion}">
                        <strong>This story has been edited by the author, but the edit is still pending approval and is not live.</strong>
                        <br/>Showing the latest approved version of the story.
                    </g:elseif>
                    <g:else>
                        <strong>This story is pending approval and is not live.</strong>
                    </g:else>
                </div>

                <g:render template="showAdminButtons" model="[storyInstance: storyInstance]" />

                <g:render template="ratingsStatsPanel"  model="[storyInstance: storyInstance]" />

                <div class="main_content">

                    <article id="story" class="">
                        <g:if test="${storyInstance?.sponsored}">
                            <div class="alert alert-danger alert-animated" role="alert">
                                <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                                <span class="sr-only">Error:</span>
                                This is a sponsored story teaser. It is incomplete, and contains a link to the site where you can purchase the rest. If you'd prefer not to read further, please don't.
                            </div>
                        </g:if>

                        <h3>
                            <g:if test="${isReview && (archive?.oldName != storyInstance?.name)}">
                                <span class='revision revision-removed'>${archive?.oldName}</span>
                                <span class='revision revision-inserted'>${storyInstance?.name}</span>
                            </g:if>
                            <g:elseif test="${isShowingOlderVersion}">
                                ${archive?.oldName}
                            </g:elseif>
                            <g:else>
                                ${storyInstance?.name}
                            </g:else>
                        </h3>

                        <g:render template="seriesNavigator" model="[storyInstance: storyInstance]" />

                        <h5>
                            <small class="font-italic">By</small>
                                <g:link controller="newAuthor" action="show" id="${storyInstance?.author?.id}">
                                    <g:if test="${isReview && (archive?.oldAuthorName != storyInstance?.author?.displayName)}">
                                        <span class='revision revision-removed'>${archive?.oldAuthorName}</span>
                                        <span class='revision revision-inserted'>${storyInstance?.author?.displayName}</span>
                                    </g:if>
                                    <g:else>
                                        ${storyInstance?.author?.displayName}</g:else></g:link>
                                <g:userIcon userid="${storyInstance.owners[0]?.id}"/>
                            <small class="font-italic">published</small> <g:formatDate type="date" style="long" date="${storyInstance.savePublishDate}" />
                            <span class="pull-right">
                                <button type="button" class="btn btn-sm btn-primary like-button hidden" data-toggle="tooltip" title="Put this story on your favourite list."></button>

                                <a href="javascript:" rel="nofollow" role="button" class="scrollToComments">
                                    <button type="button" class="btn btn-sm btn-primary comment-count" title="Jump to the first comment for the story.">
                                        <g:if test="${storyInstance?.commentCount > 0}">
                                            <i class="fa fa-comments"></i> ${storyInstance?.commentCount}
                                        </g:if>
                                        <g:else>
                                            <i class="fa fa-comments-o"></i>
                                        </g:else>
                                    </button>
                                </a>
                            </span>
                        </h5>

                        <div class="d-flex align-items-center justify-content-between w-100 mb-5">
                            <div class="">
                                <g:if test="${storyInstance.showEmail && storyInstance.emailAddress?.length() > 0}">
                                    <h5 id="emailAddress"><small><a href="mailto:${storyInstance.emailAddress}">${storyInstance.emailAddress}</a></small></h5>
                                </g:if>
                                <g:else>
                                    <sec:ifAnyGranted roles="ROLE_SUPER_ADMIN">
                                        <h5 id="emailAddress"><small><a href="mailto:${storyInstance.emailAddress}">${storyInstance.emailAddress}</a> (hidden to public!)</small></h5>
                                    </sec:ifAnyGranted>
                                </g:else>

                                <small class="existing-tags">
                                    <g:render template="tags"  model="[storyInstance: storyInstance]"/>
                                </small>
                                <g:if test="${!storyInstance.anonymous && !storyInstance.hideCopyright}">
                                    <div class="copyright mt-1">
                                        Copyright © ${storyInstance.copyrightYear} ${storyInstance?.author?.displayName}. All rights reserved. This story may not be reproduced or used in any manner whatsoever without the express written permission of the author.
                                    </div>
                                </g:if>
                            </div>

                            <div class="d-flex flex-wrap flex-lg-nowrap align-items-center justify-content-center">
                                <g:each in="${storyInstance?.getRatingMarker()}" status="r" var="rating">
                                    <img class="storyRatingIcon-${r} storyRating${rating.effect}-${r}" src="${assetPath(src: Rating.getRatingIconMap()[rating?.ratName])}" data-toggle="tooltip" title="${rating.toolTip}"/>
                                </g:each>
                            </div>
                        </div>

                        <div class='alert alert-secondary mb-5 mx-3'>
                            <h5>Summary</h5>
                            ${raw(summary)}
                        </div>

                        <g:if test="${storyInstance?.coverImageLink}">
                            <img class="sponsored" src="${storyInstance?.coverImageLink}">
                        </g:if>

                        <div id="storyDisplay" class="storyText">
                            ${raw(storyText)}
                        </div>

                        <g:if test="${storyInstance?.endTeaser}" >
                            <br/><br/>
                            <p><b>${raw(storyInstance?.endTeaserFormatted)}</b></p>
                        </g:if>

                        <g:if test="${storyInstance?.amazonBookId}" >
                            <div class="spacer20"></div>
                            <p>
                                Buy the complete story on Amazon here:
                                <span><a class="btn btn-md btn-info" href="http://www.amazon.com/dp/${storyInstance.amazonBookId}?tag=gayspiral-20#title">US</a></span>
                                <span><a class="btn btn-md btn-info" href="http://www.amazon.ca/dp/${storyInstance.amazonBookId}#title">Canada</a></span>
                                <span><a class="btn btn-md btn-info" href="http://www.amazon.co.uk/dp/${storyInstance.amazonBookId}#title">UK</a></span>
                                <span><a class="btn btn-md btn-info" href="http://www.amazon.de/dp/${storyInstance.amazonBookId}#title">Germany</a></span>
                                <span><a class="btn btn-md btn-info" href="http://www.amazon.com.au/dp/${storyInstance.amazonBookId}#title">Australia</a></span>
                            </p>
                            <div class="spacer20"></div>
                        </g:if>

                        <g:render template="seriesNavigator" model="[storyInstance: storyInstance]" />

                    </article>

                    <div class="text-center">
                        <button type="button" class="btn btn-primary like-button hidden" data-toggle="tooltip" title="Put this story on your favourite list."></button>
                    </div>
                </div>
            </section>
        </div>
    </div>

    <a id="comments"></a>

    <g:render template="ratingsPanel"  model="[storyInstance: storyInstance]" />

    <div class="row mt-3 mb-3">
        <div class="col">
            <div class="card ${storyInstance?.category == null || storyInstance.tags.size() < 3 ? 'border-warning' : ''}">
                <!--<h6 class="card-header ${storyInstance?.category == null || storyInstance.tags.size() < 3 ? 'text-warning' : ''}">Category and Tags</h6>-->
                <div class="card-body">

                    <div class="existing-tags mb-3">
                        <g:render template="tags" model="[storyInstance: storyInstance]"/>
                    </div>

                    <g:if test="${storyInstance?.category == null}">
                        <div class="mt-2 mb-3">
                            <form id="category_submission" class="form mt-3">
                                <label for="category" class="text-warning">
                                    Category missing! Please help other readers by setting the proper category.
                                </label>
                                <div id="category-div" class="input-group ${hasErrors(bean: storyInstance, field: 'category', 'has-error')} required">
                                    <g:select class="form-control custom-select" id="category" name="category" required="" from="${categories}" optionValue="text" optionKey="tagId" value="${storyInstance?.category?.id}"/>
                                    <span class="input-group-append">
                                        <button type="submit" class="save btn btn-primary" title="Save updated category">Update <i class="fa fa-save"></i></button>
                                    </span>
                                </div>
                                <div class="text-danger"><g:fieldError bean="${storyInstance}" field="category"/></div>
                            </form>
                        </div>
                    </g:if>

                    <div class="mt-2">
                        <form id="tag_submission" class="form">
                            <div class="form-group tagAuthor">
                                <label for="tagAuthor">
                                    Author
                                </label>
                                <g:textField class="form-control" name="tagAuthor" id="tagAuthor"/>
                            </div>

                            <label for="newTags" data-toggle="tooltip" data-placement="right" data-html="true" title="<p>Everyone can add or modify tags!</p><p>3 tags required!</p>Use the button on the left to get a list of all known tags.">
                                <g:if test="${storyInstance.tags.size() < 3}">
                                    <span class="text-warning">Insufficient number of tags, please add more!</span>
                                </g:if>
                                <g:else>
                                    Modify Tags
                                </g:else>
                                <i class="fa fa-info-circle" aria-hidden="true"></i>
                            </label>
                            <div class="input-group tagTag ${hasErrors(bean: storyInstance, field: 'tags', 'has-error')}">
                                <span class="input-group-prepend">
                                    <button class="btn btn-primary" id="btnShowTags" type="button"><i class="fa fa-list" title="Select tags from list"></i></button>
                                </span>
                                <input type="text" class="form-control" name="newTags" id="newTags" value="${storyInstance.tags.join(',')}" placeholder="Press comma for each new tag, click x to delete a tag, click 'Update' to save changes."/>
                                <span class="input-group-append">
                                    <button type="submit" class="save btn btn-primary" title="Save updated list of tags">Update <i class="fa fa-save"></i></button>
                                </span>
                            </div>
                            <div class="text-danger"><g:fieldError bean="${storyInstance}" field="tags"/></div>
                            <span id="tagAddWarning" class="tagAddWarning">You've created tags exclusively for this story! Please avoid exclusive tags!</span>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <g:render template="showAdminButtons" model="[storyInstance: storyInstance]" />

    <g:render template="ratingsStatsPanel"  model="[storyInstance: storyInstance]" />

    <div class="row mt-5 mb-2">
        <div class="col">
            <a href="#" class="btn pulsate btn-primary ${disableNewComments ? 'hidden' : ''}" id="newCommentButton">New Comment <i class="fa fa-reply"></i></a>
            %{--<a class="btn btn-md btn-primary" id="newTagButton"><span class="collapse-on-small">New Tags </span><i class="fa fa-tags"></i></a>--}%
            <span class="pull-right">
                <button type="button" class="btn btn-primary like-button-small hidden" data-toggle="tooltip" title="Put this story on your favourite list."></button>
                <a class="btn btn-primary scrollToBottom" href="#"><i class="fa fa-angle-double-down"></i></a>
                <a class="btn btn-primary scrollToTop" href="#"><i class="fa fa-level-up"></i></a>
            </span>
        </div>
    </div>

    <div class="row mb-3">
        <div class="col">
            <section>
                <div id="comment-list" class="spiral-spinner">
                    %{--<g:render template="comment" collection="${comments}" var="comment"/>--}%
                </div>

                <div>
                    <h2 class="hidden">New Comment</h2>

                    <form id="comment_submission" action="" class="form newComments ${disableNewComments ? 'hidden' : ''}">
                        <div class="spacer15"></div>

                        <div class="form-group name-row">
                            <label for="commentAuthor">
                                Name (optional)
                            </label>
                            <g:textField class="form-control" name="commentAuthor" id="commentAuthor"/>
                        </div>
                        <div class="form-group comment-row">
                            <div class="d-flex justify-content-between">
                                <label for="commentText">
                                    Comment
                                </label>
                                <div class="custom-checkbox custom-control fieldcontain">
                                    <g:checkBox class="custom-control-input" id="extendedEditor" name="extendedEditor"/>
                                    <label for="extendedEditor" class="custom-control-label" >Extended editor</label>
                                </div>
                            </div>
                            <g:textArea rows="9" class="form-control" name="commentText" id="commentText"/>
                        </div>
                        <div class="form-group password-row">
                            <label for="passwordText">
                                Password
                            </label>
                            <g:textField class="form-control" name="passwordText" id="passwordText"/>
                        </div>
                        <div class="form-group family-row">
                            <label for="familyText">
                                Family
                            </label>
                            <g:textField class="form-control" name="familyText" id="familyText"/>
                        </div>

                        <button type="submit" id="addCommentButton" class="save btn btn-primary"><i class="fa fa-comment"></i> Add Comment</button>
                        <span class="pull-right">
                            <a class="btn btn-primary scrollToComments" href="javascript:" rel="nofollow" role="button"><i class="fa fa-angle-double-up"></i></a> <a class="btn btn-primary scrollToTop" href="javascript:" rel="nofollow" role="button"><i class="fa fa-level-up"></i></a>
                        </span>
                    </form>
                </div>
            </section>
        </div>
    </div>

    <div id="bottomOfPageLink"></div>

    <!-- Modal -->

    <g:render template="/templates/tagSelectDialog" model="['dlgTagListName':'dlgTagList']"/>

    <div id="sendEmailModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body">
                    <p>Should all notification emails be sent about this story?</p>
                </div>
                <div class="modal-footer">
                    <button id="sendApprovedEmailNo" type="button" class="btn" data-dismiss="modal">No</button>
                    <button id="sendApprovedEmailYes" type="button" class="btn btn-default" data-dismiss="modal">Yes</button>
                </div>
            </div>

        </div>
    </div>

    <g:render template="/templates/dialogMessages"/>

    <content tag="scripts">

        <asset:javascript src="star-rating.js"/>
        <asset:javascript src="inscrybmde.js"/>

        <script>

            <asset:script>

                (function ($, window, document, undefined)
                {
                    'use strict';

                    var adminMessageDlgOk;
                    var adminMessageDlgCancel;
                    var storyId;
                    var adminCommentSet = null;

                    function showAdminMessageDlg(id, headline, message, adminComment, onOk, onCancel) {
                        $('#adminMessageHeadline').html(headline);
                        $('#adminMessageText').html(message);
                        $('#adminMessageComment').val(adminComment);
                        adminCommentSet = null;
                        adminMessageDlgOk = onOk;
                        adminMessageDlgCancel = onCancel;
                        storyId = id;
                        $('#adminMessageDlg').modal({});
                    }

                    $('#adminMessageOk').click(function(e) {
                        var fkt = adminMessageDlgOk;
                        adminMessageDlgOk = null;

                        adminCommentSet = $('#adminMessageComment').val();

                        if(fkt != null)
                            fkt(e);
                    });

                    $('#adminMessageCancel').click(function(e) {
                        var fkt = adminMessageDlgCancel;
                        adminMessageDlgCancel = null;
                        if(fkt != null)
                            fkt(e);
                    });

                    $(function ()
                    {
                        <g:render template="/templates/tagScripts" model="['allowNew': true, 'selector': '#newTags', 'newTagSelector': '#tagAddWarning', 'thisStory': storyInstance.id, 'dlgTagListName': '#dlgTagList', 'btnShowTagList': '#btnShowTags']"/>

                        function updateRating(event, value) {
                            if(event.target.id != null)
                            {
                                var data = {
                                    'id': '${storyInstance.id}',
                                    'ratingId': event.target.id,
                                    'rating': value
                                };

                                $.ajax({
                                    url: "${createLink(action: 'changeRating', controller: 'newStory')}",
                                    type: 'POST',
                                    async: true,
                                    global: false,
                                    timeout: 2000,
                                    data: data
                                });
                            }
                        }

                        $('.story_rating').each( function() {
                            var empty = $(this).data('empty');
                            var full = $(this).data('full');
                            $(this).rating({
                                min: 0, max: 5, step: 0.5, stars: 5, size: 'xx',
                                showClear: true,
                                showCaption: false,
                                readonly: ${isAuthor},
                                clearButton: "<i class='ratingclear fa fa-minus-square'/>",
                                emptyStar: "<img class='ratingstar empty' src='" + empty + "'/>",
                                filledStar: "<img class='ratingstar' src='" + full + "'/>"})
                        }).on('rating:change', function(event, value, caption) {
                            updateRating(event,value);
                        }).on('rating:clear', function(event) {
                            updateRating(event,-1);
                        });

                        $('#newCommentButton').on("click", function addNewComment(e) {
                            if ($(e.target).hasClass("pulsate")) {
                                $(e.target).removeClass("pulsate");
                            }

                            e.preventDefault();

                            if(inscrybmde.codemirror !== undefined)
                                inscrybmde.codemirror.focus();
                            else
                                $("#commentText").focus();

                            $('html, body').animate({
                                scrollTop: $("#comment_submission").offset().top
                            }, 500);
                        });

                        $(".scrollToComments").click(function() {
                            $([document.documentElement, document.body]).animate({
                                scrollTop: $("#comments").offset().top
                            }, 1000);
                        });

                        $(".scrollToTop").click(function() {
                            $([document.documentElement, document.body]).animate({
                                scrollTop: $("#topOfPage").offset().top
                            }, 1000);
                        });

                        $(".scrollToBottom").click(function() {
                            $([document.documentElement, document.body]).animate({
                                scrollTop: $("#bottomOfPageLink").offset().top
                            }, 1000);
                        });

                        $("#tag_submission").submit(
                            function(e) {
                                e.preventDefault();

                                var tagAuthor = $("#tagAuthor").val();
                                var newTags = $('#newTags').val();

                                var data = {
                                    'tagAuthor': tagAuthor,
                                    'id': '${storyInstance.id}',
                                    'newTags': newTags
                                };

                                $.ajax({
                                    url: "${createLink(action: 'editTags')}",
                                    type: 'POST',
                                    data: data,
                                    success: function(response) {
                                        $(".existing-tags").html(response);
                                    },
                                    failure: function(response) {
                                        $(".existing-tags").html(response);
                                    }
                                });
                            }
                        );

                        $("#category_submission").submit(
                            function(e) {
                                e.preventDefault();

                                var catElem = $('#category');
                                var category = catElem.val();

                                var data = {
                                    'id': '${storyInstance.id}',
                                    'category': category
                                };

                                $.ajax({
                                    url: "${createLink(action: 'editCategory')}",
                                    type: 'POST',
                                    data: data,
                                    success: function(response) {
                                        $(".existing-tags").html(response);
                                    },
                                    failure: function(response) {
                                            $(".existing-tags").html(response);
                                    }
                                });
                            }
                        );

                        $(".adminComment_save").click(
                            function(e) {
                                var cmtElem = $(this).parent().siblings(".adminComment");
                                var comment = cmtElem.val();

                                var data = {
                                    'id': ${storyInstance.id},
                                    'adminComment': comment
                                };

                                $.ajax({
                                    url: "${createLink(controller: 'newStory', action: 'saveAdminComment')}",
                                    type: 'POST',
                                    dataType: "json",
                                    data: data,
                                    success: function(json) {
                                        if(json.errorMessage) {
                                            showErrorMessage(json.errorMessage);
                                        }
                                        else {
                                            $(".adminComment").val(json.adminComment)
                                        }
                                    },
                                    failure: function(response) {
                                        showErrorMessage(response);
                                    }
                                });
                            }
                        );

                        $(".password-row").remove();

                        $("#comment_submission").submit(
                            function(e) {
                                e.preventDefault();

                                //window.alert("Submit!!!");
                                //var data = $('#comment_submission').serialize();

                                var commentAuthor = $("#commentAuthor").val();
                                var commentText;
                                if(inscrybmde.codemirror !== undefined)
                                        commentText = inscrybmde.value();
                                    else
                                        commentText = $("#commentText").val();
                                var familyText = $("#familyText").val();
                                var passwordText = $("#passwordText").val();

                                if ((commentText == null) || (commentText.length == 0))
                                {
                                    // no comment to add
                                    return;
                                }


                                var data = {
                                    'commentText': commentText,
                                    'storyId': '${storyInstance.id}',
                                    'family': familyText,
                                    'password': passwordText
                                };

                                if ((commentAuthor != null) && (commentAuthor.length > 0))
                                {
                                    data['commentAuthor'] = commentAuthor;
                                }

                                $.ajax({
                                    url: "${createLink(action: 'addNewComment')}",
                                    type: 'POST',
                                    data: data,
                                    dataType: 'json',
                                    success: function(response) {
                                        var commentHtml = response.commentHtml;
                                        var admin = response.admin;
                                        var commentId = response.commentId;

                                        $(commentHtml).hide().appendTo("#comment-list").fadeIn("slow", function(){
                                            addDeleteCommentButtons();

                                            $(".deleteCommentButton").removeClass("hidden");
                                        });

                                        var commentCount = response.commentCount;
                                        if (commentCount > 0)
                                            $(".comment-count").html("<i class='fa fa-comments'></i> " + commentCount);

                                        $("#commentText").val("");
                                        if(inscrybmde.codemirror !== undefined)
                                            inscrybmde.value("");
                                    }
                                });
                            }

                        );


                        // WYSIWYG Editor

                        var showExtendedEditor = localStorage.getItem("showExtendedEditor");
                        if(showExtendedEditor === null)
                            showExtendedEditor = "1";

                        $('#extendedEditor').prop("checked", showExtendedEditor === "1");

                        var inscrybmde = new InscrybMDE({
                            element: $("#commentText")[0],
                            minHeight: "200px",
                            showIcons: ["bold", "italic", "quote", "|", "link", "|", "preview"],
                            hideIcons: ["unordered-list", "ordered-list", "table", "side-by-side", "fullscreen", "guide", "code", "heading", "table", "image", "side-by-side"],
                            renderingConfig: {singleLineBreaks: false},
                            promptURLs: true,
                            shortcuts: {
                                "toggleOrderedList": null,
                                "toggleUnorderedList": null,
                                "drawTable": null,
                                "toggleSideBySide": null,
                                "toggleFullScreen": null
                            },
                            inputStyle: 'contenteditable',
                            autoRender: showExtendedEditor === "1"
                        });

                        $('#extendedEditor').change(function(e){
                            var sel = $(e.target).is(":checked");
                            if(sel) {
                                inscrybmde.toEditor();
                                showExtendedEditor = "1";
                            }
                            else {
                                inscrybmde.toTextArea();
                                showExtendedEditor = "0";
                            }
                            localStorage.setItem("showExtendedEditor",showExtendedEditor);
                        });
                    });

                    function fetchRatings() {
                        var data = {'id': '${storyInstance.id}'};

                        $.ajax({
                            url: "${createLink(action: (isAuthor ? 'ratingResults' : 'ratings'), controller: 'newStory')}",
                            type: 'POST',
                            cache: false,
                            data: data,
                            global: false,
                            dataType: 'json',
                            success: function(response) {
                                <g:each var="rating" in="${Rating.getRatingList()}">
                                    $('#${rating}').rating('update',response.${rating});
                                </g:each>
                            }, error: function(jqXHR, textStatus, errorThrown) {
                                // error?
                                console.log("Error: " + textStatus);
                            }

                        });
                    }

                    function ratingLine(stats, rating)
                    {
                        return "<td data-toggle='tooltip' data-html='true' title='" +
                            "<h6>&rsquo;" + rating + "&rsquo; Ratings</h6>" +
                            "<table class=\"w-100\"><tr><th/><th>Users</th><th>Anon</th><th>All</th></tr>" +
                            "<tr><td>Ratings</td><td>" + stats.numNamed + "</td><td>" + stats.numAnon + "</td><td>" + stats.numAll + "</td></tr>" +
                            "<tr><td>Average</td><td>" + stats.avgNamed + "</td><td>" + stats.avgAnon + "</td><td>" + stats.avgAll + "</td></tr>" +
                            "<tr><td>Median</td><td>" + stats.medianNamed + "</td><td>" + stats.medianAnon + "</td><td>" + stats.medianAll + "</td></tr>" +
                            "<tr><td>Variance</td><td>" + stats.varNamed + "</td><td>" + stats.varAnon + "</td><td>" + stats.varAll + "</td></tr></table>'>" +
                            stats.avgAll + "</td>\n"
                    }

                    function rankLine(rank, type, rating)
                    {
                        if(rank == 0) {
                            return '<td data-toggle="tooltip" title="Not enough ratings for a ranking!">-</td>\n';
                        }
                        else
                            var title;
                            if(type === "overall")
                                title = "This story&rsquo;s overall rank for &rsquo;" + rating + "&rsquo; among all stories is " + rank;
                               else
                                title = "This story&rsquo;s rank for &rsquo;" + rating + "&rsquo; in the " + type + " of its release is " + rank;

                            return "<td data-toggle='tooltip' title='" + title + "'>" + rank + "</td>\n";
                    }

                    function fetchRatingStats() {
                        <sec:ifAnyGranted roles="ROLE_ADMIN, ROLE_AUTHOR">
                        <g:if test="${ratingsVisible}">
                        var data = {'id': '${storyInstance.id}'};

                        $.ajax({
                            url: "${createLink(action: 'ratingStats', controller: 'newStory')}",
                            type: 'POST',
                            cache: false,
                            global: false,
                            data: data,
                            dataType: 'json',
                            success: function(response) {
                                var stats;
                                <g:each var="rating" in="${Rating.getRatingList()}">
                                    stats = response.${rating};
                                    if(stats)
                                        $('.statisticTable').append("<tr>" +
                                            '<td><asset:image class="ratinglabel" src="${Rating.getRatingIconMap()[rating]}" data-toggle="tooltip" data-placement="top" title="${Rating.getRatingLabelMap()[rating]}"/></td>\n' +
                                            ratingLine(stats, '${Rating.getRatingLabelMap()[rating]}') +
                                            rankLine(stats.rankMonth, 'month', '${Rating.getRatingLabelMap()[rating]}') +
                                            rankLine(stats.rankYear, 'year', '${Rating.getRatingLabelMap()[rating]}') +
                                            rankLine(stats.rankOverall, 'overall', '${Rating.getRatingLabelMap()[rating]}'))
                                    else
                                        $('.statisticTable').append("<tr>"+
                                                    '<td><asset:image class="ratinglabel" src="${Rating.getRatingIconMap()[rating]}" data-toggle="tooltip" data-placement="left" title="${Rating.getRatingLabelMap()[rating]}"/></td>' +
                                                    "<td colspan='4'>No ratings yet!</td>")
                                </g:each>
                                $('.statistics').slideDown({
                                    complete: function() {
                                        $('.statistics').find('[data-toggle="tooltip"]').tooltip();
                                    }
                                });
                            }, error: function(jqXHR, textStatus, errorThrown) {
                                // error?
                                console.log("Error: " + textStatus);
                            }
                        });
                        </g:if>
                        </sec:ifAnyGranted>
                    }

                    function deleteComment(id) {
                        var data = {'id': id};

                        $.ajax({
                            url: "${createLink(action: 'deleteComment', controller: 'newComment')}",
                            type: 'GET',
                            data: data,
                            success: function(response) {
                                if (response == 'empty') {
                                } else {
                                    var divId = "#comment-" + id;

                                    $(divId).hide('slow', function(){ $(divId).remove(); });
                                };
                            }
                        });
                    }

                    function addDeleteCommentButtons() {
                        $(".deleteCommentButton:not(.deleteCommentInitializedAlready)").click(
                                function() {
                                    var id = $(this).data('id');
                                    deleteComment(id);
                                }
                        );

                        $(".deleteCommentButton:not(.deleteCommentInitializedAlready)").addClass("deleteCommentInitializedAlready");
                    }

                    function getHtmlForLikeButton(enabled, liked, small) {
                        var ret = "";

                        if (enabled) {
                            if (liked)
                            {
                                ret = "<i class='fa fa-thumbs-up'></i>";
                            } else {
                                ret = "<i class='fa fa-thumbs-o-up'></i>";
                            }
                        }

                        if(small) {
                            ret += " Like";
                        } else {
                            ret += " I Like this story";
                        }

                        return ret
                    }

                    function fixLikeButton(enabled, liked) {
                        var isSmall = true;
                        var html = getHtmlForLikeButton(enabled, liked, !isSmall);
                        var smallHtml = getHtmlForLikeButton(enabled, liked, isSmall);

                        var likeButtons = $(".like-button");
                        likeButtons.html(html);
                        likeButtons.data("liked", liked);
                        likeButtons.removeClass("hidden");

                        var smallLikeButtons = $(".like-button-small");
                        smallLikeButtons.html(smallHtml);
                        smallLikeButtons.data("liked", liked);
                        smallLikeButtons.removeClass("hidden");
                    }

                    function likeButtonClick() {
                        var liked = $(this).data("liked");

                        var newLiked = true;
                        if (liked)
                            newLiked = false;

                        // send it over now
                        var data = {'id': '${storyInstance.id}', 'liked': newLiked};

                        $.ajax({
                            url: "${createLink(action: 'toggleLike', controller: 'newStory')}",
                            type: 'GET',
                            data: data,
                            dataType: 'json',
                            success: function(response) {
                                fixLikeButton(true, newLiked);
                            }, error: function(jqXHR, textStatus, errorThrown) {
                                // error?
                            }
                        });
                    }

                    function fetchStoryAuthorData() {
                        var data = {'id': '${storyInstance.id}'};

                        $.ajax({
                            url: "${createLink(action: 'fetchStoryAuthorData', controller: 'newStory')}",
                            type: 'GET',
                            data: data,
                            dataType: 'json',
                            success: function(response) {
                                var storyLikes = response.storyLikes;
                                var totalViewCount = response.totalViewCount;
                                var uniqueViewCount = response.uniqueViewCount;

                                $(".like-count").html("<i class='fa fa-thumbs-up'></i> " + storyLikes);

                                $('.total-view-count').html("<i class='fa fa-eye'></i> " + totalViewCount);
                                $('.unique-view-count').html("<i class='fa fa-eye-slash'></i> " + uniqueViewCount);

                            }, error: function(jqXHR, textStatus, errorThrown) {
                                // error?
                                console.log("Error: " + textStatus);
                            }
                        });
                    }

                    function reenableNewCommentButtons()
                    {
                        $("#newCommentButton").removeClass('hidden');
                        $("#comment_submission").removeClass('hidden');
                    }

                    function initDisableCommentsButton(disableCommentsValue) {

                        function setDisableCommentsButton(disableCommentsValue) {
                            var disableCommentsHtml = disableCommentsValue ? "<i class='fa fa-volume-off' style='color: red'></i>" : "<i class='fa fa-volume-up'></i>"
                            $('.disableCommentsButton').html(disableCommentsHtml);
                            $('.disableCommentsButton').data('disableComments', disableCommentsValue);
                        }

                        setDisableCommentsButton(disableCommentsValue);

                        function updateDisableCommentsButton(disableCommentsValue) {
                            var data = {
                                'id': '${storyInstance.id}',
                                'disableComments': disableCommentsValue
                            };

                            $.ajax({
                                url: "${createLink(action: 'saveDisableComments')}",
                                type: 'POST',
                                data: data,
                                success: function(response) {
                                    setDisableCommentsButton(disableCommentsValue);
                                },
                                error: function(response) {
                                    setDisableCommentsButton(!disableCommentsValue);
                                },
                                complete: function() {
                                    $('.disableCommentsButton').removeAttr("disabled");
                                }
                            });
                        }

                        $('.disableCommentsButton').on("click", function() {
                            $('.disableCommentsButton').attr("disabled", 1);

                            var disableCommentsValue = !$(this).data("disableComments");

                            updateDisableCommentsButton(disableCommentsValue);
                        });
                    }

                    function initApprovedButton(approved) {

                        function setApprovedButton(approvedValue) {
                            var approvedHtml = approvedValue ? "<i class='fa fa-unlock'></i>" : "<i class='fa fa-lock' style='color: red'></i>"
                            $('.approvedButton').html(approvedHtml);
                            $('.approvedButton').data('approved', approvedValue);

                            if (!approvedValue) {
                                $('.approvalSections').fadeIn("slow");
                            } else {
                                $('.approvalSections').fadeOut("slow");
                            }
                        }

                        setApprovedButton(approved);

                        function updateApproved(approved, sendEmail) {
                            var data = {
                                'id': '${storyInstance.id}',
                                'approved': approved,
                                'sendEmail': sendEmail
                            };

                            if(adminCommentSet)
                                data.comment = adminCommentSet;
                            adminCommentSet = null;

                            $.ajax({
                                url: "${createLink(action: 'saveApproved')}",
                                type: 'POST',
                                dataType: "json",
                                data: data,
                                success: function(json) {
                                    if(json.errorMessage) {
                                        setApprovedButton(!approved);
                                        showErrorMessage(json.errorMessage);
                                    }
                                    else {
                                        setApprovedButton(approved);
                                        if(json.adminComment != null)
                                            $(".adminComment").val(json.adminComment);
                                    }
                                },
                                error: function(json) {
                                    setApprovedButton(!approved);
                                },
                                complete: function() {
                                    $('.approvedButton').removeAttr("disabled");
                                }
                            });
                        }

                        $('#sendApprovedEmailNo').on("click", function(){updateApproved(true, false);});
                        $('#sendApprovedEmailYes').on("click", function(){updateApproved(true, true);});

                        $('.approvedButton').on("click", function() {
                            $('.approvedButton').attr("disabled", 1);

                            var approved = $(this).data("approved");
                            var adminComment = $('.adminComment').filter(":first").val();

                            var cancelApprove = function() {
                                $('.approvedButton').removeAttr("disabled");
                            };

                            if (!approved) {
                                var doApprove = function() {
                                    if(${! storyInstance.previouslyApproved}) {
                                        $('#sendEmailModal').modal({});
                                    }
                                    else
                                        updateApproved(true, false);
                                };
                                if(adminComment != null && adminComment.length > 0) {
                                    showAdminMessageDlg(${storyInstance.id},"Approve Story","<h5>Do you want to approve this story?</h5>Please note and possibly change the following comment on this story:",adminComment,doApprove,cancelApprove);
                                }
                                else {
                                    doApprove();
                                }
                            } else {
                                showAdminMessageDlg(${storyInstance.id},"Un-Approve Story","<h5>Do you want to un-approve this story?</h5>Please add a reason for your decision:",adminComment,function() { updateApproved(false, false) },cancelApprove);
                            }
                        });
                    }

                    function initDeleteButton(deleted) {

                        function setDeleteButton(deletedValue) {
                            var deletedHtml = deletedValue ? "<i class='fa fa-trash' style='color: red'></i>" : "<i class='fa fa-trash-o'></i>"
                            $('.deleteButton').html(deletedHtml);
                            $('.deleteButton').data('deleted', deletedValue);
                        }

                        setDeleteButton(deleted);

                        function updateDeleted(deleted) {
                            var data = {
                                'id': '${storyInstance.id}',
                                'deleted': deleted
                            };

                            if(adminCommentSet)
                                data.comment = adminCommentSet;
                            adminCommentSet = null;

                            $.ajax({
                                url: "${createLink(action: 'saveDeleted')}",
                                type: 'POST',
                                dataType: "json",
                                data: data,
                                success: function(json) {
                                    if(json.errorMessage) {
                                        setDeleteButton(! deleted);
                                        showErrorMessage(json.errorMessage);
                                    }
                                    else {
                                        setDeleteButton(deleted);
                                        if(json.adminComment != null)
                                            $(".adminComment").val(json.adminComment);
                                    }
                                },
                                error: function(json) {
                                    setDeleteButton(!deleted);
                                },
                                complete: function() {
                                    $('.deleteButton').removeAttr("disabled");
                                }
                            });
                        }

                        $('.deleteButton').on("click", function () {
                            var deleted = $(this).data("deleted");

                            var adminComment = $('.adminComment').filter(":first").val();

                            var cancelDelete = function() {
                                $('.deleteButton').removeAttr("disabled");
                            };

                            $('.deleteButton').attr("disabled", 1);

                            if (!deleted) {
                                showAdminMessageDlg(${storyInstance.id},"Delete Story","<h5>Do you want to delete this story?</h5>Please give a reason for deleting this story:",adminComment,function() { updateDeleted(true) },cancelDelete);
                            } else {
                                showAdminMessageDlg(${storyInstance.id},"Un-Delete Story","<h5>Do you want to un-delete this story?</h5>Please note and possibly change the following comment on this story:",adminComment,function() { updateDeleted(false) },cancelDelete);
                            }
                        });
                    }

                    function initAdminButtons(approved, deleted, disableComments) {

                        initApprovedButton(approved);

                        initDisableCommentsButton(disableComments);

                        initDeleteButton(deleted);

                        $('.admin').removeClass("hidden");
                    }

                    function initReviewButtons() {
                        var revisionIndex = 0;
                        var revisionCount = $(".revision").length;

                        function updateButtonStates() {
                            $("#review-prev").prop("disabled", revisionIndex > 0);
                            $("#review-next").prop("disabled", revisionIndex <= (revisionCount - 1));
                        }

                        function moveToRevision(increment) {
                            var lastRevision = $(".revision").eq(revisionIndex);
                            lastRevision.removeClass("revision-highlight");

                            revisionIndex += increment;
                            if (revisionIndex < 0)
                                revisionIndex = 0;
                            else if (revisionIndex >= revisionCount)
                                revisionIndex = revisionCount - 1;

                            var revision = $(".revision").eq(revisionIndex);
                            revision.addClass("revision-highlight");
                            $('html, body').animate({scrollTop:revision.position().top - 50 }, 'fast');

                            updateButtonStates();
                        }

                        function reviewButtonPrevClick() {
                            moveToRevision(-1);
                        }

                        function reviewButtonNextClick() {
                            moveToRevision(+1);
                        }

                        $("#review-prev").on("click", reviewButtonPrevClick);
                        $("#review-next").on("click", reviewButtonNextClick);

                        updateButtonStates();
                    }

                    function fetchComments() {
                        var data = {'id': '${storyInstance.id}'};

                        $.ajax({
                            url: "${createLink(action: 'fetchComments', controller: 'newStory')}",
                            type: 'GET',
                            data: data,
                            dataType: 'json',
                            cache: true,
                            success: function(response) {
                                var valid = response.valid;

                                var commentHtml = response.comments;
                                var admin = response.admin;
                                var isLoggedIn = response.isLoggedIn;
                                var liked = response.liked;
                                var commentAuthor = response.commentAuthor
                                var myStories = response.myStories;

                                $("#commentAuthor").val(commentAuthor);

                                // install the like button click handler and set it up
                                if (isLoggedIn && !myStories) {
                                    fixLikeButton(true, liked);

                                    $(".like-button").on("click", likeButtonClick);
                                    $(".like-button-small").on("click", likeButtonClick);
                                }

                                if (myStories || admin) {
                                    fetchStoryAuthorData();
                                }

                                if (admin) {
                                    $(".authorPanelHeading").html("Site Admin");

                                    var deleted = response.deleted;
                                    var approved = response.approved;
                                    var disableComments = response.disableComments;

                                    initAdminButtons(approved, deleted, disableComments);

                                    reenableNewCommentButtons();
                                }

                                if (myStories || admin)
                                    $('.mystories').slideDown();

                                var divId = "#comment-list";

                                $(divId).removeClass("spiral-spinner");
                                $(divId).html("");

                                $(commentHtml).hide().prependTo("#comment-list").fadeIn("slow", function(){
                                    addDeleteCommentButtons();
                                });

                                if (admin) {
                                    $(".admin-card").show()

                                    $(".comment-ip").show()
                                }

                                var thresID = sessionStorage.getItem("lastCommentID");

                                var sessionUpdate = new Date(sessionStorage.getItem("lastCommentIDtime"));
                                if(sessionUpdate) {
                                    var anHourAgo = new Date(Date.now() - 1000 * 60 * 60);
                                    if(sessionUpdate < anHourAgo) {
                                        thresID = null;
                                    }
                                }

                                sessionStorage.setItem("lastCommentIDtime",new Date().toJSON());

                                if(! thresID)
                                {
                                    thresID = localStorage.getItem("lastCommentID")
                                    if(! thresID)
                                        thresID = 0;

                                    sessionStorage.setItem("lastCommentID",thresID);
                                }

                                var highID = thresID;

                                $(".newCommentStar").each(function(index,elem) {
                                    var itemID = $(elem).data('id');
                                    if(itemID > thresID)
                                        $(elem).removeClass("d-none")
                                    if(itemID > highID)
                                        highID = itemID;
                                });

                                localStorage.setItem("lastCommentID",highID);
                            }, error: function(jqXHR, textStatus, errorThrown) {
                                // error?
                            }

                        });
                    }

                    'use strict';

                    $(function ()
                    {
                        //addDeleteCommentButtons();
                        fetchComments();
                        fetchRatings();
                        fetchRatingStats();
                    });

                    initReviewButtons();
                })(jQuery, window, document);

            </asset:script>

        </script>

    </content>

    </body>
    </html>
</g:applyLayout>

