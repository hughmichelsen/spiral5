<g:applyLayout name="newMain" model="[active:'user']" >
    <html>
    <head>
        <title><g:pageTitle title='Edit Password'/></title>

        <meta name="robots" content="noindex">
        %{--<link type="text/css" rel='stylesheet' href="${resource(dir: 'stylesheets', file: 'edituser.css')}" />--}%
    </head>
    <body>

    <div class="container" role="main">
        <h1>Edit Password</h1>

        <g:render template="userErrors" model="[userInstance: userInstance]" />

        <g:form url="[action:'updateSelfPassword', controller:'user']" id="create-form" name="create-form" role="form" useToken="true" >

            <div class="form-group fieldcontain ${hasErrors(bean: userInstance, field: 'password', 'has-error')}">
                <label for="password">Password</label>
                <input type="password" class="text_ form-control" name="password" id="password" placeholder="Password" required/>

                <label for="passwordAgain" class="sr-only">Re-enter Password</label>
                <div class="spacer10"></div>
                <input type="password" class="text_ form-control" name="passwordAgain" id="passwordAgain" placeholder="Password Again" required/>
            </div>

            <button class="btn btn-lg btn-primary btn-block" type="submit">Change Password</button>
        </g:form>
    </div>

    </body>
    </html>
</g:applyLayout>

