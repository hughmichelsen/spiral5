<g:applyLayout name="newMain" model="[active:'user']" >
    <html>
    <head>
        <title><g:pageTitle title='Sign Up'/></title>

        <asset:stylesheet href="auth.css"/>
        <meta name="robots" content="noindex">
    </head>
    <body>

    <div class="container">

        <form action="resetPassword" method="POST" id="loginForm" class="form-signin cssform">
            <h2 class="form-signin-heading">Reset your password</h2>

            <input type="hidden" name="emailAddress" value="${userInstance?.emailAddress}" />
            <input type="hidden" name="unique" value="${unique}" />

            <div class="form-group fieldcontain ${hasErrors(bean: userInstance, field: 'password', 'has-error')}">
                <label for="password" class="sr-only">New Password</label>
                <input type="password" class="text_ form-control" name="password" id="password" placeholder="Password" required/>

                <label for="passwordAgain" class="sr-only">Re-enter New Password</label>
                <input type="password" class="text_ form-control" name="passwordAgain" id="passwordAgain" placeholder="Password" required/>
            </div>

            <button class="btn btn-lg btn-primary btn-block" type="submit">Change Password</button>
        </form>
    </div> <!-- /container -->
    </body>
</g:applyLayout>