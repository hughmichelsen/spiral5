<g:applyLayout name="newMain" model="[active:'user']" >
    <html>
    <head>
        <title><g:pageTitle title='Forgot Password'/></title>

        <asset:stylesheet href="auth.css"/>
        <meta name="robots" content="noindex">
    </head>
    <body>

    <div class="row">
        <div class="col">

            <h3 class="form-signin-heading text-center">Please enter your email address</h3>

            <form action="sendPasswordResetMail" method="POST" id="loginForm" class="form-signin cssform">

                <label for="emailAddress" class="sr-only">Email Address</label>
                <input type="text" class="text_ form-control" data-toggle="tooltip" title="The email address you used to create your account!" placeholder="you@you.com" name="emailAddress" id="emailAddress" required autofocus/>

                <div class="spacer20"></div>
                <button class="btn btn-lg btn-primary btn-block" type="submit">Send Reset Password Email</button>
            </form>

        </div>
    </div> <!-- /container -->
    </body>
</g:applyLayout>