<g:applyLayout name="newMain" model="[active:'user']" >
    <html>
    <head>
        <title><g:pageTitle title='Forgot Password'/></title>

        <asset:stylesheet href="auth.css"/>
        <meta name="robots" content="noindex">
    </head>
    <body>

    <div class="container">

        <div class="alert alert-info text-center">
            <h4>Reset request sent. Please check your email and follow the instructions to reset your password.</h4>
            If you continue to have problems to log on, email the Site Admin (<a href="mailto:admin@gayspiralstories.com">admin@gayspiralstories.com</a>).
        </div>
    </div> <!-- /container -->
    </body>
</g:applyLayout>