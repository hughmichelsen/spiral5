<g:hasErrors bean="${userInstance}">
    <g:eachError bean="${userInstance}" var="error">
        <div class="alert alert-danger">
            <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
            <span class="sr-only">Error:</span>
            <g:message error="${error}"/>
        </div>
    </g:eachError>
    <div class="spacer20"></div>
</g:hasErrors>