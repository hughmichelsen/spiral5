<g:applyLayout name="newMain" model="[active:'user']" >
    <html>
    <head>
        <title><g:pageTitle title='Edit Profile'/></title>
        <link type="text/css" rel='stylesheet' href="${resource(dir: 'stylesheets', file: 'edituser.css')}" />
        <meta name="robots" content="noindex">
    </head>
    <body>

    <div class="row">
        <div class="col">
            <h2 class="text-center">Edit Profile</h2>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <g:render template="userErrors" model="[userInstance: userInstance]" />
        </div>
    </div>

    <div class="row">
        <div class="col">
            <g:form url="[action:'updateSelf', controller:'user']" id="create-form" name="create-form" role="form" useToken="true" >

                <div class="card card-default mb-3">
                    <div class="card-header">
                        Basic User Options
                    </div>
                    <div class="card-body">
                        <div id="email-div" class="form-group fieldcontain ${hasErrors(bean: userInstance, field: 'emailAddress', 'has-error') || hasErrors(bean: userInstance, field: 'username', 'error')} required">
                            <label for="emailAddress" data-toggle="tooltip" data-placement="right" data-html="true" title="<p>Your email address is your identifier for logging on to the site.</p>
    <p>The address should be valid and you should be able to receive mails sent there, otherwise you will not be able to reset the password in case you ever forget it and certain functions of the site will not work properly either.</p>
    Your email address will be kept secret and will not be visible to anyone unless you explicitly allow it.">
                                Email Address <i class="fa fa-info-circle" aria-hidden="true"></i>
                            </label>
                            <input id="emailAddress" name="emailAddress" type="text" class="form-control" value="${userInstance?.emailAddress}" required="" />
                        </div>

                        <div id="author-div" class="form-group fieldcontain ${hasErrors(bean: userInstance, field: 'authorName', 'has-error')}">
                            <label for="authorName" data-toggle="tooltip" data-placement="right" title="The default author name used for new stories and comments you submit. You can still alter this name each time you submit anything.">
                                Your Public Name for Comments and Stories <i class="fa fa-info-circle" aria-hidden="true"></i>
                            </label>
                            <input id="authorName" name="authorName" type="text" class="form-control" value="${userInstance?.authorName}"/>
                        </div>

                        <div id="anonForum-div" class="custom-control custom-checkbox fieldcontain ${hasErrors(bean: userInstance, field: 'anonForum', 'has-error')}">
                            <g:checkBox class="custom-control-input" id="anonForum" name="anonForum" value="${userInstance?.anonForum}"/>
                            <label class="custom-control-label" for="anonForum">
                                Use forum in Anonymous Mode
                            </label>
                            <i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="If this is ticked, you can use the forum without using your email address there. That will restrict some of the forum's functions, though."></i>
                        </div>
                    </div>
                </div>

                <div class="card card-default mb-3">
                    <div class="card-header">
                        Author Options
                    </div>
                    <div class="card-body">

                        <sec:ifAllGranted roles="ROLE_AUTHOR">
                            <div class="alert alert-success" role="alert">
                                You're an active author. Thank you very much for contributing to GaySpiralStories. The following options are for you!
                            </div>
                        </sec:ifAllGranted>
                        <sec:ifNotGranted roles="ROLE_AUTHOR">
                            <div class="alert alert-warning" role="alert">
                                You haven't submitted any story yet, so these options have little meaning for you. In case you ever want to submit a story, you can set them now anyway.
                            </div>
                        </sec:ifNotGranted>

                        <div id="showEmail-div" class="custom-control custom-checkbox fieldcontain ${hasErrors(bean: userInstance, field: 'showEmail', 'has-error')}">
                            <g:checkBox  class="custom-control-input" id="showEmail" name="showEmail" value="${userInstance?.showEmail}"/>
                            <label class="custom-control-label" for="showEmail">
                                Default for 'Email visible publicly' setting when submitting stories
                            </label>
                            <i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="Untick this, if you want to keep your email address private by default (can still be changed for each story you submit)."></i>
                        </div>

                        <div id="emailComment-div" class="custom-control custom-checkbox fieldcontain ${hasErrors(bean: userInstance, field: 'emailComment', 'has-error')}">
                            <g:checkBox  class="custom-control-input" id="emailComment" name="emailComment" value="${userInstance?.emailComment}"/>
                            <label class="custom-control-label" for="emailComment">
                                Notify by mail when a new comment to your stories has been written
                            </label>
                            <i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="Tick this if you want to get a notification mail if someone adds a comment to one of your stories."></i>
                        </div>
                    </div>
                </div>

                <div class="card border-warning mb-3">
                    <div class="card-header bg-warning text-dark">
                        Patreon options
                    </div>
                    <div class="card-body">
                        <sec:ifAnyGranted roles="ROLE_PATREON,ROLE_ADMIN">
                            <div class="alert alert-success" role="alert">
                                <p>You're an active Patreon donator. Thank you very much for helping to finance GaySpiralStories.</p>
                                The following options are available to Patreons only!
                            </div>
                        </sec:ifAnyGranted>
                        <sec:ifNotGranted roles="ROLE_PATREON,ROLE_ADMIN">
                            <div class="alert alert-danger" role="alert">
                                <p>You're currently not an active Patreon donator. In case you ever decide to help finance GaySpiralStories, click the Patreon button in the menu.</p>
                                The following options are available to Patreons only!
                            </div>
                        </sec:ifNotGranted>

                        <div id="emailApproved-div" class="custom-control custom-checkbox fieldcontain ${hasErrors(bean: userInstance, field: 'emailApproved', 'has-error')}">
                            <g:checkBox  class="custom-control-input" id="emailApproved" name="emailApproved" disabled="${! userInstance.getIsPatreon() && ! userInstance.hasRole('ROLE_ADMIN')}" value="${userInstance?.emailApproved}"/>
                            <label class="custom-control-label" for="emailApproved">
                                Notify by mail when a new story is published
                            </label>
                            <i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="Tick this if you want to get a notification mail each time a new story is published on GaySpiralStories."></i>
                        </div>

                        <div id="hidePatreon-div" class="custom-control custom-checkbox fieldcontain ${hasErrors(bean: userInstance, field: 'hidePatreon', 'has-error')}">
                            <g:checkBox  class="custom-control-input" id="hidePatreon" name="hidePatreon" disabled="${! userInstance.getIsPatreon() &&  ! userInstance.hasRole('ROLE_ADMIN')}" value="${userInstance?.hidePatreon}"/>
                            <label class="custom-control-label" for="hidePatreon">
                                Hide Patreon button
                            </label>
                            <i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="You're already a Patreon, so if you don't want to be bothered by that Patreon button, you can now hide it!"></i>
                        </div>
                    </div>
                </div>

                <fieldset class="buttons">
                    <g:submitButton name="Apply" class="save btn btn-lg btn-primary" value="Apply" />
                </fieldset>
            </g:form>
        </div>
    </div>

    </body>
    </html>
</g:applyLayout>

