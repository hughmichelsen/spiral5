<g:applyLayout name="newMain" model="[active:'user']" >
    <html>
    <head>
        <title><g:pageTitle title='Sign Up'/></title>

        <asset:stylesheet href="auth.css"/>

        <meta name="robots" content="noindex">
    </head>
    <body>

    <div class="container">

        <h2 class="form-signin-heading text-center">Create an Account</h2>

        <form action="save" method="POST" id="loginForm" class="form-signin cssform">

            <div class="form-group fieldcontain ${hasErrors(bean: userInstance, field: 'emailAddress', 'has-error')}">
                <label for="username" class="sr-only">Email Address</label>
                <input type="text" class="text_ form-control" placeholder="Email" name="username" id="username" value="${userInstance.emailAddress}" required autofocus/>
            </div>

            <div class="form-group fieldcontain ${hasErrors(bean: userInstance, field: 'password', 'has-error')}">
                <label for="password" class="sr-only">Password</label>
                <input type="password" class="text_ form-control" name="password" id="password" placeholder="Password" required/>

                <label for="passwordAgain" class="sr-only">Re-enter Password</label>
                <input type="password" class="text_ form-control" name="passwordAgain" id="passwordAgain" placeholder="Password Again" required/>
            </div>

            <div id="over18-div" class="custom-control custom-checkbox fieldcontain required has-error">
                <g:checkBox class="custom-control-input" id="over18" name="over18"/>
                <label for="over18" class="custom-control-label" ><span class="ageWarning">I am over 18 years of age</span></label>
            </div>

            <button class="btn btn-lg btn-primary btn-block mt-3" type="submit">Create</button>

            <div class="spacer30"></div>
            <div class="center-block text-center">
                <g:link rel="noFollow" controller="User" action="forgotPassword" class="btn btn-sm btn-primary">Forgot my Password</g:link>
            </div>
        </form>
        <p style="font-size: small;" class="text-center center-block">
            <g:link controller="info" action="tos">Terms of Service</g:link> - <g:link controller="info" action="privacy">Privacy Policy</g:link>
        </p>
    </div> <!-- /container -->
    </body>
</g:applyLayout>