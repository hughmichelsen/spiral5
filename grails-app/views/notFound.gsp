<g:applyLayout name="newMain" model="[active:'home']">
    <html>
    <head>
        <title><g:pageTitle title='Error'/></title>
    </head>
    <body>

    <div class="row">
        <div class="col">
            <div class="well center-block text-center alert alert-danger">
                This page does not exist!<br/>Return to the <a href="${createLink(uri: '/')}">Home Page</a>
            </div>
        </div>
    </div>

    </body>
    </html>
</g:applyLayout>