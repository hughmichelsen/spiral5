<g:applyLayout name="commentList" model="[commentInstanceList: commentInstanceList, title: 'Your Comments']">

    <content tag="paginate">
        <g:paginate class="justify-content-center d-flex flex-wrap" action="mine" total="${commentInstanceCount ?: 0}" />
    </content>

</g:applyLayout>