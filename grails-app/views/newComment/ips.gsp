<g:applyLayout name="commentList" model="[commentInstanceList: commentInstanceList, title: 'Comments For ' + ip, NoIndexing: true]">

    <content tag="paginate">
        <g:paginate class="justify-content-center d-flex flex-wrap" action="showIps" id="${id}" total="${commentInstanceCount ?: 0}" />
    </content>

</g:applyLayout>