<g:applyLayout name="commentList" model="[commentInstanceList: commentInstanceList, title: 'Recent Comments']">

    <content tag="paginate">
        <sec:ifAllGranted roles="ROLE_AUTHOR">
            <div class="center-block text-center mb-2">
                <g:link action="index" params="[ownOnly: ! params.ownOnly]">
                    <button type="button" class="btn btn-primary">
                        ${params.ownOnly ? 'Show all comments' : 'Show comments for your own stories only'}
                    </button>
                </g:link>
            </div>
        </sec:ifAllGranted>

        <g:paginate class="justify-content-center d-flex flex-wrap" total="${commentInstanceCount ?: 0}" params="[ownOnly: params.ownOnly]"/>
    </content>

</g:applyLayout>

