<g:applyLayout name="commentList" model="[commentInstanceList: commentInstanceList, title: 'Comments For ' + user.username]">

    <content tag="title">
        <h1 class="text-center">Comments by ${user.username}</h1>
        <h1 class="text-center"><g:userButtons user="${user}"/></h1>
    </content>

    <content tag="paginate">
        <g:paginate class="justify-content-center d-flex flex-wrap" action="showUser" id="${user.id}" total="${commentInstanceCount ?: 0}" />
    </content>

</g:applyLayout>