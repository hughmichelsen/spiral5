<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="grails.util.Environment" %>
<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <meta http-equiv="language" content="english, en">
    <meta name="description" content="Gay Spiral Stories - Hot gay stories involving mind control, hypnosis, transformation and more.">
    <meta name="keywords" content="Gay,Sex,Hypnosis,Mind Control,Stories,NCMC,narcistic,cursed">
    <meta name="author" content="">
    <meta name="rating" content="mature">

    <meta name="robots" content="index,follow">

    <g:if env="production">
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-111700944-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-111700944-1');
        </script>
    </g:if>

    <link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
    <link rel="manifest" href="/favicon/manifest.json">
    <link rel="mask-icon" href="/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="/favicon/favicon.ico">
    <meta name="msapplication-config" content="/favicon/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">

    <title><g:layoutTitle default="Spiral Story Collection"/></title>

    <tz:detect />

    <g:themeSelector />

    <asset:stylesheet href="font-awesome.css"/>

    <asset:stylesheet href="bootstrap-editable.css"/>
    <asset:stylesheet href="tokenfield-typeahead.css"/>
    <asset:stylesheet href="bootstrap-tokenfield.css"/>
    <asset:stylesheet href="divLoading.css"/>

    <asset:stylesheet href="newmainex.css"/>

    %{--<tz:detect />--}%

    <g:layoutHead />

    <noscript>
        <style>
            .container, .navbar, .cookieFooter
            {
                display: none !important;
            }

            .noscript-warning
            {
                opacity: 100% !important;
                display: block !important;
            }
        </style>
    </noscript>

    <div id="divLoading"></div>

    <asset:javascript src="newmainex.js" />

    <script>
        function selectTheme(theme) {
            switch(theme) {
                <g:themeScript/>
            }
        }
    </script>

</head>
<body class="banner">

<header>
    <nav class="navbar navbar-expand-sm fixed-top navbar-autohiding navbar-dark bg-dark">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand d-flex" href="#"><div class="d-flex d-sm-none navbar-mobilelogo"><asset:image class="navbar-logo" src="banner/GSSLogoVector.svg"/><span class="site-home-text"><g:fullSiteTitle/></span></div></a>
        <div class="d-flex d-sm-none">
            <ul class="navbar-nav flex-row">
                <li class="nav-item ml-2 ${active == 'home' ? 'active' : ''}" title="Home"><a class="nav-link" href="${createLink(uri: '/')}"><i class="fa fa-home d-inline d-lg-none d-xl-inline" aria-hidden="true"></i></a></li>
                <li class="nav-item ml-2 ${active == 'search' ? 'active' : ''}" title="Search"><g:link class="nav-link" controller="newStory" action="search"><i class="fa fa-search d-inline d-lg-none d-xl-inline" aria-hidden="true"></i></g:link></li>
                <li class="nav-item ml-2 ${active == 'comments' ? 'active' : ''}" title="Newest Comments"><g:link class="nav-link" controller="newComment" action="index"><i class="fa fa-comments d-inline d-lg-none d-xl-inline"></i></g:link></li>
            </ul>
        </div>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item ${active == 'home' ? 'active' : ''}" title="Home"><a class="nav-link" href="${createLink(uri: '/')}"><i class="fa fa-home d-inline d-lg-none d-xl-inline" aria-hidden="true"></i><span class="d-inline d-sm-none d-lg-inline"> Home</span></a></li>

                <li class="nav-item ${active == 'new story' ? 'active' : ''}" title="Submit a new story"><g:link class="nav-link" controller="newStory" action="create"><i class="fa fa-plus d-inline d-lg-none d-xl-inline" aria-hidden="true"></i><span class="d-inline d-sm-none d-lg-inline"> New Story</span></g:link></li>

                <sec:ifLoggedIn>
                    <li class="nav-item" title="Community"><a href="https://forum.gayspiralstories.com" class="nav-link openDiscourse"><span class="fa fa-users d-inline d-lg-none d-xl-inline"></span><span class="d-inline d-sm-none d-lg-inline"> Community</span></a></li>
                </sec:ifLoggedIn>
                <sec:ifNotLoggedIn>
                    <li class="nav-item" title="Community"><a href="https://forum.gayspiralstories.com" class="nav-link openDiscourseNoLogon"><span class="fa fa-users d-inline d-lg-none d-xl-inline"></span><span class="d-inline d-sm-none d-lg-inline"> Community</span></a></li>
                </sec:ifNotLoggedIn>

                <li class="nav-item ${active == 'search' ? 'active' : ''}" title="Search"><g:link class="nav-link" controller="newStory" action="search"><i class="fa fa-search d-inline d-lg-none d-xl-inline" aria-hidden="true"></i><span class="d-inline d-sm-none d-lg-inline"> Search</span></g:link></li>

                <li class="nav-item ${active == 'comments' ? 'active' : ''}" title="Newest Comments"><g:link class="nav-link" controller="newComment" action="index"><i class="fa fa-comments d-inline d-lg-none d-xl-inline"></i><span class="d-inline d-sm-none d-lg-inline"> Comments</span></g:link></li>

                <li class="nav-item dropdown ${['stories', 'authors', 'defaultedStories', 'topStories'].contains(active) ? 'active' : ''}">
                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-file-text d-inline d-lg-none d-xl-inline" aria-hidden="true"></i><span class="d-inline d-sm-none d-lg-inline"> Stories</span></a>
                    <div class="dropdown-menu">
                        <g:link controller="newStory" class="dropdown-item" action="listTopRated"><i class="fa fa-star dropdown-icon" aria-hidden="true"></i> Top Rated Stories</g:link>
                        <g:link controller="newStory" class="dropdown-item" action="index"><i class="fa fa-file-text dropdown-icon" aria-hidden="true"></i> All Stories</g:link>
                        <g:link controller="newStory" class="dropdown-item" action="defaultedStories"><i class="fa fa-medkit dropdown-icon" aria-hidden="true"></i> Stories needing attention</g:link>
                        <div class="dropdown-divider"></div>
                        <g:link controller="newAuthor" class="dropdown-item" action="index"><i class="fa fa-pencil dropdown-icon"aria-hidden="true"></i> Authors</g:link>
                    </div>
                </li>

                <li class="nav-item dropdown ${['about', 'news', 'affiliates'].contains(active) ? 'active' : ''}">
                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-folder-open d-inline d-lg-none d-xl-inline" aria-hidden="true"></i><span class="d-inline d-sm-none d-lg-inline"> More</span></a>
                    <div class="dropdown-menu">
                        <g:link controller="newHome" class="dropdown-item" action="affiliates"><i class="fa fa-info dropdown-icon" aria-hidden="true"></i> Recommended Sites</g:link>
                        <div class="dropdown-divider"></div>
                        <g:link controller="newHome" class="dropdown-item" action="about"><i class="fa fa-info dropdown-icon" aria-hidden="true"></i> About</g:link>
                        <g:link controller="news" class="dropdown-item" action="index"><i class="fa fa-newspaper-o dropdown-icon" aria-hidden="true"></i> News</g:link>
                    </div>
                </li>
                <li class="nav-item">
                    <div class="pl-2 hidden d-sm-inline"> </div>
                    <g:patreonButton>
                        <asset:image src="Patreon_logo_kit/Logomark/Color on Coral/Patreon logomark (color on coral).svg" height="20" width="20"/>
                    </g:patreonButton>
                </li>
            </ul>
            <ul class="navbar-nav">
                <sec:ifAnyGranted roles="ROLE_ADMIN,ROLE_SUPER_ADMIN,ROLE_ULTRA_ADMIN">
                    <li class="nav-item dropdown ${active == 'admin' ? 'active' : ''}">
                        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench d-inline d-lg-none d-xl-inline" aria-hidden="true"></i><span class="d-inline d-sm-none d-lg-inline"> Admin</span></a>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <g:link rel="noFollow" controller="newStory" class="dropdown-item" action="listUnapproved"><i class="fa fa-asterisk dropdown-icon" aria-hidden="true"></i> Unapproved Stories</g:link>
                            <div class="dropdown-divider"></div>
                            <g:link rel="noFollow" controller="tag" class="dropdown-item" action="index"><i class="fa fa-tag dropdown-icon" aria-hidden="true"></i> Tag Management</g:link>
                            <sec:ifAnyGranted roles="ROLE_SUPER_ADMIN,ROLE_ULTRA_ADMIN">
                                <g:link rel="noFollow" controller="secUser" class="dropdown-item" action="index"><i class="fa fa-user dropdown-icon" aria-hidden="true"></i> User Management</g:link>
                                <g:link rel="noFollow" controller="newStory" class="dropdown-item" action="listDeleted"><i class="fa fa-trash dropdown-icon" aria-hidden="true"></i> Deleted Stories</g:link>
                                <div class="dropdown-divider"></div>
                                <g:link rel="noFollow" controller="systemStats" class="dropdown-item" action="index"><i class="fa fa-cog dropdown-icon" aria-hidden="true"></i> System Config</g:link>
                                <g:link rel="noFollow" controller="newHome" class="dropdown-item" action="activity"><i class="fa fa-newspaper-o dropdown-icon"></i> Activity</g:link>
                            </sec:ifAnyGranted>
                        </ul>
                    </li>
                </sec:ifAnyGranted>
                    <li class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-tint d-inline d-lg-none d-xl-inline" aria-hidden="true"></i><span class="d-inline d-sm-none d-lg-inline"> Theme</span></a>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <g:themeSelectors/>
                        </ul>
                    </li>
                <sec:ifLoggedIn>
                    <li class="nav-item dropdown ${active == 'user' ? 'active' : ''}">
                        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" title="${sec.username()}"><g:userIcon userid="${sec.loggedInUserInfo(field: 'id')}"/><span> User</span></a>
                        <ul class="dropdown-menu dropdown-menu-right">

                            <sec:ifAllGranted roles="ROLE_AUTHOR">
                                <g:link rel="noFollow" controller="newAuthor" class="dropdown-item" action="listAuthorStories"><i class="fa fa-book dropdown-icon"></i> Your Stories</g:link>
                                <div class="dropdown-divider"></div>
                            </sec:ifAllGranted>

                            <g:link rel="noFollow" controller="newComment" class="dropdown-item" action="mine"><i class="fa fa-comments dropdown-icon"></i> Your Comments</g:link>
                            <g:link rel="noFollow" controller="newStory" class="dropdown-item" action="listLiked"><i class="fa fa-thumbs-up dropdown-icon" aria-hidden="true"></i> Your Liked Stories</g:link>
                            <div class="dropdown-divider"></div>
                            <g:link rel="noFollow" controller="user" class="dropdown-item" action="editSelf"><i class="fa fa-user-secret dropdown-icon"></i> Edit Profile</g:link>
                            <g:link rel="noFollow" controller="user" class="dropdown-item" action="editSelfPassword"><i class="fa fa-lock dropdown-icon"></i> Change Password</g:link>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="/logoff"><i class="fa fa-sign-out dropdown-icon" aria-hidden="true"></i> Log out</a>
                        </ul>
                    </li>
                </sec:ifLoggedIn>
                <sec:ifNotLoggedIn>
                    <li class="nav-item" title="Login"><g:link class="nav-link" controller='login' action='auth'><i class="fa fa-sign-in" aria-hidden="true"></i> Login</g:link></li>
                </sec:ifNotLoggedIn>
            </ul>
        </div>
    </nav>

    <div class="navbarspacer">
    </div>

    <div class="row hidden d-sm-block">
        <div class="col">
            <a href="${createLink(uri: '/')}" class="bannerlink">
                <div class="desktopbanner">
                    <asset:image src="banner/GSSLogoVector.svg" class="site-logo"/><p class="site-home-text"><g:fullSiteTitle/></p>
                </div>
            </a>
        </div>
    </div>
</header>

<div class="container">

    <g:if test="${flash.message}">
        <div class="row">
            <div class="col">
                <div class="alert alert-primary center-block text-center">
                    ${raw(flash.message)}
                </div>
            </div>
        </div>
    </g:if>

    <g:layoutBody/>

    <div id="ageCheckModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header d-flex flex-wrap justify-content-center">
                    <div class="dialogbanner">
                        <asset:image src="banner/GSSLogoVector.svg" class="site-logo"/><p class="site-home-text"><g:fullSiteTitle/></p>
                    </div>
                    <h1 class="text-center alert alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Age Verification</h1>
                </div>
                <div class="modal-body text-center">
                    <strong>
                    <p>This is an R rated site. If you are not 18 or older, do not enter.</p>
                    <p>Are you over 18 years of age?</p>
                    </strong>
                </div>
                <div class="modal-footer">
                    <button id="ageCheckNo" type="button" class="btn">No</button>
                    <button id="ageCheckYes" type="button" class="btn btn-default" data-dismiss="modal">Yes</button>
                </div>
            </div>

        </div>
    </div>

    <g:pageProperty name="page.dialogs"/>

</div>

<footer class="footer fixed-bottom">
    <div class="row noscript-warning">
        <div class="col">
            <div class="alert alert-danger center-block text-center">
                <p class="larger-text">This site requires Javascript to run.<br>
                    Please enable scripting or whitelist this site in the browser and your script-blocker.</p>
                Without scripting, many functions don't work properly.<br/>
                We don't load any external content or scripts.<br/>
                Scripting is only used for menus and to improve usability.
            </div>
        </div>
    </div>

    <div class="row cookieFooter">
        <div class="col">
            <div class="alert alert-warning d-flex justify-content-between">
                <small class="center-block align-self-center">Be aware: this site uses local cookies (to save some user settings) and uses google analytics, to aggregate anonymous usage statistics.</small>
                <button id="showCookieWarning" class="btn btn-success btn-sm">Got it!</button>
            </div>
        </div>
    </div>
</footer>

<div id="callDiscourse" tabindex="-1" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h3 class="modal-title text-light">The community forum needs to validate your email address!</h3>
            </div>
            <div class="modal-body" align="">
                <p>In your profile, you've set the following email address: <strong id="emailAddress-div"></strong><br/>
                <p>Please make sure that this address is valid and that you can receive mails sent to that address!</p>
                <p>The forum can also be used anonymously. This way, you won't need a working email address. This will disable all forum notifications!<br>
                    This choice will be remembered and can be changed in your user management.</p>
                <p><br/>Your name in the forum will be <strong id="forumname-div"></strong>. You can change that name in the forum settings.</p>
            </div>
            <div class="modal-footer">
                <button id="openDiscourseNormal" type="button" class="btn btn-primary" data-dismiss="modal">Use Email</button>
                <button id="openDiscourseAnon" type="button" class="btn btn-warning" data-dismiss="modal">Anonymous Mode</button>
                <button id="openDiscourseCancel" type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<div id="callDiscourseAnon" tabindex="-1" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h3 class="modal-title text-light">You are not logged on!</h3>
            </div>
            <div class="modal-body" align="">
                <p>You're currently not logged on, so you can only access the forum anonymously!</p>
                <p>If you have an account, please log on first and then click on the Community link again to access the forum with your account.<br/>
                <p>If you prefer to use the forum anonymously, you can do so. You can even create postings using a special anonymous account which is accessible by clicking "Anonymous access" below.</p>
            </div>
            <div class="modal-footer">
                <button id="openDiscourseLogin" type="button" class="btn btn-primary" data-dismiss="modal">Login</button>
                <button id="openDiscourseNoLogin" type="button" class="btn btn-warning" data-dismiss="modal">Anonymous access</button>
                <button id="openDiscourseCancel2" type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

</body>

<g:pageProperty name="page.scripts"/>

<asset:deferredScripts/>

<script>
    (function ($, window, document, undefined) {
        'use strict';

        function botCheck(){
            var botPattern = "(googlebot\/|Googlebot-Mobile|Googlebot-Image|Google favicon|Mediapartners-Google|bingbot|slurp|java|wget|curl|Commons-HttpClient|Python-urllib|libwww|httpunit|nutch|phpcrawl|msnbot|jyxobot|FAST-WebCrawler|FAST Enterprise Crawler|biglotron|teoma|convera|seekbot|gigablast|exabot|ngbot|ia_archiver|GingerCrawler|webmon |httrack|webcrawler|grub.org|UsineNouvelleCrawler|antibot|netresearchserver|speedy|fluffy|bibnum.bnf|findlink|msrbot|panscient|yacybot|AISearchBot|IOI|ips-agent|tagoobot|MJ12bot|dotbot|woriobot|yanga|buzzbot|mlbot|yandexbot|purebot|Linguee Bot|Voyager|CyberPatrol|voilabot|baiduspider|citeseerxbot|spbot|twengabot|postrank|turnitinbot|scribdbot|page2rss|sitebot|linkdex|Adidxbot|blekkobot|ezooms|dotbot|Mail.RU_Bot|discobot|heritrix|findthatfile|europarchive.org|NerdByNature.Bot|sistrix crawler|ahrefsbot|Aboundex|domaincrawler|wbsearchbot|summify|ccbot|edisterbot|seznambot|ec2linkfinder|gslfbot|aihitbot|intelium_bot|facebookexternalhit|yeti|RetrevoPageAnalyzer|lb-spider|sogou|lssbot|careerbot|wotbox|wocbot|ichiro|DuckDuckBot|lssrocketcrawler|drupact|webcompanycrawler|acoonbot|openindexspider|gnam gnam spider|web-archive-net.com.bot|backlinkcrawler|coccoc|integromedb|content crawler spider|toplistbot|seokicks-robot|it2media-domain-crawler|ip-web-crawler.com|siteexplorer.info|elisabot|proximic|changedetection|blexbot|arabot|WeSEE:Search|niki-bot|CrystalSemanticsBot|rogerbot|360Spider|psbot|InterfaxScanBot|Lipperhey SEO Service|CC Metadata Scaper|g00g1e.net|GrapeshotCrawler|urlappendbot|brainobot|fr-crawler|binlar|SimpleCrawler|Livelapbot|Twitterbot|cXensebot|smtbot|bnf.fr_bot|A6-Indexer|ADmantX|Facebot|Twitterbot|OrangeBot|memorybot|AdvBot|MegaIndex|SemanticScholarBot|ltx71|nerdybot|xovibot|BUbiNG|Qwantify|archive.org_bot|Applebot|TweetmemeBot|crawler4j|findxbot|SemrushBot|yoozBot|lipperhey|y!j-asr|Domain Re-Animator Bot|AddThis)";
            var re = new RegExp(botPattern, 'i');
            var userAgent = navigator.userAgent;
            if (re.test(userAgent)) {
                return true;
            }else{
                return false;
            }
        }

        $(function () {
            $(".noscript-warning").hide();

            $('.dropdown-toggle').dropdown();
            $('[data-toggle="tooltip"]').tooltip();
            $('[data-toggle="popover"]').popover();
            $('.popover-dismiss').popover({
                trigger: 'focus'
            })

            if(isMobile.any())
                $(".my-responsive-table").rtResponsiveTables();

            $(".switchTheme").click(function() {
                $(".switchTheme").removeClass("active")
                var theme = $(this).data('theme');
                var path = selectTheme(theme);
                $(this).addClass("active")
                $('body').fadeOut(400, function() {
                    $("#bootstrapLink").attr("href", path);
                    document.cookie = "currentTheme4=" + theme + "; path=/; expires=" + new Date("01/01/2050").toUTCString()
                    $('body').fadeIn();
                });
            })

            $(".navbar-autohiding").autoHidingNavbar({
                disableAutoHide: false,
                showOnUpscroll: true,
                showOnBottom: false,
                hideOffset: 'auto',
                animationDuration: 300
            });

            $(".navbar-autohiding").on('shown.bs.collapse', function () {
                $(".navbar-autohiding").removeClass("fixed-top");
                $('.navbar-autohiding').autoHidingNavbar('setDisableAutohide', true);
                window.scroll(0,0);
            });

            $(".navbar-autohiding").on('hidden.bs.collapse', function (){
                $(".navbar-autohiding").addClass("fixed-top");
                $('.navbar-autohiding').autoHidingNavbar('setDisableAutohide', false);
            });

            var cookieVerifyName = "spiral.cookie.warning3";
            var cookieTextVerified = "seen";
            var cookieText = localStorage.getItem(cookieVerifyName);

            if (cookieText !== cookieTextVerified) {
                cookieText = Cookies.get(cookieVerifyName);
                localStorage.setItem(cookieVerifyName, cookieText);
            }

            if (cookieText === cookieTextVerified) {
                $(".cookieFooter").hide();
            } else {
                $("#showCookieWarning").on("click", function () {
                    $(".cookieFooter").slideUp();

                    localStorage.setItem(cookieVerifyName, cookieTextVerified);
                });
            }

            var cookieAgeName = "spiral.age.verification";
            var verifiedText = "verified";
            var ageCheckVerified = localStorage.getItem(cookieAgeName);

            if (ageCheckVerified !== verifiedText && ! botCheck())
            {
                $('#ageCheckNo').on("click", function () {
                    // redirect
                    localStorage.setItem(cookieAgeName, "unverified");
                    window.location.replace("http://google.com");
                });

                $('#ageCheckYes').on("click", function () {
                    localStorage.setItem(cookieAgeName, verifiedText);
                });

                setTimeout( function() {
                    $(".modal-backdrop").css("opacity",1).css("background", "#7f7f7f");
                }, 0);
                $('#ageCheckModal').modal({backdrop: 'static', keyboard: false});
            }
        });


        $(function () {
            function openDiscourse(anon) {
                var data = {
                    'anon': anon
                };

                $.ajax({
                    url: "${createLink(action: 'updateAnonForum', controller: 'user')}",
                    type: 'POST',
                    data: data,
                    dataType: 'json',
                    success: function (response) {
                    },
                    error: function (response) {
                    },
                    complete: function () {
                        // redirect to forum
                        window.location.href = "https://forum.gayspiralstories.com/session/sso";
                    }
                });
            }

            function getCurrentUserDataAndOpenDiscourse() {
                var data = {};

                $.ajax({
                    url: "${createLink(action: 'getCurrentUserData', controller: 'user')}",
                    type: 'POST',
                    data: data,
                    dataType: 'json',
                    success: function (response) {
                        $('#emailAddress-div').text(response.emailAddress);
                        $('#forumname-div').text(response.forumname);

                        if (response.anonForum == null)
                            $('#callDiscourse').modal({});
                        else
                            window.location.href = "https://forum.gayspiralstories.com/session/sso";
                    },
                    error: function (response) {
                    },
                    complete: function () {
                    }
                });
            }

            $('#openDiscourseAnon').on("click", function () {
                openDiscourse(true);
            });
            $('#openDiscourseNormal').on("click", function () {
                openDiscourse(false);
            });

            $(".openDiscourse").click(function () {
                getCurrentUserDataAndOpenDiscourse();
                return false;
            });

            $('#openDiscourseLogin').on("click", function () {
                window.location.href = '<g:createLink controller="login" action="auth"/>'
            });
            $('#openDiscourseNoLogin').on("click", function () {
                window.location.href = "https://forum.gayspiralstories.com";
            });

            $(".openDiscourseNoLogon").click(function () {
                $('#callDiscourseAnon').modal({});
                return false;
            });

        });
    })(jQuery, window, document);
</script>

</html>