<g:applyLayout name="newMain" model="[active:'comments']" >
    <html>
    <head>
        <title><g:pageTitle title='${title}'/></title>
        <link type="text/css" rel='stylesheet' href="${resource(dir: 'stylesheets', file: 'comment.css')}" />

    </head>
    <body>

    <div class="row">
        <g:pageProperty name="page.title">
            <h1 class="text-center">${title}</h1>
        </g:pageProperty>
    </div>

    <div class="row">
        <div class="col mb-2">
            <g:pageProperty name="page.paginate"/>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <g:each in="${commentInstanceList}" var="comment">
                <g:applyLayout template="/newStory/comment" model="[comment: comment, includeStoryTitle: true]"/>
            </g:each>
        </div>
    </div>

    <div class="row">
        <div class="col mb-2">
            <g:pageProperty name="page.paginate"/>
        </div>
    </div>

        <script>
            <asset:script>
                (function ($, window, document, undefined)
                {
                    'use strict';

                    $(function ()
                    {
                        var thresID = sessionStorage.getItem("lastCommentID");

                        // Auto-Timeout for id 'session'

                        var sessionUpdate = new Date(sessionStorage.getItem("lastCommentIDtime"));
                        if(sessionUpdate) {
                            var anHourAgo = new Date(Date.now() - 1000 * 60 * 60);
                            if(sessionUpdate < anHourAgo) {
                                thresID = null;
                            }
                        }

                        sessionStorage.setItem("lastCommentIDtime",new Date().toJSON());

                        if(! thresID)
                        {
                            thresID = localStorage.getItem("lastCommentID")
                            if(! thresID)
                                thresID = 0;

                            sessionStorage.setItem("lastCommentID",thresID);
                        }

                        var highID = thresID;

                        $(".newCommentStar").each(function(index,elem) {
                            var itemID = $(elem).data('id');
                            if(itemID > thresID)
                                $(elem).removeClass("d-none");
                            if(itemID > highID)
                                highID = itemID;
                        });

                        localStorage.setItem("lastCommentID",highID);
                    });

                    <sec:ifAllGranted roles="ROLE_BASIC">
                        $(function ()
                        {
                            $(".deleteCommentButton").click(
                                    function() {

                                        var id = $(this).data('id');
                                        var data = {'id': id};

                                        $.ajax({
                                            url: "${createLink(action: 'deleteComment', controller: 'newComment')}",
                                            type: 'GET',
                                            data: data,
                                            success: function(response) {
                                                if (response == 'empty') {
                                                } else {
                                                    var divId = "#comment-" + id;

                                                    $(divId).hide('slow', function(){ $(divId).remove(); });
                                                }
                                            }
                                        });
                                    }
                            );
                        });
                    </sec:ifAllGranted>
                })(jQuery, window, document);
            </asset:script>
        </script>
    </body>
    </html>

</g:applyLayout>

