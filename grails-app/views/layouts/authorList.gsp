<g:applyLayout name="newMain" model="[active:'authors']">
    <html>
    <head>
        <title><g:pageTitle title="${title ?: 'Authors'}"/></title>
    </head>
    <body>

    <%
        //sort=name&max=30&order=asc
        if (!params.sort)
            params.sort = "name"
        if (!params.order)
            params.order = "asc"
    %>

    <div class="row">
        <div class="col">
            <ul class="pagination pagination-sm justify-content-center">
                <li class="page-item ${params.authorLetter == null ? 'active' : ''}"><g:link class="page-link" action="index" controller="newStory">All</g:link></li>
                <g:each in="${"abcdefghijklmnopqrstuvwxyz".toCharArray()}">
                    <li class="page-item ${params.authorLetter == it ? 'active' : ''}"><g:link class="page-link" action="letter" controller="newAuthor" id="${it}">${it.toUpperCase()}</g:link></li>
                </g:each>
            </ul>
        </div>
    </div>

    <div class="row">
        <div class="col mb-2">
            <g:pageProperty name="page.paginate"/>
        </div>
    </div>

    <div class="row">
        <table id="authorList" class="table table-striped" data-rtContainerBreakPoint="539">
            <thead>
            <tr>

                <g:sortableColumn property="name" title="${message(code: 'story.author.label', default: 'Name')}" />

            </tr>
            </thead>
            <tbody>
            <g:each in="${authorInstanceList}" status="i" var="authorInstance">
                <tr>

                    <td><g:link action="show" id="${authorInstance.id}" controller="newAuthor">${fieldValue(bean: authorInstance, field: "name")}</g:link></td>

                </tr>
            </g:each>
            </tbody>
        </table>
    </div>

    <div class="row">
        <div class="col">
            <g:pageProperty name="page.paginate"/>
        </div>
    </div>

    <content tag="scripts" >
        <g:render template="/newStory/quickLinksScript" />
    </content>

    </body>
    </html>
</g:applyLayout>