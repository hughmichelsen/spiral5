<g:applyLayout name="newMain" model="[active:'stories']">
    <html>
    <head>
        <title><g:pageTitle title='${title ?: 'Stories'}'/></title>

        <link href="${resource(dir: 'stylesheets', file: 'newstories.css')}" rel="stylesheet">
    </head>
    <body>

    <div class="row">
        <div class="col">
            <g:pageProperty name="page.headline"/>
        </div>
    </div>

    <div class="row">

        <%
            //sort=name&max=30&order=asc
            if (!params.sort)
                params.sort = "publishDate"
            if (!params.order)
                params.order = "desc"
        %>
    </div>

    <g:if test="${showLetterLinks}">
        <div class="row">
            <div class="col">
                <ul class="pagination pagination-sm justify-content-center d-flex flex-wrap">
                    <li class="page-item ${params.storyLetter == null ? 'active' : ''}"><g:link class="page-link" action="index" controller="newStory">All</g:link></li>
                    <g:each in="${"abcdefghijklmnopqrstuvwxyz".toCharArray()}">
                        <li class="page-item ${params.storyLetter == it ? 'active' : ''}"><g:link class="page-link" action="letter" controller="newStory" id="${it}">${it.toUpperCase()}</g:link></li>
                    </g:each>
                </ul>
            </div>
        </div>
    </g:if>

    <div class="row">
        <div class="col">
            <g:pageProperty name="page.title" ><h1 class="center-block text-center">${title}</h1></g:pageProperty>
        </div>
    </div>

    <div class="d-flex flex-column flex-sm-row justify-content-center justify-content-sm-between">
        <div class="mb-2 d-flex justify-content-center">
            <g:render template="/templates/categorySelector" model="[instance: '1']"/>
        </div>
        <div class="mb-2">
            <g:pageProperty name="page.paginate"/>
        </div>
        <sec:ifAllGranted roles="ROLE_SUPER_ADMIN">
            <div class="mb-2 d-flex justify-content-center">
                <g:render template="/newStory/storyAdminControls" />
            </div>
        </sec:ifAllGranted>
    </div>

    <div class="row">
        <g:render template="/newStory/storyTable" model="[storyInstanceList: storyInstanceList]" />
    </div>

    <div class="d-flex flex-column flex-sm-row justify-content-center justify-content-sm-between">
        <div class="mb-2 d-flex justify-content-center">
            <g:render template="/templates/categorySelector" model="[instance: '2']"/>
        </div>
        <div class="mb-2">
            <g:pageProperty name="page.paginate"/>
        </div>
    </div>

    <g:pageProperty name="page.dialogs"/>

    </body>
</html>
</g:applyLayout>