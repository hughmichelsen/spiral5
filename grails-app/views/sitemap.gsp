<?xml version="1.0" encoding="UTF-8"?>

<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <!-- Mainpage -->
    <url>
        <loc>https://www.gayspiralstories.com</loc>
        <changefreq>hourly</changefreq>
        <priority>1.0</priority>
    </url>

    <!-- Comments -->
    <url>
        <loc>https://www.gayspiralstories.com/newComment/index</loc>
        <changefreq>hourly</changefreq>
        <priority>0.3</priority>
    </url>

    <!-- About -->
    <url>
        <loc>https://www.gayspiralstories.com/newHome/about</loc>
        <changefreq>yearly</changefreq>
        <priority>0.2</priority>
    </url>

    <!-- News -->
    <url>
        <loc>https://www.gayspiralstories.com/news/index</loc>
        <changefreq>weekly</changefreq>
        <priority>0.2</priority>
    </url>

    <!-- All Stories -->
    <g:each in="${stories}" var="story">
        <url>
            <loc>https://www.gayspiralstories.com/newStory/show/${story.id}</loc>
            <changefreq>yearly</changefreq>
            <lastmod>${story.lastModifiedDate.format("yyyy-MM-dd")}</lastmod>
            <priority>0.5</priority>
        </url>
    </g:each>


    <!-- All Authors -->
    <g:each in="${authors}" var="author">
        <url>
            <loc>https://www.gayspiralstories.com/newAuthor/show/${author.id}</loc>
            <changefreq>monthly</changefreq>
            <priority>0.4</priority>
        </url>
    </g:each>

    <!-- All Series -->
    <g:each in="${series}" var="serial">
        <url>
            <loc>https://www.gayspiralstories.com/newSeries/show/${serial.id}</loc>
            <changefreq>monthly</changefreq>
            <lastmod>${serial.lastChapterUpdated.format("yyyy-MM-dd")}</lastmod>
            <priority>0.4</priority>
        </url>
    </g:each>

    <!-- All Categories -->
    <g:each in="${categories}" var="category">
        <url>
            <loc>https://www.gayspiralstories.com/newStory/search?srchCategory=${category.id}&amp;tagList=&amp;tagSearchMode=or&amp;useAlias=true&amp;searchTerm=</loc>
            <changefreq>weekly</changefreq>
            <priority>0.3</priority>
        </url>
    </g:each>

    <!-- All Tags -->
    <g:each in="${tags}" var="tag">
        <url>
            <loc>https://www.gayspiralstories.com/newStory/search?srchCategory=&amp;tagList=${tag.id}&amp;tagSearchMode=or&amp;useAlias=true&amp;searchTerm=</loc>
            <changefreq>weekly</changefreq>
            <priority>0.3</priority>
        </url>
    </g:each>
</urlset>