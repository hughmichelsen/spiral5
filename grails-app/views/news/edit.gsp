<g:applyLayout name="newMain" model="[active: 'user']">
    <html>
        <head>
            <meta name="robots" content="noindex">
            <title><g:pageTitle title='Edit News'/></title>

            <asset:stylesheet href="inscrybmde.css"/>
        </head>
        <body>

        <div class="content container" role="main">
            <h1>Edit News!</h1>

            <g:render template="showErrors" model="[newsInstance: newsInstance]" />

            <g:form url="[action:'update']" id="edit-form" name="edit-form" role="form" useToken="true" >
                <g:render template="createNewsFields" model="[newsInstance: newsInstance, submitButtonText: 'Update']" />
            </g:form>
        </div>

        </body>
    </html>

    <content tag="scripts">

        <asset:javascript src="inscrybmde.js"/>

        <script>

            <asset:script>

                (function ($, window, document, undefined)
                {
                    'use strict';

                    $(function ()
                    {
                        // WYSIWYG Editor

                        let showExtendedEditor = localStorage.getItem("showExtendedEditor");
                        if(showExtendedEditor === null)
                        showExtendedEditor = "1";

                        $('#extendedEditor').prop("checked", showExtendedEditor === "1");

                        const inscrybmde = new InscrybMDE({
                            element: $("#text")[0],
                            minHeight: "200px",
                            showIcons: ["bold", "italic", "strikethrough", "code", "heading", "|", "quote", "horizontal-rule", "clean-block", "|", "link", "image", "|", "preview"],
                            hideIcons: ["unordered-list", "ordered-list", "table", "side-by-side", "fullscreen", "guide"],
                            autosave: {
                                enabled: false,
                            },
                            renderingConfig: {singleLineBreaks: false},
                            promptURLs: true,
                            shortcuts: {
                                "toggleOrderedList": null,
                                "toggleUnorderedList": null,
                                "drawTable": null,
                                "toggleSideBySide": null,
                                "toggleFullScreen": null
                            },
                            inputStyle: 'contenteditable',
                            autoRender: showExtendedEditor === "1"
                            });

                        $('#extendedEditor').change(function(e){
                            const sel = $(e.target).is(":checked");
                            if(sel) {
                                inscrybmde.toEditor();
                                showExtendedEditor = "1";
                            }
                            else {
                                inscrybmde.toTextArea();
                                showExtendedEditor = "0";
                            }
                            localStorage.setItem("showExtendedEditor",showExtendedEditor);
                        });
                    });
                })(jQuery, window, document);

            </asset:script>
        </script>
    </content>
</g:applyLayout>