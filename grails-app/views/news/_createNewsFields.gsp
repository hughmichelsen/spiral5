<fieldset class="form">
    <div id="autoformat-div" class="checkbox fieldcontain ${hasErrors(bean: newsInstance, field: 'autoFormat', 'has-error')}">
        <label for="autoFormat">
            <g:checkBox id="autoFormat" name="autoFormat" value="${newsInstance?.autoFormat ?: true}"/> Auto Format
        </label>
    </div>
    <div id="showbanner-div" class="checkbox fieldcontain ${hasErrors(bean: newsInstance, field: 'showBanner', 'has-error')}">
        <label for="showBanner">
            <g:checkBox id="showBanner" name="showBanner" value="${newsInstance?.showBanner ?: false}"/> Show Banner
        </label>
    </div>
    <div id="news-div" class="form-group fieldcontain ${hasErrors(bean: newsInstance, field: 'text', 'has-error')}">
        <div class="d-flex justify-content-between">
            <label for="text">
                News Text
                <span class="required-indicator">*</span>
            </label>
            <div class="custom-checkbox custom-control fieldcontain">
                <g:checkBox class="custom-control-input" id="extendedEditor" name="extendedEditor"/>
                <label for="extendedEditor" class="custom-control-label" >Extended editor</label>
            </div>
        </div>
        <g:textArea class="form-control" name="text" rows="10" value="${newsInstance?.text}" />
    </div>
    <g:if test="${includeId}">
        <input type="hidden" name="id" value="${newsInstance?.id}" />
    </g:if>
</fieldset>
<fieldset class="buttons">
    <g:submitButton name="save" class="save btn btn-lg btn-primary" value="${submitButtonText}" />
</fieldset>