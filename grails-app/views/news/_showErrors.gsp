<g:hasErrors bean="${newsInstance}">
    <g:eachError bean="${newsInstance}" var="error">
        <div class="alert alert-danger">
            <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
            <span class="sr-only">Error:</span>
            <g:message error="${error}"/>
        </div>
    </g:eachError>
</g:hasErrors>