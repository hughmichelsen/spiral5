<g:applyLayout name="newMain" model="[active:'news']">
<html>
    <head>
        <title><g:pageTitle title="News"/></title>
    </head>
    <body>
        <div class="row">
            <div class="spacer15"></div>
            <div class="card card-default news">
                <div class="card-header"><h3 class="card-title"><g:formatDate format="MMM dd, yyyy" date="${newsInstance?.creationDate}" /></h3></div>
                <div class="card-body">
                    ${raw(newsInstance?.textFormatted)}
                </div>
            </div>
        </div>
    </body>
</html>
</g:applyLayout>