<g:applyLayout name="newMain" model="[active:'news']">
    <html>
    <head>
        <title><g:pageTitle title="News"/></title>
    </head>

    <body>
    <h1>News</h1>

    <g:if test="${showPagination}">
        <div class="col">
            <g:paginate class="justify-content-center d-flex flex-wrap" action="index" total="${newsInstanceCount ?: 0}" />
        </div>
    </g:if>

    <sec:ifAllGranted roles="ROLE_SUPER_ADMIN">
        <div class="row mb-3">
            <div class="col">
                <div class="btn-group btn-group-sm pull-right">
                    <g:link class="" action="create" title="Create some news!"><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-plus" aria-hidden="true"></i></button></g:link>
                </div>
            </div>
        </div>
    </sec:ifAllGranted>

    <g:each var="newsInstance" in="${newsList}">

        <div class="row mb-3" id="news-${newsInstance.id}">
            <div class="col">
                <div class="card card-default news">
                    <h5 class="card-header d-flex justify-content-between">
                        <span class="pull-left">
                            <g:formatDate type="date" style="MEDIUM" date="${newsInstance?.creationDate}" />
                        </span>
                        <dfn class="small">
                            by ${newsInstance?.user?.authorName}
                        </dfn>
                        <span class="pull-right">
                            <sec:ifAllGranted roles="ROLE_SUPER_ADMIN">
                                <g:link action="edit" id="${newsInstance?.id}" title="Edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></g:link>
                            </sec:ifAllGranted>
                            <g:link action="show" id="${newsInstance?.id}" title="Permanent link to this article"><i class="fa fa-link" aria-hidden="true"></i></g:link>
                        </span>
                    </h5>
                    <div class="card-body">
                        ${raw(newsInstance.textFormatted)}

                        <sec:ifAllGranted roles="ROLE_SUPER_ADMIN">
                            <p class="pull-right">
                                <button class="deleteNewsButton btn btn-danger btn-xs" type="button" data-id="${newsInstance.id}">Delete</button>
                            </p>
                        </sec:ifAllGranted>
                    </div>
                </div>
            </div>
        </div>
    </g:each>

    <g:if test="${showPagination}">
        <div class="col">
            <g:paginate class="justify-content-center d-flex flex-wrap" action="index" total="${newsInstanceCount ?: 0}" />
        </div>
    </g:if>

    <h4 class="text-center">
        <small>
            <g:link controller="newHome" action="news"><button type="button" class="btn btn-info">Pre-2017 Archives</button></g:link>
        </small>
    </h4>

    <script>
        <asset:script>
            (function ($, window, document, undefined)
            {
                'use strict';

                function deleteNews(id) {
                    var data = {'id': id};

                    $.ajax({
                        url: "${createLink(action: 'deleteAjax', controller: 'news')}",
                        type: 'GET',
                        data: data,
                        success: function(response) {
                            if (response == 'empty') {
                            } else {
                                var divId = "#news-" + id;

                                $(divId).hide('slow', function(){ $(divId).remove(); });
                            };
                        }
                    });
                }

                function addDeleteNewsButtons() {
                    $(".deleteNewsButton").click(
                        function() {
                            var id = $(this).data('id');
                            deleteNews(id);
                        }
                    );
                }

                addDeleteNewsButtons();

            })(jQuery, window, document);
        </asset:script>

    </script>
</body>
</html>
</g:applyLayout>