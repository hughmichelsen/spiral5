<g:applyLayout name="newMain" model="[active:'news']">
    <html>
    <head>
        <title><g:pageTitle title="News"/></title>
    </head>
    <body>

    <h1>Site News From Before July 2017</h1>

    <div class="card card-default news">
        <div class="card-header"><h3 class="card-title">June 4, 2017</h3></div>
        <div class="card-body">
            <p>
                Modified the layout on the site a little so that the navigation buttons to jump between pages is tighter
                and so that there are fewer numbers on smaller screens. We really didn't need 10 notches to choose from.
            </p>
        </div>
    </div>

    <div class="card card-default news">
        <div class="card-header"><h3 class="card-title">May 30, 2017</h3></div>
        <div class="card-body">
            <p>
                The Wiki was down. It isn't any longer. Thanks to those who emailed me about it!
            </p>
        </div>
    </div>

    <div class="card card-default news">
        <div class="card-header"><h3 class="card-title">September 11, 2016</h3></div>
        <div class="card-body">
            <p>Someone emailed me a few days ago and told me they couldn't log in anymore.</p>

            <p>Turns out when I made it so that log-ins were case insensitive, it caused a problem for users that had created multiple accounts with the same email address but different cases (ie. m@m.com and M@m.com).</p>

            <p>I manually went through the database and transferred stats, comments and stories to users this happened to, deleting the account that was the oldest. It was pretty straight forward for most. The three that it wasn't, I emailed.</p>

            <p>I also made it so that on account creation, it checks for this case and doesn't allow it.</p>

            <p>Think I also cleaned up the error displays a little too.</p>
        </div>
    </div>

    <div class="card card-default news">
        <div class="card-header"><h3 class="card-title">September 5, 2016</h3></div>
        <div class="card-body">
            <p>Upgraded to newest Grails (3.1.10), newest Spring Security Core Plugin (3.1.1) and newest ElasticSearch plugin (1.2.0).</p>
            <p>Made the load balancer check the terms of services page, which should not require a database query and therefore be better for the site as a whole.</p>
            <p>Switched over to using local ElasticSearch, instead of non-local CloudSearch. This saves the site $40 a month. Whoopee! Yahoo-ooooo-oooooo-oooooooooo...</p>
            <p>Finally, made it so that usernames on log-in are not case sensitive. And it's about damn time. That was so annoying.</p>
            <p>Oh! Less interesting (ish?). I added a <g:link controller="info" action="tos">terms of service page</g:link> and a <g:link controller="info" action="privacy">privacy page</g:link>. In case anyone was dying to read one of those. They're at the bottom of the about page now and at the user account creation page.</p>
        </div>
    </div>

    <div class="card card-default news">
        <div class="card-header"><h3 class="card-title">August 27, 2016</h3></div>
        <div class="card-body">
            <p>Added in a field to hide the copyright notice when posting a story. This was mainly because I didn't want the copyright notice when I posted a feedback request.</p>
            <p>Also fixed it so that authors get emailed when their story is posted properly now. And if they have a user account, it links directly to where to edit the story. If they don't have a user account, it instead tells them they can create one.</p>
        </div>
    </div>

    <div class="card card-default news">
        <div class="card-header"><h3 class="card-title">August 23, 2016</h3></div>
        <div class="card-body">
            <p>Auto-detection of timezone failed. Added in timezone setting to user profile instead. Also added minutes to the date fields. For the Wiki, that's a little more important, but better with everything.</p>
            <p>Also made the visited links into a darker color. Should make it easier to see, I hope.</p>
        </div>
    </div>

    <div class="card card-default news">
        <div class="card-header"><h3 class="card-title">August 21, 2016</h3></div>
        <div class="card-body">
            <p>Added auto-detection of timezone and display of dates according to that timezone. All thanks to this great Grails plugin: <a href="https://github.com/dustindclark/grails-timezone-detection">https://github.com/dustindclark/grails-timezone-detection</a>. I had to modify it slightly to work with Grails 3, but that took about ten minutes.</p>
            <p>Wanna see what timezone the site thinks you're in? Check out the <g:link controller="newHome" action="about">About page</g:link>.</p>
        </div>
    </div>

    <div class="card card-default news">
        <div class="card-header"><h3 class="card-title">August 21, 2016</h3></div>
        <div class="card-body">
            <p>Added in buttons to the home page and the wiki story list page to go direct to the edit pages for those wiki entries.</p>
            <p>Also added a new option to the Edit Profile page so that logged in users can turn off wiki stories showing up on the home page. Because some people aren't down with the wiki at all.</p>
        </div>
    </div>

    <div class="card card-default news">
        <div class="card-header"><h3 class="card-title">August 20, 2016</h3></div>
        <div class="card-body">
            <p>Got some proper mechanisms in for authorizing google drive. Wikis re-enabled.</p>
        </div>
    </div>

    <div class="card card-default news">
        <div class="card-header"><h3 class="card-title">August 18, 2016</h3></div>
        <div class="card-body">
            <p>Scratch that. I couldn't get the Google Drive permissions to work from the server. I'll have to try again tomorrow. Disabling all of the menu stuff for now.</p>
        </div>
    </div>

    <div class="card card-default news">
        <div class="card-header"><h3 class="card-title">August 18, 2016</h3></div>
        <div class="card-body">
            <p>I've added the Wiki. It's only got one story up, but the infrastructure is in there to add more.</p>
            <p>The editing is done through Google Drive. There's currently no way to tag the stories and no way to comment on them, but Google Drive allows comments and revisions, so I think that's good enough. I'll do tags later.</p>
            <p>The Spiral caches the data from the Google Drive docs and updates it every 30 minutes. Every hour, if the Wiki has changed, it will update the link on the Home Page.</p>
        </div>
    </div>

    <div class="card card-default news">
        <div class="card-header"><h3 class="card-title">August 11, 2016</h3></div>
        <div class="card-body">
            <p>HTML links have been modified so that if they are over 32 characters long, the text will say "click here" instead of the link. Links longer than that many characters don't wrap properly and screw up the div's, on mobile screens in particular. I could probably switch to a wrapping text thing, but this works too.</p>
            <p>Also, work on a new wiki, done through Google Drive, continues.</p>
        </div>
    </div>

    <div class="card card-default news">
        <div class="card-header"><h3 class="card-title">July 18, 2016</h3></div>
        <div class="card-body">
            <p>The stories of the author mw-scot have been removed at the author's request.</p>
            <p>Also, the rules for posting new stories have been updated slightly.</p>
            <p>Oh, at some point recently, I also made it possible for admins to sort users that are supporting the site. Just click on the button at the top of the column for that field. Only works on desktop, unfortunately.</p>
        </div>
    </div>

    <div class="card card-default news">
        <div class="card-header"><h3 class="card-title">June 12, 2016</h3></div>
        <div class="card-body">
            <p>Thanks to an awesome bug report from a site user, I found and fixed sorting of search results by date.</p>
        </div>
    </div>

    <div class="card card-default news">
        <div class="card-header"><h3 class="card-title">April 30, 2016</h3></div>
        <div class="card-body">
            <p>Added tracking of user login dates (if it works).</p>
            <p>Replaced the site's banner with something that stretches across the whole screen.</p>
        </div>
    </div>

    <div class="card card-default news">
        <div class="card-header"><h3 class="card-title">April 25, 2016</h3></div>
        <div class="card-body">
            <p>In an attempt to make the login button easier to discover, I've collapsed Stories, Authors, About and News into a combined menu called "Links".</p>
            <p>I've made the navbar header clickable to get home now. Mostly. Mostly because it's not complete coverage. I'm still not learned enough in the ways of CSS-foo.</p>
            <p>I've made SecUsers track comment, story and like counts. So that admins can see them in the user list now.</p>
            <p>I've improved admin pages for viewing users all over the site, and improved the experience of administering users via the user's list page, for desktops only. The mobile version of that page has taken a turn for the worse, I'm afraid.</p>
            <p>I made start up threads chain together. Which means they'll be slower, slightly, but more reliable.</p>
        </div>
    </div>

    <div class="card card-default news">
        <div class="card-header"><h3 class="card-title">April 23, 2016</h3></div>
        <div class="card-body">
            <p>The site automatically redirects to HTTPS now. It should do it automatically and hopefully won't screw up anyone's bookmarks (because it'll redirect to the same page). It will unfortunately force anyone logged in via HTTP to log-in again, but only once, I hope.</p>
        </div>
    </div>

    <div class="card card-default news">
        <div class="card-header"><h3 class="card-title">April 17, 2016</h3></div>
        <div class="card-body">
            <p>Logins were meant to happen with ssl/https, and I couldn't quite get that to work, so HTTPS is now on for everything. If you go to http://www.gayspiralstories.com now, it'll redirect you to https://www.gayspiralstories.com:8443.</p>
            <p>There's issues running Grails and having the default HTTPS port (443) open, same as the HTTP port (80). Grails defaults them to 8443 and 8080, so I had to muddle around to get that to happen.</p>
            <p>Whatever. It looks like it works and I think it's more secure. I mean, I'm definitely not a security guy, so if you're worried, don't use a password on this site that you use anywhere else. And/or use a password app that generates them for you, like Strip or an equivalent.</p>
        </div>
    </div>

    <div class="card card-default news">
        <div class="card-header"><h3 class="card-title">April 9, 2016</h3></div>
        <div class="card-body">
            <p>The series' name on the home page now links to the last story in the series. The icon with the story count and the story book icon is now a link to the series.</p>
            <p>Comment counts for the last story in the series are now displayed, and they link to the comments on the last story page.</p>
        </div>
    </div>

    <div class="card card-default news">
        <div class="card-header"><h3 class="card-title">April ?, 2016</h3></div>
        <div class="card-body">
            <p>
                Whole bunch of changes, in no particular order:
                <ul>
                    <li>logged in users will be turned into authors if they post a story</li>
                    <li>there’s now a page to view all of the stories assigned to a particular author</li>
                    <li>there’s functionality for admins to assign stories to a particular user</li>
                    <li>emails sent out on story approval will indicate who approved them to the admin users being emailed</li>
                    <li>the comment list page is cached client side with etags now</li>
                    <li>new users can create and edit themselves now</li>
                    <li>users can reset their passwords</li>
                    <li>admins can clear password resets now</li>
                    <li>Two new roles added: author and basic. Basic is so that the system can ensure someone is logged in and/or an author or has another sec role</li>
                    <li>Users have author names and a flag for whether or not to show email when posting stories</li>
                    <li>Stories now have ‘owners’. This will usually (always?) be one: the user that uploaded the story, if they are logged in.</li>
                    <li>Stories have to be at least 1000 words now, in order to be posted.</li>
                    <li>Stories track how many comments they have now, so it can be displayed on the home page / story list pages (coming later?)</li>
                    <li>Comments have text that looks like a link in them auto-replaced with an HTML link, and if it’s too long, it’s replaced with “click here” so that the comments page doesn’t get super wide for that one word on phone screens.</li>
                    <li>To attempt to prevent that from slowing the comment page down, the comments are cached too. I’m hoping that doesn’t turn into a memory problem, and am unsure if caching 200 comments is nearly enough. Probably not.</li>
                    <li>Moved the logout button to the User dropdown menu instead of the admin. Makes more sense.</li>
                    <li>Made the story editing buttons placed a little nicer on the individual story display page.</li>
                    <li>Re-enabled google analytics, which had been temporarily disabled</li>
                </ul>
                Also, I should really make news a proper database loaded thing already. This page is getting unwieldy.
            </p>
        </div>
    </div>

    <div class="card card-default news">
        <div class="card-header"><h3 class="card-title">March 12, 2016</h3></div>
        <div class="card-body">
            <p>The navigation buttons on the story page have been modified so that they line up properly on iPhone 6 (because they don't have text anymore).</p>
            <p>Some groundwork for general user accounts has been done. It's not quite ready for prime time yet.</p>
            <p>The site is no longer fully funded. CloudSearch costs way too much, and I'm investigating doing something else, but until I get that something else working, we need more people to pledge :(</p>
            <p>Some groundwork for showing comment counts in story lists has been done, but again, it's not quite ready for primetime.</p>
        </div>
    </div>

    <div class="card card-default news">
        <div class="card-header"><h3 class="card-title">February 23, 2016</h3></div>
        <div class="card-body">
            <p>All story lists display tags as clickable links.</p>
            <p>Tag search results are properly sorted by date now. Text search results are always displayed based on relevance to the search text.</p>
        </div>
    </div>

    <div class="card card-default news">
        <div class="card-header"><h3 class="card-title">February 21, 2016</h3></div>
        <div class="card-body">
            <p>Authors can now decide whether or not the story should be displayed with HTML links and email addresses automatically made clickable. It defaults to on.</p>
            <p>More than 4 newline breaks in comments will now get collapsed into 1.</p>
        </div>
    </div>

    <div class="card card-default news">
        <div class="card-header"><h3 class="card-title">February 20, 2016</h3></div>
        <div class="card-body">
            <p>Authors now get an email when their story is posted, assuming they enter an email address. They also now get private links so that they can edit their stories.</p>
        </div>
    </div>

    <div class="card card-default news">
        <div class="card-header"><h3 class="card-title">February 19, 2016</h3></div>
        <div class="card-body">
            <p>I made changes to the bottom of the show story pages. The add comment fields are hidden now, and require the user to press a button. The same with adding tags. The order of the comments is now chronological, as expected (oldest down to newest). There are also a couple of buttons to jump around quickly on the screen.</p>
            <p>There's also some new styling: links are bolded now and show when you've clicked on them already.</p>
            <p>Oh! And the nav bar auto-hides when you scroll down, and auto-shows when you scroll up.</p>
        </div>
    </div>

    <div class="card card-default news">
        <div class="card-header"><h3 class="card-title">February 16, 2016</h3></div>
        <div class="card-body">
            <p>One pass of removing stories containing copyrighted, trademarked or real life characters done. I probably missed some and will remove them as I find them.</p>
            <p>Also, I've made the source code for this site available for anyone interested. It's here: <a href="https://bitbucket.org/hughmichelsen/spiral5">https://bitbucket.org/hughmichelsen/spiral5</a></p>
        </div>
    </div>

    <div class="card card-default news">
        <div class="card-header"><h3 class="card-title">February 10, 2016</h3></div>
        <div class="card-body">
            <p>Series are now tracked as a thing. The home page displays them, with the date being that of the last chapter update.</p>
        </div>
    </div>

    <div class="card card-default news">
        <div class="card-header"><h3 class="card-title">February 6, 2016</h3></div>
        <div class="card-body">
            <p>I've modified the rules for story posting to include a new rule: don't post someone else's story here, especially without their permission. Authors still own their work, despite being posted online, and in most cases, they should be asked if they'd like their story to be posted, not told that they can request it to be taken down after the fact.</p>
        </div>
    </div>

    <div class="card card-default news">
        <div class="card-header"><h3 class="card-title">January 22, 2016</h3></div>
        <div class="card-body">
            <p>I've modified the tag search pages to be even smarter with malformed queries. You can specify comma separated lists now, with all the white space you want.</p>
            <p>Also, not that anyone but me cares, but I've made it so that views of the author and story show pages, when supplied with garbage story and author ids, will be throttled so that the request takes a long time. If someone has a reason for sending invalid id's, I'm all ears. Otherwise, I don't want those requests taking up valuable server time and resources when I suspect it's just bots.</p>
        </div>
    </div>

    <div class="card card-default news">
        <div class="card-header"><h3 class="card-title">January 19, 2016</h3></div>
        <div class="card-body">
            <p>I've added an option you can toggle to enlarge text (in the menu on phones or along the right of the navigation bar on the top of the page. Not all of the text has been changed, but the most important parts, for readability, will get larger.</p>
            <p>If there's some other text you feel rather strongly should be larger, please take a screen shot and email it to me, highlighting or circling the part you think should be larger.</p>
            <p><a class="btn btn-lg btn-primary" href="mailto:admin@gayspiralstories.com" role="button">Email me!</a></p>
        </div>
    </div>

    <div class="card card-default news">
        <div class="card-header"><h3 class="card-title">January 17, 2016</h3></div>
        <div class="card-body">
            <p>I've added a popup to verify users ages. It's done with Javascript, and once someone is verified, a cookie is stored so that they don't get asked again...</p>
            <p>...which means side-note: Javascript and Cookies now have to be enabled, or the site won't work.</p>
        </div>
    </div>

    <div class="card card-default news">
        <div class="card-header"><h3 class="card-title">January 12, 2016</h3></div>
        <div class="card-body">
            <p>There are now bigger margins alongside text when viewing individual stories, the tag search pages now allow sorting when clicking on the columns, and the text is no longer made smaller on certain browsers.</p>
        </div>
    </div>

    <div class="card card-default news">
        <div class="card-header"><h3 class="card-title">January 11, 2016</h3></div>
        <div class="card-body">
            <p>Text search works! Try it out <g:link controller="newStory" action="search">here</g:link>.</p>
            <p>There's also some cool new icons, which makes things look that much slicker..</p>
        </div>
    </div>

    <div class="card card-default news">
        <div class="card-header"><h3 class="card-title">January 6, 2016</h3></div>
        <div class="card-body">
            <p>As you can see, there's been a major site overhaul. There's a new look and a new tag searching page.</p>
            <p>As always, if anything's broken or you have suggestions for improvements, send me a mail.</p>
            <p><a class="btn btn-lg btn-primary" href="mailto:admin@gayspiralstories.com" role="button">Email me!</a></p>
        </div>
    </div>

    <div class="card card-default news">
        <div class="card-header"><h3 class="card-title">January 1, 2016</h3></div>
        <div class="card-body">
            <p>Thanks to the generosity of a few awesome members, we're actually fully funded right now. Thanks guys!</p>
            <p><a class="btn btn-lg btn-primary" href="https://www.patreon.com/hughmichelsen" role="button">Patreon Campaign</a></p>
        </div>
    </div>

    </body>
    </html>
</g:applyLayout>