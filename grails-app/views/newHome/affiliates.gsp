<g:applyLayout name="newMain" model="[active:'about']">
    <html>
    <head>
        <title><g:pageTitle title='Recommended Sites'/></title>
        <asset:stylesheet href="affiliates.css"/>
    </head>
    <body>

    <h2 class="text-center mb-5">Other sites worth to be checked out</h2>

    <div class="card-deck mb-5">
        <a class="card" href="https://mcstories.com" target="_new">
            <asset:image class="card-img-top" src="affiliates/MCStories.jpg"/>
            <div class="card-body text-center">
                <h5 class="card-title">MCStories</h5>
                <h6>The Erotic Mind-Control Story Archive</h6>
                A huge collection of stories (straight and gay) around mind-control and hypnosis
            </div>
        </a>
        <a class="card" href="https://www.nifty.org" target="_new">
            <asset:image class="card-img-top" src="affiliates/Nifty.jpg"/>
            <div class="card-body text-center">
                <h5 class="card-title">Nifty</h5>
                <h6>Erotic Stories Archive</h6>
                The probably oldest and one of the biggest collection of gay, lesbian, trangender and other kinky stories
            </div>
        </a>
        <a class="card" href="https://mygaysites.com" target="_new">
            <asset:image class="card-img-top" src="affiliates/MyGaySites.jpg"/>
            <div class="card-body text-center">
                <h5 class="card-title">MyGaySites</h5>
                A hub for free and premium gay porn sites
            </div>
        </a>
    </div>
    <div class="card-deck mb-5">
        <a class="card" href="https://gayauthors.org/" target="_new">
            <asset:image class="card-img-top" src="affiliates/GayAuthors.jpg"/>
            <div class="card-body text-center">
                <h5 class="card-title">Gay Authors</h5>
                A huge collection of free and premium gay stories. Also home of some well-known "Signature Authors"
            </div>
        </a>
        <a class="card" href="https://www.gaydemon.com/" target="_new">
            <asset:image class="card-img-top" src="affiliates/GayDemon.jpg"/>
            <div class="card-body text-center">
                <h5 class="card-title">Gay Demon</h5>
                <h6>Reviews, Sex Stories & Free Gay Porn</h6>
                gaydemon's guide to the best in gay porn since 1999
            </div>
        </a>
        <a class="card invisible" href="https://gayauthors.org/" target="_new">
            <asset:image class="card-img-top" src="affiliates/GayAuthors.jpg"/>
            <div class="card-body text-center">
                <h5 class="card-title">Gay Authors</h5>
                A huge collection of free and premium gay stories. Also home of some well-known "Signature Authors"
            </div>
        </a>
    </div>

    </body>
    </html>
</g:applyLayout>