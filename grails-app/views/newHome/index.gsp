<%@ page import="grails.plugin.springsecurity.SpringSecurityUtils; com.ncmc4.Rating" %>
<g:applyLayout name="newMain" model="[active:'home']">
    <html>
    <head>
        <title><g:pageTitle title='Home'/></title>

        <asset:stylesheet href="newhome.css"/>
    </head>
    <body>

    <g:if test="${bannerNews.size() > 0}">
        <div class="mt-5 newsspacer hidden"></div>
        <div class="row">
            <div class="col">
                <g:each in="${bannerNews}" status="i" var="newsInstance">
                    <div class="card border-primary newsbanner hidden" id="infoBanner-${newsInstance.id}" data-cookie="spiral.cookie.newsBanner-${newsInstance.id}">
                        <h5 class="card-header bg-primary text-light d-flex">
                            <div class="d-flex pr-3 justify-content-between align-items-center flex-wrap flex-shrink-1" style="flex: auto;">
                                <div class="small">
                                    by ${newsInstance?.user?.authorName} <g:userIcon userid="${newsInstance?.user?.id}"/>
                                </div>
                                <div class="card-news-header-text">
                                    Newsflash
                                </div>
                                <div class="small">
                                    <g:formatDate type="date" style="MEDIUM" date="${newsInstance?.creationDate}" />
                                </div>
                            </div>
                            <div class="d-flex card-news-header-hide flex-shrink-1">
                                <button type="button" class="btn btn-info newsHideButton" title="Close this newsflash"><i class="fa fa-times" aria-hidden="true"></i></button>
                            </div>
                        </h5>
                        <div class="card-body">
                            ${raw(newsInstance.textFormatted)}
                        </div>
                    </div>
                </g:each>
            </div>
        </div>
        <div class="mb-5 newsspacer hidden"></div>
    </g:if>

        <%
            params.sort = "lastChapterUpdated"
            params.order = "desc"
        %>

    <div class="row">
        <div class="col">
            <ul class="nav nav-pills mb-3 d-flex align-items-stretch justify-content-around" id="mainSeriesTab" role="tablist">
                <li class="nav-item d-flex text-center">
                    <a class="nav-link active font-weight-bold d-flex align-items-center larger-text" id="latestTab" data-toggle="tab" href="#latest" role="tab" aria-controls="home" aria-selected="false">New Stories</a>
                </li>
                <li class="nav-item d-flex text-center">
                    <a class="nav-link d-flex flex-column align-items-center" id="topTab" data-toggle="tab" href="#top" role="tab" aria-controls="home" aria-selected="false">
                        <g:if test="${topSeriesMap}">
                            <span class="d-inline-block">Top Rated Stories - ${raw(topPeriodName)}</span>
                            <b class="font-italic d-inline-block">${topStoriesSegment+1}<sup>${topStoriesSegmentOrd}</sup> Selection</b>
                        </g:if>
                        <g:else>
                            <span class="d-inline-block">Top Rated Stories</span>
                            <b class="font-italic d-inline-block">Starting January 5<sup>th</sup></b>
                        </g:else>
                    </a>
                </li>
                <g:if test="${randomSeriesList}">
                    <li class="nav-item d-flex text-center">
                        <a class="nav-link d-flex flex-column align-items-center" id="randomTab" data-toggle="tab" href="#random" role="tab" aria-controls="home" aria-selected="false">
                            <span class="d-inline-block">Stories From the Archives</span>
                            <b class="font-italic d-inline-block">${randomSeriesSegment+1}<sup>${randomSeriesSegmentOrd}</sup> Selection</b>
                        </a>
                    </li>
                </g:if>
            </ul>
        </div>
    </div>

    <div class="tab-content" id="mainSeriesTabContent">
        <div class="tab-pane fade show active" id="latest" role="tabpanel" aria-labelledby="latest-tab">
            <div class="d-flex flex-column flex-sm-row justify-content-center justify-content-sm-between">
                <div class="mb-2 d-flex justify-content-center">
                    <g:render template="/templates/categorySelector" model="[instance: '1']"/>
                </div>
                <div class="mb-2">
                    <g:paginate class="justify-content-center d-flex flex-wrap" total="${seriesInstanceCount ?: 0}" />
                </div>
            </div>

            <div class="row">
                <table class="table table-bordered table-striped storyList my-responsive-table" data-rtContainerBreakPoint="539">
                    <thead class="thead-dark">
                    <tr>
                        <th>Author</th>
                        <th>Name</th>
                        <th>Published</th>
                    </tr>
                    </thead>
                    <tbody>
                    <g:each in="${seriesInstanceList}" status="i" var="seriesInstance">
                        <g:render template="/newSeries/seriesTableEntry" model="[seriesInstance: seriesInstance]"/>
                    </g:each>
                    </tbody>
                </table>
            </div>

            <div class="d-flex flex-column flex-sm-row justify-content-center justify-content-sm-between">
                <div class="mb-2 d-flex justify-content-center">
                    <g:render template="/templates/categorySelector" model="[instance: '2']"/>
                </div>
                <div class="mb-2">
                    <g:paginate class="justify-content-center d-flex flex-wrap" total="${seriesInstanceCount ?: 0}" />
                </div>
            </div>
        </div>
        <g:if test="${topSeriesMap == null}">
            <div class="tab-pane fade" id="top" role="tabpanel" aria-labelledby="top-tab">
                <div class="row mb-1">
                    <div class="alert alert-warning mx-auto">
                        <h4 class="text-center">Starting on Saturday, January 5th, you'll find a new selection of top rated stories here!</h4>
                        <div class="text-center">Every other week we'll present a new selection of one of the highest rated stories or series for each rating category.</div>
                        <div class="text-center">Then you'll also be able to <g:link action="listTopRated" controller="newStory">view the list of all top rated stories presented so far</g:link>!</div>
                    </div>
                </div>
            </div>
        </g:if>
        <g:else>
            <div class="tab-pane fade" id="top" role="tabpanel" aria-labelledby="top-tab">
                <div class="row">
                    <div class="alert alert-info mx-auto">
                        <h4 class="text-center">Current selection of the top rated stories - ${raw(topPeriodName)}</h4>
                        <h5 class="text-center">${topStoriesSegment+1}<sup>${topStoriesSegmentOrd}</sup> selection</h5>
                        <div class="text-center">Every other Saturday we present a new selection of one of the highest rated stories or series for each rating category.</div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="mx-auto">
                        <g:link action="listTopRated" controller="newStory" class="button btn-sm btn-secondary">List of all top rated stories presented so far</g:link>
                    </div>
                </div>
                <g:render template="/newSeries/topRatedSeriesList" model="[topSeriesMap: topSeriesMap]"/>
                <div class="row mb-4">
                    <div class="mx-auto">
                        <g:link action="listTopRated" controller="newStory" class="button btn-sm btn-secondary">List of all top rated stories presented so far</g:link>
                    </div>
                </div>
            </div>
        </g:else>
        <g:if test="${randomSeriesList}">
            <div class="tab-pane fade" id="random" role="tabpanel" aria-labelledby="top-tab">
                <div class="row mb-1">
                    <div class="alert alert-info mx-auto">
                        <h4 class="text-center">This week's selection of stories from the archives</h4>
                        <h5 class="text-center">${randomSeriesSegment+1}<sup>${randomSeriesSegmentOrd}</sup> selection</h5>
                    <div class="text-center">Each Wednesday we'll present ${randomSeriesPerSegment} stories or series selected randomly out of our archives</div>
                </div>
            </div>
            <div class="row">
                <table class="table table-bordered table-striped randomSeriesList my-responsive-table" data-rtContainerBreakPoint="539">
                    <thead class="thead-dark">
                        <tr>
                            <th>Author</th>
                            <th>Name</th>
                            <th>Published</th>
                        </tr>
                    </thead>
                    <tbody>
                        <g:each in="${randomSeriesList}" status="i" var="seriesInstance">
                            <g:render template="/newSeries/seriesTableEntry" model="[seriesInstance: seriesInstance]"/>
                        </g:each>
                    </tbody>
                </table>
            </div>
        </g:if>
    </div>

    </body>

    <%
        //sort=name&max=30&order=asc
        if (!params.max)
            params.max = 10
        if (!params.offset)
            params.offset = 0
    %>

    <script>
        <asset:script>
            (function ($, window, document, undefined)
            {
                'use strict';

                $(function ()
                {
                    $(document).on('shown.bs.tab', 'a[data-toggle="tab"]', function (e) {
                        fix_responsive_tables();
                    });

                    $('#mainSeriesTab a').on('click', function (e) {
                      e.preventDefault()
                      $(this).tab('show')
                      var href = $(this).attr('href')
                      localStorage.setItem("mainSeriesTabActive",href);
                      if(href == '#top') {
                        var hasSeen = localStorage.setItem("hasSeenTopTab", "${topStoriesSegmentId}")
                        $(this).removeClass("topStoriesPulse")
                      }
                      else if(href == '#random') {
                        var hasSeen = localStorage.setItem("hasSeenRandomTab", "${randomSeriesSegmentId}")
                        $(this).removeClass("topStoriesPulse")
                      }
                    })

                    var lastTab = localStorage.getItem("mainSeriesTabActive")
                    if(lastTab)
                        $('#mainSeriesTab a[href="' + lastTab + '"]').tab('show')

                    if(lastTab != '#top') {
                        var hasSeen = localStorage.getItem("hasSeenTopTab")
                        if(hasSeen != "${topStoriesSegmentId}") {
                            $("#topTab").addClass("topStoriesPulse")
                        }
                    }

                    if(lastTab != '#random') {
                        var hasSeen = localStorage.getItem("hasSeenRandomTab")
                        if(hasSeen != "${randomSeriesSegmentId}") {
                            $("#randomTab").addClass("topStoriesPulse")
                        }
                    }
                })

                $(function ()
                {
                    var thresID = sessionStorage.getItem("lastStoryID");

                    var sessionUpdate = new Date(sessionStorage.getItem("lastStoryIDtime"));
                    if(sessionUpdate) {
                        var anHourAgo = new Date(Date.now() - 1000 * 60 * 60);
                        if(sessionUpdate < anHourAgo) {
                            thresID = null;
                        }
                    }

                    sessionStorage.setItem("lastStoryIDtime",new Date().toJSON());

                    if(! thresID)
                    {
                        thresID = localStorage.getItem("lastStoryID")
                        if(! thresID)
                            thresID = 0;

                        sessionStorage.setItem("lastStoryID",thresID);
                    }

                    var highID = thresID;

                    $(".newStoryStar").each(function(index,elem) {
                        var itemID = $(elem).data('id');
                        if(itemID > thresID)
                            $(elem).removeClass("hidden");
                        else
                            $(elem).addClass("hidden");
                        if(itemID > highID)
                            highID = itemID;
                    });

                    localStorage.setItem("lastStoryID",highID);
                });

                $(function ()
                {
                    var updateHash = "${updateHash}";
                    var ids = [${ids.join(',')}];
                    var data = {
                        updateHash: updateHash,
                        ids: ids,
                        offset: "${params.offset}",
                        max: "${params.max}"
                    };

                    $.ajax({
                        url: "${createLink(action: 'fetchCommentCounts', controller: 'newHome')}",
                        type: 'GET',
                        data: data,
                        success: function(response) {
                            // replace the fields for the comment count and the story id link
                            var items = response.comments

                            for (var i = 0; i < items.length; i++) {
                                var item = items[i];
                                if (item.commentCount > 0) {
                                    var html = "<a class='badge badge-primary' href='" + item.commentLink + "#comments'><i class='fa fa-comments'></i> " + item.commentCount + "</a>";
                                    $("#comments-" + item.seriesId).html(html);
                                }
                            }
                        },
                        error: function(response) {
                        },
                        complete: function() {
                        }
                    });
                });

                $(function ()
                {
                    $(".newsbanner").each( function( index, el )
                    {
                        var newsElem = $(el);
                        var cookieName = newsElem.data("cookie");
                        var cookieTextVerified = "seen";
                        var cookieText = localStorage.getItem(cookieName);

                        if (cookieText != cookieTextVerified) {
                            cookieText = Cookies.get(cookieName);
                            localStorage.setItem(cookieName, cookieText);
                        }

                        if (cookieText != cookieTextVerified) {
                            newsElem.removeClass("hidden");
                            $(".newsspacer").removeClass("hidden");

                            newsElem.find(".newsHideButton").on("click", function () {
                                newsElem.fadeOut();

                                localStorage.setItem(cookieName, cookieTextVerified);
                            });
                        }
                    });
                });

                $(function ()
                {
                    $(".updateTitleButton").click(
                        function() {

                            var id = $(this).data('id');
                            var textInputName = "#seriesname-" + id;
                            var textInput = $(textInputName);

                            var name = textInput.val();

                            var data = {'id': id, name: name};

                            textInput.attr("disabled", 1);

                            $.ajax({
                                url: "${createLink(action: 'updateSeriesName', controller: 'newHome')}",
                                type: 'GET',
                                data: data,
                                success: function(response) {
                                },
                                error: function(response) {
                                    textInput.val("Update failed!");
                                },
                                complete: function() {
                                    textInput.removeAttr("disabled");
                                }
                            });
                        }
                    );
                });
            })(jQuery, window, document);
        </asset:script>

    </script>

    </html>
</g:applyLayout>