<%@ page import="com.ncmc4.Activity" %>

<g:applyLayout name="newMain" model="[active:'news']">
    <html>
    <head>
        <meta name="robots" content="noindex">
        <title><g:pageTitle title='Activity'/></title>
    </head>
    <body>

    <div class="row">
        <div class="col">
            <h1 class="center-block text-center">Recent Activity</h1>
        </div>
    </div>


    <div class="row">
        <div class="col">
            <g:paginate class="justify-content-center d-flex flex-wrap" action="activity" total="${count ?: 0}" />
        </div>
    </div>

    <div class="table-responsive-lg">
        <table id="activityList" class="table table-sm table-bordered table-striped storyList">
            <thead class="thead-light">
            <tr>
                <g:sortableColumn property="created" title="Created"/>
                <g:sortableColumn property="class" title="Type"/>
                <th><span class="text-primary">Object</span></th>
                <th><span class="text-primary">Action</span></th>
                <th><span class="text-primary">Admin User</span></th>
            </tr>
            </thead>
            <tbody>
            <g:each in="${activityList}" status="i" var="activityInstance">
                <tr data-id="${activityInstance.id}">
                    <td><g:formatDate date="${activityInstance.created}" /></td>
                    <td>${activityInstance?.class?.name}</td>

                    <td>
                        <g:if test="${activityInstance.class == com.ncmc4.StoryActivity}">
                            <g:link action="show" id="${activityInstance.story?.id}" controller="newStory">${activityInstance.story?.name}</g:link>
                        </g:if>
                        <g:elseif test="${activityInstance.class == com.ncmc4.UserActivity}">
                            #${activityInstance.activityUserId} - ${activityInstance.activityUsername}
                        </g:elseif>
                        <g:else>
                            ${activityInstance.activityTagName}
                        </g:else>
                    </td>

                    <td>${activityInstance.action}</td>

                    <td>${activityInstance.user?.username}</td>
                </tr>
            </g:each>
            </tbody>
        </table>
    </div>


    <div class="row">
        <div class="col">
            <g:paginate class="justify-content-center d-flex flex-wrap" action="activity" total="${count ?: 0}" />
        </div>
    </div>

    </body>
    </html>
</g:applyLayout>