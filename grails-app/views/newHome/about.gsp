<g:applyLayout name="newMain" model="[active:'about']">
    <html>
    <head>
        <title><g:pageTitle title='About'/></title>
    </head>
    <body>

    <div class="jumbotron homenews">
        <div class="container">
            <p>
                Welcome to the Gay Spiral Mind Control Story Collection!
            </p>
            <p>
                This is one of the few places on the web where you can come to read stories of men cursed, hypnotized, converted from straight to gay, converted from gay to straight, shrunk, grown, muscled up, muscled down, humiliated, and/or ensorcelled. Most stories feature some form or other of mind control, and sex.
            </p>
            <p>
                Interested in submitting a story? You can submit a story <g:link controller="newStory" action="create">here</g:link>. Once the story's been submitted, it will be approved shortly after, usually within 12 hours (assuming it doesn't violate any of the rules, isn't too short, and features men and some kind of mind control).
            </p>
            <!-- <p>
                Looking to support the site? Depending on the features enabled on the site, it costs anywhere from $25 to over $100, every month. If you'd like to see more features on the site, consider pledging to the <a href="https://www.patreon.com/hughmichelsen">Patreon campaign</a>.
            </p> -->
            <p>
                Want to send some feedback on the site? Request a new feature? Or request a change to stories you've already submitted? Send us a mail at <a href="mailto:admin@gayspiralstories.com">admin@gayspiralstories.com</a>.
            </p>
            <h4>Legal stuff</h4>
            <ul style="font-size: small;">
                <li><g:link controller="info" action="tos">Terms of Service</g:link><br/></li>
                <li><g:link controller="info" action="privacy">Privacy Policy</g:link></li>
            </ul>
            <h4>Credits</h4>
            <ul style="font-size: small;">
                <li>Coding: Hugh Michelsen & Martin
                <li>Design: Hugh, Martin, Cris Kane, Foxtrotter & Bulk
            </ul>
        </div>
    </div>

    </body>
    </html>
</g:applyLayout>