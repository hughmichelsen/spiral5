<rss version="2.0" xmlns:dc="http://purl.org/dc/elements/1.1/">
    <channel>
        <description>Stories about mind controlled men.</description>
        <title>${title}</title>
        %{--<generator>Tumblr (3.0; @betafinder)</generator>--}%
        <link>${link}</link>
        %{--<image>--}%
            %{--<url>http://kylewbanks.com/static/images/favicon.ico</url>--}%
            %{--<title>Kyle W. Banks</title>--}%
            %{--<link>http://kylewbanks.com</link>--}%
        %{--</image>--}%
        <g:each in="${list}" var="item">
            <item>
                <title>${item.title}</title>
                <description>${item.description}</description>
                <link>${item.link}</link>
                <guid>${item.id}</guid>
                <pubDate>${item.pubDate}</pubDate>
                <category>other</category>
            </item>
        </g:each>
    </channel>
</rss>