<%@ page import="com.ncmc4.SecRole" contentType="text/html;charset=UTF-8" %>
<table>
    <tr><td>Title</td><td><g:link controller="newStory" action="show" id="${storyInstance?.id}" absolute="true">${storyInstance?.name}</g:link></td></tr>
    <tr><td>Author</td><td><g:link controller="newAuthor" action="show" id="${storyInstance?.author?.id}" absolute="true">${storyInstance?.author?.displayName}</g:link></td></tr>
    <tr><td>Creation Date</td><td><g:formatDate date="${storyInstance.creationDate}" /></td></tr>
    <tr><td>Author Email</td><td><a href="mailto:${storyInstance.emailAddress}">${storyInstance.emailAddress}</a></tr>
    <g:if test="${storyInstance.prevInSeries}">
        <tr><td>Last Chapter</td><td><g:link action="show" id="${storyInstance?.prevInSeries?.id}" absolute="true">${storyInstance?.prevInSeries?.name}</g:link></td></tr>
    </g:if>
    <g:if test="${storyInstance.nextInSeries}">
        <tr><td>Next Chapter</td><td><g:link action="show" id="${storyInstance?.nextInSeries?.id}" absolute="true">${storyInstance?.nextInSeries?.name}</g:link></td></tr>
    </g:if>
    <tr><td>Category</td><td>${storyInstance?.category?.tag}</td></tr>
    <tr><td>Description</td><td>${storyInstance.summary}</td></tr>
    <g:if test="${storyInstance?.tags}">
        <tr><td>Tags</td><td>${(storyInstance?.tags*.tag).join(', ')}</td></tr>
    </g:if>
</table>
<p>- The GSS Admin staff</p>