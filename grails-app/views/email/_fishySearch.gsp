<h3>Fishy search request!</h3>
<table>
    <tr><td>Search Params</td><td>${searchParams?.encodeAsHTML()}</td></tr>
    <tr><td>Exception</td><td>${exception?.toString()?.encodeAsHTML()}</td></tr>
    <tr><td>SQL</td><td>${sql?.encodeAsHTML()}</td></tr>
    <tr><td>Source</td><td>${source?.encodeAsHTML()}</td></tr>
</table>

<p>- The GSS Admin staff</p>