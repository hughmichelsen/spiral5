<%@ page import="com.ncmc4.SecRole" contentType="text/html;charset=UTF-8" %>
<p>Greetings!</p><p>A new comment has been added to one of your stories. Here are the details. Enjoy!</p>
<table>
    <tr><td>Story</td><td><g:link controller="newStory" action="show" id="${storyInstance?.id}" absolute="true">${storyInstance?.name}</g:link></td></tr>
    <tr><td>Comment Author</td><td>${commentAuthor}</td></tr>
    <tr><td>Comment</td><td>${raw(commentText)}</td></tr>
</table>

<p>- The GSS Admin staff</p>