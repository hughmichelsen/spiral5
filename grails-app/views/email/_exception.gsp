<%@ page import="com.ncmc4.SecRole" contentType="text/html;charset=UTF-8" %>
<h3>Error on GSS!</h3>
<table>
    <tr><td>Request Exception</td><td>${request?.'javax.servlet.error.exception_type'?.encodeAsHTML()} / ${request?.'javax.servlet.error.exception'?.encodeAsHTML()}: ${request?.'javax.servlet.error.message'?.encodeAsHTML()}</td></tr>
    <tr><td>URI</td><td>${request?.'javax.servlet.error.request_uri'?.encodeAsHTML()} - ${request?.'javax.servlet.error.servlet_name'?.encodeAsHTML()}</td></tr>
    <tr><td>Wrapped Exception</td><td>${exception?.className?.encodeAsHTML()} @ ${exception?.lineNumber?.encodeAsHTML()}: ${exception?.message?.encodeAsHTML()}</td></tr>
    <tr><td>GspFile</td><td>${exception?.gspFile?.encodeAsHTML()}</td></tr>
    <tr><td>StackTrace</td><td>${exception?.stackTraceText?.encodeAsHTML()}</td></tr>
    <tr><td>Source</td><td>${source?.encodeAsHTML()}</td></tr>
</table>

<p>- The GSS Admin staff</p>