<p>Greetings!</p>
<p>Your story "${storyInstance.name}" has been submitted to the <a href="https://www.gayspiralstories.com">Gay Spiral Stories!</a> The site runs on authors like you posting their stories. Thank you!</p>
<p>Assuming your story doesn't violate any rules, it should be posted within 12 hours.</p>
<p>If you'd like to view your story now, you can do so here: <a href="${showLink}">${showLink}</a>.</p>
<p>If you'd like to edit your story, you can do so here: <a href="${editLink}">${editLink}</a>.</p>
<p>Keep in mind that each time you edit your story after it's been posted, the previous version will still be shown to all other visitors until an Admin has approved your edit.</p>
<p>Feel free to edit your stories as often as you want. We don't mind approving edits (all your changes are highlighted automatically), and it's always good to improve a story!</p>
<p>DO NOT SHARE THESE LINKS WITH OTHERS! These are private to you, so that you can edit and view your story. They may become invalid if the Admins have reason to believe you have shared them with anyone else.</p>
<p>If you have any questions, feel free to reply this email.</p>
<p>- The GSS Admin staff</p>