<h2>A password reset for your account at the Gay Spiral Stories has been requested!</h2>
<p>If you requested this password reset yourself, <g:link controller="User" action="showResetPassword" params="['emailAddress': userInstance?.emailAddress, 'unique': unique]" absolute="true">click on this link to enter a new password</g:link>.</p>
<p><strong>Note that the above link will expire in 30 minutes!</strong><br/>
    After that, you have to request a new password reset mail <g:link rel="noFollow" controller="User" action="forgotPassword" absolute="true">here</g:link>.</p>
If you have problems resetting your password or logging on to the site, please contact the site admin (<a href="mailto:admin@gayspiralstories.com">admin@gayspiralstories.com</a>)!
<p/>
<p>If you did not request a password reset, please forward this mail to <a href="mailto:admin@gayspiralstories.com">admin@gayspiralstories.com</a>. Someone is most likely trying to hack your account.</p>
<p>- The GSS Admin staff</p>