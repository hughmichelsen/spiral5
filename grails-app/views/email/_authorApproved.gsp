<p>Greetings!</p>
<p>Your story "${storyInstance.name}", submitted to the Gay Spiral Stories, has been approved!</p>
<p>View it here: <a href="${showLink}">${showLink}</a>.</p>
<g:if test="${createdWithUserAccount}">
    <p>Edit it here: <a href="${editLink}">${editLink}</a>.</p>
</g:if>
<g:else>
    <p>Why haven't you created an user account yet? Especially if you're an author, using an account gives you many advantages. All of your stories can be linked to your account, giving you the ability to edit them, read access statistics and the ratings.</p>
</g:else>
<p>If you have any questions, feel free to reply to this email.</p>

<p>- The GSS Admin staff</p>