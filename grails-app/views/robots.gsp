User-agent: *
Disallow: /author/
Disallow: /story/
Disallow: /newStory/fetchComments
Disallow: /user/getCurrentUserData
Disallow: /newStory/showSecret
Disallow: /newStory/editSecret
