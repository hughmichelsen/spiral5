package com.ncmc4

import difflib.DiffUtils
import difflib.Delta

class ArchiveService {
    static transactional = false
    static scope = "prototype"

    StoryTextArchive createRevision(newStory, authorName, seriesName) {
        // we don't handle unformatted stories
        if (!newStory.unformatted)
            return null

        // only have one active at once
        if (StoryTextArchive.findByStory(newStory))
            return null

        boolean changed = false
        def params = [story: newStory]

        // use a new session to get the old version of the story
        Story.withNewSession {
            Story original = Story.read(newStory.id)

            params.oldText = original.text
            params.oldAuthorName = original.author.name
            params.oldName = original.name
            params.oldSummary = original.summary
            params.oldSeriesName = original.series.name

            if (original.text != newStory.text)
                changed = true

            if (original.author.name != authorName)
                changed = true

            if (original.name != newStory.name)
                changed = true

            if (original.summary != newStory.summary)
                changed = true

            if (original.series.name != seriesName)
                changed = true
        }

        if (!changed)
            return null

        StoryTextArchive archive = new StoryTextArchive(params)

        return archive
    }

    String generateHtmlDiff(String original, String revised) {
        // convert the text to lines
        def originalLines = original.split("\n") as List<String>
        def revisedLines = revised.split("\n") as List<String>

        // get the patches
        def patch = DiffUtils.diff(originalLines, revisedLines)

        StringBuilder sb = new StringBuilder()
        String output = ""

        // apply to the original

        // red strikethrough for delete
        // green for added
        // anchors, so it can be linked to next and prev

        int lastPosition = 0;
        for (Delta delta: patch.getDeltas()) {
            if (lastPosition < delta.original.position) {
                sb.append(originalLines.subList(lastPosition, delta.original.position).join("\n"))
                sb.append("\n")
                lastPosition = delta.original.position
            }

            switch (delta.type)
            {
                case Delta.TYPE.CHANGE:
                    applyChange(delta, sb)
                    break

                case Delta.TYPE.DELETE:
                    applyDelete(delta, sb)
                    break

                case Delta.TYPE.INSERT:
                    applyInsert(delta, sb)
                    break
            }

            // move us up
            lastPosition += delta.original.size()
        }

        if (lastPosition < originalLines.size()) {
            sb.append(originalLines.subList(lastPosition, originalLines.size()).join("\n"))
        }

        return sb.toString()
    }

    private void appendLines(lines, StringBuilder sb) {
        boolean first = true
        for (String line : lines) {
            if (!first) {
                sb.append("<br>\n")
            }
            // replace cr's with soemthing more visible
            line = line.replace("\r", "&#x23CE")
            sb.append(line)
        }
    }

    private void applyChange(Delta delta, StringBuilder sb) {
        sb.append("<span class='revision revision-removed'>")
        appendLines(delta.original.lines, sb)
        sb.append("</span>\n")

        sb.append("<span class='revision revision-inserted'>")
        appendLines(delta.revised.lines, sb)
        sb.append("</span>\n")
    }

    private void applyDelete(Delta delta, StringBuilder sb) {
        sb.append("<span class='revision revision-removed'>")
        appendLines(delta.original.lines, sb)
        sb.append("</span>\n")
    }

    private void applyInsert(Delta delta, StringBuilder sb) {
        sb.append("<span class='revision revision-inserted'>")
        appendLines(delta.revised.lines, sb)
        sb.append("</span>\n")
    }
}