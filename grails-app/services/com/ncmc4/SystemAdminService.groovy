package com.ncmc4

import grails.core.GrailsApplication
import groovy.sql.GroovyRowResult
import groovy.sql.Sql
import org.grails.web.util.WebUtils;
import static grails.async.Promises.*

import java.util.concurrent.Executors

class SystemAdminService {
    static scope = "singleton"
    static transactional = false

    GrailsApplication grailsApplication
    StoryService storyService
    def seriesService
    def mailService

    def threadPool = Executors.newFixedThreadPool(1)

    def getLogPrefix() {
        def ip = getIp()
        def request = WebUtils.retrieveGrailsWebRequest().getCurrentRequest()

        return "IP: ${ip}-${request.getHeader('User-Agent')} - "
    }

    def getIp()
    {
        def request = WebUtils.retrieveGrailsWebRequest().getCurrentRequest()

        // we're running behind ELB, so use the ELB headers to get info on the client ip, if we can
        String ret = request.getHeader("X-Forwarded-For")
        if (ret == null)
            ret = request.getHeader("Client-IP")
        if (ret == null)
            ret = request.getRemoteAddr()

        def comma = ret?.indexOf(",")
        if (comma != null && comma >= 0)
            ret = ret.substring(0, comma);

        return ret
    }

    def fixUserCounts() {
        threadPool.submit {
            log.info "Starting user count fixing thread"
            SecUser.withNewSession { dbSession ->
                int offset = 0
                int max = 50

                def query = {
                    isNull("commentCount")
                    isNull("likeCount")
                    isNull("storyCount")
                }

                def criteria = SecUser.createCriteria()
                def users = criteria.list([offset: offset, max: max], query)
                while (users) {
                    try {
                        log.error "Fixing user counts for $users"

                        users.each { SecUser user ->
                            def userCriteria = UserStoryStats.createCriteria()
                            user.likeCount = userCriteria.count {
                                eq('user', user)
                                eq('liked', true)
                            }

                            userCriteria = Story.createCriteria()
                            user.storyCount = userCriteria.count {
                                owners {
                                    eq("id", user.id)
                                }
                            }

                            userCriteria = Comment.createCriteria()
                            user.commentCount = userCriteria.count {
                                eq('poster', user)
                            }

                            user.save flush:true, failOnError:true
                        }
                    }
                    catch (e) {
                        log.error "Exception while fixing user counts: ", e
                    }

                    offset += max

                    criteria = SecUser.createCriteria()
                    users = criteria.list([offset: offset, max: max], query)
                }
            }
            log.info "Finished user count fixing thread"
        }
    }

    def resetSeriesLinks() {
        try
        {
            def runningTask = task {
                log.info "Starting most recent story fix thread"
                Story.withNewSession { dbSession ->
                    def seriesList = Series.getAll()
                    if (seriesList) {
                        log.info "Num of Series: ${seriesList.size()}"
                        try {
                            seriesList.eachWithIndex { Series series, index ->
                                if (index % 100 == 0)
                                    log.info "Batch: ${index}"

                                seriesService.resetSeriesLinks(series)
                            }
                        } catch (e) {
                            log.error "Exception while fixing most recent stories in series: ", e
                        }
                    }
                }
                log.info "Finished most recent story fix thread"
            }
        }
        catch(Exception e) {
            log.error(e);
        }
    }

    def fixCommentCounts(Boolean rebuildAll = false) {
        try
        {
            def runningTask = task {
                log.info "Starting comment fix thread"
                Story.withNewSession { dbSession ->
                    int offset = 0
                    int max = 50

                    def criteria = Story.createCriteria()
                    def stories = criteria.list([offset: offset, max: max]) {
                        if (!rebuildAll)
                            isNull("commentCount")
                    }

                    log.info "Fixing comment counts for ${stories}"
                    while (stories) {
                        try {
                            stories.each { Story story ->
                                log.debug "Fixing for story: ${story}"
                                story.commentCount = story.comments.size()
                                story.save failOnError: true
                            }

                            log.info "Flushing story comment batch: $offset (${stories.size()})"
                            dbSession.flush()
                            dbSession.clear()
                        } catch (e) {
                            log.error "Exception while fixing comment counts: ", e
                        }

                        dbSession.flush()
                        dbSession.clear()

                        offset += max

                        criteria = Story.createCriteria()
                        stories = criteria.list([offset: offset, max: max]) {
                            if (!rebuildAll)
                                isNull("commentCount")
                        }
                    }
                }
                log.info "Finished comment fix thread"
            }
        }
        catch(Exception e) {
            log.error(e);
        }
    }

    def patreonClient

    def resetPatreon() {
        try {
            SecRole patreonRole = SecRole.createIfNeeded(SecRole.ROLE_PATREON)

            // Remove existing assignments
            SecUserSecRole.removeAll(patreonRole, true)

            List<String> patreonMails = patreonClient.getCachedPatreonEmailAddresses()

            patreonMails.each {
                SecUser user = SecUser.findByEmailAddressIlike(it)
                if(user) {
                    SecUserSecRole.create(user,patreonRole,true)
                    log.info "Assigning patreon role to ${user.emailAddress} (${user.authorName})!"
                }
                else
                    log.info "Patreon ${it} not found!"
            }
        }
        catch(Exception e) {
            log.error(e);
        }
    }

    def sendAdminMail(def subjectText, def htmlBody) {
        try {
            def adminEmail = grailsApplication.config.getProperty("grails.adminEMail")
            if(adminEmail) {
                mailService.sendMail {
                    to adminEmail
                    subject subjectText
                    html htmlBody
                }
            }
        }
        catch (Exception e) {
            log.error "Error loggin exception: ${e?.message}"
        }
    }

    private TimeZone setCurrentTimezone(def timeZone) {
        def request = WebUtils.retrieveGrailsWebRequest().getCurrentRequest()

        def ses = request.getSession(true)
        ses.timeZone = timeZone
    }

    private TimeZone getCurrentTimezone() {
        def request = WebUtils.retrieveGrailsWebRequest().getCurrentRequest()

        def ses = request.getSession(true)
        def ret = ses.timeZone

        return ret
    }

    TimeZone getUserTimeZone() {
        TimeZone timeZone = getCurrentTimezone()

        // Fallback to cookie, for performance
        if (!timeZone) {
            def request = WebUtils.retrieveGrailsWebRequest().getCurrentRequest()

            def tzone = request.getCookies().find { it.name == 'timezone' }?.value
            if (tzone) {
                timeZone = TimeZone.getTimeZone(tzone) ?: "UNKNOWN"
                setCurrentTimezone(timeZone)
            } else
                timeZone = TimeZone.getDefault()
        }

        return timeZone
    }
}