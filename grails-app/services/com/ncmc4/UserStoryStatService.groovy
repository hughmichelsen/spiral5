package com.ncmc4

import java.util.concurrent.Executors

import static grails.plugin.springsecurity.SpringSecurityUtils.*

class UserStoryStatService {
    static transactional = false
    static scope = "singleton"

    def storyStatsUpdatePool = Executors.newFixedThreadPool(1)

    def systemAdminService

    // query for whether or not this user liked a list of stories or series
    Map<Story, Boolean> getUserLikes(SecUser user, Collection<Story> stories) {
        UserStoryStats statsList = UserStoryStats.findAllByStoryInList(stories)

        Map<Story, Boolean> ret = [:]

        statsList.each { UserStoryStats stats ->
            ret[stats.story] = stats.liked
        }

        return ret
    }

    Integer countStoryLikes(Story story) {
        def criteria = UserStoryStats.createCriteria()
        return criteria.count {
            eq('story', story)
            eq('liked', true)
        }
    }

    // returns a Map -> [list: Collection<Story>, count: Integer]
    def findLikedStories(Map paginationParams, SecUser user) {
        def isAdmin = ifAnyGranted(SecRole.ROLE_SUPER_ADMIN)

        // Basic query (stories with liked stats for this user)
        String query = """
            select s.story from UserStoryStats as s
            inner join s.story as story
            where
                (s.user = :user)
                and
                (s.liked = :liked)
            """

        String countQuery = """
            select count(*) from UserStoryStats as s
            inner join s.story as story
            where
                (s.user = :user)
                and
                (s.liked = :liked)
            """

        def queryArgs = [user:user, liked: true]

        // only admins get to see deleted and unapproved stories
        if (!isAdmin)
        {
            def adminSubQuery = """
                and
                (s.story.deleted = :deleted)
                and
                (s.story.approved = :approved)
                """

            query += adminSubQuery
            countQuery += adminSubQuery

            queryArgs.deleted = false
            queryArgs.approved = true
        }

        // have to handle sorting manually for HQL queries
        // and have to sanitize them
        def sort = paginationParams.containsKey("sort") ? paginationParams.sort : "likedOn"

        def allowedOrders = ["desc", "asc"]
        def order = paginationParams.containsKey("order") ? paginationParams.order.toLowerCase() : "desc"

        if (!allowedOrders.contains(order))
            order = allowedOrders[0]

        // also, so that we don't have to switch everything else, we'll convert the story fields
        // (authorSlug, nameSlug and creationDate) into join query stuff
        switch (sort) {
            case "likedOn":
            default:
                sort = "s.likedOn"
                break

            case "authorSlug":
                sort = "s.story.authorSlug"
                break

            case "nameSlug":
                sort = "s.story.nameSlug"
                break

            case "publishDate":
                sort = "s.story.publishDate"
                break
        }

        query += "ORDER BY $sort $order"

        def stories = UserStoryStats.executeQuery(query.toString(), queryArgs, paginationParams)
        def count = UserStoryStats.executeQuery(countQuery.toString(), queryArgs)[0]

        return [list: stories, count: count]
    }

    Boolean isLiked(SecUser user, Story story) {
        UserStoryStats ret = UserStoryStats.findByUserAndStory(user, story)
        return ret?.liked ?: false
    }

    // like or unlike a particular story (create the stats for the user, if needed)
    void toggleLike(SecUser user, Story story, Boolean like) {
        def stats = createOrGet(user, story)

        stats.liked = like

        // only update the liked date if they liked it
        if (like)
            stats.likedOn = new Date()

        stats.save failOnError: true, flush: true
    }

    private void incrementStoryViewCounts(Story story, boolean isNewUserView) {

        if(! story || story?.id == 0)
            return

        // make a local copy of the story id, so that we don't hold onto the story instance outside of the session it was
        // fetched with
        Long storyId = story?.id

        // for extra special db contention freeness, we'll use a single thread to update story counts
        storyStatsUpdatePool.submit {
            try {
                StoryStats.withNewSession {
                    // attempt to do a fast as possible, can't fail update on the db
                    // by not requesting the data back and not making anything conditional
                    if (isNewUserView)
                        StoryStats.executeUpdate("update StoryStats set totalViewCount = (totalViewCount + 1), loggedInUserViewCount = (loggedInUserViewCount + 1), version = (version + 1) where story.id = :storyId", [storyId: storyId])
                    else
                        StoryStats.executeUpdate("update StoryStats set totalViewCount = (totalViewCount + 1), version = (version + 1) where story.id = :storyId", [storyId: storyId])
                }
            } catch (e) {
                log.error "${systemAdminService?.logPrefix}Error updating story stat counts for story id ${storyId}. Exception: ", e
            }
        }
    }

    void incrementUserStoryViewCounts(SecUser user, Story story) {

        if(! story)
            return

        boolean isUsersFirstViewOfThisStory = false
        if (user != null) {
            UserStoryStats stats = UserStoryStats.findByUserAndStory(user, story)
            isUsersFirstViewOfThisStory = stats == null
            if (isUsersFirstViewOfThisStory) {
                stats = new UserStoryStats(user: user, story: story)
            }

            stats.lastView = new Date()
            stats.viewCount++

            stats.save flush: true, failOnError: true
        }

        incrementStoryViewCounts(story, isUsersFirstViewOfThisStory)
    }

    private UserStoryStats createOrGet(SecUser user, Story story) {
        UserStoryStats ret = UserStoryStats.findByUserAndStory(user, story)
        if (ret)
            return ret

        ret = new UserStoryStats(user: user, story: story)
        return ret
    }
}