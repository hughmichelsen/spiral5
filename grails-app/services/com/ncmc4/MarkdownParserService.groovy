package com.ncmc4

import android.util.Patterns
import com.vladsch.flexmark.ext.gfm.strikethrough.StrikethroughSubscriptExtension
import com.vladsch.flexmark.ext.tables.TablesExtension
import com.vladsch.flexmark.ext.typographic.TypographicExtension
import com.vladsch.flexmark.html.HtmlRenderer
import com.vladsch.flexmark.parser.Parser
import com.vladsch.flexmark.superscript.SuperscriptExtension
import com.vladsch.flexmark.util.options.MutableDataSet

class MarkdownParserService {

    def htmlCleaner

    def options = new MutableDataSet()
    def parser
    def renderer

    MarkdownParserService() {
        if (!renderer) {
            options.set(Parser.EXTENSIONS, Arrays.asList(
                    TablesExtension.create(),
                    StrikethroughSubscriptExtension.create(),
                    SuperscriptExtension.create(),
                    TypographicExtension.create()
            )).
                    set(Parser.BLOCK_QUOTE_PARSER, true).
                    set(Parser.FENCED_CODE_BLOCK_PARSER, false).
                    set(Parser.FENCED_CODE_CONTENT_BLOCK, false).
                    set(Parser.HEADING_NO_LEAD_SPACE, true).
                    set(Parser.HTML_BLOCK_PARSER, false).
                    set(Parser.INDENTED_CODE_BLOCK_PARSER, false)

            parser = Parser.builder(options).build()
            renderer = HtmlRenderer.builder(options).build()
        }
    }

    String formatText(String text, Boolean autoExpandLinksInText) {
        // String formatted = text?.replaceAll(/\r\n/, "<br>")
        String formatted = text

        if (autoExpandLinksInText) {

            // hyperlinks
            def m = Patterns.WEB_URL.matcher(formatted)
            StringBuffer result = new StringBuffer()
            while (m.find()) {
                String url = m.group(0)

                // don't match the ends of email addresses
                if (url.count('.') > 1) {
                    if (m.group(2) == null)
                        url = "http://" + url

                    String displayText = m.group(0)
                    if (displayText.length() > 32)
                        displayText = "click here"

                    m.appendReplacement(result, "<a href='${url}' target='blank'>${displayText}</a>")
                } else {
                    m.appendReplacement(result, url)
                }
            }
            m.appendTail(result)

            formatted = result.toString()

            // email addresses
            m = Patterns.EMAIL_ADDRESS.matcher(formatted)

            result = new StringBuffer()
            while (m.find()) {
                String address = m.group(0)

                m.appendReplacement(result, "<a href='mailto:${address}'>${address}</a>")
            }
            m.appendTail(result)

            formatted = result.toString()
        }

        return formatted
    }

    String parseAndSanitizeText(String input, Boolean unformatted, Boolean expandLinks) {
        def res = input

        if (unformatted) {
            res = formatText(res, expandLinks)

            def doc = parser.parse(res)
            res = renderer.render(doc)
        }

        res = cleanHtml(res)

        return res
    }

    String cleanHtml(String input) {
        return htmlCleaner.cleanHtml(input, 'spiral5')
    }
}