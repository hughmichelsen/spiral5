package com.ncmc4


import grails.gorm.transactions.Transactional
import groovy.sql.Sql
import groovy.transform.Synchronized

import java.time.Month
import java.time.YearMonth
import java.time.ZoneId
import java.time.ZonedDateTime

import static com.ncmc4.Rating.*

class RatingService {

    static def minNumRatings = 10                   // Minimum number of ratings required for rating icon
    static def ratingCalcDelay = 0                  // Minimum age in days before icon can show up
    static def minValidRatingCategories = 5           // Minimum number of ratings category to meet the minNumRatings for Icons to show up and to be considered for "Top Rated"
    static def ratingMinVal = 4.35 * 20             // Minimum average before icon is shown
    static def ratingMinValSilver = 4.55 * 20       // Minimum average before icon is shown with silver shine
    static def ratingMinValGold = 4.85 * 20         // Minimum average before icon is shown with golden shine
    static def maxRatingIconsShown = 5              // Max number How many rating icons shown on story or series


    def systemAdminService

    def findRatingByUser(Story story, SecUser user) {
        if(user)
            return Rating.findByStoryAndRatedBy(story, user)
        else
            return null
    }

    def findRatingByIP(Story story, String clientIP) {
        def ipMap = getIntForIP(clientIP)

        return Rating.findByStoryAndIpUpperAndIpLower(story, ipMap.upper, ipMap.lower)
    }

    def createRating(Story story, String clientIP, SecUser user)
    {
        def ipMap = getIntForIP(clientIP)

        Rating rating = new Rating(story: story, ipUpper: ipMap.upper, ipLower: ipMap.lower, ratedBy: user)

        return rating
    }

    @Transactional
    @Synchronized
    def updateRating(Story story, String clientIP, SecUser user, String ratingID, String value)
    {
        Rating rating = findRatingByUser(story, user)
        if(! rating)
            rating = findRatingByIP(story,clientIP)

        if(! rating)
            rating = createRating(story, clientIP, user)

        Integer intVal = scaleRating(value)
        if(intVal)
        {
            switch(ratingID)
            {
                case 'ratingIdea': rating.ratingIdea = intVal; break;
                case 'ratingWriting': rating.ratingWriting = intVal; break;
                case 'ratingHot': rating.ratingHot = intVal; break;
                case 'ratingMC': rating.ratingMC = intVal; break;
                case 'ratingCum': rating.ratingCum = intVal; break;
            }

            log.debug("Added rating: ${rating.toString()}")

            rating.save(flush: true, failOnError: true)
        }
    }

    Map getRatingMap(Story story, String clientIP, SecUser user)
    {
        Rating rating = findRatingByUser(story, user)
        if(! rating)
            rating = findRatingByIP(story,clientIP)

        if(! rating)
            return [ratingIdea: null, ratingWriting: null, ratingHot: null, ratingMC: null, ratingCum: null]
        else
            return [ratingIdea: unscaleRating(rating.ratingIdea),
                    ratingWriting: unscaleRating(rating.ratingWriting),
                    ratingHot: unscaleRating(rating.ratingHot),
                    ratingMC: unscaleRating(rating.ratingMC),
                    ratingCum: unscaleRating(rating.ratingCum)]
    }

    Map getRatingResults(Story story)
    {
        Map stats = getStatisticsForStoryMap(story)
        return [ratingIdea: stats?.ratingIdea?.avgAll,
                ratingWriting: stats?.ratingWriting?.avgAll,
                ratingHot: stats?.ratingHot?.avgAll,
                ratingMC: stats?.ratingMC?.avgAll,
                ratingCum: stats?.ratingCum?.avgAll]
    }

    Map getIntForIP(String ipStr)
    {
        InetAddress ip = InetAddress.getByName(ipStr)
        byte[] bytes = ip.getAddress()
        
        BigInteger bigInt = new BigInteger(1, bytes)

        return [upper: bigInt.divide(Long.MAX_VALUE as BigInteger).toLong(), lower: bigInt.mod(Long.MAX_VALUE).toLong()]
    }

    String getIPforInt(Long upper, Long lower)
    {
        BigInteger bigInt = new BigInteger(new byte[16])
        bigInt = bigInt.add(upper as BigInteger)
        bigInt = (BigInteger) bigInt.multiply(Long.MAX_VALUE)
        bigInt = bigInt.add(lower as BigInteger)

        InetAddress ip = InetAddress.getByAddress(bigInt.toByteArray())

        return ip.getHostAddress()
    }

    class StoryRatingStats {
        int ratVal
        String ratName

        int sumAll
        int sumAnon
        int sumNamed
        int numAll
        int numAnon
        int numNamed
        float avgAllVal
        float avgAnonVal
        float avgNamesVal
        int rankOverall
        int rankYear
        int rankMonth
        List<Integer> sortedAll = new ArrayList<Integer>()
        List<Integer> sortedAnon = new ArrayList<Integer>()
        List<Integer> sortedNamed = new ArrayList<Integer>()
        String avgAll
        String avgAnon
        String avgNamed
        String medianAll
        String medianAnon
        String medianNamed
        String varAll
        String varAnon
        String varNamed

        String effect = ""
        String toolTip = ""
    }

    StoryRatingStats getInsufficientRating() {
        def ratEntry = new StoryRatingStats()

        ratEntry.ratVal = -1
        ratEntry.ratName = "ratingInsufficient"
        ratEntry.toolTip = "Needs more readers' ratings!"

        return ratEntry
    }


    static Float average(int sum, int num)
    {
        return (float)sum / (float)num
    }

    static Float median(List<Integer> list)
    {
        int num = list.size()
        if(num % 2 == 0)
            return (list[(int)(num / 2) - 1] + list[(int)(num / 2)]) / 2
        else
            return list[(int)(num / 2)]
    }

    static Float variance(List<Integer> list, Float average)
    {
        float sumDelta = 0.0
        list.each { value ->
            sumDelta += Math.pow(average - value, 2)
        }
        return Math.sqrt(sumDelta / list.size())
    }

    Map<String, StoryRatingStats> getRatingStatisticsForStory(Story story)
    {
        checkRatingStatistics()

        HashMap<String, StoryRatingStats> ratingStats = new HashMap<String, StoryRatingStats>()

        def storyProxy = Story.load(story.id) // Notwendig weil es sonst bei aus ElasticSearch geladenen Story-Objekten zu einer Exception kommt
        def ratingList = Rating.findAllByStory(storyProxy)
        def ratingRanks = RatingStatistics.findAllByStory(storyProxy)

        ratingList.each{ rating ->
            Map ratingMap = rating.getRatingValueMap()

            ratingMap.each{ String ratName, Integer ratVal ->
                if(ratVal >= 0)
                {
                    def ratEntry = ratingStats.get(ratName)
                    if(! ratEntry){
                        ratEntry = new StoryRatingStats()
                        ratEntry.ratVal = ratVal
                        ratEntry.ratName = ratName
                        ratingStats.put(ratName,ratEntry)
                    }

                    ratEntry.sumAll += ratVal
                    ratEntry.numAll++
                    ratEntry.sortedAll.add(ratVal)
                    if(rating.ratedBy)
                    {
                        ratEntry.sumNamed += ratVal
                        ratEntry.numNamed ++
                        ratEntry.sortedNamed.add(ratVal)
                    }
                    else
                    {
                        ratEntry.sumAnon += ratVal
                        ratEntry.numAnon ++
                        ratEntry.sortedAnon.add(ratVal)
                    }

                    def ranks = ratingRanks.find { it.ratingType == ratName}
                    if(ranks) {
                        ratEntry.rankOverall = ranks.rankOverall
                        ratEntry.rankYear = ranks.rankYear
                        ratEntry.rankMonth = ranks.rankMonth
                    }
                }
            }
        }

        ratingStats.each { ratName, ratStats ->
            if(ratStats.numAll > 0) {
                ratStats.sortedAll.sort()
                ratStats.avgAllVal = average(ratStats.sumAll, ratStats.numAll)
                ratStats.avgAll = unscaleRating(ratStats.avgAllVal)
                ratStats.medianAll = unscaleRating(median(ratStats.sortedAll))
                ratStats.varAll = unscaleRating(variance(ratStats.sortedAll, ratStats.avgAllVal))

                if (ratStats.avgAllVal >= ratingMinValGold) {
                    ratStats.effect = "Gold"
                    ratStats.toolTip = "Outstandingly ${Rating.getRatingTooltipMap()[ratName]}"
                } else if (ratStats.avgAllVal >= ratingMinValSilver) {
                    ratStats.effect = "Silver"
                    ratStats.toolTip = "Exceptionally ${Rating.getRatingTooltipMap()[ratName]}"
                }
                else if (ratStats.avgAllVal >= ratingMinVal) {
                    ratStats.effect = "Normal"
                    ratStats.toolTip = "Very ${Rating.getRatingTooltipMap()[ratName]}"
                }
                else
                    ratStats.effect = ""
            }
            if(ratStats.numAnon > 0)
            {
                ratStats.sortedAnon.sort()
                ratStats.avgAnonVal = average(ratStats.sumAnon, ratStats.numAnon)
                ratStats.avgAnon = unscaleRating(ratStats.avgAnonVal)
                ratStats.medianAnon = unscaleRating(median(ratStats.sortedAnon))
                ratStats.varAnon = unscaleRating(variance(ratStats.sortedAnon, ratStats.avgAnonVal))
            }
            if(ratStats.numNamed > 0)
            {
                ratStats.sortedNamed.sort()
                ratStats.avgNamesVal = average(ratStats.sumNamed, ratStats.numNamed)
                ratStats.avgNamed = unscaleRating(ratStats.avgNamesVal)
                ratStats.medianNamed = unscaleRating(median(ratStats.sortedNamed))
                ratStats.varNamed = unscaleRating(variance(ratStats.sortedNamed, ratStats.avgNamesVal))
            }
        }

        return ratingStats
    }

    Map<String, Map> getStatisticsForStoryMap(Story story) {

        def ratingStats = getStatisticsForStory(story)

        HashMap<String, Map> results = new HashMap<String, Map>()
        ratingStats.each { ratName, ratStats ->
            results.put(ratName, ratStats.getProperties())
        }

        return results
    }

    def maxRatingStatisticsAge = 60 * 60 * 24 // Statistics View einmal pro Tag neu aktualisieren
    def lastRatingStatisticsMaterzialized = 0

    @Synchronized
    def checkRatingStatistics() {
        def curTime = System.currentTimeSeconds()

        if(! lastRatingStatisticsMaterzialized || lastRatingStatisticsMaterzialized + maxRatingStatisticsAge < System.currentTimeSeconds()) {
            rematerializeRatingStatisticsView()
            lastRatingStatisticsMaterzialized = curTime
        }
    }

    def dataSource

    @Synchronized
    def rematerializeRatingStatisticsView() {
        final Sql sql = new Sql(dataSource)

        sql.execute("refresh materialized view rating_statistics with data")
    }

    def maxRatingCacheAge = 60 * 60  // Cache Eine Stunde
    
    private class RatingCache {
        long cacheDate
        long storyID
        Map ratings
    } 
    
    Map<String, RatingCache> ratingCache = [:]

    @Synchronized
    Map<String, StoryRatingStats> getStatisticsForStory(Story story) {
        def cache = ratingCache.get(story.id)
        
        def curTime = System.currentTimeSeconds()
        
        if(! cache || cache.cacheDate + maxRatingCacheAge < System.currentTimeSeconds()) {
            cache = new RatingCache()
            cache.cacheDate = curTime
            cache.storyID = story.id

            cache.ratings = getRatingStatisticsForStory(story)
            
            ratingCache.put(story.id, cache)
        }

        return cache.ratings
    }


    // ----------------------------
    // Top Rated Stories mechanisms
    // -----------------------------

    // Look half a year back
    final monthDelayUntilNextPeriod = 6
    // Each period is for half a year
    final periodDurationMonth = 6


    // Store list in DB (should be true to be persistant over site restarts)
    final boolean makeListsPersistent = true

    // Name of ItemList in DB
    final def randomTopRangeListName = "randomTopSeries"


    Map<String,Map<String, List<Long>>> periodTopListByRatingMap = [:]
    Map<String, List<RatingStatistics>> periodTopListMap = [:]
    List<Long> randomTopSeriesItemList = []

    // Top Series List Basic Preparation
    Map prepareRandomTopStoriesData(ZonedDateTime now) {
        Map topStoriesDefinition

        boolean topListHasAllRatings = true
        // If true, the top list will always show at least one entry per rating.

        def startOfCalculation = ZonedDateTime.of(2019,1, 5, 0, 0, 0, 0, systemAdminService.getUserTimeZone().toZoneId())

        // Start date of new Top Stories algorithm: 06-Oct-2018 (1st Saturday in October 2018)
        if (now.isAfter(startOfCalculation)) {
            if (topListHasAllRatings) {
                topStoriesDefinition = calcTopPeriodSegmentData(now,
                        periodDurationMonth,
                        monthDelayUntilNextPeriod,
                        Calendar.SATURDAY,
                        1,
                        12,
                        2,
                        topListHasAllRatings)
            } else {
                topStoriesDefinition = calcTopPeriodSegmentData(now,
                        periodDurationMonth,
                        monthDelayUntilNextPeriod,
                        Calendar.SATURDAY,
                        3,
                        26,
                        1,
                        topListHasAllRatings)
            }
        }

        return topStoriesDefinition
    }

    static private def calcRandomTopListRangeId(YearMonth periodStart, YearMonth periodEnd, int numSeries) {
        periodStart.toString() + "#" + periodEnd.toString() + "#" + numSeries.toString()
    }

    @Synchronized
    private def calcRandomTopStoriesListByRating(YearMonth periodStart, YearMonth periodEnd, int numOfSeries) {
        def ratingTopRatedStories = [:]

        checkRatingStatistics()

        Random randomGen = new Random(periodStart.hashCode() + periodEnd.hashCode())

        Map<String, List> topListPerRating = [:]
        ratingList.each { rating ->
            ratingTopRatedStories[rating] = []

            def c = RatingStatistics.createCriteria()

            topListPerRating[rating] = c.list {
                eq("ratingType", rating)

                if(periodStart.year == periodEnd.year) {
                    between("publishMonth", periodStart.monthValue, periodEnd.monthValue)
                    eq("publishYear", periodStart.year)
                }
                else
                or {
                    and {
                        eq("publishYear", periodStart.year)
                        ge("publishMonth", periodStart.monthValue)
                    }
                    and {
                        eq("publishYear", periodEnd.year)
                        le("publishMonth", periodEnd.monthValue)
                    }
                    if(periodEnd.year - periodStart.year >= 2) {
                        between("publishYear", periodStart.year +1 , periodEnd.year -1)
                    }
                }

                order("average", "desc")
            }
        }

        Map<String,Integer> indexForRating = [:]
        def AllEntriesUsed = false
        def AllEntriesFull = false

        def index = 0

        while(! AllEntriesUsed && ! AllEntriesFull) {
            AllEntriesUsed = true

            ratingList.each { rating ->
                if (indexForRating[rating] < topListPerRating[rating].size()) {
                    AllEntriesUsed = false

                    def topListEntry = topListPerRating[rating][index]
                    def story = topListEntry.story
                    if (story.previouslyApproved && !story.deleted) {

                        // Check if the story the minimum number of valid rating categories
                        List storyRatingsList = getStatisticsForStoryMap(story).findResults { entry -> (entry.value.numAll >= minNumRatings) ? entry : null }
                        if (storyRatingsList.size() >= minValidRatingCategories) {
                            def seriesId = story.seriesId

                            def found = ratingTopRatedStories.find { ratingType ->
                                return ratingType.value.find { seriesIdForRating ->
                                    seriesIdForRating == seriesId
                                }
                            } != null

                            if (!found) {
                                if (ratingTopRatedStories[rating].size() < numOfSeries)
                                    ratingTopRatedStories[rating].add(story.seriesId)
                            }
                        }
                        else
                            log.debug("Story ${story.id} doesn't have enough valid ratings!")
                    }
                }

                if (!ratingTopRatedStories.find {
                    it.value.size() < numOfSeries
                }) {
                    AllEntriesFull = true
                }
            }

            index++
        }

        getRatingList().each { rating ->
            Collections.shuffle(ratingTopRatedStories[rating], randomGen)
        }

        return ratingTopRatedStories
    }

    @Synchronized
    def getRandomTopStoriesListByRating(YearMonth periodStart, YearMonth periodEnd, int numSeries) {
        def rangeId = calcRandomTopListRangeId(periodStart, periodEnd, numSeries)

        if(! periodTopListByRatingMap.containsKey(rangeId)) {
            def foundAllInDB = true
            periodTopListByRatingMap[rangeId] = [:]

            getRatingList().each { rating ->
                def instanceName = "${randomTopRangeListName}-${rangeId}-${rating}"

                ItemList itemList = ItemList.findByIdentifier(instanceName)
                if (itemList && itemList.itemList) {
                    periodTopListByRatingMap[rangeId][rating] = itemList.itemList
                }
                else
                    foundAllInDB = false
            }

            if (! foundAllInDB) {
                periodTopListByRatingMap[rangeId] = calcRandomTopStoriesListByRating(periodStart, periodEnd, numSeries)

                if (makeListsPersistent) {
                    getRatingList().each { rating ->
                        def instanceName = "${randomTopRangeListName}-${rangeId}-${rating}"

                        ItemList itemList = new ItemList(identifier: instanceName)
                        itemList.itemList = periodTopListByRatingMap[rangeId][rating]
                        itemList.save flush: true
                    }
                }
            }
        }

        return periodTopListByRatingMap[rangeId]
    }

    @Synchronized
    private List<RatingStatistics> calcRandomTopStoriesList(YearMonth periodStart, YearMonth periodEnd, int numOfSeries) {
        checkRatingStatistics()

        List<RatingStatistics> ratingTopRatedStories = []

        Random randomGen = new Random(periodStart.hashCode() + periodEnd.hashCode())

        def c = RatingStatistics.createCriteria()

        List<RatingStatistics> topList = (List<RatingStatistics>)c.list {
            if(periodStart.year == periodEnd.year) {
                between("publishMonth", periodStart.monthValue, periodEnd.monthValue)
                eq("publishYear", periodStart.year)
            }
            else
                or {
                    and {
                        eq("publishYear", periodStart.year)
                        ge("publishMonth", periodStart.monthValue)
                    }
                    and {
                        eq("publishYear", periodEnd.year)
                        le("publishMonth", periodEnd.monthValue)
                    }
                    if(periodEnd.year - periodStart.year >= 2) {
                        between("publishYear", periodStart.year +1 , periodEnd.year -1)
                    }
                }

            order("average", "desc")
        }

        def index = 0
        while(index < topList.size() && ratingTopRatedStories.size() < numOfSeries) {
            def topListEntry = topList[index]
            Story story = topListEntry.story
            if(story.previouslyApproved && ! story.deleted) {
                def seriesId = story.seriesId

                def found = ratingTopRatedStories.find { rating ->
                    rating.story.seriesId == seriesId
                } != null

                if(! found) {
                    ratingTopRatedStories.add(topListEntry)
                }
            }
            index++
        }

        Collections.shuffle(ratingTopRatedStories, randomGen)

        return ratingTopRatedStories
    }

    @Synchronized
    List<RatingStatistics> getRandomTopStoriesList(YearMonth periodStart, YearMonth periodEnd, int numSeries) {
        def rangeId = calcRandomTopListRangeId(periodStart, periodEnd, numSeries)

        if(! periodTopListMap.containsKey(rangeId)) {
                periodTopListMap[rangeId] = calcRandomTopStoriesList(periodStart, periodEnd, numSeries)
        }

        return periodTopListMap[rangeId]
    }

    static def getEnglishOrdinal(long number) {
        if(number % 100 < 10 || number % 100 >= 20) {
            switch(number % 10) {
                case 1: return "st"
                case 2: return "nd"
                case 3: return "rd"
                default: return "th"
            }
        }
        else return "th"
    }

    Map calcTopPeriodSegmentData(ZonedDateTime forDateTime,
                                 int periodDurationMonth = 6,                   // Length of one "best of" period in months
                                 int periodStartDelayMonth = 3,                 // Delay in Month before switching to the latest "best of" period
                                 int periodStartWeekDay = Calendar.SATURDAY,    // Day on witch switches to another period may occur
                                 def topStoriesPerRatingAndSegment = 1,         // Number of stories shown for each rating per segment
                                 def segmentsPerPeriod = 26,                    // SegmentsPerPeriod -> How many segments are there
                                 def weeksPerSegment = 1,                    // SegmentsPerPeriod -> How many segments are there
                                 boolean topListHasAllRatings = true)           // Every Segment has all ratings filled up
    {
        // For Debugging

        YearMonth curMonth = YearMonth.from(forDateTime)
        Calendar cal = new GregorianCalendar(systemAdminService.getUserTimeZone())
        cal.setTime(Date.from(forDateTime.toInstant()))

        cal.setFirstDayOfWeek(periodStartWeekDay)    // We want to show a new segment each saturday!
        cal.setMinimalDaysInFirstWeek(7)            // We need full weeks always

        int calWeek = cal.get(Calendar.WEEK_OF_YEAR)
        int calWeekOfMonth = cal.get(Calendar.WEEK_OF_MONTH)

        // Match the current month to the calendar week to avoid an early switch to the next period (switches may only occur on first day of the week)
        YearMonth baseMonth = curMonth
        if(calWeekOfMonth == 0) {
            baseMonth = curMonth.minusMonths(1)
        }

        // + 11 because monthValue is 1 based and we want to have a positive modulo, so we have to add a year (12-1)
        int monthOffs = ((baseMonth.monthValue + 11 - periodStartDelayMonth) % periodDurationMonth) + periodDurationMonth + periodStartDelayMonth
        YearMonth periodStart = baseMonth.minusMonths(monthOffs)
        YearMonth periodEnd = periodStart.plusMonths(periodDurationMonth-1)

        // For the calculation of the current segment number, we need the number of weeks since the start of the current period
        int monthOffsCur = ((baseMonth.monthValue + 11 - periodStartDelayMonth) % periodDurationMonth)
        YearMonth periodStartCur = baseMonth.minusMonths(monthOffsCur)

        // Period Start
        cal.set(periodStartCur.year,periodStartCur.monthValue - 1,1)

        // Set to first period week start day
        cal.add(Calendar.DATE, periodStartWeekDay - cal.get(Calendar.DAY_OF_WEEK))

        def startWeek = cal.get(Calendar.WEEK_OF_YEAR)

        def segment = (int)((calWeek - startWeek) / weeksPerSegment)

        if(segment < 0)
            segment += 52
        if(segment >= segmentsPerPeriod)
            segment = segmentsPerPeriod - 1

        int numTopSeries = topStoriesPerRatingAndSegment * segmentsPerPeriod

        def segmentId = periodStart.toString() + "#" + periodEnd.toString() + "#" + segment.toString()

        def topPeriodName = "${periodStart.month < Month.JUNE ? '1<sup>st</sup>' : '2<sup>nd</sup>'} half of ${periodStart.year}"

        return [topStoriesPeriodStart: periodStart, topStoriesPeriodEnd: periodEnd, topStoriesNumSeries: numTopSeries, topStoriesSegment: segment,
                topStoriesPerRatingAndSegment: topStoriesPerRatingAndSegment, topListHasAllRatings: topListHasAllRatings,
                topStoriesSegmentOrd: getEnglishOrdinal(segment+1), topStoriesSegmentId: segmentId, topPeriodName: topPeriodName]
    }

    def fetchTopSeriesMap(Map topStoriesDefinition, boolean all = false) {

        if(topStoriesDefinition){
            if(topStoriesDefinition.topListHasAllRatings)
                fetchTopStoriesWithFullRatings(topStoriesDefinition, all)
            else
                fetchTopSeriesMapSimple(topStoriesDefinition,all)
        }
    }

    def fetchTopStoriesWithFullRatings(Map topStoriesDefinition, boolean all) {
        def randomTopStoriesList = getRandomTopStoriesListByRating(
                topStoriesDefinition.topStoriesPeriodStart, topStoriesDefinition.topStoriesPeriodEnd, topStoriesDefinition.topStoriesNumSeries)

        def topSeriesMap = [:]
        randomTopStoriesList.each { ratingType ->
            for(int i = (all ? 0 : topStoriesDefinition.topStoriesSegment * topStoriesDefinition.topStoriesPerRatingAndSegment);
                i < (topStoriesDefinition.topStoriesSegment+1) * topStoriesDefinition.topStoriesPerRatingAndSegment;
                i++) {
                if(! topSeriesMap[ratingType.key])
                    topSeriesMap[ratingType.key] = []
                if(ratingType.value[i])
                {
                    try {
                        def series = Series.get(ratingType.value[i])
                        if(series && series.storyCount > 0)
                            topSeriesMap[ratingType.key].add(0,series)
                    }
                    catch(e) {
                        // Ignore
                    }
                }
            }
        }
        topStoriesDefinition.topSeriesMap = topSeriesMap
    }

    List<Long> getRandomTopSeriesItemList(Map topStoriesDefinition) {
        if (randomTopSeriesItemList)
            return randomTopSeriesItemList

        def rangeId = calcRandomTopListRangeId(topStoriesDefinition.topStoriesPeriodStart, topStoriesDefinition.topStoriesPeriodEnd, topStoriesDefinition.topStoriesNumSeries)

        def instanceName = "${randomTopRangeListName}-${rangeId}"

        ItemList itemList = ItemList.findByIdentifier(instanceName)
        if (itemList && itemList.itemList) {
            randomTopSeriesItemList = itemList.itemList
        }
        else {
            randomTopSeriesItemList = []
            def randomTopStoriesList = getRandomTopStoriesList(
                    topStoriesDefinition.topStoriesPeriodStart, topStoriesDefinition.topStoriesPeriodEnd, topStoriesDefinition.topStoriesNumSeries)

            randomTopStoriesList.each { RatingStatistics statistics ->
                randomTopSeriesItemList += statistics.story.seriesId
            }

            if (makeListsPersistent) {
                itemList = new ItemList(identifier: instanceName)
                itemList.itemList = randomTopSeriesItemList
                itemList.save flush: true
            }
        }

        return randomTopSeriesItemList
    }

    def fetchTopSeriesMapSimple(Map topStoriesDefinition, boolean all = false) {

        if(topStoriesDefinition){
            if(topStoriesDefinition.topListHasAllRatings)
                fetchTopStoriesWithFullRatings(topStoriesDefinition)
            else
                fetchTopSeriesMap(topStoriesDefinition,false)
        }

        List<Long> topItemList = getRandomTopSeriesItemList(topStoriesDefinition)

        Map<String,List<Series>> topSeriesMap = [:]

        for(int i = (all ? 0 : topStoriesDefinition.topStoriesSegment * topStoriesDefinition.topStoriesPerRatingAndSegment);
            i < (topStoriesDefinition.topStoriesSegment+1) * topStoriesDefinition.topStoriesPerRatingAndSegment;
            i++) {
            if (topItemList[i]) {
                if (!topSeriesMap[topItemList[i].attribute])
                    topSeriesMap[topItemList[i].attribute] = []

                topSeriesMap[topItemList[i].attribute].add(0,Series.get(topItemList[i].itemId))
            }
        }

        topStoriesDefinition.topSeriesMap = topSeriesMap
    }

}
