package com.ncmc4

import com.google.common.cache.*
import org.hibernate.FlushMode

import java.util.concurrent.TimeUnit

class KeyValueCacheService {
    static transactional = false
    static scope = "singleton"

    private LoadingCache<String, String> valueCache = CacheBuilder.newBuilder()
            .maximumSize(10)
            .build(new CacheLoader<String, String>() {
                public String load(String key) {
                    String ret = null

                    SystemSettings.withNewSession { dbSession ->
                        dbSession.flushMode = FlushMode.MANUAL

                        SystemSettings setting = SystemSettings.findByName(key)
                        ret = setting?.value
                    }

                    return ret
                }
            });

    String getValue(String key, defaultValue) {
        return valueCache.get(key) ?: defaultValue.toString()
    }

    void setValue(String key, value) {
        value = value.toString()

        // set it in the cache first
        valueCache.put(key, value)

        // reflect this in the database too
        SystemSettings setting = SystemSettings.findByName(key)
        if (setting == null)
            setting = new SystemSettings(name:key, value:value)
        else
            setting.value = value

        setting.save flush:true, failOnError:true
    }
}