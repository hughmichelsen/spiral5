package com.ncmc4

import grails.gorm.transactions.Transactional
import com.google.common.cache.*
import org.grails.web.util.WebUtils

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpSession

class TagService {

    static transactional = false
    static scope = "singleton"

    def springSecurityService

    def storyService
    def systemAdminService

    final String cacheSeparator = "_,_"

    private LoadingCache<String, String[]> tagCache = CacheBuilder.newBuilder()
            .maximumSize(5)
            .build(
            new CacheLoader<String, String[]>() {
                public String[] load(String key) {
                    Collection<String> ret = null

                    Tag.withNewSession {
                        def tags = Tag.list(readOnly: true)
                        ret = (tags*.tag)
                    }

                    return (String[]) ret.toArray()
                }
            });

    public void resetCache() {
        tagCache.invalidateAll()
    }

    public String[] getTagListCached(String etag) {
        def tags = Tag.list(readOnly: true)
        return (tags*.tag)
    }

    public String[] getTagList() {
        def tags = Tag.list(readOnly: true)
        return (tags*.tag)
    }

    def getAllTags(Integer excludeStory)
    {
        try
        {
            def result
            if(excludeStory)
            {
                Story.withNewSession {
                    result = Story.executeQuery("select distinct tags from Story s where s.id <> :story)",[story: excludeStory])
                }
                if(result.first() == null)
                    result = Tag.list(readOnly: true)
            }
            else
                result = Tag.list(readOnly: true)

            return result
        }
        catch(Exception e)
        {
            return Tag.list(readOnly: true)
        }
    }

    def getSpecTagsJSon(Boolean promotedFlag, String start, String end)
    {
        def tagList
        def result = []

        try
        {
            def query = Tag.where{}
            if(promotedFlag)
                query = query.where{promoted == true}
            if(start != '')
                query = query.where{tag >= start}
            if(end != '')
                query = query.where{tag <= end}

            tagList = query.list(sort:"tag", order:"asc", readOnly: true)

            result = tagList.collect { tag ->
                [id: tag.id, tag: tag.tag, promoted: tag.promoted, isCategory: tag.isCategory, aliases: tag.getAliasList()]
            }
        }
        catch(Exception e)
        {
            log.error "${systemAdminService?.logPrefix}Exception in getSpecTagsJSon!", e
        }

        return result;
    }

    List<Long> getTagIds(Collection<Tag> searchTags, Boolean resolveAlias){
        List<Long> tagIDSet = []
        searchTags.each { tag->
            if(resolveAlias && tag.aliasGroup)
            {
                tag.aliasGroup.aliases.each { alias ->
                    tagIDSet.add(alias.id)
                }
            }
            else
                tagIDSet.add(tag.id)
        }

        return tagIDSet
    }

    public def parseTags(String newTags)
    {
        if (!newTags)
            return []

        return newTags.split(",").collect({it.trim()})
    }

    public class EditTagStatus {
        Set<Tag> tagsAdded = []
        Set<Tag> tagsDeleted = []
    }

    @Transactional
    public EditTagStatus editTags(Collection submittedTags, Story storyInstance)
    {
        EditTagStatus result = new EditTagStatus()

        def prevTags = storyInstance.tags ?: []

        def newTagList = []
        submittedTags.each { it ->
            def tag = Tag.findByTag(it)
            if(tag == null) {
                tag = new Tag(tag: it)
                tag.save flush: true
                logActivity(tag, "Created by user")
            }

            newTagList << tag
        }

        if(prevTags != newTagList) {
            result.tagsAdded = (newTagList - prevTags)
            result.tagsDeleted = (prevTags - newTagList)

            storyInstance.tags = newTagList

            storyInstance.lastTagDate = new Date()
        }

        return result
    }

    @Transactional
    public def logActivity(Tag tag, String action)
    {
        def user = springSecurityService.isLoggedIn() ? springSecurityService.loadCurrentUser() : null

        def activity = new TagActivity(user: user, activityTagId: tag.id, activityTagName: tag.tag, action: action);
        activity.save flush:true, failOnError: true
    }

    @Transactional
    public String doDeleteTag(tag) {
        try
        {
            if (tag != null) {
                def series = Series.withCriteria {
                    tags {
                        eq('tag', tag.tag)
                    }
                }

                series.each { serial ->
                    serial.tags.remove(tag)
                    serial.save(flush: true)
                }

                def stories = Story.withCriteria {
                    tags {
                        eq('tag', tag.tag)
                    }
                }

                stories.each { story ->
                    story.tags.remove(tag)
                    story.save(flush: true)

                    // storySearchService.updateRecord(story)
                }

                tag.delete(flush: true)

                logActivity(tag, "deleted")
            }
        }
        catch(Exception ex)
        {
            log.error "${systemAdminService?.logPrefix}Exception in doDeleteTag!", ex
            return ex.message
        }

        return ""
    }

    private void doMergeTag(Tag tag, Tag mergeTag)
    {
        if (tag != null && mergeTag != null) {
            def series = Series.withCriteria {
                tags {
                    eq('tag', mergeTag.tag)
                }
            }

            series.each { serial ->
                if(! serial.tags.contains(tag))
                {
                    serial.tags.add(tag)
                    serial.save(flush: true)
                }
            }

            def stories = Story.withCriteria {
                tags {
                    eq('tag', mergeTag.tag)
                }
            }

            stories.each { story ->
                if(! story.tags.contains(tag)) {
                    story.tags.add(tag)
                    story.save(flush: true)
                }
            }

            logActivity(tag, "merged ${mergeTag.tag}")
        }
    }

    @Transactional
    public String doMergeTags(Tag tag, String mergeTags)
    {
        try
        {
            def tags = mergeTags.split(",")
            tags.each { elem ->
                elem = elem.trim()
                if(elem != tag.tag)
                {
                    def mergeTag = Tag.findByTag(elem)
                    if(mergeTag != null) {
                        doMergeTag(tag, mergeTag);
                        doDeleteTag(mergeTag);
                    }
                    else
                    {
                        log.error "${systemAdminService?.logPrefix}mergeAjax: Tag ${elem} not found!"
                        return "mergeAjax: Tag ${elem} not found!"
                    }
                }
            }
        }
        catch(Exception ex)
        {
            log.error "${systemAdminService?.logPrefix}Exception in doMergaTags", ex
            return ex.message
        }

        return ""
    }


    @Transactional
    def doUpdateName(Tag tag, String newName)
    {
        try
        {
            tag.tag = newName
            tag.save(flush:true)
        }
        catch(Exception ex)
        {
            log.error "${systemAdminService?.logPrefix}Exception in doUpdateName", ex
            return ex.message
        }

        return ""
    }

    @Transactional
    def doCreateAlias(Tag tag, String aliasName)
    {
        try
        {
            TagAliasGroup aliasGroup = tag.aliasGroup
            if(! aliasGroup)
            {
                aliasGroup = new TagAliasGroup()
                tag.aliasGroup = aliasGroup
                aliasGroup.save(flush: true)
                tag.save(flush:true)
            }
            else
                aliasGroup = tag.aliasGroup

            Tag aliasTag = new Tag(tag: aliasName)

            aliasTag.aliasGroup = aliasGroup
            aliasTag.save(flush: true)
        }
        catch(Exception ex)
        {
            log.error "${systemAdminService?.logPrefix}Exceptin in doCreateAlias!", ex
            return ex.message
        }

        return ""
    }

    @Transactional
    def doAddAlias(Tag tag, Tag aliasTag) {
        try {
            if(tag.aliasGroup && aliasTag.aliasGroup == tag.aliasGroup)
            {
                // Bereits die selbe Alias-Gruppe: Nichts zu tun
                return "Already aliases!"
            }
            if(aliasTag.aliasGroup && tag.aliasGroup && tag.aliasGroup != aliasTag.aliasGroup)
            {
                // Verschiedene Allias-Groups, zusammenfassen
                TagAliasGroup targetAliasGroup = tag.aliasGroup
                TagAliasGroup sourceAliasGroup = aliasTag.aliasGroup

                sourceAliasGroup.aliases.each { combTag ->
                    combTag.aliasGroup = targetAliasGroup
                    combTag.save(flush:true)
                }

                // Alte TagGrupper löschen
                sourceAliasGroup.delete()
            }
            else
            {
                TagAliasGroup aliasGroup = tag.aliasGroup
                if(! aliasGroup)
                    aliasGroup = aliasTag.aliasGroup

                if(! aliasGroup)
                    aliasGroup = new TagAliasGroup()

                tag.aliasGroup = aliasGroup
                tag.save(flush: true)
                aliasTag.aliasGroup = aliasGroup
                aliasTag.save(flush: true)
            }
        }
        catch(Exception ex)
        {
            log.error "${systemAdminService?.logPrefix}Exception in doAddAlias!", ex
            return ex.message
        }

        return ""
    }


    @Transactional
    def doAddAliases(Tag tag, String aliasNames)
    {
        try
        {
            def tags = aliasNames.split(",")
            tags.each { elem ->
                elem = elem.trim()
                if(elem != tag.tag)
                {
                    def aliasTag = Tag.findByTag(elem)
                    if(aliasTag != null) {
                        doAddAlias(tag, aliasTag)
                    }
                    else
                    {
                        log.error "${systemAdminService?.logPrefix}addAliasAjax: Tag ${elem} not found!"
                        return "addAliasAjax: Tag ${elem} not found!"
                    }
                }
            }
        }
        catch(Exception ex)
        {
            log.error "${systemAdminService?.logPrefix}Exception in doAddAliases!", ex
            return ex.message
        }

        return ""
    }

    @Transactional
    def doCreateDuplicate(Tag tag, String duplicateName)
    {
        try
        {
            Tag duplicateTag = new Tag(tag: duplicateName)
            duplicateTag.save(flush: true)

            def series = Series.withCriteria {
                tags {
                    eq('tag', tag.tag)
                }
            }

            series.each { serial ->
                serial.tags << duplicateTag
                serial.save(flush: true)
            }

            def stories = Story.withCriteria {
                tags {
                    eq('tag', tag.tag)
                }
            }

            stories.each { story ->
                story.tags << duplicateTag
                story.save(flush: true)
            }
        }
        catch(Exception ex)
        {
            log.error "${systemAdminService?.logPrefix}Exception in doCreateDuplicate!", ex
            return ex.message
        }

        return ""
    }

    static class SelectCategory{
        String tagId
        String text
    }
    def getCategorySelection(String emptyText = "Select category for this story"){
        def catList = Tag.findAllByIsCategory(true)

        List<SelectCategory> result = []
        catList.each{ cat ->
            def selCat = new SelectCategory(tagId: cat.id)
            selCat.text = cat.tag.capitalize()
            if(cat.getAliasList() != "")
                selCat.text += " (${cat.getAliasList()})"
            result << selCat
        }

        result.sort{ SelectCategory a, SelectCategory b -> a.text < b.text ? -1 : 1}

        result.add(0, new SelectCategory(tagId:0, text:emptyText))

        return result
    }
}
