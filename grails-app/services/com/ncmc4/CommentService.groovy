package com.ncmc4

import android.util.Patterns

import java.util.concurrent.TimeUnit

import com.google.common.cache.*

class CommentService {
    static scope = "singleton"
    static transactional = false

    KeyValueCacheService keyValueCacheService

    static final String SettingCacheKey = "com.spiral.commentCacheDate"

    def markdownParserService

    private LoadingCache<Long, String> formattedCommentCache = CacheBuilder.newBuilder()
            .maximumSize(200)
            .build(
            new CacheLoader<Long, String>() {
                public String load(Long key) {
                    Comment.withNewSession {
                        Comment comment = Comment.read(key)

                        String commentText = markdownParserService.parseAndSanitizeText(comment.comment,true,false)

                        comment.discard()

                        return commentText
                    }

                }
            });

    def updateCache() {
        keyValueCacheService.setValue(SettingCacheKey, new Date().time)
    }

    def resetCache() {
        updateCache()

        formattedCommentCache.invalidateAll()
    }

    String getCacheLastUpdate() {
        return keyValueCacheService.getValue(SettingCacheKey, 0)
    }

    public def formatComment(Comment comment, boolean isAdmin = false, boolean isOwnComment = false)
    {
        String commentText = ""

        // use the cache if we can; if we can't, it's because we're creating the comment
        // and we still want this to work, so that Spam responses look the same as non-spam responses
        if (comment.id != null)
            commentText = formattedCommentCache.get(comment.id)
        else
            commentText = markdownParserService.parseAndSanitizeText(comment.comment,true,false)

        return [title: comment.name, creationDate: comment.creationDate, text: commentText, story: comment.story, id: comment.id, ipAddress: isAdmin ? comment.ipAddress : "0.0.0.0", poster: comment.poster, isOwnComment: isOwnComment, isAdmin: isAdmin]
    }

    public def list(params, authorID = -1) {
        def criteria = Comment.createCriteria()
        def comments
        if(authorID >= 0)
        {
            comments = criteria.list(params) {
                story {
                    eq('approved', true)
                    eq('deleted', false)
                    owners {
                        eq('id', authorID)
                    }
                }
            }
        }
        else
        {
            comments = criteria.list(params) {
                story {
                    eq('approved', true)
                    eq('deleted', false)
                }
            }
        }

        return [list:comments, count: comments.totalCount]
    }
}