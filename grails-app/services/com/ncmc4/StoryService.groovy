package com.ncmc4

import grails.gorm.DetachedCriteria
import grails.plugins.elasticsearch.ElasticSearchResult
import groovy.sql.Sql
import org.grails.web.util.WebUtils

import javax.servlet.http.Cookie

import static grails.plugin.springsecurity.SpringSecurityUtils.*
import org.hibernate.FlushMode

class StoryService
{
    static scope = "prototype"
    static transactional = false

    static final String SettingCacheKey = "com.spiral.storyCacheDate"

    def systemAdminService
    def groovyPageRenderer

    KeyValueCacheService keyValueCacheService

    def updateStoryCache() {
        keyValueCacheService.setValue(SettingCacheKey, new Date().time)
    }

    String getStoryCacheLastUpdate() {
        return keyValueCacheService.getValue(SettingCacheKey, 0)
    }

    def list(params)
    {
        //return [list: Story.findAllByApprovedAndDeleted(true, false, params), count: Story.count()]
        def isAdmin = ifAnyGranted(SecRole.ROLE_ADMIN)
        def isSuperAdmin = ifAnyGranted(SecRole.ROLE_SUPER_ADMIN)

        def catFilter = getCategoryFilter()

        def criteria = Story.createCriteria()
        def stories = criteria.list(params) {
            if(! isAdmin)
                or {
                    eq("approved", true)
                    eq("previouslyApproved", true)
                }

            if(! isSuperAdmin)
                eq("deleted", false)

            if(catFilter)
                category {
                    'in'("id", catFilter)
                }
        }

        return [list: stories, count: stories.totalCount]
    }

    def findAllByOwner(SecUser author, params = [:])
    {
        def isSuperAdmin = ifAnyGranted(SecRole.ROLE_SUPER_ADMIN)

        def criteria = Story.createCriteria()
        def stories = criteria.list(params) {
            if (!isSuperAdmin)
                eq("deleted", false)

            owners {
                eq("id", author.id)
            }
        }

        return [list: stories, count: stories.totalCount]
    }

    def findAllBySeries(Series seriesInstance, params = [:])
    {
        def isAdmin = ifAnyGranted(SecRole.ROLE_ADMIN)
        def isSuperAdmin = ifAnyGranted(SecRole.ROLE_SUPER_ADMIN)

        def criteria = Story.createCriteria()
        def stories = criteria.list(params) {
            if (!isAdmin)
                or {
                    eq("approved", true)
                    eq("previouslyApproved", true)
                }
            if(!isSuperAdmin)
                eq("deleted", false)
            eq("series", seriesInstance)
        }

        return [list: stories, count: stories.totalCount]
    }

    def findAllApprovedBySeries(Series seriesInstance, params = [:])
    {
        params["flushMode"] = FlushMode.ALWAYS

        def criteria = Story.createCriteria()
        def stories = criteria.list(params) {
            or {
                eq("approved", true)
                eq("previouslyApproved", true)
            }
            eq("deleted", false)
            eq("series", seriesInstance)
        }

        return [list: stories, count: stories.totalCount]
    }

    def findAllUnapproved(params)
    {
        def criteria = Story.createCriteria()
        def stories = criteria.list(params) {
            eq("approved", false)
            eq("deleted", false)
        }

        return [list: stories, count: stories.totalCount]
    }

    def findAllDeleted(params)
    {
        def criteria = Story.createCriteria()
        def stories = criteria.list(params) {
            eq("deleted", true)
        }

        return [list: stories, count: stories.totalCount]
    }

    def listAllDefaulted(params)
    {
        def criteria = Story.createCriteria()
        def stories = criteria.list(params) {
            and {
                or {
                    eq("approved", true)
                    eq("previouslyApproved", true)
                }
                eq("deleted", false)

                or {
                    isNull("category")
                    lt("tagCount", 3)
                }
            }
        }

        return [list: stories, count: stories.totalCount]
    }

    def findAllByLetter(letter, params)
    {
        //def authorList = Author.findAllByNameIlike("$letter%")
        def isAdmin = ifAnyGranted(SecRole.ROLE_ADMIN)
        def isSuperAdmin = ifAnyGranted(SecRole.ROLE_SUPER_ADMIN)

        if (!params.sort)
            params.sort = "nameSlug"
        if (!params.order)
            params.order = "asc"

        def criteria = Story.createCriteria()
        def stories = criteria.list(params) {
            if(! isSuperAdmin)
                eq("deleted", false)

            if(! isAdmin)
                or {
                    eq("approved", true)
                    eq("previouslyApproved", true)
                }

            ilike("name", "${letter}%")
        }

        return [list: stories, count: stories.totalCount]
    }

    def findAllByAuthorSlug(authorSlug, params)
    {
        def isAdmin = ifAnyGranted(SecRole.ROLE_ADMIN)
        def isSuperAdmin = ifAnyGranted(SecRole.ROLE_SUPER_ADMIN)

        def catFilter = getCategoryFilter()

        def criteria = Story.createCriteria()
        def stories = criteria.list(params) {
            if(! isSuperAdmin)
                eq("deleted", false)

            if(! isAdmin)
                or {
                    eq("approved", true)
                    eq("previouslyApproved", true)
                }

            if(catFilter)
                category {
                    'in'("id", catFilter)
                }

            eq("authorSlug", authorSlug)
        }

        return [list: stories, count: stories.totalCount]
    }

    def findAllByTags(params, Collection<Tag> searchTags, boolean findDeleted = false, boolean resolveAlias = false, findUnApproved = false)
    {
        def criteria = getCriteriaAllByTags(searchTags,findDeleted,resolveAlias,findUnApproved)
        def stories = criteria.list(params)

        return [list: stories, count: stories.totalCount]
    }

    DetachedCriteria<Story> getCriteriaAllByTags(Collection<Tag> searchTags, boolean findDeleted = false, boolean resolveAlias = false, findUnApproved = false)
    {
        if(! searchTags || searchTags.size() == 0)
            return Story.where { 0==1 }

        // Resolve Alias
        Set tagIDSet = []
        searchTags.each { tag->
            if(resolveAlias && tag.aliasGroup)
            {
                tag.aliasGroup.aliases.each { alias ->
                    tagIDSet << alias.id
                }
            }
            else
                tagIDSet << tag.id
        }

        return Story.where {
            tags {
                'in'("id", tagIDSet)
            }
            if(! findUnApproved)
                or {
                    eq("approved", true)
                    eq("previouslyApproved", true)
                }

            eq('deleted', findDeleted)
        }
    }

    def advancedStorySearch(Set<Tag> srchTags, String searchTerm, Tag srchCategory, Boolean andSearchMode, Boolean useAlias, Map params) {
        def result = [totalCount: 0, list: []]

        searchTerm = searchTerm ?: ""
        searchTerm = searchTerm?.trim()
        srchTags = srchTags ?: []

        if(srchTags?.size() == 0 && searchTerm?.length() == 0 && srchCategory == null)
            return result

        def isAdmin = ifAnyGranted('ROLE_ADMIN,ROLE_SUPER_ADMIN,ROLE_ADMIN,ROLE_ULTRA_ADMIN')
        def isSuperAdmin = ifAnyGranted('ROLE_SUPER_ADMIN,ROLE_ADMIN,ROLE_ULTRA_ADMIN')

        def fishyCharFound = params.any { it ->
            def matches = (it.value.toString() =~ /[\'\"]/).count
            // true when odd number of quotation marks
            (matches % 2) == 1
        }

        def logParamStr = "Term: <${searchTerm}> - Cat: <${srchCategory}> - Tags: <${srchTags}> (${andSearchMode ? 'and' : 'or'}-${useAlias ? 'alias' : 'exact'}) - Offs: ${params.offset} - Max: ${params.max} - Sort: ${params.sort} - Order: ${params.order}"

        if(fishyCharFound){
            log.warn "${systemAdminService?.logPrefix}Fishy character in search request! ${logParamStr}"
            // systemAdminService.sendAdminMail("Fishy search", groovyPageRenderer.render(template: "/email/fishySearch", model: [searchParams: logParamStr, source: systemAdminService.getLogPrefix()]).toString())
        }
        else
            log.debug "Search request: ${logParamStr}"

        if(params?.offset == null)
            params.offset = 0
        if(params?.max == null)
            params.max = 30
        if(params?.sort == null)
            params.sort = 'publish_date'
        if(params?.order == null)
            params.order = 'desc'

        List addWhereClauses = []

        if(srchCategory) {
            addWhereClauses << "s.category_id = ${srchCategory.id}"
        }

        def sqlWhere = ""

        if(srchTags != null && srchTags.size() > 0)
        {
            if (!andSearchMode) {
                List paramTagIDSet = []
                srchTags.eachWithIndex { tag, idx ->
                    if (useAlias && tag.aliasGroup) {
                        tag.aliasGroup.aliases.each { alias ->
                            paramTagIDSet << alias.id
                        }
                    } else
                        paramTagIDSet << "${tag.id}"
                }

                addWhereClauses << 'st.tag_id in (' + paramTagIDSet.join(',') + ')'
                sqlWhere = 'join grails_story_grails_tag st on s.id = st.story_tags_id where ' + addWhereClauses.join(' and ')
            } else {
                List joins = []
                List wheres = []
                srchTags.eachWithIndex { tag, idx ->

                    joins.add("join grails_story_grails_tag st${idx} on s.id = st${idx}.story_tags_id")

                    List paramTagIDSet = []
                    if (useAlias && tag.aliasGroup) {
                        tag.aliasGroup.aliases.each { alias ->
                            paramTagIDSet << alias.id
                        }
                    } else
                        paramTagIDSet << "${tag.id}"

                    wheres.add("st${idx}.tag_id in (${paramTagIDSet.join(',')})")
                }

                wheres.addAll(addWhereClauses)
                sqlWhere = joins.join(" ") + ' where ' + wheres.join(" and ")
            }
        }
        else if(addWhereClauses.size() > 0) {
            sqlWhere = "where " + addWhereClauses.join(" and ")
        }

        List checkids = null

        if(sqlWhere != "") {
            def sqlStr = ""
            try
            {
                if(! isAdmin)
                {
                    sqlWhere += " and (approved or previously_approved)"
                }

                if(! isSuperAdmin)
                {
                    sqlWhere += " and not deleted"
                }

                Story.withSession { session ->
                    def sql = new Sql(session.connection())
                    sqlStr = "select s.id from grails_story s " + sqlWhere
                    checkids = sql.rows(sqlStr).collect { row -> row.id }

                    if(checkids.size() == 0)
                        return result
                }
            }
            catch(Exception ex) {
                log.error "${systemAdminService?.logPrefix}SQL Exception. sqlStr: '${sqlWhere}' - ${logParamStr}", ex
                systemAdminService.sendAdminMail("Fishy search", groovyPageRenderer.render(template: "/email/fishySearch", model: [searchParams: logParamStr, exception: ex, sql: sqlStr, source: systemAdminService.getLogPrefix()]).toString())
                return result
            }
        }

        def highlighter = {
            field 'text'
            field 'summary'
            field 'author.name'
            preTags '<mark>'
            postTags '</mark>'
        }

        try {
            ElasticSearchResult txtSrchResult = Story.search(searchType: '', highlight: highlighter, from: params.offset, size: params.max, sort: params.sort, order: params.order) {
                bool {
                    if(searchTerm?.length() > 0) {
                        must {
                            query_string(query: searchTerm)
                        }
                    }
                    if(! isSuperAdmin) {
                        must {
                            term(deleted: false)
                        }
                    }
                    if(! isAdmin) {
                        must {
                            term(previouslyApproved: true)
                        }
                    }
                    if(checkids != null) {
                        must {
                            ids(values: checkids)
                        }
                    }
                }
            }

            if(txtSrchResult?.total == 0)
                return result

            result.totalCount = txtSrchResult.total
            result.list = txtSrchResult.searchResults
            result.highlight = txtSrchResult.highlight
            return result
        }
        catch(Exception e) {
            log.error "${systemAdminService?.logPrefix}Elastic Search Exception: ${e.toString()}! ${logParamStr}"
            systemAdminService.sendAdminMail("Fishy search", groovyPageRenderer.render(template: "/email/fishySearch", model: [searchParams: logParamStr, exception: e, source: systemAdminService.getLogPrefix()]).toString())
            return result
        }
    }


    String makeAuthorSlug(String authorName)
    {
        return authorName.toLowerCase()
    }

    String makeStoryTitleSlug(String title)
    {
        return title.toLowerCase().replaceAll("\\W", "")
    }

    static class FilterCategory{
        String tagId
        String name
        String aliases
        Boolean selected
    }
    final String selCatAttrName = "selectedCategories"

    def getCategoryFilterList(){
        def catList = Tag.findAllByIsCategory(true)

        def request = WebUtils.retrieveGrailsWebRequest().getCurrentRequest()
        def response = WebUtils.retrieveGrailsWebRequest().getCurrentResponse()

        def updateCookie = false
        def selCatsStr = request.getParameter("catFilter")
        def selCats = []
        if(selCatsStr != null) {
            updateCookie = true
            selCats = (selCatsStr == "" ? [] : selCatsStr.split(","))
        }
        else {
            selCatsStr = request.getCookies().find{ it.name==selCatAttrName }?.value ?: ""
            selCats = (selCatsStr == "" ? [] : selCatsStr.split("#"))
        }


        def foundCats = []
        def foundAllCats = true
        def lastCatName = ""

        List<FilterCategory> result = []
        catList.each{ cat ->
            def filterCat = new FilterCategory(tagId: cat.id)
            filterCat.name = cat.tag.capitalize()
            filterCat.aliases = cat.getAliasList()
            if(selCats.size() == 0 || selCats.contains(cat.id.toString()) || selCats.contains(filterCat.name)) {
                foundCats.add(cat.id)
                filterCat.selected = true
                lastCatName = filterCat.name
            }
            else {
                filterCat.selected = false
                foundAllCats = false
            }

            result << filterCat
        }

        if (updateCookie) {
            def cookie = new Cookie(selCatAttrName, foundAllCats ? "" : foundCats.join("#"))
            cookie.setPath("/")
            cookie.setMaxAge(365*24*60*60) // 1 Jahr
            response.addCookie(cookie)
        }

        request.getSession().setAttribute(selCatAttrName, foundAllCats ? "" : foundCats.join("#"))

        result.sort{ FilterCategory a, FilterCategory b -> a.name < b.name ? -1 : 1}

        def label = ""
        if(foundAllCats)
            label = "Categories: <b><i>All</i></b>"
        else if(foundCats.size() == 1)
            label = "Category: <b>${lastCatName}</b>"
        else
            label = "Categories: <b><i>Filtered</i></b>"

        return [list: result, label: label, highlight: !foundAllCats]
    }

    def getCategoryFilterCookie() {
        def request = WebUtils.retrieveGrailsWebRequest().getCurrentRequest()

        def selCatsStr = request.getSession(true).getAttribute(selCatAttrName)
        if(selCatsStr == null)
            selCatsStr = request.getCookies().find { it.name == selCatAttrName }?.value ?: ""

        return selCatsStr
    }

    def getCategoryFilter() {
        def selCatsStr = getCategoryFilterCookie()

        if (selCatsStr && selCatsStr != "") {
            return selCatsStr.split("#").collect { Long.parseLong(it) }
        }
        else {
            return null
        }
    }

}
