package com.ncmc4

import groovy.transform.Synchronized

import java.time.LocalDate
import java.time.YearMonth
import java.time.ZoneOffset
import java.time.ZonedDateTime
import java.time.temporal.ChronoField

class SeriesService {
    static transactional = false

    def storyService

    List<Long> randomSeriesList = []
    def randomStoryPeriodEnd = YearMonth.of(2017,12)
    final def randomSeriesListName = "randomSeriesUntil2017"

    def initialDay = LocalDate.of(2018,9,19).getLong(ChronoField.EPOCH_DAY)

    def systemAdminService

    def findAllApproved(params)
    {
        def catFilter = storyService.getCategoryFilter()

        def criteria = Series.createCriteria()
        def series = criteria.list(params) {
            gt("storyCount", 0)
            if(catFilter)
                category {
                    'in'("id", catFilter)
                }

            order("lastChapterUpdated", "desc")
            fetchMode("author", org.hibernate.FetchMode.JOIN)
        }

        return [list: series, count: series.totalCount]
    }

    Story loadMostRecentStory(Series series) {
        if (series.mostRecentStory != null)
            return series.mostRecentStory

        def stories = storyService.findAllApprovedBySeries(series)

        stories = stories.list

        // check if we can delete this
        if (stories.size() == 0) {
            return null
        }

        stories = stories.sort { -it.publishDate.time }

        series.mostRecentStory = stories[0]
        series.save flush:true, failOnError:true

        return series.mostRecentStory
    }

    void resetSeriesLinks(Series series, Tag newCategory = null) {
        try {
            def stories = storyService.findAllApprovedBySeries(series)

            stories = stories.list

            // check if we can delete this
            if (stories.size() == 0) {
                if (series.wikiStory) {
                    return
                }

                def anyStories = Story.findAllBySeries(series)
                if (anyStories.size() == 0)
                {
                    series.delete flush: true, failOnError:true
                    return;
                }
            }

            stories = stories.sort { it.publishDate.time }

            Set<Tag> tags = [] as Set

            // set em up
            Story prev = null
            stories.each { Story story ->
                if (prev != null)
                    prev.nextInSeries = story

                story.prevInSeries = prev
                story.nextInSeries = null

                series.lastChapterUpdated = story.savePublishDate

                tags.addAll(story.tags)

                if(! newCategory)
                    newCategory = story.category

                prev = story
            }

            if(! newCategory)
            {
                // No Category set in any story, search by tag
                newCategory = tags.find { tag -> tag?.isCategory }
                if(newCategory)
                    log.info("Set Category to series ${series.name}: ${newCategory.tag}")
            }

            if(newCategory)
            {
                // Synchronize Category to all stories
                stories.each { Story story ->
                    if(story.category != newCategory)
                        story.category = newCategory
                }
            }

            series.category = newCategory

            series.tags = tags

            if (prev) {
                series.author = prev.author
                series.authorSlug = prev.authorSlug
            }
            series.mostRecentStory = prev

            // save stories
            stories.each {
                // it.refresh()
                it.save flush: true, failOnError: true
            }

            // set up the series
            series.storyCount = stories.size()
            refreshTagCache(series)

            series.save flush: true, failOnError:true
        }
        catch (e) {
            log.error "${systemAdminService?.logPrefix}ResetSeriesLinks Exception!", e
            throw e
        }
    }

    Series CreateSeries(Story storyInstance, String seriesName = null) {

        Series ret = new Series()

        seriesName = seriesName ?: storyInstance.name

        ret.name = seriesName
        ret.nameSlug = storyService.makeStoryTitleSlug(seriesName)
        ret.author = storyInstance.author
        ret.authorSlug = storyInstance.authorSlug
        ret.lastChapterUpdated = storyInstance.savePublishDate
        ret.storyCount = (storyInstance.approved && !storyInstance.deleted) ? 1 : 0
        ret.sponsored = storyInstance.sponsored ?: false
        ret.summary = storyInstance.summary
        ret.save flush:true, failOnError:true

        Set<Tag> tags = storyInstance.tags as Set

        if (tags.size() > 0) {
            tags.each { ret.addToTags(it) }
        }

        refreshTagCache(ret)
        ret.save flush:true, failOnError:true

        storyInstance.series = ret
        storyInstance.save flush:true, failOnError:true

        return ret
    }

    void refreshTags(Series series, Tag newCategory = null) {
        if (series == null)
            return

        series.refresh()

        Tag category = newCategory;

        def stories = storyService.findAllApprovedBySeries(series, [readOnly:true]).list

        Set<Tag> tags = [] as Set

        stories.each { Story story ->
            tags.addAll(story.tags)

            if(! category)
                category = story.category
        }

        if(category)
        {
            // Synchronize Category to all stories
            stories.each { Story story ->
                if(story.category != category)
                {
                    story.category = category
                    story.save(flush:true)
                }
            }
        }

        series.tags = tags

        series.category = category

        refreshTagCache(series)

        series.save flush:true, failOnError:true
    }

    public void refreshTagCache(Series series) {
        if (series.tags != null)
            series.tagCache = (series.tags*.tag).join(",")
        else
            series.tagCache = "";
    }

    @Synchronized
    List<Long> getRandomSeriesList() {
        if(! randomSeriesList || randomSeriesList.size() == 0) {

            ItemList seriesList = ItemList.findByIdentifier(randomSeriesListName)
            if (seriesList && seriesList.itemList)
            {
                randomSeriesList = seriesList.itemList
            }
            else {
                randomSeriesList = calcRandomSeriesList()

                seriesList = new ItemList(identifier: randomSeriesListName)
                seriesList.itemList = randomSeriesList

                seriesList.save flush: true, failOnError: true
            }
        }

        return randomSeriesList
    }

    @Synchronized
    private List<Long> calcRandomSeriesList() {
        Random randomGen = new Random(randomStoryPeriodEnd.hashCode())

        def c = Series.createCriteria()

        def seriesList = c.list {
            lte("lastChapterUpdated", Date.from(randomStoryPeriodEnd.atEndOfMonth().atStartOfDay().toInstant(ZoneOffset.UTC)))
            gt("storyCount", 0)

            order("id", "asc")
        }

        List<Long> idList = seriesList.collect {
            it.id
        }

        Collections.shuffle(idList, randomGen)

        return idList
    }

    def calcRandomPeriodSegmentData(ZonedDateTime forDateTime,
                                 def randomSeriesPerSegment = 3)                   // Number of series shown per segment
    {
        int segment = (int)((long)forDateTime.getLong(ChronoField.EPOCH_DAY) - initialDay) / 7

        segment = ((segment * randomSeriesPerSegment) % getRandomSeriesList().size()) / randomSeriesPerSegment

        def segmentId = "Rnd#" + segment.toString()

        return [randomSeriesSegment: segment, randomSeriesPerSegment: randomSeriesPerSegment,
                randomSeriesSegmentOrd: RatingService.getEnglishOrdinal(segment+1), randomSeriesSegmentId: segmentId]
    }

    def fetchRandomSeriesList(Map randomSeriesDefinition) {
        List<Long> randomSeriesList = getRandomSeriesList()

        def randomSeriesSubList = []
        def start = (int)randomSeriesDefinition.randomSeriesPerSegment * (int)randomSeriesDefinition.randomSeriesSegment
        def end = (int)randomSeriesDefinition.randomSeriesPerSegment * (int)(randomSeriesDefinition.randomSeriesSegment + 1)
        for(int i = start; i < end; i++) {
            try {
                def series = Series.get(randomSeriesList[i])
                if(series && series.storyCount > 0)
                    randomSeriesSubList.add(series)
            }
            catch(e) {
                // Ignore
            }
        }

        randomSeriesDefinition.randomSeriesList = randomSeriesSubList

        return randomSeriesSubList
    }
}