package com.ncmc4

import grails.plugin.springsecurity.userdetails.GormUserDetailsService
import org.springframework.security.core.userdetails.UserDetails
import grails.gorm.transactions.Transactional
import static grails.plugin.springsecurity.SpringSecurityUtils.*
import org.springframework.security.core.userdetails.UsernameNotFoundException
import grails.plugin.springsecurity.userdetails.NoStackUsernameNotFoundException
import org.springframework.security.core.GrantedAuthority

class UserDetailsService extends GormUserDetailsService
{
    def systemAdminService

    @Transactional(readOnly=true, noRollbackFor=[IllegalArgumentException, UsernameNotFoundException])
    UserDetails loadUserByUsername(String username, boolean loadRoles) throws UsernameNotFoundException {


        // find duplicates/problems:
        // select id, username from sec_user where lower(username) in (select lower(username) from sec_user group by lower(username) having count(*) > 1) order by lower(username);

        def conf = securityConfig
        String userClassName = conf.userLookup.userDomainClassName
        assert userClassName == "com.ncmc4.SecUser"
        assert conf.userLookup.usernamePropertyName == "username"

        // find the user name that matches that was most recently used
        def criteria = SecUser.createCriteria()
        def users = criteria.list() {
            eq('username', username, ignoreCase: true)
            order('lastSignedIn', 'desc')
        }

        def user = users ? users[0] : null
        if (!user) {
            log.warn "${systemAdminService?.logPrefix}User not found: ${username}"
            throw new NoStackUsernameNotFoundException()
        }

        // figure out the real, case sensitive name now
        username = user."${conf.userLookup.usernamePropertyName}"

        Collection<GrantedAuthority> authorities = loadAuthorities(user, user.username, loadRoles)
        createUserDetails user, authorities
    }
}