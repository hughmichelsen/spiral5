databaseChangeLog = {

    changeSet(author: "mkorn (generated)", id: "1543769142401-1") {
        addColumn(tableName: "grails_story") {
            column(name: "admin_comment", type: "varchar(255)")
        }
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-2") {
        addUniqueConstraint(columnNames: "uuid", constraintName: "UC_GRAILS_STORYUUID_COL", tableName: "grails_story")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-3") {
        addUniqueConstraint(columnNames: "tag", constraintName: "UC_GRAILS_TAGTAG_COL", tableName: "grails_tag")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-4") {
        dropForeignKeyConstraint(baseTableName: "test_story_archive2", constraintName: "fk1sfbufa6c60kkuxay0bfg6nm9")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-5") {
        dropForeignKeyConstraint(baseTableName: "story_tracker", constraintName: "fk_8w86qsv7ng3kciei0i4n8be99")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-6") {
        dropForeignKeyConstraint(baseTableName: "story_tracker", constraintName: "fk_9di8s422j3pff00d8rgkuyj3m")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-7") {
        dropForeignKeyConstraint(baseTableName: "grails_sec_user_sec_role", constraintName: "fk_cnym43yua8yiloms43mksm435")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-8") {
        dropForeignKeyConstraint(baseTableName: "grails_sec_user_sec_role", constraintName: "fk_kbs95qreejmnlpvjj74qmyx83")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-9") {
        dropForeignKeyConstraint(baseTableName: "test_story_archive", constraintName: "fk_pfx4318vopkna4iyju0ydpx02")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-10") {
        dropForeignKeyConstraint(baseTableName: "poll_votes", constraintName: "fk_rails_848ece0184")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-11") {
        dropForeignKeyConstraint(baseTableName: "poll_votes", constraintName: "fk_rails_a6e6974b7e")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-12") {
        dropForeignKeyConstraint(baseTableName: "poll_options", constraintName: "fk_rails_aa85becb42")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-13") {
        dropForeignKeyConstraint(baseTableName: "polls", constraintName: "fk_rails_b50b782d08")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-14") {
        dropForeignKeyConstraint(baseTableName: "poll_votes", constraintName: "fk_rails_b64de9b025")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-15") {
        dropUniqueConstraint(constraintName: "uk_63mol08brplhnltu80mmfgjon", tableName: "grails_sec_role")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-16") {
        dropUniqueConstraint(constraintName: "uk_ddo4festmh1bsdyye10v7sbo1", tableName: "grails_sec_user")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-17") {
        dropView(viewName: "badge_posts")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-18") {
        dropTable(tableName: "api_keys")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-19") {
        dropTable(tableName: "application_requests")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-20") {
        dropTable(tableName: "ar_internal_metadata")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-21") {
        dropTable(tableName: "badge_groupings")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-22") {
        dropTable(tableName: "badge_types")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-23") {
        dropTable(tableName: "badges")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-24") {
        dropTable(tableName: "categories")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-25") {
        dropTable(tableName: "categories_web_hooks")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-26") {
        dropTable(tableName: "category_custom_fields")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-27") {
        dropTable(tableName: "category_featured_topics")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-28") {
        dropTable(tableName: "category_groups")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-29") {
        dropTable(tableName: "category_search_data")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-30") {
        dropTable(tableName: "category_tag_groups")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-31") {
        dropTable(tableName: "category_tag_stats")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-32") {
        dropTable(tableName: "category_tags")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-33") {
        dropTable(tableName: "category_users")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-34") {
        dropTable(tableName: "child_themes")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-35") {
        dropTable(tableName: "color_scheme_colors")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-36") {
        dropTable(tableName: "color_schemes")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-37") {
        dropTable(tableName: "custom_emojis")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-38") {
        dropTable(tableName: "developers")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-39") {
        dropTable(tableName: "directory_items")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-40") {
        dropTable(tableName: "draft_sequences")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-41") {
        dropTable(tableName: "drafts")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-42") {
        dropTable(tableName: "email_change_requests")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-43") {
        dropTable(tableName: "email_logs")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-44") {
        dropTable(tableName: "email_tokens")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-45") {
        dropTable(tableName: "embeddable_hosts")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-46") {
        dropTable(tableName: "facebook_user_infos")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-47") {
        dropTable(tableName: "github_user_infos")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-48") {
        dropTable(tableName: "given_daily_likes")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-49") {
        dropTable(tableName: "google_user_infos")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-50") {
        dropTable(tableName: "grails_sec_role")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-51") {
        dropTable(tableName: "grails_sec_user")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-52") {
        dropTable(tableName: "grails_sec_user_sec_role")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-53") {
        dropTable(tableName: "group_archived_messages")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-54") {
        dropTable(tableName: "group_custom_fields")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-55") {
        dropTable(tableName: "group_histories")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-56") {
        dropTable(tableName: "group_mentions")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-57") {
        dropTable(tableName: "group_users")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-58") {
        dropTable(tableName: "groups")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-59") {
        dropTable(tableName: "groups_web_hooks")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-60") {
        dropTable(tableName: "incoming_domains")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-61") {
        dropTable(tableName: "incoming_emails")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-62") {
        dropTable(tableName: "incoming_links")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-63") {
        dropTable(tableName: "incoming_referers")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-64") {
        dropTable(tableName: "instagram_user_infos")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-65") {
        dropTable(tableName: "invited_groups")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-66") {
        dropTable(tableName: "invites")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-67") {
        dropTable(tableName: "javascript_caches")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-68") {
        dropTable(tableName: "message_bus")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-69") {
        dropTable(tableName: "muted_users")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-70") {
        dropTable(tableName: "notifications")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-71") {
        dropTable(tableName: "oauth2_user_infos")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-72") {
        dropTable(tableName: "onceoff_logs")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-73") {
        dropTable(tableName: "optimized_images")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-74") {
        dropTable(tableName: "permalinks")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-75") {
        dropTable(tableName: "plugin_store_rows")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-76") {
        dropTable(tableName: "poll_options")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-77") {
        dropTable(tableName: "poll_votes")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-78") {
        dropTable(tableName: "polls")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-79") {
        dropTable(tableName: "post_action_types")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-80") {
        dropTable(tableName: "post_actions")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-81") {
        dropTable(tableName: "post_custom_fields")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-82") {
        dropTable(tableName: "post_details")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-83") {
        dropTable(tableName: "post_replies")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-84") {
        dropTable(tableName: "post_reply_keys")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-85") {
        dropTable(tableName: "post_revisions")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-86") {
        dropTable(tableName: "post_search_data")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-87") {
        dropTable(tableName: "post_stats")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-88") {
        dropTable(tableName: "post_timings")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-89") {
        dropTable(tableName: "post_uploads")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-90") {
        dropTable(tableName: "posts")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-91") {
        dropTable(tableName: "push_subscriptions")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-92") {
        dropTable(tableName: "queued_posts")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-93") {
        dropTable(tableName: "quoted_posts")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-94") {
        dropTable(tableName: "remote_themes")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-95") {
        dropTable(tableName: "scheduler_stats")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-96") {
        dropTable(tableName: "schema_migration_details")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-97") {
        dropTable(tableName: "schema_migrations")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-98") {
        dropTable(tableName: "screened_emails")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-99") {
        dropTable(tableName: "screened_ip_addresses")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-100") {
        dropTable(tableName: "screened_urls")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-101") {
        dropTable(tableName: "search_logs")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-102") {
        dropTable(tableName: "shared_drafts")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-103") {
        dropTable(tableName: "shared_story")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-104") {
        dropTable(tableName: "single_sign_on_records")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-105") {
        dropTable(tableName: "site_settings")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-106") {
        dropTable(tableName: "skipped_email_logs")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-107") {
        dropTable(tableName: "story_tracker")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-108") {
        dropTable(tableName: "stylesheet_cache")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-109") {
        dropTable(tableName: "tag_group_memberships")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-110") {
        dropTable(tableName: "tag_group_permissions")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-111") {
        dropTable(tableName: "tag_groups")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-112") {
        dropTable(tableName: "tag_search_data")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-113") {
        dropTable(tableName: "tag_users")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-114") {
        dropTable(tableName: "tags")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-115") {
        dropTable(tableName: "test_story_archive")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-116") {
        dropTable(tableName: "test_story_archive2")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-117") {
        dropTable(tableName: "theme_fields")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-118") {
        dropTable(tableName: "theme_settings")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-119") {
        dropTable(tableName: "themes")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-120") {
        dropTable(tableName: "top_topics")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-121") {
        dropTable(tableName: "topic_allowed_groups")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-122") {
        dropTable(tableName: "topic_allowed_users")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-123") {
        dropTable(tableName: "topic_custom_fields")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-124") {
        dropTable(tableName: "topic_embeds")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-125") {
        dropTable(tableName: "topic_invites")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-126") {
        dropTable(tableName: "topic_link_clicks")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-127") {
        dropTable(tableName: "topic_links")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-128") {
        dropTable(tableName: "topic_search_data")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-129") {
        dropTable(tableName: "topic_tags")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-130") {
        dropTable(tableName: "topic_timers")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-131") {
        dropTable(tableName: "topic_users")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-132") {
        dropTable(tableName: "topic_views")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-133") {
        dropTable(tableName: "topics")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-134") {
        dropTable(tableName: "translation_overrides")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-135") {
        dropTable(tableName: "twitter_user_infos")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-136") {
        dropTable(tableName: "unsubscribe_keys")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-137") {
        dropTable(tableName: "uploads")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-138") {
        dropTable(tableName: "user_actions")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-139") {
        dropTable(tableName: "user_api_keys")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-140") {
        dropTable(tableName: "user_archived_messages")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-141") {
        dropTable(tableName: "user_auth_token_logs")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-142") {
        dropTable(tableName: "user_auth_tokens")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-143") {
        dropTable(tableName: "user_avatars")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-144") {
        dropTable(tableName: "user_badges")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-145") {
        dropTable(tableName: "user_custom_fields")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-146") {
        dropTable(tableName: "user_emails")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-147") {
        dropTable(tableName: "user_exports")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-148") {
        dropTable(tableName: "user_field_options")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-149") {
        dropTable(tableName: "user_fields")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-150") {
        dropTable(tableName: "user_histories")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-151") {
        dropTable(tableName: "user_open_ids")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-152") {
        dropTable(tableName: "user_options")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-153") {
        dropTable(tableName: "user_profile_views")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-154") {
        dropTable(tableName: "user_profiles")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-155") {
        dropTable(tableName: "user_search_data")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-156") {
        dropTable(tableName: "user_second_factors")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-157") {
        dropTable(tableName: "user_stats")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-158") {
        dropTable(tableName: "user_uploads")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-159") {
        dropTable(tableName: "user_visits")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-160") {
        dropTable(tableName: "user_warnings")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-161") {
        dropTable(tableName: "users")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-162") {
        dropTable(tableName: "watched_words")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-163") {
        dropTable(tableName: "web_crawler_requests")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-164") {
        dropTable(tableName: "web_hook_event_types")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-165") {
        dropTable(tableName: "web_hook_event_types_hooks")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-166") {
        dropTable(tableName: "web_hook_events")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-167") {
        dropTable(tableName: "web_hooks")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-168") {
        dropColumn(columnName: "has_stories", tableName: "sec_user")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-169") {
        dropColumn(columnName: "hide_wiki_stories_on_home_page", tableName: "sec_user")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-170") {
        dropColumn(columnName: "show_copyright", tableName: "grails_story")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-172") {
        dropColumn(columnName: "timezone", tableName: "sec_user")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-173") {
        dropColumn(columnName: "unique_identifier", tableName: "grails_comment")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-174") {
        addNotNullConstraint(columnDataType: "varchar(255)", columnName: "action", tableName: "grails_activity")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-175") {
        dropNotNullConstraint(columnDataType: "bigint", columnName: "author_id", tableName: "series")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-176") {
        dropNotNullConstraint(columnDataType: "varchar(255)", columnName: "author_slug", tableName: "series")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-196") {
        addNotNullConstraint(columnDataType: "boolean", columnName: "previously_approved", tableName: "grails_story")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-197") {
        addNotNullConstraint(columnDataType: "int", columnName: "rating_cum", tableName: "rating")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-198") {
        addNotNullConstraint(columnDataType: "int", columnName: "rating_hot", tableName: "rating")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-199") {
        addNotNullConstraint(columnDataType: "int", columnName: "rating_idea", tableName: "rating")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-200") {
        addNotNullConstraint(columnDataType: "int", columnName: "rating_writing", tableName: "rating")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-201") {
        addNotNullConstraint(columnDataType: "int", columnName: "ratingmc", tableName: "rating")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-202") {
        addNotNullConstraint(columnDataType: "bigint", columnName: "series_tags_id", tableName: "series_grails_tag")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-203") {
        addNotNullConstraint(columnDataType: "bigint", columnName: "story_owners_id", tableName: "grails_story_sec_user")
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-204") {
        addNotNullConstraint(columnDataType: "bigint", columnName: "wiki_story_tags_id", tableName: "wiki_story_grails_tag")
    }


    changeSet(author: "mkorn (generated)", id: "1543769142401-205") {
        dropIndex(indexName: "story_approval_idx", tableName: "grails_story")

        createIndex(indexName: "story_approval_idx", tableName: "grails_story") {
            column(name: "previously_approved")

            column(name: "approved")
        }
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-206") {
        createIndex(indexName: "story_creationDate_idx", tableName: "grails_story") {
            column(name: "creation_date")
        }
    }

    changeSet(author: "mkorn (generated)", id: "1543769142401-207") {
        createIndex(indexName: "story_publishDate_idx", tableName: "grails_story") {
            column(name: "publish_date")
        }
    }

    changeSet(author: "mkorn (generated)", id: "1544545732832-1") {
        dropTable(tableName: "grails_message")
    }

    changeSet(author: "mkorn (generated)", id: "1544545732832-2") {
        dropTable(tableName: "message_target")
    }

    changeSet(author: "mkorn (generated)", id: "1544545732832-3") {
        dropColumn(columnName: "email_address_unconstrained", tableName: "grails_story")
    }

    changeSet(author: "mkorn (generated)", id: "1544545732832-5") {
        dropNotNullConstraint(columnDataType: "boolean", columnName: "anonymous", tableName: "grails_story")
    }

    changeSet(author: "mkorn (generated)", id: "1544545732832-6") {
        dropNotNullConstraint(columnDataType: "boolean", columnName: "approved", tableName: "grails_story")
    }

    changeSet(author: "mkorn (generated)", id: "1544545732832-7") {
        dropNotNullConstraint(columnDataType: "boolean", columnName: "deleted", tableName: "grails_story")
    }

    changeSet(author: "mkorn (generated)", id: "1544545732832-25") {
        dropNotNullConstraint(columnDataType: "boolean", columnName: "previously_approved", tableName: "grails_story")
    }

    changeSet(author: "mkorn (generated)", id: "1544545732832-26") {
        dropNotNullConstraint(columnDataType: "timestamp", columnName: "publish_date", tableName: "grails_story")
    }

    changeSet(author: "mkorn (generated)", id: "1544545732832-27") {
        addNotNullConstraint(columnDataType: "bigint", columnName: "story_tags_id", tableName: "grails_story_grails_tag")
    }

    changeSet(author: "mkorn (generated)", id: "1545498301416-5") {
        createIndex(indexName: "series_category_idx", tableName: "series") {
            column(name: "category_id")
        }
    }

    changeSet(author: "mkorn (generated)", id: "1545498301416-6") {
        createIndex(indexName: "series_lastUpdated_idx", tableName: "series") {
            column(name: "last_chapter_updated")
        }
    }

    changeSet(author: "mkorn (generated)", id: "1545498301416-7") {
        createIndex(indexName: "story_category_idx", tableName: "grails_story") {
            column(name: "category_id")
        }
    }

    changeSet(author: "mkorn (generated)", id: "1545498301416-26") {
        dropIndex(indexName: "story_approval_idx", tableName: "grails_story")

        createIndex(indexName: "story_approval_idx", tableName: "grails_story") {
            column(name: "previously_approved")

            column(name: "approved")
        }
    }

    changeSet(author: "mkorn (generated)", id: "1545591704956-7") {
        dropColumn(columnName: "get", tableName: "rating")
    }


    changeSet(author: "mkorn (generated)", id: "1545645273170-1") {
        createTable(tableName: "item_list") {
            column(autoIncrement: "true", name: "id", type: "BIGINT") {
                constraints(primaryKey: "true", primaryKeyName: "item_listPK")
            }

            column(name: "version", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "context_date", type: "TIMESTAMP WITHOUT TIME ZONE") {
                constraints(nullable: "false")
            }

            column(name: "identifier", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "mkorn (generated)", id: "1545645273170-2") {
        createTable(tableName: "item_list_item_list") {
            column(name: "item_list_id", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "item_list_long", type: "BIGINT")

            column(name: "item_list_idx", type: "INT")
        }
    }

    changeSet(author: "mkorn (generated)", id: "1545645273170-3") {
        addUniqueConstraint(columnNames: "identifier", constraintName: "UC_ITEM_LISTIDENTIFIER_COL", tableName: "item_list")
    }
}
