package com.ncmc4


import grails.util.Environment

import javax.servlet.http.Cookie

class StoryCollectionTagLib {
    def springSecurityService
    def patreonClient

    def fullSiteTitle = { attrs ->
        if (Environment.current == Environment.PRODUCTION) {
            out << "Gay Spiral Stories"
        } else {
            out << "Dev Spiral Stories"
        }
    }

    def pageTitle = { attrs ->
        if (Environment.current == Environment.PRODUCTION) {
            out << attrs.title << " - Gay Spiral Stories"
        } else {
            out << attrs.title << " - Dev Spiral Stories"
        }
    }

    def devEnv = { attrs, body ->
        log.debug "writing dev tag"
        if (Environment.current == Environment.DEVELOPMENT) {
            log.debug "exec body"
            body()
        }
    }

    def prodEnv = { attrs, body ->
        if (Environment.current == Environment.PRODUCTION) {
            body()
        }
    }

    def userButtons = { attrs ->
        def writer = out

        if ((attrs.userId == null) && (attrs.user == null)) {
            throwTagError("Tag [userButtons] is missing required attribute [userId] or [user]")
        }

        def user = attrs.user
        if (user == null) {
            user = SecUser.read(attrs.userId)
        }

        def btnClass = 'btn btn-sm btn-info'
        if(attrs.simple == "true"){
            btnClass = ''
        }

//        <td><g:link controller="newAuthor" action="listAdminAuthorStories" id="${instance.id}"><span id="${instance.id}-story-count"></span></g:link></td>
//        <td><g:link controller="newComment" action="showUser" id="${instance.id}"><span id="${instance.id}-comment-count"></span></g:link></td>
//        <td><g:link controller="newStory" action="listUserLiked" id="${instance.id}"><span id="${instance.id}-like-count"></span></g:link></td>

        //writer << "<span>"

        boolean needSpace = false
        if (user.storyCount > 0) {
            String link = g.createLink(controller: "newAuthor", action: "listAdminAuthorStories", id: user.id)
            writer << "<a title='Click for stories authored by this user' class='${btnClass}' href='$link'><i class='fa fa-book'></i> ${user.storyCount}</a>"
            needSpace = true
        }
        if (user.likeCount > 0) {
            if (needSpace)
                writer << "&nbsp;"

            String link = g.createLink(controller: "newStory", action: "listUserLiked", id: user.id)
            writer << "<a title='Click for stories by this user' class='${btnClass}' href='$link'><i class='fa fa-thumbs-up'></i> ${user.likeCount}</a>"
            needSpace = true
        }
        if (user.commentCount > 0) {
            if (needSpace)
                writer << "&nbsp;"

            String link = g.createLink(controller: "newComment", action: "showUser", id: user.id)
            writer << "<a title='Click for comments by this user' class='${btnClass}' href='$link'><i class='fa fa-comments'></i> ${user.commentCount}</a>"
            needSpace = true
        }

        //writer << "</span>"
    }


    def patreonButton = { attrs, body ->
        def currentUser = springSecurityService?.getCurrentUser()
        if(currentUser == null || ! currentUser.getIsPatreon() || ! currentUser.getHidePatreon()) {
            def perc = patreonClient?.cachedGoalPercentage ?: 0
            def max100perc = Math.min(perc,100)
            def opac = perc >= 100 ? 0.4 : 1.0
            out << "                            <div class='patreon-button' style='opacity: ${opac};'>\n" +
                    '                                <a href="https://www.patreon.com/GaySpiralStories">\n' +
                    '                                    <div title="Become a Patreon">\n' + body() +
                    '                                        <span class="patreon-button-text"> Patreon</span>\n' +
                    '                                    </div>\n' +
                    '                                    <div title="Monthly costs covered">\n' +
                    '                                        <div class="patreon-progress">\n' +
                    "                                           <div class='progress-bar' style='width:${max100perc}%;'>\n" +
                    "                                               <div class='progress-value'>${perc}%</div>\n" +
                    '                                           </div>' +
                    '                                        </div>\n' +
                    '                                    </div>\n' +
                    '                                </a>\n' +
                    '                            </div>'
        }
    }

    def patreonProgressNr = { attrs, body ->
        def perc = patreonClient.cachedGoalPercentage
        out << perc
    }

    def userIcon = { attrs, body ->

        SecUser user = SecUser.get(attrs.userid)
        if(attrs.userid && user) {
            def roles = user.getAuthorities()
            if(roles?.find { it.authority == SecRole.ROLE_ULTRA_ADMIN })
                out << '<i class="fa fa-certificate userIconHL" data-toggle="tooltip" title="Site Owner"></i>'
            else if (roles?.find { it.authority == SecRole.ROLE_SUPER_ADMIN })
                out << '<i class="fa fa-star userIconHL" data-toggle="tooltip" title="Admin User"></i>'
            else if (roles?.find { it.authority == SecRole.ROLE_ADMIN })
                out << '<i class="fa fa-gavel userIconHL" data-toggle="tooltip" title="Approver"></i>'
            else if (roles?.find { it.authority == SecRole.ROLE_PATREON })
                out << '<i class="fa fa-heart userIconHL" data-toggle="tooltip" title="Patreon User"></i>'
            else if (roles?.find { it.authority == SecRole.ROLE_AUTHOR })
                out << '<i class="fa fa-pencil userIconStd" data-toggle="tooltip" title="Registered Author"></i>'
            else
                out << '<i class="fa fa-user userIconStd" data-toggle="tooltip" title="Registered User"></i>'
        }

    }

    def themeList = [[id: 'classic', name: 'Classic'],
                     [id: 'slate', name: 'Slate [dark]'],
                     [id: 'cerulean', name: 'Cerulean'],
                     [id: 'cosmo', name: 'Cosmo'],
                     [id: 'cyborg', name: 'Cyborg [dark]'],
                     [id: 'darkly', name: 'Darkly [dark]'],
                     [id: 'flatly', name: 'Flatly'],
                     [id: 'journal', name: 'Journal'],
                     [id: 'litera', name: 'Litera'],
                     [id: 'lumen', name: 'Lumen'],
                     [id: 'lux', name: 'Lux'],
                     [id: 'materia', name: 'Materia'],
                     [id: 'minty', name: 'Minty'],
                     [id: 'pulse', name: 'Pulse'],
                     [id: 'sandstone', name: 'Sandstone'],
                     [id: 'simplex', name: 'Simplex'],
                     [id: 'sketchy', name: 'Sketchy'],
                     [id: 'solar', name: 'Solar [dark]'],
                     [id: 'spacelab', name: 'Spacelab'],
                     [id: 'superhero', name: 'Superhero [dark]'],
                     [id: 'united', name: 'United'],
                     [id: 'yeti', name: 'Yeti']
                    ]


    def themeScript = { attrs, body ->
        themeList.each{ theme ->
            out << "            case '${theme.id}': return '${g.assetPath(src: 'theme_' + theme.id + '.css')}'; break;\n"
        }
    }

    def themeSelectors = { attrs, body ->
        def curTheme = request.getCookies().find{ it.name=='currentTheme4' }?.value
        if(! curTheme || curTheme.length() == 0)
            curTheme = "slate"
        themeList.each { theme ->
            out << "<a href='#' title='${theme.name}' class='dropdown-item switchTheme ${theme.id == curTheme ? 'active' : ''}' data-theme='${theme.id}'><i class='fa fa-tint dropdown-icon' aria-hidden='true'></i> ${theme.name}</a>"
        }
    }

    def themeSelector = { attrs, body ->
        def resetCookie = false
        def themeRes = null
        def theme = request.getCookies().find{ it.name=='currentTheme4' }?.value

        if(params.theme) {
            // Theme forced by URL param
            theme = params.theme
            resetCookie = true
        }

        if(theme && theme in themeList.collect{ it.id })
            themeRes = g.assetPath(src:"theme_${theme}.css")

        if(! themeRes) {
            theme = "slate"
            themeRes = g.assetPath(src:"theme_${theme}.css")
        }

        if(resetCookie) {
            def cookie = new Cookie("currentTheme4", theme)
            cookie.setPath("/")
            cookie.setMaxAge(10*365*24*60*60) // 10 years
            response.addCookie(cookie)
        }

        out << "<link rel='stylesheet' id='bootstrapLink' href='${themeRes}'/>"
    }
}