package com.ncmc5

import org.springframework.web.servlet.support.RequestContextUtils as RCU

class BootstrapTagLib {

    /*
     * This g:paginate tag fix is based on:
     * https://github.com/grails/grails-core/blob/master/grails-plugin-gsp/src/main/groovy/org/codehaus/groovy/grails/plugins/web/taglib/RenderTagLib.groovy
     */

    /**
     * Creates next/previous links to support pagination for the current controller.<br/>
     *
     * &lt;g:paginate total="${Account.count()}" /&gt;<br/>
     *
     * @emptyTag
     *
     * @attr total REQUIRED The total number of results to paginate
     * @attr action the name of the action to use in the link, if not specified the default action will be linked
     * @attr controller the name of the controller to use in the link, if not specified the current controller will be linked
     * @attr id The id to use in the link
     * @attr params A map containing request parameters
     * @attr prev The text to display for the previous link (defaults to "Previous" as defined by default.paginate.prev property in I18n messages.properties)
     * @attr next The text to display for the next link (defaults to "Next" as defined by default.paginate.next property in I18n messages.properties)
     * @attr max The number of records displayed per page (defaults to 10). Used ONLY if params.max is empty
     * @attr maxsteps The number of steps displayed for pagination (defaults to 10). Used ONLY if params.maxsteps is empty
     * @attr offset Used only if params.offset is empty
     * @attr fragment The link fragment (often called anchor tag) to use
     */
    def paginate = { attrs ->

//        def configTabLib = grailsApplication.config.grails.plugins.twitterbootstrap.fixtaglib
//        if (!configTabLib) {
//            def renderTagLib = grailsApplication.mainContext.getBean('org.codehaus.groovy.grails.plugins.web.taglib.UrlMappingTagLib')
//            renderTagLib.paginate.call(attrs)
//            return
//        }

        def writer = out
        if (attrs.total == null) {
            throwTagError("Tag [paginate] is missing required attribute [total]")
        }
        def messageSource = grailsAttributes.messageSource
        def locale = RCU.getLocale(request)

        def total = attrs.int('total') ?: 0
        def action = (attrs.action ? attrs.action : (params.action ? params.action : "index"))
        def offset = params.int('offset') ?: 0
        def max = params.int('max')

        def maxsteps = (attrs.int('maxsteps') ?: 5)

        if (!offset) offset = (attrs.int('offset') ?: 0)
        if (!max) max = (attrs.int('max') ?: 10)

        def linkParams = [:]
        if (attrs.params) linkParams.putAll(attrs.params)
        linkParams.offset = offset - max
        linkParams.max = max
        if (params.sort) linkParams.sort = params.sort
        if (params.order) linkParams.order = params.order

        def linkTagAttrs = [action:action]
        if (attrs.namespace) {
            linkTagAttrs.namespace = attrs.namespace
        }
        if (attrs.controller) {
            linkTagAttrs.controller = attrs.controller
        }
        if (attrs.id != null) {
            linkTagAttrs.id = attrs.id
        }
        if (attrs.fragment != null) {
            linkTagAttrs.fragment = attrs.fragment
        }
        //add the mapping attribute if present
        if (attrs.mapping) {
            linkTagAttrs.mapping = attrs.mapping
        }

        linkTagAttrs.params = linkParams
        linkTagAttrs.class = "page-link"

        def cssClasses = "pagination pagination-sm"
        if (attrs.class) {
            cssClasses = "pagination pagination-sm " + attrs.class
        }

        // determine paging variables
        def steps = maxsteps > 0
        int currentstep = (offset / max) + 1
        int firststep = 1
        int laststep = Math.round(Math.ceil(total / max))

        writer << "<div class=\"${cssClasses}\">"
        // display previous link when not on firststep
        if (currentstep > firststep) {
            linkParams.offset = offset - max
            writer << '<div class="prev page-item">'
            writer << link(linkTagAttrs.clone()) {
                (attrs.prev ?: messageSource.getMessage('paginate.prev', null, '&laquo;', locale))
            }
            writer << '</div>'
        }
        else {
            writer << '<div class="prev disabled page-item">'
            writer << '<span class="page-link">'
            writer << (attrs.prev ?: messageSource.getMessage('paginate.prev', null, '&laquo;', locale))
            writer << '</span>'
            writer << '</div>'
        }

        // display steps when steps are enabled and laststep is not firststep
        if (steps && laststep > firststep) {
            linkTagAttrs.class = 'step page-link'

            // determine begin and endstep paging variables
            int beginstep = currentstep - Math.round(maxsteps / 2) + (maxsteps % 2)
            int endstep = currentstep + Math.round(maxsteps / 2) - 1

            if (beginstep < firststep) {
                beginstep = firststep
                endstep = maxsteps
            }
            if (endstep > laststep) {
                beginstep = laststep - maxsteps + 1
                if (beginstep < firststep) {
                    beginstep = firststep
                }
                endstep = laststep
            }

            // display firststep link when beginstep is not firststep
            if (beginstep > firststep) {
                linkParams.offset = 0
                writer << '<div class="page-item">'
                writer << link(linkTagAttrs.clone()) {firststep.toString()}
                writer << '</div>'
                writer << '<div class="disabled page-item"><span class="page-link"><i class="fa fa-ellipsis-h" aria-hidden="true"></i></span></div>'
            }

            // display paginate steps
            (beginstep..endstep).each { i ->
                if (currentstep == i) {
                    writer << "<div class=\"active page-item\">"
                    writer << "<span class=\"page-link\">${i}</span>"
                    writer << "</div>";
                }
                else {
                    linkParams.offset = (i - 1) * max
                    writer << "<div class=\"page-item\">";
                    writer << link(linkTagAttrs.clone()) {i.toString()}
                    writer << "</div>";
                }
            }

            // display laststep link when endstep is not laststep
            if (endstep < laststep) {
                writer << '<div class="disabled page-item"><span class="page-link"><i class="fa fa-ellipsis-h" aria-hidden="true"></i></span></div>'
                linkParams.offset = (laststep -1) * max
                writer << '<div class="page-item">'
                writer << link(linkTagAttrs.clone()) { laststep.toString() }
                writer << '</div>'
            }
        }

        linkTagAttrs.class = 'page-link'
        // display next link when not on laststep
        if (currentstep < laststep) {
            linkParams.offset = offset + max
            writer << '<div class="next page-item">'
            writer << link(linkTagAttrs.clone()) {
                (attrs.next ? attrs.next : messageSource.getMessage('paginate.next', null, '&raquo;', locale))
            }
            writer << '</div>'
        }
        else {
            linkParams.offset = offset + max
            writer << '<div class="disabled page-item">'
            writer << '<span class="page-link">'
            writer << (attrs.next ? attrs.next : messageSource.getMessage('paginate.next', null, '&raquo;', locale))
            writer << '</span>'
            writer << '</div>'
        }

        writer << '</div>'
    }

    /**
     *
     * @attr body REQUIRED The body must be breadcrumbLink
     */
    def breadcrumb = { attrs, body ->
        out << '<ol class="breadcrumb">'
        out << body()
        out << '</ol>'
    }

    /**
     * @emptyTag
     *
     * @attr value REQUIRED The value is the value of the link
     * @attr active True if the link is active
     */
    def breadcrumbLink = { attrs ->
        def value = attrs.value
        def active = attrs.boolean('active')
        def link = attrs.link ?: '#'
        if (active) {
            out << "<li class=\"active\">${value}</li>"
        } else {
            out << "<li><a href=\"${link}\">${value}</a></li>"
        }
    }

}