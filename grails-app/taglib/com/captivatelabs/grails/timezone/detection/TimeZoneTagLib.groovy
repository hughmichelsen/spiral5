package com.captivatelabs.grails.timezone.detection

import javax.servlet.http.Cookie

class TimeZoneTagLib {
    static namespace = 'tz'

    def springSecurityService
    def systemAdminService

    def detect = {
        try {
            //Don't do this on a POST because posted data will be lost.
            if (!systemAdminService.getCurrentTimezone()) {
                def tz = request.getCookies().find{ it.name=='timezone' }?.value
                if(tz) {
                    systemAdminService.setCurrentTimezone(TimeZone.getTimeZone(tz) ?: "UNKNOWN")
                    response.sendRedirect(request.forwardURI)
                }
                else if(request.method == 'GET') {
                    if (params.tz) {
                        systemAdminService.setCurrentTimezone(TimeZone.getTimeZone(params.tz) ?: "UNKNOWN")
                        log.debug "Timezone set to ${session.timeZone}."

                        // Save cookie for faster recovery next time without redirects
                        def cookie = new Cookie("timezone", params.tz)
                        cookie.setPath("/")
                        cookie.setMaxAge(7*24*60*60) // 1 week
                        response.addCookie(cookie)

                        response.sendRedirect(request.forwardURI)
                    } else {
                        out << asset.javascript(src: 'timezone-detection.js')
                        out << "<script type='text/javascript'>"
                        out << "doTimezoneDetection('${request.forwardURI}');"
                        out << "</script>"
                    }
                }
            }
        } catch (Exception ex) {
            log.error "${systemAdminService?.logPrefix}An error occurred attempting to detect the timezone!",ex
        }
    }

    def show = { final attrs ->
        try {
            TimeZone timeZone = systemAdminService.getCurrentTimezone()
            if (!timeZone) {
                return
            }
            if (attrs.attribute) {
                out << timeZone."${attrs.attribute}"
            } else {
                out << timeZone.getDisplayName(timeZone.inDaylightTime(new Date()), TimeZone.SHORT, request.getLocale())
            }
        } catch (Exception ex) {
            log.error "${systemAdminService?.logPrefix}An error occurred attempting to show the timezone", ex
        }
    }

    def datePicker = { attrs ->
        def formTagLib = getDefaultFormTagLib()
        if (systemAdminService.getCurrentTimezone() && attrs.value) {
            attrs.value = TimeZoneUtil.offsetDate(Calendar.getInstance().timeZone, systemAdminService.getCurrentTimezone(), attrs.value)
        }
        formTagLib.datePicker.call(attrs)
        if (!attrs.suppressTimezone) {
            out << "<span class='tz'>"
            out << tz.show()
            out << "</span>"
        }
    }

    def formatDate = { attrs ->
        def formatTagLib = getDefaultFormatTagLib()
        if (!attrs.timeZone) {
            attrs.timeZone = systemAdminService.getCurrentTimezone()
        }
        if (!attrs.style) {
            attrs.style = "SHORT"
        }
        if (!attrs.type) {
            attrs.type = "datetime"
        }
        out << formatTagLib.formatDate.call(attrs)
    }

    private def getDefaultFormTagLib() {
        return grailsApplication.mainContext.getBean('org.grails.plugins.web.taglib.FormTagLib')
    }

    private getDefaultFormatTagLib() {
        return grailsApplication.mainContext.getBean('org.grails.plugins.web.taglib.FormatTagLib')
    }
}
